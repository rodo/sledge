var path = require('path')
module.exports = {
  entry: {
    app: './www/src/init.js',
    preview: './www/src/initPreview.js'
  },
  output: {
    path: __dirname,
    filename: './www/dist/bundle_[name].js'
  },
  module: {
    rules: [
      {
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      }
    ]
  },
  devtool: 'cheap-module-source-map',
  resolve: {
    extensions: ['.js'],
    modules: [
      'node_modules'
    ]
  }
}
