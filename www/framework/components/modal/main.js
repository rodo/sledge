(function ($sledge) {
  $sledge.createComponent('modal', {
    container: true,
    onInit: (elem) => {
      let rmIcn = document.createElement('button')
      rmIcn.title = 'delete'
      rmIcn.className = 'sd-modal-rmicn'
      elem.appendChild(rmIcn)
      document.querySelector('.sg-mask').classList.add('sg-visible')
      window.requestAnimationFrame(() => {
        elem.querySelector('button').addEventListener('click', e => {
          elem.parentNode.removeChild(elem)
        })
      })
    },
    onRemove: (elem) => {
      document.querySelector('.sg-mask').classList.remove('sg-visible')
    }
  })
}($sledge))
