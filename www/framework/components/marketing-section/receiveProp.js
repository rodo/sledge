export default function receiveProp (node, type, value, valueImage) {
  if (node.classList.contains('sg-mark-section-img')) {
    node.style.minHeight = value.height + 'vh'
  } else {
    
    if (type === 'custom') {

      if (value.hasButton) {
        node.querySelector('sg-buttonset').style.display = 'block'
      } else {
        node.querySelector('sg-buttonset').style.display = 'none'
      }  
  
      if (value.position === 'right') {
        const wp = node.querySelector('.sg-mark-section-wrapper')
        wp.classList.add('sg-position-right')
      } else {
        const wp = node.querySelector('.sg-mark-section-wrapper')
        wp.classList.remove('sg-position-right')
      } 
    }
  }
}
