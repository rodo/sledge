export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Image', en: 'Image'},
    key: 'backgroundImg',
    type: 'image',
    applyTo: '.sg-mark-section-img'
  },
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  },
  {
    name: {fr: 'Hauteur', en: 'Height'},
    key: 'height',
    type: 'custom',
    default: {
      height: 30
    },
    data: [
      {
        kind: 'height',
        type: 'range',
        default: 30,
        name: {fr: 'Hauteur minimale (pour écrans moyen et large)', en: 'Min height (for medium and large screen)'},
        conf: {
          min: 10,
          max: 100
        }
      }
    ],
    applyTo: '.sg-mark-section-img'
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    applyTo: '.sg_title',
    default: {
      hSize: '3'
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription',
    type: 'text_long',
    applyTo: '.sg_txt'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      spacing: 'small'
    },
    data: [
      {
        kind: 'hasButton',
        type: 'checkbox',
        name: {fr: 'Ajouter des boutons', en: 'Add buttons'},
        ref: 'sd-f-has-button'
      },
      {
        kind: 'position',
        type: 'select',
        default: 'left',
        name: {fr: `Position de l'image`, en: 'Image position'},
        conf: [
          {
            ref: 'sd-pos-left',
            value: 'left',
            label: {
              fr: 'À gauche',
              en: 'Left'
            }
          },
          {
            ref: 'sd-pos-right',
            value: 'right',
            label: {
              fr: 'À droite',
              en: 'Right'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
