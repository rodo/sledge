export default `
<div class="sg-mark-section-wrapper">
    <div class="sg-mark-section-img"></div>
    <div class="sg-mark-section-txt">
        <h3 class="sg_title">Dreams feel real while we're in them.</h3>
        <div class="sg_txt">
            <p>I'm going to improvise. Listen, there's something you should know about me... about inception. An idea is like a virus, resilient, highly contagious. The smallest seed of an idea can grow. It can grow to define or destroy you.</p>
        </div>
        <sg-buttonset style="display:none;"></sg-buttonset>
    </div>
</div>`
