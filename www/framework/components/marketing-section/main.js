import {createComponent} from '../../src/init.js'
import props from './props.js'
import receiveProp from './receiveProp.js'
import template from './template.js'

createComponent('marketing-section', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp
})
