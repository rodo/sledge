import {createComponent} from '../../src/init.js'

const props = [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Texte', en: 'Texte'},
    key: 'text',
    type: 'text',
    applyTo: '.sg_title_h'
  }
]

createComponent('title', {
  render: tag => `<h1 class="sg_title_h">${window.sledge.languageCode !== 'fr' ? 'Title !' : 'Titre !'}</h1>`,
  properties: props
})
