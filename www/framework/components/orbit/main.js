import {createComponent} from '../../src/init.js'
import template from './template.js'
import props from './props.js'
import renderMarkup from './render.js'
import custom from './custom.js'

function receiveProp (comp, prop, value) {
  switch (prop) {
    case 'carousel':
      renderMarkup(comp, value)
      window.$sledge.refreshOrbit()
      break
    case 'custom':
      custom(comp, value)
      window.$sledge.refreshOrbit()
      break
    default:
  }
}

createComponent('orbit', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp,
  onInit: elem => {
    $(elem).foundation()
  }
})
