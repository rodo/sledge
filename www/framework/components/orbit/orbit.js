let timeout = false
let delay = 250
window.addEventListener('resize', () => {
  clearTimeout(timeout)
  timeout = setTimeout(() => window.$sledge.refreshOrbit(), delay)
})

window.$sledge.refreshOrbit = function () {
  if ($('[data-orbit]').length === 0) {
    return
  }
  window.requestAnimationFrame(() => window.Foundation.reInit('orbit'))
}
