export default function custom (comp, value) {
  comp.classList.remove('sg-small')
  comp.classList.remove('sg-large')
  comp.classList.remove('sg-sb-medium')
  comp.classList.remove('sg-sb-large')

  if (value.size && value.size !== 'medium') {
    comp.classList.add('sg-' + value.size)
  }
  if (value.spacebetween && value.spacebetween !== 'small') {
    comp.classList.add('sg-sb-' + value.spacebetween)
  }
  if (window.$sledge.isBuilder) {
    window.PubSub.publish('ACTION_ZONE_RESIZE')
  }

  const orbit = comp.querySelector('.orbit')
  orbit.dataset.autoPlay = 'false'
  if (value.autoplay && !window.$sledge.isBuilder) {
    orbit.dataset.autoPlay = 'true'
  }

  let cont = comp.querySelector('.orbit-slide div')
  let rg = /sg-color-scheme-(\d*)/
  let $containers = $(comp).find('.orbit-slide > div')
  if (cont.className) {
    let match = cont.className.match(rg)
    if (match && match[1]) {
      $containers.removeClass(`sg-color-scheme-${match[1]}`)
    }
  }
  if (value.color) {
    $containers.addClass('sg-color-scheme-' + value.color)
  }
}
