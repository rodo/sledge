import {getImagePath} from '../../src/images.js'

export default function renderMarkup (comp, value) {
  let constrolsCont = comp.querySelector('.orbit-bullets')
  let orbitCont = comp.querySelector('.orbit-container')
  let markupControls = ''
  let markupOrbit = ''
  let idx = 0
  if (value.images) {
    value.images.forEach(image => {
      let caption = null
      if (image.text && image.text[window.$sledge.language]) {
        caption = window.decodeURIComponent(image.text[window.$sledge.language])
      }

      markupOrbit += `
      <li class="orbit-slide ${!idx ? 'is-active' : ''}">
        <div>
          ${getContainerWithPath(image)}
          ${caption !== null
          ? `<div class="orbit-caption">${caption}</div>`
          : ''}
        </div>
      </li>`

      markupControls += `
      ${!idx
      ? `<button class="is-active" data-slide="0"><span class="show-for-sr">slide details 0</span><span class="show-for-sr">Current</span></button>`
      : `<button data-slide="${idx}"><span class="show-for-sr">slide details ${idx}</span></button>`
      }`
      idx++
    })
    constrolsCont.innerHTML = markupControls
    orbitCont.innerHTML = markupOrbit
  }
}

function getContainerWithPath (image) {
  let path = getImagePath(image)
  if (path.style) {
    return `<div style="${path.style}"></div>`
  } else {
    return `<div data-interchange="${path.interchange}"></div>`
  }
}
