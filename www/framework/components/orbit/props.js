export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Images', en: 'Images'},
    key: 'images',
    type: 'carousel',
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      size: 'medium',
      color: 8
    },
    data: [
      {
        kind: 'size',
        type: 'select',
        name: {fr: 'Taille (hauteur)', en: 'Size (height)'},
        conf: [
          {
            ref: 'sd-size-small',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-size-medium',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-size-large',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'spacebetween',
        type: 'select',
        default: 'small',
        name: {fr: 'Espacement', en: 'Space between'},
        conf: [
          {
            ref: 'sd-sb-sm',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-sb-m',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-sb-lg',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'autoplay',
        type: 'checkbox',
        name: {fr: 'D&eacute;filement automatique', en: 'Auto play'},
        ref: 'sd-prop-autoplay'
      },
      {
        kind: 'color',
        type: 'themeColor',
        name: {fr: 'Couleur du conteneur d\'image', en: 'Image container color'}
      }
    ],
    applyTo: 'self'
  }
]
