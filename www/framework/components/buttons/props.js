export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  },
  {
    name: {fr: 'Positions', en: 'Positions'},
    key: 'settings',
    type: 'custom',
    default: {
      align: 'left',
      direction: 'row'
    },
    data: [
      {
        kind: 'align',
        type: 'select',
        name: {fr: 'Alignement', en: 'Align'},
        conf: [
          {
            ref: 'sd-align-right',
            value: 'right',
            label: {
              fr: 'Droite',
              en: 'Right'
            }
          },
          {
            ref: 'sd-align-left',
            value: 'left',
            label: {
              fr: 'Gauche',
              en: 'Left'
            }
          },
          {
            ref: 'sd-align-center',
            value: 'center',
            label: {
              fr: 'Centre',
              en: 'Center'
            }
          }
        ]
      },
      {
        kind: 'direction',
        type: 'select',
        name: {fr: 'Orientation', en: 'Direction'},
        conf: [
          {
            ref: 'sd-dir-row',
            value: 'row',
            label: {
              fr: 'En ligne',
              en: 'Row'
            }
          },
          {
            ref: 'sd-dir-column',
            value: 'column',
            label: {
              fr: 'Colonne',
              en: 'Column'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
