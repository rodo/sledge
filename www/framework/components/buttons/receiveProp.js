export default function (comp, prop, value, fullValue) {
  if (prop === 'custom') {
    const cont = comp.querySelector('.sg_buttonset')
    if (value.align) {
      cont.classList.remove('sg-buttons-center')
      cont.classList.remove('sg-buttons-right')
      if (value.align !== 'left') {
        cont.classList.add(`sg-buttons-${value.align}`)
      }
    }

    if (value.direction) {
      cont.classList.remove('sg_vertical')
      if (value.direction !== 'row') {
        cont.classList.add(`sg_vertical`)
      }
    }
  }
}
