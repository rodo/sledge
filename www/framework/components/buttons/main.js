import {createComponent} from '../../src/init.js'
import props from './props.js'
import template from './template.js'
import receiveProp from './receiveProp.js'

createComponent('buttons', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp
})
