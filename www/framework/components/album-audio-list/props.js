export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    },
    default: {
      color: '8'
    }
  },
  {
    name: {fr: 'Fichier Audio', en: 'Audio File'},
    key: 'audio',
    type: 'audio',
    applyTo: 'self',
    multiple: true,
    image: true,
    title: true
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    default: {
      hSize: '5'
    },
    applyTo: '.sl-aa-list-title'
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      colorKind: 'primary'
    },
    data: [
      {
        kind: 'colorKind',
        type: 'select',
        default: 'primary',
        name: {fr: 'Thème barre de progression', en: 'Progress bar theme'},
        conf: [
          {
            ref: 'sd-al-sec',
            value: 'secondary',
            label: {
              fr: 'Secondaire',
              en: 'Secondary'
            }
          },
          {
            ref: 'sd-al-suc',
            value: 'success',
            label: {
              fr: 'Succès',
              en: 'Success'
            }
          },
          {
            ref: 'sd-al-war',
            value: 'warning',
            label: {
              fr: 'Information',
              en: 'Warning'
            }
          },
          {
            ref: 'sd-al-al',
            value: 'alert',
            label: {
              fr: 'Alerte',
              en: 'Alert'
            }
          },
          {
            ref: 'sd-al-def',
            value: 'primary',
            label: {
              fr: 'Par défaut',
              en: 'Default'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
