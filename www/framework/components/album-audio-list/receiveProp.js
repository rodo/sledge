import {addMediaForExport} from '../../src/utilities'
import {getImagePath} from '../../src/images'

export default function (comp, prop, value, projName) {
  if (value.sounds) {
    let urlStart = ''
    if (window.sledge.isPublish) {
      urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'
    }
    const sounds = []
    let markup = ''
    let idx = 0
    value.sounds.forEach(sound => {
      let imgPath = {}
      if (sound.image) {
        addMediaForExport(sound.image)
        imgPath = getImagePath(sound.image, true)
        if (imgPath.path) {
          imgPath.path
          imgPath.interchange
        }
        
      }
      markup += `<li><a data-idx="${idx}">${sound.desc || sound.originalName}</a></li>`
      if (window.sledge.isPublish) {
        addMediaForExport(sound, true)
        sounds.push(
          {
            file: `${urlStart}/media/${sound.name}.mp3`,
            name: sound.desc || sound.originalName,
            interchange: imgPath.interchange || null,
            imagePath: imgPath.path || null
          }
        )
      } else {
        sounds.push(
          {
            file: `http://localhost:${window.localStorage.getItem('listennerPort')}/${projName}/media/${sound.name}.mp3`,
            name: sound.desc || sound.originalName,
            interchange: imgPath.interchange || null,
            imagePath: imgPath.path || null
          }
        )
      }
      idx++
    })
    comp.querySelector('.sl-audio').dataset.sounds = JSON.stringify(sounds)
    let menu = comp.querySelector('.menu')
    menu.innerHTML = markup
  }

  if (prop === 'custom') {
    const pr = comp.querySelector('.progress')
    pr.className = 'progress'
    if (value.colorKind && value.colorKind !== 'primary') {
      pr.classList.add(value.colorKind)
    }
    
    //const met = comp.querySelector('.progress-meter')
    const h6 = comp.querySelector('.sl-aa-audio-ctrl-wrap h6')
    const border = comp.querySelector('.sl-aa-list-title')
    const tc = window.localStorage.getItem('themeConfig')
    let theme = window.sledge.currentTheme === 'default' ? window.$sledge.defaultThemeConf : (window.sledge.themeConfig || JSON.parse(tc))
    let bgcolor = theme['foundation-palette'][value.colorKind]

    if (bgcolor) {
      h6.style.color = bgcolor.value
      border.style.borderBottomColor = bgcolor.value
    }
  }
}
