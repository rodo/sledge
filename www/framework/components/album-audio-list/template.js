export default `<div class="sl-audio" data-sounds="[]">
<div class="sl-audio-wrapper">
  <audio controls="controls" src="framework/components/simple-audio/img/sound.mp3">
  No htlm audio support <code>audio</code>.
  </audio>
</div>
<div class="sl-aa-audio-cont">
  <h5 class="sl-aa-list-title">Album</h5>
  <div class="sl-aa-audio-current">  
    <div class="sl-aa-audio-img"></div>
    <div class="sl-aa-audio-ctrl-wrap">
      <h6><i class="fi-music"></i> <span class="sd_audio_title">Music title</span></h6>
      <div class="progress" role="progressbar" tabindex="0" aria-valuenow="25" aria-valuemin="0" aria-valuetext="25 percent" aria-valuemax="100">
        <div class="progress-meter" style="width: 25%"></div>
      </div>
    </div>
  </div>
  <div class="sl-aa-audio-list">
    <ul class="vertical menu">
      <li><a>Sound 1</a></li>
      <li><a>Sound 2</a></li>
      <li><a>Sound 3</a></li>
    </ul>
  </div>
</div>
</div>
`

/*
 class="is-active"
  */
