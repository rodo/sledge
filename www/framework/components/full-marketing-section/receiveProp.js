export default function receiveProp (node, type, value, valueImage) {
    if (type === 'custom') {
      node.style.minHeight = value.height + 'vh'

      const txt = node.querySelector('.sg-fmark-section-txt')
      
      txt.classList.remove('sg-strong-text')
      txt.classList.remove('sg-strong-light-text')
      txt.classList.remove('sg-strong-dark-text')

      if (value && value.txt === 'txtStrong') {
        txt.classList.add('sg-strong-text')
      } 

      if (value && value.txt === 'txtStrongLight') {
        txt.classList.add('sg-strong-light-text')
      }

      if (value && value.txt === 'txtStrongDark') {
        txt.classList.add('sg-strong-dark-text')
      }
    }
}
