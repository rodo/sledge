export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-13'
    },
    allow: {
      image: true,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Hauteur', en: 'Height'},
    key: 'height',
    type: 'custom',
    default: {
      height: 30
    },
    data: [
      {
        kind: 'height',
        type: 'range',
        default: 30,
        name: {fr: 'Hauteur minimale (pour écrans moyen et large)', en: 'Min height (for medium and large screen)'},
        conf: {
          min: 10,
          max: 100
        }
      }
    ],
    applyTo: 'self'
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription',
    type: 'text_long',
    applyTo: '.sg-fmark-section-txt'
  },
  {
    name: {fr: 'Relief du texte', en: 'Text visibility'},
    key: 'settings',
    type: 'custom',
    default: {
      txt : 'none'
    },
    data: [
      {
        kind: 'txt',
        type: 'radioList',
        name: {fr: 'Texte ombré', en: 'Texte Shadow'},
        conf: [
          {
            ref: 'sd-sh-no',
            value: 'none',
            label: {fr: 'Sans ombre', en: 'No shadow'}
          },
          {
            ref: 'sd-sh-d',
            value: 'txtStrongDark',
            label: {fr: 'Texte avec ombre sombre', en: 'Dark shadow text'}
          },
          {
            ref: 'sd-sh-l',
            value: 'txtStrongLight',
            label: {fr: 'Texte avec ombre claire', en: 'Light shadow text'}
          },
          {
            ref: 'sd-sh-db',
            value: 'txtStrong',
            label: {fr: 'Texte ombré double', en: 'Double shadow text'}
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
