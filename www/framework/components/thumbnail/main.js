import {createComponent} from '../../src/init.js'
import props from './props.js'
import receiveProp from './receiveProp.js'

const template = `<img class="thumbnail" src="framework/components/thumbnail/img/thumbnail.png" alt="">`

createComponent('thumbnail', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp
})
