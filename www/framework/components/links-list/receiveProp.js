import {getImagePath} from '../../src/images.js'
import linkData from '../../src/properties/links.js'

export default function receiveProp (comp, prop, value, fullVal) {
  let cont = comp.querySelector('ul')
  if (prop === 'custom') {
    if (value.pageColor) {
      let rgLink = /sg-color-scheme-(\d*)-link/
      let matchLink = comp.className.match(rgLink)
      if (matchLink && matchLink[1]) {
        comp.classList.remove(`sg-color-scheme-${matchLink[1]}-link`)
      }
      comp.classList.add(`sg-color-scheme-${value.pageColor}-link`)
    }
    if (value.align) {
      cont.classList.remove('align-right')
      cont.classList.remove('align-center')
      if (value.align !== 'left') {
        cont.classList.add(`align-${value.align}`)
      }
    }

    if (value.orientation && value.orientation === 'col') {
      cont.classList.add('vertical')
    } else {
      cont.classList.remove('vertical')
    }

    if (value.icnpos) {
      cont.classList.remove('icon-top')
      cont.classList.remove('icon-left')
      cont.classList.remove('icon-right')
      cont.classList.remove('icon-bottom')
      cont.classList.add('icons')
      cont.classList.add('icon-' + value.icnpos)
      console.info(fullVal)
      manageItems(fullVal['links'], cont)
    }
  }

  if (prop === 'links') {
    manageItems(value, cont)
  }
}

function manageItems (value, cont) {
  if (value.links && value.links.length !== 0) {
    let markup = ''
    value.links.forEach(item => {
      markup += renderItem(item, cont)
    })
    cont.innerHTML = markup
  }
}

function renderItem (data, cont) {
  let imageTag = ''
  let className = ''
  switch (data.imageType) {
    case 'logo':
      imageTag = `<i class="${data.image}"></i>`
      break
    case 'media':
      className = 'sg-lists-item-img'
      let imgObj = getImagePath(data)
      if (imgObj.interchange) {
        imageTag = `<i class="sg-logo-img" data-interchange="${imgObj.interchange}">&nbsp;</i>`
      } else {
        imageTag = `<i class="sg-logo-img" style="background-image:url(${imgObj.path});">&nbsp;&nbsp;</i>`
      }
      break
    default:
      imageTag = `<i class="fi-list"></i>`
  }

  let link = linkData(data)

  if (cont.classList.contains('icon-bottom') || cont.classList.contains('icon-right')) {
    return `<li class="${className}"><a ${link.dataset} href="${link.href}"><span>${data.name && data.name[window.$sledge.language] ? decodeURIComponent(data.name[window.$sledge.language]) : ''}</span>&nbsp;${imageTag}</a></li>`
  } else {
    return `<li class="${className}"><a ${link.dataset} href="${link.href}">${imageTag}&nbsp;<span>${data.name && data.name[window.$sledge.language] ? decodeURIComponent(data.name[window.$sledge.language]) : ''}</span></a></li>`
  }
}
