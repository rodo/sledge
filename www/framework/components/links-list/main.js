import {createComponent} from '../../src/init.js'
import props from './props.js'
import receiveProp from './receiveProp.js'

const template = `    
<ul class="menu">
<li><a href="#"><i class="fi-list"></i> <span>One</span></a></li>
<li><a href="#"><i class="fi-list"></i> <span>Two</span></a></li>
<li><a href="#"><i class="fi-list"></i> <span>Three</span></a></li>
<li><a href="#"><i class="fi-list"></i> <span>Four</span></a></li>
</ul>`

createComponent('links-list', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp
})
