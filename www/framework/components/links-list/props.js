export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Liens', en: 'Links'},
    key: 'links',
    type: 'links',
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      orientation: 'row',
      align: 'left'
    },
    data: [
      {
        kind: 'icnpos',
        type: 'select',
        default: 'left',
        name: {fr: 'Position des icones', en: 'Icons position'},
        conf: [
          {
            ref: 'sd-pos-left',
            value: 'left',
            label: {
              fr: 'Gauche',
              en: 'Left'
            }
          },
          {
            ref: 'sd-pos-right',
            value: 'right',
            label: {
              fr: 'Droite',
              en: 'Right'
            }
          },
          {
            ref: 'sd-pos-top',
            value: 'top',
            label: {
              fr: 'Haut',
              en: 'Top'
            }
          },
          {
            ref: 'sd-pos-bottom',
            value: 'bottom',
            label: {
              fr: 'Bas',
              en: 'Bottom'
            }
          }
        ]
      },
      {
        kind: 'orientation',
        type: 'radioList',
        name: {fr: 'Orientation', en: 'Orientation'},
        conf: [
          {
            ref: 'sd-ori-row',
            value: 'row',
            label: {
              fr: 'En ligne',
              en: 'Row'
            }
          },
          {
            ref: 'sd-ori-col',
            value: 'col',
            label: {
              fr: 'En colonne',
              en: 'Column'
            }
          }
        ]
      },
      {
        kind: 'align',
        type: 'select',
        name: {fr: 'Alignement', en: 'Align'},
        conf: [
          {
            ref: 'sd-align-right',
            value: 'right',
            label: {
              fr: 'Droite',
              en: 'Right'
            }
          },
          {
            ref: 'sd-align-left',
            value: 'left',
            label: {
              fr: 'Gauche',
              en: 'Left'
            }
          },
          {
            ref: 'sd-align-center',
            value: 'center',
            label: {
              fr: 'Centre',
              en: 'Center'
            }
          }
        ]
      },
      {
        kind: 'pageColor',
        type: 'themeColor',
        name: {fr: 'Couleur des icones', en: 'Icone color'}
      }
    ],
    applyTo: 'self'
  }
]
