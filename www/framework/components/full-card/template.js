export default `
<div class="card">
  <div class="card-divider">
    This is a header
  </div>
  <div class="sd-card-img card-image">
  <img src="${!window.sledge.isPublish ? 'framework/components/card/img/card.png' : ''}">
  </div>  
  <div class="card-section">
    <h4 class="sg_title">This is a card.</h4>
    <div class="sg_txt">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <sg-buttonset style="display:none;"></sg-buttonset>    
  </div>
</div>`
