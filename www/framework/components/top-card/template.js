export default `<div class="card">
<div class="card-section">
  <div class="sg_txt">  
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  </div>
  <sg-buttonset style="display:none;"></sg-buttonset>  
</div>
<div class="sd-card-img card-image">
<img src="${!window.sledge.isPublish ? 'framework/components/card/img/card.png' : ''}">
</div>
</div>`
