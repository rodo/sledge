import {createComponent} from '../../src/init.js'
import props from './props.js'
import receiveProp from './receiveProp.js'
import template from './template.js'

createComponent('image-link', {
  properties: props,
  receiveProp: receiveProp,
  render: tag => template
})
