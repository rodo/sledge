import {guidGenerator, setColorFromCode} from '../../src/utilities.js'

export default function (comp, prop, value) {
  if (prop === 'custom') {
    comp.style.minHeight = value.height + 'vh'

    let cont = comp.querySelector('.sd-il-title')
    let rg = /sg-color-scheme-(\d*)-text/
    let match = cont.className.match(rg)
    if (match && match[1]) {
      cont.classList.remove(`sg-color-scheme-${match[1]}-text`)
    }
    if (value.colorScheme) {
      comp.classList.add(`sg-color-scheme-${value.colorScheme}-text`)
    }
    let cont2 = comp.querySelector('.sd-il-link')
    let match2 = cont2.className.match(rg)
    if (match2 && match2[1]) {
      cont2.classList.remove(`sg-color-scheme-${match2[1]}-text`)
    }
    if (value.color) {
      cont.classList.add(`sg-color-scheme-${value.color}-text`)
      cont2.classList.add(`sg-color-scheme-${value.color}-text`)
    }

    let hr = comp.querySelector('hr')
    window.setTimeout(e => {
      console.info(window.getComputedStyle(cont).backgroundColor)
      hr.style.borderColor = window.getComputedStyle(cont).color
    }, 100)
    /*setColorFromCode(value.color, c => {
      hr.style.borderColor = c
    })*/

    comp.classList.remove('sd-il-lg')
    comp.classList.remove('sd-il-med')
    if (value.margin !== 'small') {
      comp.classList.add('sd-il-' + value.margin)
    }

    // **** custom && link
    let href = '#'
    const hrefTag = comp.querySelector('a')
    hrefTag.href = '#'

    if (value.linkType === 'external') {
      href = `${decodeURIComponent(value.external)}`
    }
    if (value.linkType === 'email') {
      href = `mailto:${decodeURIComponent(value.email)}`
    }
    if (value.linkType === 'internal') {
      if (window.sledge.isPublish) {
        let pageData = window.$sledge.pages[value.page]
        href = pageData._id === window.$sledge.bridge.projectConf.startPage ? `./index.html` : ` ./${pageData.name.replace(/\s/g, '_')}.html`
      } else {
        href = `switchpage://${value.page}`
      }
    }
    let idm = guidGenerator()
    if (value.linkType === 'view') {
      hrefTag.dataset.view = value.view
      hrefTag.dataset.effect = value.effect || 'modal'
      hrefTag.dataset.toggle = idm
      if (!window.$sledge.isBuilder) {
        window.$sledge.bridge.addHiddenView(value.view, idm)
      }
    } else {
      hrefTag.dataset.view = null
      hrefTag.dataset.effect = null
      hrefTag.dataset.toggle = null
      hrefTag.href = href
    }
  }
}
