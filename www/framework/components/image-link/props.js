export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Image', en: 'Image'},
    key: 'backgroundBack',
    type: 'background',
    applyTo: '.sd-il-back',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-7'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    applyTo: '.sd-il-title',
    default: {
      text: {
        fr: 'Lorem Ipsum !',
        en: 'Lorem Ipsum !'
      }
    },
    conf: {
      padding: false,
      align: false,
      hSize: false,
      color: false
    }
  },
  {
    name: {fr: 'Sous Titre', en: 'Sub title'},
    key: 'text2',
    type: 'text',
    applyTo: '.sd-il-link',
    default: {
      text: {
        fr: 'Cliquez ici',
        en: 'Click here'
      }
    },
    conf: {
      padding: false,
      align: false,
      hSize: false,
      color: false
    }
  },
  {
    name: {fr: 'Paramètres', en: 'Parameters'},
    key: 'settings',
    type: 'custom',
    default: {
      color: '1',
      margin: 'small',
      height: 30
    },
    data: [
      {
        kind: 'height',
        type: 'range',
        default: 30,
        name: {fr: 'Hauteur minimale', en: 'Min height'},
        conf: {
          min: 10,
          max: 100
        }
      },
      {
        kind: 'link',
        type: 'link',
        name: {fr: 'Lien', en: 'Link'}
      },
      {
        kind: 'margin',
        type: 'select',
        default: 'small',
        name: {fr: 'Marges', en: 'Margins'},
        conf: [
          {
            ref: 'sd-link-top-s',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-link-top-m',
            value: 'med',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-link-top-lg',
            value: 'lg',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'color',
        type: 'themeColor',
        ref: 'sd-prop-color',
        name: {fr: 'Couleur des textes', en: 'texts Color'}
      }
    ],
    applyTo: 'self'
  }
]
