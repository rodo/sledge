module.exports = {
  entry: {
    audio: './audio.js'
  },
  output: {
    path: __dirname,
    filename: './dist/[name].js'
  },
  module: {
    rules: [
      {
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      }
    ]
  }
}
