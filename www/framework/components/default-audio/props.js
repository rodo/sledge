export default [
  {
    name: {fr: 'Fichier Audio', en: 'Audio File'},
    key: 'audio',
    type: 'audio',
    applyTo: 'self',
    multiple: false,
    title: false
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    default: {
      hSize: '5'
    },
    applyTo: '.sd-sa-title'
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      hasButton: false,
      hasImgDivider: false
    },
    data: [
      {
        kind: 'hasButton',
        type: 'checkbox',
        name: {fr: 'Ajouter des boutons', en: 'Add buttons'},
        ref: 'sd-f-has-button'
      },
      {
        kind: 'hasImgDivider',
        type: 'checkbox',
        name: {fr: 'Espacer l\'image', en: 'Image with padding'},
        ref: 'sd-f-has-img-padding'
      }

    ],
    applyTo: 'self'
  }
]
