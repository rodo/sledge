import {addMediaForExport} from '../../src/utilities'

export default function (comp, prop, value, projName) {
  if (value.sounds && value.sounds[0]) {
    let urlStart = ''
    if (window.sledge.isPublish) {
      urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'
    }        
    let audioTag = comp.querySelector('audio')
    if (window.sledge.isPublish) {
      addMediaForExport(value.sounds[0])
      audioTag.src = `${urlStart}/media/${value.sounds[0].name}.mp3`
    } else {
      audioTag.src = `http://localhost:${window.localStorage.getItem('listennerPort')}/${projName}/media/${value.sounds[0].name}.mp3`
    }
  }
}
