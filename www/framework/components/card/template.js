export default `<div class="card">
<div class="card-divider">
  <h4 class="sg_title">Title</h4>
</div>
<div class="sd-card-img card-image">
  <img src="${!window.sledge.isPublish ? 'framework/components/card/img/card.png' : ''}">
</div>
<div class="card-section">
  <div class="sg_txt">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  </div>
  <sg-buttonset style="display:none;"></sg-buttonset>  
</div>
</div>`

