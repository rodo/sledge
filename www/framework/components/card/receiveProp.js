export default function (comp, prop, value, fullValue) {
  let node = comp.querySelector('.sd-card-img')
  node.classList.remove('card-section')
  if (prop === 'custom') {
    if (value.hasImgDivider) {
      node.classList.add('card-section')
    }
    if (value.hasButton) {
      comp.querySelector('sg-buttonset').style.display = 'block'
    } else {
      comp.querySelector('sg-buttonset').style.display = 'none'
    }
  }
}
