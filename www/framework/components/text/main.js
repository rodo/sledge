import {createComponent} from '../../src/init.js'

const props = [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: true,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Contenu', en: 'Text content'},
    key: 'text',
    type: 'text_long',
    applyTo: '.sg_text_cont',
    default: {
      text: {
        fr: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tempor semper tellus non viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam ut ornare magna. Nam tempor elit sit amet fermentum interdum. Cras quis ipsum vitae sem pretium semper vitae at nisl. Sed in laoreet est. Cras non lacus malesuada, lacinia mi eu, bibendum velit.',
        en: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tempor semper tellus non viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam ut ornare magna. Nam tempor elit sit amet fermentum interdum. Cras quis ipsum vitae sem pretium semper vitae at nisl. Sed in laoreet est. Cras non lacus malesuada, lacinia mi eu, bibendum velit.'
      }
    }
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      hasButton: false,
      hasImgDivider: false,
      spacing: 'small'
    },
    data: [
      {
        kind: 'spacing',
        type: 'select',
        default: 'small',
        name: {fr: 'Espacement', en: 'Spacing'},
        conf: [
          {
            ref: 'sd-size-small',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-size-medium',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-size-large',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]

createComponent('text', {
  render: tag => `<div class="sg_text_cont"></div>`,
  properties: props,
  receiveProp: (comp, prop, value) => {
    if (prop === 'custom') {
      comp.classList.remove('sg-padding-large')
      comp.classList.remove('sg-padding-medium')
      if (value.spacing === 'large' || value.spacing === 'medium') {
        comp.classList.add(`sg-padding-${value.spacing}`)
      }
    }
  }
})
