export default function (comp, prop, value) {
  if (prop === 'embed') {
    let iframe = null
    let vidCont = comp.querySelector('.responsive-embed')
    vidCont.classList.remove('widescreen')
    vidCont.classList.remove('panorama')
    vidCont.classList.remove('square')
    vidCont.classList.remove('vertical')
    
    let code = (value.iframe && value.iframe[window.$sledge.language]) ? window.decodeURIComponent(value.iframe[window.$sledge.language]) : null
    console.info(code)
    switch (value.ratio) {
      case '43':
        iframe = code || `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.9916256990905!2d2.292287251879602!3d48.8583700791858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e2964e34e2d%3A0x8ddca9ee380ef7e0!2sTour+Eiffel!5e0!3m2!1sfr!2sfr!4v1507725439896" width="420" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>`
        break
      case '169':
        vidCont.classList.add('widescreen')
        iframe = code || `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.9916256990905!2d2.292287251879602!3d48.8583700791858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e2964e34e2d%3A0x8ddca9ee380ef7e0!2sTour+Eiffel!5e0!3m2!1sfr!2sfr!4v1507725439896" width="560" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>`
        break
      case '256':
        vidCont.classList.add('panorama')
        iframe = code || `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.9916256990905!2d2.292287251879602!3d48.8583700791858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e2964e34e2d%3A0x8ddca9ee380ef7e0!2sTour+Eiffel!5e0!3m2!1sfr!2sfr!4v1507725439896" width="1024" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>`
        break
      case '916':
        vidCont.classList.add('vertical')
        iframe = code || `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.9916256990905!2d2.292287251879602!3d48.8583700791858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e2964e34e2d%3A0x8ddca9ee380ef7e0!2sTour+Eiffel!5e0!3m2!1sfr!2sfr!4v1507725439896" width="315" height="560" frameborder="0" style="border:0" allowfullscreen></iframe>`
        break
      case '11':
        vidCont.classList.add('square')
        iframe = code || `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.9916256990905!2d2.292287251879602!3d48.8583700791858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e2964e34e2d%3A0x8ddca9ee380ef7e0!2sTour+Eiffel!5e0!3m2!1sfr!2sfr!4v1507725439896" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>`
        break
      default:
        vidCont.classList.add('widescreen')
        iframe = code || `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.9916256990905!2d2.292287251879602!3d48.8583700791858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e2964e34e2d%3A0x8ddca9ee380ef7e0!2sTour+Eiffel!5e0!3m2!1sfr!2sfr!4v1507725439896" width="560" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>`
    }
    vidCont.innerHTML = iframe
  }

  if (prop === 'custom') {
    comp.classList.remove('sg-padding-large')
    comp.classList.remove('sg-padding-medium')
    if (value.spacing === 'large' || value.spacing === 'medium') {
      comp.classList.add(`sg-padding-${value.spacing}`)
    }
  }
}
