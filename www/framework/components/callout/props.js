export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription',
    type: 'text_long',
    applyTo: '.sg_cal_text'
  },
  {
    name: {fr: 'Paramètres', en: 'Parameters'},
    key: 'settings',
    type: 'custom',
    default: {
      role: 'primary'
    },
    data: [
      {
        kind: 'hasButton',
        type: 'checkbox',
        name: {fr: 'Ajouter des boutons', en: 'Add buttons'},
        ref: 'sd-f-has-button'
      },
      {
        kind: 'role',
        type: 'select',
        name: {fr: `Rôle`, en: 'Role'},
        conf: [
          {
            ref: 'sd-pos-no',
            value: 'no',
            label: {
              fr: 'Sans rôle',
              en: 'No role'
            }
          },
          {
            ref: 'sd-pos-primary',
            value: 'primary',
            label: {
              fr: 'Premier',
              en: 'Primary'
            }
          },
          {
            ref: 'sd-pos-secondary',
            value: 'secondary',
            label: {
              fr: 'Second',
              en: 'Secondary'
            }
          },
          {
            ref: 'sd-pos-success',
            value: 'success',
            label: {
              fr: 'Succès',
              en: 'Success'
            }
          },
          {
            ref: 'sd-pos-warning',
            value: 'warning',
            label: {
              fr: 'Avertissement',
              en: 'Warning'
            }
          },
          {
            ref: 'sd-pos-alert',
            value: 'alert',
            label: {
              fr: 'Alerte',
              en: 'Alert'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
