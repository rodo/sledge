export default function receiveProp (node, type, value, valueImage) {
  if (type === 'custom') {
    if (value.hasButton) {
      node.querySelector('sg-buttonset').style.display = 'block'
    } else {
      node.querySelector('sg-buttonset').style.display = 'none'
    }
    if (value.role) {
      const collout = node.querySelector('.callout')
      collout.classList.remove('primary')
      collout.classList.remove('secondary')
      collout.classList.remove('warning')
      collout.classList.remove('alert')
      collout.classList.remove('success')
      if (value.role !== 'no') {
        collout.classList.add(value.role)
      }
    }
  }
}
