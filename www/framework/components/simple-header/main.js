import {createComponent} from '../../src/init.js'
import Menu from '../../src/menu.js'

const props = [
  {
    name: {fr: 'Menu', en: 'Menu'},
    key: 'menu',
    type: 'menu',
    sticky: false,
    title: false,
    position: true,
    default: {
      position: 'bottom'
    },
    applyTo: 'self'
  },
  {
    name: {fr: 'Options du menu', en: 'Menu options'},
    key: 'map',
    type: 'sitemap',
    toggle: false,
    onlyPage: false,
    applyTo: 'self'
  },
  {
    name: {fr: 'Logo', en: 'Logo'},
    key: 'logo',
    type: 'logobackground',
    position: false,
    applyTo: 'self'
  },
  {
    name: {fr: 'Hauteur', en: 'Height'},
    key: 'height',
    type: 'vh',
    applyTo: 'self',
    default: {
      vh: 40
    }
  },
  {
    name: {fr: 'Contenu', en: 'Content'},
    key: 'block',
    position: false,
    type: 'headertextblock',
    applyTo: 'self'
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      color: '#cccccc',
      backgroundtype: 'pattern',
      pattern: 'sd-back-3'
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'text',
    type: 'text',
    applyTo: '.sg_title',
    default: {
      text: {
        fr: 'Salut par ici !',
        en: 'Hello here !'
      }
    }
  }
]

const template = `<div class="sg-top-section"><h1 class="sg_title">Mon super site !</h1></div>`

createComponent('simple-header', {
  render: tag => template,
  properties: props,
  receiveProp: (comp, prop, value, fullValue) => {
    value = value || {}

    let sitemapVal = {}
    if (prop === 'sitemap') {
      prop = 'menu'
      sitemapVal = value
      value = fullValue.menu
    } else if (prop === 'menu') {
      sitemapVal = fullValue.sitemap
    }

    if (prop === 'menu' && value.asMenu) {
      $(comp).find('.menu-centered').remove()

      let addClass = ''
      if (value.menuBack === 'transDark') {
        addClass = 'sg-menu-back-dark'
      }
      if (value.menuBack === 'transLight') {
        addClass = 'sg-menu-back-light'
      }
      if (value.menuBack === 'trans') {
        addClass = 'sg-menu-back-trans'
      }

      let cont = document.createElement('div')
      cont.className = 'menu-centered top-bar'
      if (addClass !== '') {
        cont.classList.add(addClass)
      }
      let menuGenerator = new Menu()
      cont.innerHTML = menuGenerator.buildMap(sitemapVal)
      if (value.position === 'bottom') {
        comp.append(cont)
      } else {
        comp.prepend(cont)
        comp.querySelector('.top-bar').style.marginTop = 0
      }
      if (value.colorScheme) {
        $(comp).find('li a').addClass(`sg-color-scheme-${value.colorScheme}-text`)
      }
    }

    if (prop === 'menu' && value.asMenu === false) {
      $(comp).find('.menu-centered').remove()
    }

    if (prop === 'logobackground' && value.asLogo === false) {
      $(comp).find('.sg-logo').remove()
    }

    if (prop === 'logobackground' && value.asLogo) {
      $(comp).find('.sg-logo').remove()

      let cont = document.createElement('div')
      cont.className = 'sg-logo'
      cont.style.backgroundImage = 'url(./styles/img/logoSledge200.png)'
      let contA = document.createElement('a')
      contA.href = './index.html'
      contA.appendChild(cont)
      $('.sg-top-section').prepend(contA)

      switch (value.size) {
        case 'medium':
          // default
          break
        case 'large':
          cont.classList.add('sg-large')
          break
        case 'small':
          cont.classList.add('sg-small')
          break
        default:
      }

      if (value.logoHeight) {
        if (value.logoHeight === 201) {
          cont.height = 'auto'
        }
        cont.style.height = value.logoHeight + 'px'
      }

      if (value.media) {
        let url = window.sledge.isPublish ? '.' : `${window.$sledge.conf.dbServerUrl}/${window.localStorage.getItem('currentProject')}`
        cont.style.backgroundImage = `url(${url}/media/${decodeURIComponent(value.media)}.${value.mediaType}`
      }
    }
  }
})
