import {createComponent} from '../../src/init.js'
import GetMap from '../../src/sitemap.js'

const props = [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: true,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    data: [
      {
        kind: 'spacing',
        type: 'select',
        default: 'small',
        name: {fr: 'Espacement', en: 'Spacing'},
        conf: [
          {
            ref: 'sd-size-small',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-size-medium',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-size-large',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'titleColor',
        type: 'themeColor',
        name: {fr: 'Couleur des titres', en: 'Titles color'}
      },
      {
        kind: 'pageColor',
        type: 'themeColor',
        name: {fr: 'Couleur des liens', en: 'Links color'}
      }
    ],
    applyTo: 'self'
  },
  {
    name: {fr: 'Options du plan', en: 'Map options'},
    key: 'map',
    type: 'sitemap',
    toggle: false,
    applyTo: 'self'
  }
]

function receiveProp (comp, prop, value) {
  if (prop === 'custom') {
    if (value.pageColor) {
      let rgLink = /sg-color-scheme-(\d*)-link/
      let matchLink = comp.className.match(rgLink)
      if (matchLink && matchLink[1]) {
        comp.classList.remove(`sg-color-scheme-${matchLink[1]}-link`)
      }
      comp.classList.add(`sg-color-scheme-${value.pageColor}-link`)
    }
    if (value.titleColor) {
      let rgTitle = /sg-color-scheme-(\d*)-title/
      let matchTitle = comp.className.match(rgTitle)
      if (matchTitle && matchTitle[1]) {
        comp.classList.remove(`sg-color-scheme-${matchTitle[1]}-title`)
      }
      comp.classList.add(`sg-color-scheme-${value.titleColor}-title`)
    }
    comp.classList.remove('sg-padding-large')
    comp.classList.remove('sg-padding-medium')
    if (value.spacing === 'large' || value.spacing === 'medium') {
      comp.classList.add(`sg-padding-${value.spacing}`)
    }
  }

  if (prop === 'sitemap') {
    let mapGenerator = new GetMap()
    comp.innerHTML = mapGenerator.buildMap(false, value)
  }
}

createComponent('footer-sitemap', {
  render: tag => {
    let mapGenerator = new GetMap()
    return mapGenerator.buildMap()
  },
  properties: props,
  receiveProp: receiveProp
})
