export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-7'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      height: 30
    },
    data: [
      {
        kind: 'height',
        type: 'range',
        default: 30,
        name: {fr: 'Hauteur minimale', en: 'Min height'},
        conf: {
          min: 10,
          max: 100
        }
      }
    ],
    applyTo: 'self'
  }
]
