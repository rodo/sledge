export default function (comp, prop, value) {
  if (prop === 'custom') {
    comp.style.minHeight = value.height + 'vh'
  }
}
