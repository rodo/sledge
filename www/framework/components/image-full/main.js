import {createComponent} from '../../src/init.js'
import props from './props.js'
import receiveProp from './receiveProp.js'

createComponent('image-full', {
  properties: props,
  receiveProp: receiveProp
})
