import Menu from '../../src/menu'

export default function (comp, prop, value, fullValue) {
  let menuGenerator = new Menu()

  function addScrollWatcher ($cont) {
    window.addEventListener('scroll', e => {
      let distanceY = window.pageYOffset || document.documentElement.scrollTop
      let shrinkOn = 50
      let menu = $cont
      if (distanceY > shrinkOn) {
        menu.addClass('sg-menu-sticky-small')
      } else {
        if (menu.hasClass('sg-menu-sticky-small')) {
          menu.removeClass('sg-menu-sticky-small')
        }
      }
    })
  }

  if (prop === 'menu' && value.asMenu === false) {
    $(comp).find('.sg-menu').remove()
  }
  let sitemapVal = {}
  if (prop === 'sitemap') {
    prop = 'menu'
    sitemapVal = value
    value = fullValue.menu
  } else if (prop === 'menu') {
    sitemapVal = fullValue.sitemap
  }
  if (prop === 'menu' && value.asMenu) {
    comp.classList.add('sg-asmenu')
    let $comp = $(comp)
    if (value.vh) {
      $comp.height(value.vh + 10 + 'vh')
    }
    let addClass = ''
    if (value.menuBack === 'transDark') {
      addClass = 'sg-menu-back-dark'
    }
    if (value.menuBack === 'transLight') {
      addClass = 'sg-menu-back-light'
    }
    if (value.menuBack === 'trans') {
      addClass = 'sg-menu-back-trans'
    }
    $comp.find('.sg-menu').remove()
    let cont = document.createElement('div')
    cont.className = 'sg-menu'
    $comp.removeClass('sg-menu-shrink')
    if (value.menuSticky) {
      if (value.menuShrink) {
        $comp.addClass('sg-menu-shrink')
      }
    }
    if (value.menuSticky && !window.$sledge.isBuilder) {
      addScrollWatcher($comp)
      $comp.addClass('sg-menu-sticky')
    }

    cont.innerHTML = `
      <div class="top-bar ${addClass}">
        <div class="top-bar-title">
          <span data-responsive-toggle="${comp.id + 'topmenu'}" data-hide-for="medium">
            <button class="menu-icon dark" type="button" data-toggle></button>
          </span>
          <strong>${value.menuTitle && value.menuTitle[$sledge.language] ? decodeURIComponent(value.menuTitle[$sledge.language]) : ''}</strong>
        </div>
        <div id="${comp.id + 'topmenu'}" class="sg-top-menu">
          <div class="top-bar-left">
              ${menuGenerator.buildMap(sitemapVal)}
          </div>
          <div class="top-bar-right"></div>
        </div>
      </div>`
    comp.appendChild(cont)
    if (value.colorScheme) {
      $(comp).find('li a').addClass(`sg-color-scheme-${value.colorScheme}-text`)
    }
  }

  if (prop === 'logobackground' && value.asLogo === false) {
    $(comp).find('.sg-logo').remove()
  }
  if (prop === 'logobackground' && value.asLogo) {
    $(comp).find('.sg-logo').parent().remove()
    let appendCont = comp
    let inMenu = false
    if ($(comp).find('.sg-menu').length !== 0) {
      appendCont = $(comp).find('.top-bar-title').get()[0]
      inMenu = true
    }
    let cont = document.createElement('div')
    cont.className = 'sg-logo'
    if (inMenu) {
      cont.classList.add('sg-logo-menu')
    }
    cont.style.backgroundImage = 'url(./styles/img/logoSledge200.png)'
    let contA = document.createElement('a')
    contA.href = './index.html'
    contA.appendChild(cont)
    $(appendCont).prepend(contA)

    if (!inMenu) {
      switch (value.position) {
        case 'content':
          // default
          break
        case 'topleft':
          cont.classList.add('sg-logo-topleft')
          break
        case 'topright':
          cont.classList.add('sg-logo-topright')
          break
        default:
      }
    }

    switch (value.size) {
      case 'medium':
        // default
        break
      case 'large':
        cont.classList.add('sg-large')
        break
      case 'small':
        cont.classList.add('sg-small')
        break
      default:
    }

    if (value.logoHeight) {
      if (value.logoHeight === 201) {
        cont.height = 'auto'
      }
      cont.style.height = value.logoHeight + 'px'
    }

    if (value.media) {
      if (window.sledge.isPublish) {
        cont.style.backgroundImage = `url(${window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'}/media/${decodeURIComponent(value.media)}.${value.mediaType})`
      } else {
        cont.style.backgroundImage = `url(${$sledge.conf.dbServerUrl}/${window.localStorage.getItem('currentProject')}/media/${decodeURIComponent(value.media)}.${value.mediaType})`
      }
    }
  }
}