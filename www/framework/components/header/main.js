import {createComponent} from '../../src/init'
import props from './props'
import receiveProp from './receiveProp'

const template = `<div class="sg-central-block-content">
  <h1 class="sg_title">Mon super site !</h1>
  <div class="subheader">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam omnis, maxime libero natus qui minus!</p>
  </div>
  <sg-buttonset></sg-buttonset>
</div>`

createComponent('header', {
  render: tag => template,
  properties: props,
  cssClass: 'sg-central-block',
  receiveProp: receiveProp
})
