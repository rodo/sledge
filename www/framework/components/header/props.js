export default [
  {
    name: {fr: 'Menu', en: 'Menu'},
    key: 'menu',
    type: 'menu',
    applyTo: 'self'
  },
  {
    name: {fr: 'Options du menu', en: 'Menu options'},
    key: 'map',
    type: 'sitemap',
    toggle: false,
    onlyPage: false,
    applyTo: 'self'
  },
  {
    name: {fr: 'Logo', en: 'Logo'},
    key: 'logo',
    type: 'logobackground',
    applyTo: 'self',
    defaultPosition: 'content',
    positionWithMenu: false,
    position: [
      {
        key: 'content',
        text: {
          en: 'Follow content',
          fr: 'Suit le contenu'
        }
      },
      {
        key: 'topleft',
        text: {
          en: 'Top left',
          fr: 'Haut &agrave; gauche'
        }
      },
      {
        key: 'topright',
        text: {
          en: 'Top right',
          fr: 'Haut &agrave; droite'
        }
      }
    ]
  },
  {
    name: {fr: 'Hauteur', en: 'Height'},
    key: 'height',
    type: 'vh',
    applyTo: 'self',
    default: {
      vh: 80
    }
  },
  {
    name: {fr: 'Contenu', en: 'Content'},
    key: 'block',
    type: 'headertextblock',
    applyTo: 'self'
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      color: '#ffffff',
      backgroundtype: 'pattern',
      pattern: 'sd-back-3'
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'text',
    type: 'text',
    applyTo: '.sg_title',
    default: {
      text: {
        fr: 'Salut par ici !',
        en: 'Hello here !'
      }
    }
  },
  {
    name: {fr: 'Description', en: 'Description'},
    key: 'textDescription',
    type: 'text_long',
    applyTo: '.subheader'
  },
  {
    name: {fr: 'Bouton', en: 'Button'},
    key: 'buttons',
    type: 'button',
    applyTo: '.button-group'
  },
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  }
]
