export default class Menu {
  constructor (params) {
    //
  }

  buildMenu (settings = {}) {
    let root = window.$sledge.bridge.siteMapManager.getMap(window.$sledge.language)
    let markup = `<ul class="vertical menu accordion-menu" data-accordion-menu data-submenu-toggle="true">`
    const remove = settings.remove || []
    root.forEach(obj => {
      if (obj.kind === 'page') {
        if (remove.indexOf(obj.id) === -1) {
          markup += this.getPageItem(obj)
        }
      } else {
        if (remove.indexOf(obj.id) === -1) {
          markup += this.explore(obj, settings.toggle)
        }
      }
    })
    return markup + '</ul>'
  }

  explore (data, toggle) {
    let markup = `<li><a href="#"><span>${data.localName}</span></a><ul class="vertical menu nested ${toggle ? ' is-active' : ''}">`
    if (data.children) {
      data.children.forEach(obj => {
        if (obj.kind === 'page') {
          markup += this.getPageItem(obj)
        } else {
          markup += this.explore(obj)
        }
      })
    } else {
      return ''
    }
    markup += `</ul></li>`
    return markup
  }

  getPageItem (data) {
    let markup = ''
    if (window.$sledge.isBuilder) {
      markup += `<li><a>${data.localName}</a></li>`
    } else {
      if (window.sledge.isPublish) {
        let pageData = window.$sledge.pages[data.id]
        markup +=
        window.$sledge.bridge.projectConf.startPage === pageData._id
        ? `<li><a href="./index.html">${data.localName}</a></li>`
        : `<li><a href="./${data.name.replace(/\s/g, '_')}.html">${data.localName}</a></li>`
      } else {
        markup += `<li><a href="switchpage://${data.id}">${data.localName}</a></li>`
      }
    }
    return markup
  }
}
