import {createComponent} from '../../src/init.js'
import GetMap from './generator.js'
import props from './props.js'

function receiveProp (comp, prop, value) {
  if (prop === 'custom') {
    let rgLink = /sg-color-scheme-(\d*)-link/
    let matchLink = comp.className.match(rgLink)
    if (matchLink && matchLink[1]) {
      comp.classList.remove(`sg-color-scheme-${matchLink[1]}-link`)
    }
    if (value.pageColor) {
      comp.classList.add(`sg-color-scheme-${value.pageColor}-link`)
    }
  }

  if (prop === 'sitemap') {
    let mapGenerator = new GetMap()
    comp.innerHTML = mapGenerator.buildMenu(value)
  }
}

createComponent('accordion-menu', {
  render: tag => {
    let mapGenerator = new GetMap()
    return mapGenerator.buildMenu()
  },
  properties: props,
  receiveProp: receiveProp
})
