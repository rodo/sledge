export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Options du menu', en: 'Menu options'},
    key: 'map',
    type: 'sitemap',
    toggle: true,
    applyTo: 'self'
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Couleur', en: 'Color'},
    key: 'settings',
    type: 'custom',
    data: [
      {
        kind: 'pageColor',
        type: 'themeColor',
        name: {fr: 'Couleur des liens', en: 'Links color'}
      }
    ],
    applyTo: 'self'
  }
]
