export default [
  {
    name: {fr: 'Image', en: 'Image'},
    key: 'image',
    type: 'image',
    applyTo: 'img'
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription',
    type: 'text_long',
    applyTo: '.sg_txt'
  },
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      hasButton: false,
      hasImgDivider: false
    },
    data: [
      {
        kind: 'hasButton',
        type: 'checkbox',
        name: {fr: 'Ajouter des boutons', en: 'Add buttons'},
        ref: 'sd-f-has-button'
      },
      {
        kind: 'hasImgDivider',
        type: 'checkbox',
        name: {fr: 'Espacer l\'image', en: 'Image with padding'},
        ref: 'sd-f-has-img-padding'
      }
    ],
    applyTo: 'self'
  }
]
