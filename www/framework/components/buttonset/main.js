import {guidGenerator, addMediaForExport} from '../../src/utilities.js'
import {refreshProperty} from '../../src/properties/properties.js'
import {initComponent} from '../../src/init.js'

let tagName = 'buttonset'
let conf = {
  private: true
}

class ButtonSet extends HTMLElement {
  constructor () {
    super()
    this.applyProperty = (key, value) => {
      refreshProperty(this, this.properties[key], value)
    }
    let parentCol = this.closest('.columns')
    if (parentCol) {
      parentCol.childrenReady()
    }
  }

  refreshProps () {
    if (conf.properties && conf.properties.length !== 0) {
      conf.properties.forEach(item => refreshProperty(this, item))
    }
  }

  receiveProp (value) {
    let markup = ''
    if (!value.buttons) {
      return
    }
    value.buttons.forEach(item => {
      let id = item.id
      let classMarkup = ''
      if (!id) {
        id = guidGenerator() // not realy used
      }
      if (!item.link && !item.img) {
        classMarkup = `${(item.size && item.size !== 'default') ? item.size : ''} ${item.role || ''} ${item.uppercase ? 'sd_uppercase' : ''} ${item.round ? 'sd_round' : ''} ${item.radius ? 'radius' : ''} ${item.hollow ? 'hollow' : ''} ${item.expanded ? 'expanded' : ''} button`
      }
      let style = null
      let sizesItem = null
      let noImg = false
      let path = 'framework/components/buttonset/icn.png'
      if (item.img) {
        let ig = {}
        if (item.image && item.image[window.$sledge.language]) {
          ig = item.image[window.$sledge.language]
          addMediaForExport(ig)
        } else {
          noImg = true
        }

        classMarkup = `${(item.size && item.size !== 'default') ? item.size : ''} button sd-img-button`

        let urlStart = `${window.$sledge.conf.dbServerUrl}/${window.localStorage.getItem('currentProject')}`
        if (window.sledge.isPublish) {
          urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'          
        }
        
        switch (ig.numberOfSizes) {
          case 1:
            style = `background-image:url(${urlStart}/media/${ig.media}.${ig.mediaType});`
            break
          case 2:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(ig.media)}_small.${ig.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(ig.media)}.${ig.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(ig.media)}.${ig.mediaType}, large]`
            break
          case 3:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(ig.media)}_small.${ig.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(ig.media)}_medium.${ig.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(ig.media)}.${ig.mediaType}, large]`
            break
          case 4:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(ig.media)}_small.${ig.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(ig.media)}_medium.${ig.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(ig.media)}_large.${ig.mediaType}, large]`
            break
          default:
            if (noImg) {
              style = `background-image:url(${path});`
            } else {
              style = `background-image:url(${urlStart}/media/${ig.media}.${ig.mediaType});`
            }
            
        }
      }
      const elemNode = (item.action === 'view' || item.type === 'submit') ? 'button' : 'a'
      let href = '#'
      
      if (item.action === 'external') {
        href = `href="${decodeURIComponent(item.external)}"`
      }
      if (item.action === 'email') {
        href = `href="mailto:${decodeURIComponent(item.email)}"`
      }
      if (item.action === 'internal') {
        if (window.sledge.isPublish) {
          let pageData = $sledge.pages[item.page]
          href = pageData._id === window.$sledge.bridge.projectConf.startPage ? `href="./index.html"` : ` href="./${pageData.name.replace(/\s/g, '_')}.html"`
        } else {
          href = ` href="switchpage://${item.page}"`
        }
      }
      let idm = guidGenerator()
      markup += `<${elemNode} 
          id="${id}"
          ${item.type ? `type="${item.type}"` : ''}
          class="${classMarkup}"
          ${style !== null ? ` style="${style}" ` : ''}
          ${sizesItem !== null ? ` data-interchange="${sizesItem}" ` : ''}
          ${href !== '#' ? href : ' '}
          ${item.action === 'view' ? `data-view=${item.view}` : ''}
          ${item.action === 'view' ? `data-effect=${item.effect || 'modal'}` : ''}
          ${item.action === 'view' ? `data-toggle=${idm}` : ''}
        >
        ${item.img ? '' : decodeURIComponent(item.text[window.$sledge.language])}
      </${elemNode}>`
      if (!$sledge.isBuilder) {
        if (item.action === 'view') {
          $sledge.bridge.addHiddenView(item.view, idm)
        }
      }
    })
    this.innerHTML = markup
    if (value.buttons.length === 1) {
      this.classList.add('sd-button-single')
    }
  }

  connectedCallback () {
    this.ref = tagName
    initComponent(this, conf)
    if (conf.cssClass) {
      this.classList.add(conf.cssClass)
    }
  }
}
window.customElements.define(`sg-${tagName}`, ButtonSet)
