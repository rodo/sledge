export default `<div class="marketing-site-hero">
  <div class="marketing-site-hero-content">
    <h1>Yeti Snowcone Agency</h1>
    <p class="subheader">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam omnis, maxime libero natus qui minus!</p>
    <a href="#" class="round button">learn more</a>
    <div class="button-group">
      <a class="button">One</a>
      <a class="button">Two</a>
      <a class="button">Three</a>
    </div>
  </div>
</div>`
