import renderBackground from '../../src/properties/background'

class SdView extends HTMLElement {
  constructor () {
    super()
  }

  connectedCallback () {
    this.classList.add('sg_view')
  }

  refresh (settings, projName) {
    if (settings) {
      renderBackground(this, null, null, settings, projName)
    }
  }

  applyMarkup (markup, settings, projName) {
    this.innerHTML = markup
    if (settings) {
      renderBackground(this, null, null, settings, projName)
    }
    window.$sledge.equalizer(this)
  }
}

window.customElements.define('sg-view', SdView)
