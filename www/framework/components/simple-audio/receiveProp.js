import {addMediaForExport} from '../../src/utilities'

export default function (comp, prop, value, projName) {
  if (value.sounds && value.sounds[0]) {
    let urlStart = ''
    if (window.sledge.isPublish) {
      urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'
    }        
    let audioTag = comp.querySelector('audio')
    let labelTag = comp.querySelector('label')
    if (window.sledge.isPublish) {
      addMediaForExport(value.sounds[0])
      audioTag.src = `${urlStart}/media/${value.sounds[0].name}.mp3`
    } else {
      audioTag.src = `http://localhost:${window.localStorage.getItem('listennerPort')}/${projName}/media/${value.sounds[0].name}.mp3`
    }
    labelTag.innerHTML = value.sounds[0].desc || value.sounds[0].originalName
  }

  if (prop === 'custom') {
    if (value.hasButton) {
      comp.querySelector('sg-buttonset').style.display = 'block'
    } else {
      comp.querySelector('sg-buttonset').style.display = 'none'
    }

    let rg = /sg-color-scheme-(\d*)/
    let node = comp.querySelector('.sl-sp-audio-cont')
    let match = node.className.match(rg)
    if (match && match[1]) {
      node.classList.remove(`sg-color-scheme-${match[1]}`)
    }
    node.style.backgroundColor = ''
    if (value.color) {
      node.classList.add(`sg-color-scheme-${value.color}`)
    }

    node.classList.remove('sl-audio-radius')
    if (value.rounded) {
      node.classList.add('sl-audio-radius')
    }

    //
    let ctrl = comp.querySelector('i')
    let rgt = /sg-color-scheme-(\d*)-text/
    let matchT = ctrl.className.match(rgt)
    console.info(matchT, ctrl)
    if (matchT && matchT[1]) {
      ctrl.classList.remove(`sg-color-scheme-${matchT[1]}-text`)
    }
    if (value.buttonColor) {
      ctrl.classList.add(`sg-color-scheme-${value.buttonColor}-text`)
    }

    let label = comp.querySelector('label')
    let matchl = label.className.match(rgt)
    if (matchl && matchl[1]) {
      label.classList.remove(`sg-color-scheme-${matchl[1]}-text`)
    }
    if (value.buttonColor) {
      label.classList.add(`sg-color-scheme-${value.buttonColor}-text`)
    }
  }
}
