export default [
  {
    name: {fr: 'Fichier Audio', en: 'Audio File'},
    key: 'audio',
    type: 'audio',
    applyTo: 'self',
    multiple: false,
    title: true
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      rounded: false,
      color: '1',
      buttonColor: '7'
    },
    data: [
      {
        kind: 'hasButton',
        type: 'checkbox',
        name: {fr: 'Ajouter des boutons', en: 'Add buttons'},
        ref: 'sd-f-has-button'
      },
      {
        kind: 'rounded',
        type: 'checkbox',
        name: {fr: 'Coins arrondis', en: 'Rounded'},
        ref: 'sd-prop-rounded'
      },
      {
        kind: 'color',
        type: 'themeColor',
        ref: 'sd-prop-color',
        name: {fr: 'Couleur de fond', en: 'background Color'}
      },
      {
        kind: 'buttonColor',
        type: 'themeColor',
        ref: 'sd-prop-color',
        name: {fr: 'Couleur des contrôles', en: 'Controls Color'}
      }
    ],
    applyTo: 'self'
  }
]
