import {createComponent} from '../../src/init.js'
import props from './props.js'
import template from './template.js'
import receiveProp from './receiveProp.js'

createComponent('simple-audio', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp,
  files: ['default-audio/dist/audio.js'],
  filesCallback: () => {
    window.requestAnimationFrame(() => window.$sledge.refresh['audioComponent']())
  }
})
