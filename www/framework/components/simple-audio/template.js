export default `
<div class="sl-audio">
  <div class="sl-audio-wrapper">
    <audio controls="controls" src="framework/components/simple-audio/img/sound.mp3">
    No htlm audio support <code>audio</code>.
    </audio>
  </div>
  <div class="sl-sp-audio-cont">
    <button class="sl-audio-play button clear small"><i class="fi-play"></i></button>
    <label>Name</label>
    <sg-buttonset style="display:none;"></sg-buttonset>
  </div>
</div>
`
