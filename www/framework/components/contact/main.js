import {createComponent} from '../../src/init.js'
import props from './props.js'
import receiveProp from './receiveProp.js'
import template from './template.js'

createComponent('contact', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp,
  onInit: comp => {
    /***** fix render issue in webkit !? *****/
    comp.style.float = 'none'
    window.setTimeout(() => {
      comp.style.float = 'left'
    }, 0)
  }
})
