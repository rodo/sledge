export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: true,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Boutons d\'envoie', en: 'Send button'},
    key: 'buttonsend',
    type: 'buttons',
    single: true,
    noAction: true,
    default: {
      buttons: [
        {
          text: {fr: 'Envoyer', en: 'Send'},
          type: 'submit'
        }
      ]
    },
    applyTo: '.sg-contact-form-send'
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    default: {
      text: {
        fr: 'Contactez-nous',
        en: 'Mail us'
      }
    },
    applyTo: '.contact-us-header'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      spacing: 'small'
    },
    data: [
      {
        kind: 'emailSend',
        type: 'text',
        special: 'email',
        name: {fr: 'Saisir le courriel d\'envoi', en: 'Enter destination email'},
        ref: 'sd-f-email'
      },
      {
        kind: 'emailSubject',
        type: 'text',
        default: {
          fr: 'Sujet',
          en: 'Subject'
        },
        name: {fr: 'Sujet par d&eacute;faut', en: 'Default subject'},
        ref: 'sd-f-emailSub'
      },
      {
        kind: 'spacing',
        type: 'select',
        default: 'small',
        name: {fr: 'Espacement', en: 'Spacing'},
        conf: [
          {
            ref: 'sd-size-small',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-size-medium',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-size-large',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'placeName',
    type: 'custom',
    data: [
      {
        kind: 'namePlace',
        type: 'text',
        name: {fr: 'Saisir le placeholder', en: 'Enter the placeholder'},
        ref: 'sd-f-name'
      }
    ],
    applyTo: '.sd-cont-name'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'placeEmail',
    type: 'custom',
    data: [
      {
        kind: 'emailPlace',
        type: 'text',
        name: {fr: 'Saisir le placeholder', en: 'Enter the placeholder'},
        ref: 'sd-f-emailP'
      }
    ],
    applyTo: '.sd-cont-email'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'placeText',
    type: 'custom',
    data: [
      {
        kind: 'textPlace',
        type: 'text',
        name: {fr: 'Saisir le placeholder', en: 'Enter the placeholder'},
        ref: 'sd-f-textp'
      }
    ],
    applyTo: '.sd-cont-text'
  }
]
