export default `<h1 class="contact-us-header">Mail Us</h1>
<form class="contact-us-form" action="" method="post" enctype="text/plain">
  <input type="text" class="sd-cont-name" name="Full name" placeholder="Full name">
  <input type="email" class="sd-cont-email" placeholder="Email" name="Email">
  <textarea name="message" class="sd-cont-text" id="" rows="12" placeholder="Type your message here"></textarea>
  <div class="contact-us-form-actions">
    <sg-buttonset class="sg-contact-form-send"></sg-buttonset>
  </div>
</form>`
