export default function (comp, prop, value) {
  if (prop === 'custom') {
    if (comp.classList.contains('sg_contact')) {
      comp.classList.remove('sg-padding-large')
      comp.classList.remove('sg-padding-medium')
      if (value.spacing === 'large' || value.spacing === 'medium') {
        comp.classList.add(`sg-padding-${value.spacing}`)
      }
      if (value.emailSend) {
        comp.querySelector('form').setAttribute('action', `mailto:${decodeURIComponent(value.emailSend)}?subject=${decodeURIComponent(value.emailSubject[window.$sledge.language])}`)
      }
    } else {
      if (value.namePlace) {
        comp.setAttribute('placeholder', decodeURIComponent(value.namePlace[window.$sledge.language]))
      }

      if (value.emailPlace) {
        comp.setAttribute('placeholder', decodeURIComponent(value.emailPlace[window.$sledge.language]))
      }

      if (value.textPlace) {
        comp.setAttribute('placeholder', decodeURIComponent(value.textPlace[window.$sledge.language]))
      }
    }
  }
}
