export default function receiveProp (comp, prop, value) {
  let cont = comp.querySelector('ul')  
  if (prop === 'custom') {
    if (value.pageColor) {
      let rgLink = /sg-color-scheme-(\d*)-link/
      let matchLink = comp.className.match(rgLink)
      if (matchLink && matchLink[1]) {
        comp.classList.remove(`sg-color-scheme-${matchLink[1]}-link`)
      }
      comp.classList.add(`sg-color-scheme-${value.pageColor}-link`)
    }
    if (value.align) {
      cont.classList.remove('align-right')
      cont.classList.remove('align-center')
      if (value.align !== 'left') {
        cont.classList.add(`align-${value.align}`)
      }
    }

    comp.classList.remove('sg-fss-medium')
    comp.classList.remove('sg-fss-large')
    if (value.size && value.size !== 'small') {
      comp.classList.add('sg-fss-' + value.size)
    }
  }

  if (prop === 'social') {
    if (value.networks) {
      let markup = ''
      value.networks.forEach(net => {
        markup += `<li title="${net.name}"><a href="${net.url}"><i class="fi-social-${net.name.toLowerCase()}" aria-hidden="true"></i></a></li>`
      })
      cont.innerHTML = markup
    }
  }
}
