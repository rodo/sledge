export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'R&eacute;seaux', en: 'Networks'},
    key: 'social',
    type: 'social',
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      align: 'left'
    },
    data: [
      {
        kind: 'align',
        type: 'select',
        name: {fr: 'Alignement', en: 'Align'},
        conf: [
          {
            ref: 'sd-align-right',
            value: 'right',
            label: {
              fr: 'Droite',
              en: 'Right'
            }
          },
          {
            ref: 'sd-align-left',
            value: 'left',
            label: {
              fr: 'Gauche',
              en: 'Left'
            }
          },
          {
            ref: 'sd-align-center',
            value: 'center',
            label: {
              fr: 'Centre',
              en: 'Center'
            }
          }
        ]
      },
      {
        kind: 'size',
        type: 'select',
        name: {fr: 'Taille', en: 'Size'},
        default: 'small',
        conf: [
          {
            ref: 'sd-size-small',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-size-medium',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-size-big',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'pageColor',
        type: 'themeColor',
        name: {fr: 'Couleur des icones', en: 'Icone color'}
      }
    ],
    applyTo: 'self'
  }
]
