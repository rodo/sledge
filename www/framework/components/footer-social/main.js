import {createComponent} from '../../src/init.js'
import props from './props.js'
import receiveProp from './receiveProp.js'

const template = `    
<ul class="menu simple">
<li><a href="https://www.facebook.com/"><i class="fi-social-facebook" aria-hidden="true"></i></a></li>
<li><a href="https://www.instagram.com/?hl=en"><i class="fi-social-instagram" aria-hidden="true"></i></a></li>
<li><a href="https://www.pinterest.com/"><i class="fi-social-pinterest" aria-hidden="true"></i></a></li>
<li><a href="https://twitter.com/?lang=en"><i class="fi-social-twitter" aria-hidden="true"></i></a></li>
</ul>`

createComponent('footer-social', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp
})
