import {createComponent} from '../../src/init.js'

const props = [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      color: '4'
    },
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      height: 30
    },
    data: [
      {
        kind: 'height',
        type: 'range',
        default: 30,
        name: {fr: 'Hauteur minimale', en: 'Min height'},
        conf: {
          min: 10,
          max: 100
        }
      }
    ],
    applyTo: 'self'
  }
]

createComponent('space', {
  properties: props,
  receiveProp: (comp, prop, value) => {
    if (prop === 'custom') {
      comp.style.minHeight = value.height + 'vh'
    }
  }
})
