import {addMediaForExport} from '../../src/utilities'

export default (comp, prop, value, fullValue) => {
  if (prop === 'marketingicon') {
    if (value.logo) {
      comp.classList.remove('sd-demo')
      comp.innerHTML = ''
      comp.style.backgroundImage = 'none'
      if (value.logo.indexOf('fi-') === 0) {
        let mk = `<i class="${value.logo}"></i>`
        $(comp).append(mk)
        if (value.colorScheme) {
          $(comp).find('i').addClass(`sg-color-scheme-${value.colorScheme}-text`)
        }
      } else {
        addMediaForExport(value)
        let sizesItem = null
        let urlStart = `${window.$sledge.conf.dbServerUrl}/${window.$sledge.currentdb}`
        if (window.sledge.isPublish) {
          urlStart = '.'
        }
        switch (value.numberOfSizes) {
          case 1:
            comp.style.backgroundImage = `url(${urlStart}/media/${value.logo}.${value.mediaType})`
            break
          case 2:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
            break
          case 3:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
            break
          case 4:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_large.${value.mediaType}, large]`
            break
        }
        if (sizesItem) {
          comp.dataset.interchange = sizesItem
        }
      }
    }

    let theme = window.sledge.currentTheme === 'default' ? $sledge.defaultThemeConf : window.sledge.themeConfig
    let bgcolor = theme['foundation-color']['$light-gray']

    if (value.background) {
      comp.style.backgroundColor = bgcolor
      comp.classList.add('sd-feature-back')
    } else {
      comp.style.backgroundColor = 'rgba(0,0,0,0)'
      comp.classList.remove('sd-feature-back')
    }
    if (value.rounded) {
      comp.classList.add('sd-feature-round')
    } else {
      comp.classList.remove('sd-feature-round')
    }
    comp.classList.remove('sd-feature-medium')
    comp.classList.remove('sd-feature-large')
    switch (value.size) {
      case 'medium':
        comp.classList.add('sd-feature-medium')
        break
      case 'large':
        comp.classList.add('sd-feature-large')
        break
      default:
    }
  }

  if (prop === 'custom') {
    comp.classList.remove('sd-feature-sb-large')
    comp.classList.remove('sd-feature-sb-medium')
    if (value.spacebetween && value.spacebetween !== 'small') {
      comp.classList.add(`sd-feature-sb-${value.spacebetween}`)
    } 

    comp.classList.remove('sd-feature-sbo-small')
    comp.classList.remove('sd-feature-sbo-large')
    comp.classList.remove('sd-feature-sbo-medium')
    if (value.spacebottom && value.spacebottom !== 'vsmall') {
      comp.classList.add(`sd-feature-sbo-${value.spacebetween}`)
    } 

    if (!value.position || value.position === 'side') {
      comp.classList.remove('sd-feature-col')
    } else {
      comp.classList.add('sd-feature-col')
    }

    let but = comp.querySelector('sg-buttonset')
    if (value.hasButton) {
      but.style.display = 'block'
      but.dataset.publish = null
    } else {
      but.style.display = 'none'
      but.dataset.publish = 'no'
    }
  }
}
