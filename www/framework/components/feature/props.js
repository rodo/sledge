export default [
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    single: false,
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    applyTo: '.sg_title',
    default: {
      hSize: '4',
      text: {
        fr: 'Titre ici !',
        en: 'Title here !'
      }
    }
  },
  {
    name: {fr: 'Description', en: 'Description'},
    key: 'textAbout',
    type: 'text_long',
    applyTo: '.sg-feature-about',
    default: {
      text: {
        fr: 'Votre super description !',
        en: 'Your super description !'
      }
    }
  },
  {
    name: {fr: 'Logo', en: 'Logo'},
    key: 'logo',
    type: 'marketingicon',
    applyTo: 'div.sd-feature-section-img'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      position: 'top',
      hasButton: false
    },
    data: [
      {
        kind: 'position',
        type: 'radioList',
        name: {fr: 'Agencement', en: 'Layout'},
        conf: [
          {
            ref: 'sd-pos-top',
            value: 'top',
            label: {
              fr: 'Image en haut',
              en: 'Image on top'
            }
          },
          {
            ref: 'sd-pos-side',
            value: 'side',
            label: {
              fr: 'Image &agrave; gauche',
              en: 'Side image'
            }
          }
        ]
      },
      {
        kind: 'hasButton',
        type: 'checkbox',
        name: {fr: 'Ajouter un bouton', en: 'Add a button'},
        ref: 'sd-f-has-button'
      },
      {
        kind: 'spacebetween',
        type: 'select',
        default: 'small',
        name: {fr: 'Espacement', en: 'Space between'},
        conf: [
          {
            ref: 'sd-sb-sm',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-sb-m',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-sb-lg',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'spacebottom',
        type: 'select',
        default: 'vsmall',
        name: {fr: 'Espace bas', en: 'Bottom space'},
        conf: [
          {
            ref: 'sd-sbo-vm',
            value: 'vsmall',
            label: {
              fr: 'Sans espace',
              en: 'No space'
            }
          },
          {
            ref: 'sd-sbo-sm',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-sbo-m',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-sbo-lg',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self'
  }
]
