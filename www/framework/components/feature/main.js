import {createComponent} from '../../src/init'
import receiveProp from './receiveProps'
import props from './props'

const template = `
<div class="sd-feature-section-img sd-demo"></div>
<div class="sd-feature-section-txt">
  <h4 class="sg_title"></h4>
  <div class="sg-feature-about sd-text"></div>
  <sg-buttonset style="display:none;"></sg-buttonset>
</div>`

createComponent('feature', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp
})
