export default [
  {
    name: {fr: 'Menu', en: 'Menu'},
    key: 'menu',
    type: 'menu',
    sticky: false,
    title: false,
    position: true,
    default: {
      position: 'bottom'
    },
    applyTo: 'self'
  },
  {
    name: {fr: 'Options du menu', en: 'Menu options'},
    key: 'map',
    type: 'sitemap',
    toggle: false,
    onlyPage: false,
    applyTo: 'self'
  },
  {
    name: {fr: 'Logo', en: 'Logo'},
    key: 'logo',
    type: 'logobackground',
    position: false,
    permanent: true,
    fixedSize: true,
    applyTo: 'self'
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      color: '#ffffff'
    },
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background1',
    type: 'background',
    applyTo: '.sd_ch_top_r',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-13'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background2',
    type: 'background',
    applyTo: '.sd_ch_top_l',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-23'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background3',
    type: 'background',
    applyTo: '.sd_ch_b1',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-11'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background4',
    type: 'background',
    applyTo: '.sd_ch_b2',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-19'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background5',
    type: 'background',
    applyTo: '.sd_ch_b3',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-22'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background6',
    type: 'background',
    applyTo: '.sd_ch_b4',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-8'
    },
    allow: {
      image: true,
      video: false,
      color: false
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'text',
    type: 'text_long',
    applyTo: '.sd_ch_txt',
    autoSize: true,
    default: {
      text: {
        fr: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius leo at facilisis hendrerit. Etiam a rutrum massa. Aenean finibus fringilla lacinia. Morbi in cursus lectus. Ut imperdiet nisl quis dui aliquet, sed auctor dui consectetur.',
        en: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius leo at facilisis hendrerit. Etiam a rutrum massa. Aenean finibus fringilla lacinia. Morbi in cursus lectus. Ut imperdiet nisl quis dui aliquet, sed auctor dui consectetur.'
      }
    }
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'title',
    type: 'text',
    applyTo: '.sg_ch_title',
    autoSize: true,
    default: {
      text: {
        fr: 'Mon super site !',
        en: 'My super site !'
      }
    }
  },
  {
    name: {fr: 'Boutons', en: 'Buttons'},
    key: 'button',
    type: 'buttons',
    default: {
      buttons: [
        {
          text: {fr: 'Salut', en: 'Hello'}
        }
      ]
    },
    applyTo: 'sg-buttonset'
  },
  {
    name: {fr: 'R&eacute;seaux', en: 'Networks'},
    key: 'social',
    type: 'social',
    applyTo: 'sg-footer-social'
  },
  {
    name: {fr: 'Couleur', en: 'Color'},
    key: 'settings',
    type: 'custom',
    data: [
      {
        kind: 'pageColor',
        type: 'themeColor',
        name: {fr: 'Couleur des icones', en: 'Icone color'}
      }
    ],
    applyTo: 'sg-footer-social'
  },
  {
    name: {fr: 'Options', en: 'Options'},
    key: 'settings',
    type: 'custom',
    default: {
      circleColor: '7',
      titleSize: 'large'
    },
    data: [
      {
        kind: 'titleSize',
        type: 'select',
        default: 'large',
        name: {fr: 'Taille du titre', en: 'Title size'},
        conf: [
          {
            ref: 'sd-al-l',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          },
          {
            ref: 'sd-al-m',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-al-s',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          }
        ]
      },
      {
        kind: 'circleColor',
        type: 'themeColor',
        ref: 'sd-prop-color',
        name: {fr: 'Couleur cercle', en: 'Circle color'}
      }
    ],
    applyTo: 'self'
  }
]
