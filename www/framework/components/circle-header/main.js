import {createComponent} from '../../src/init.js'
import Menu from '../../src/menu.js'
import template from './template.js'
import props from './props.js'

createComponent('circle-header', {
  render: tag => template,
  properties: props,
  receiveProp: (comp, prop, value, fullValue) => {
    value = value || {}

    let sitemapVal = {}
    if (prop === 'sitemap') {
      prop = 'menu'
      sitemapVal = value
      value = fullValue.menu
    } else if (prop === 'menu') {
      sitemapVal = fullValue.sitemap
    }

    if (prop === 'menu' && value.asMenu) {
      comp.classList.add('sd_asMenu')
      $(comp).find('.menu-centered').remove()

      let addClass = ''
      if (value.menuBack === 'transDark') {
        addClass = 'sg-menu-back-dark'
      }
      if (value.menuBack === 'transLight') {
        addClass = 'sg-menu-back-light'
      }
      if (value.menuBack === 'trans') {
        addClass = 'sg-menu-back-trans'
      }      

      let cont = document.createElement('div')
      cont.className = 'menu-centered top-bar-left'
      if (addClass !== '') {
        cont.classList.add(addClass)
      }
      let menuGenerator = new Menu()
      cont.innerHTML = `
      <div class="top-bar-title">
        <span data-responsive-toggle="${comp.id + 'topmenu'}" data-hide-for="medium">
          <button class="menu-icon dark" type="button" data-toggle></button>
        </span>
      </div>
      <div id="${comp.id + 'topmenu'}" class="sg-top-menu">
            ${menuGenerator.buildMap(sitemapVal)}
      </div>
      `

      if (value.position === 'bottom') {
        comp.append(cont)
      } else {
        comp.prepend(cont)
        comp.querySelector('.top-bar-left').style.marginTop = 0
      }
      if (value.colorScheme) {
        $(comp).find('li a').addClass(`sg-color-scheme-${value.colorScheme}-text`)
      }
    }

    if (prop === 'menu' && value.asMenu === false) {
      $(comp).find('.menu-centered').remove()
    }

    if (prop === 'logobackground' && value.asLogo === false) {
      $(comp).find('.sg-logo').remove()
    }

    if (prop === 'custom') {
      const circle = comp.querySelector('.sd_ch_circle')
      let rg = /sg-color-scheme-(\d*)/
      let match = circle.className.match(rg)
      if (match && match[1]) {
        circle.classList.remove(`sg-color-scheme-${match[1]}`)
      }
      circle.style.backgroundColor = ''
      if (value.circleColor) {
        circle.classList.add(`sg-color-scheme-${value.circleColor}`)
      }

      if (value.pageColor) {
        let rgLink = /sg-color-scheme-(\d*)-link/
        let matchLink = comp.className.match(rgLink)
        if (matchLink && matchLink[1]) {
          comp.classList.remove(`sg-color-scheme-${matchLink[1]}-link`)
        }
        comp.classList.add(`sg-color-scheme-${value.pageColor}-link`)
      }

      const title = comp.querySelector('.sg_ch_title')
      title.classList.remove('sd-ch-title-medium')
      title.classList.remove('sd-ch-title-small')
      if (value.titleSize && value.titleSize !== 'large') {
        title.classList.add('sd-ch-title-' + value.titleSize)
      }
    }

    if (prop === 'logobackground') {

      const cont = comp.querySelector('.sd_ch_logo')
      cont.style.backgroundImage = 'url(./styles/img/logoSledge200.png)'

      switch (value.size) {
        case 'medium':
          // default
          break
        case 'large':
          cont.classList.add('sg-large')
          break
        case 'small':
          cont.classList.add('sg-small')
          break
        default:
      }

      if (value.logoHeight) {
        if (value.logoHeight === 201) {
          cont.height = 'auto'
        }
        cont.style.height = value.logoHeight + 'px'
      }

      if (value.media) {
        let url = window.sledge.isPublish ? '.' : `${window.$sledge.conf.dbServerUrl}/${window.localStorage.getItem('currentProject')}`
        cont.style.backgroundImage = `url(${url}/media/${decodeURIComponent(value.media)}.${value.mediaType}`
      }
    }
  }
})
