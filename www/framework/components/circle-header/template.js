export default `
  <div class="sd_ch_top">  
    <div class="sd_ch_top_r"></div>
    <div class="sd_ch_top_l"></div>
  </div>
  <div class="sd_ch_bottom">
    <div class="sd_ch_b1"></div>
    <div class="sd_ch_b2"></div>
    <div class="sd_ch_b3"></div>
    <div class="sd_ch_b4"></div>
  </div>
  <div class="sd_ch_circle">
    <div class="sd_ch_logo"></div>  
    <h1 class="sg_ch_title">Mon super site !</h1>
    <div class="sd_ch_txt"></div>
    <sg-buttonset></sg-buttonset>
    <sg-footer-social data-inset="true"></sg-footer-social>
  </div>
`
