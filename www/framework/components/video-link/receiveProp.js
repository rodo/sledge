export default function (comp, prop, value) {
  if (prop === 'embed') {
    let link = null
    let iframe = null
    let vidCont = comp.querySelector('.responsive-embed')
    vidCont.classList.remove('widescreen')
    vidCont.classList.remove('panorama')
    vidCont.classList.remove('square')
    vidCont.classList.remove('vertical')
    let code = value.iframe && value.iframe[window.$sledge.language] ? window.decodeURIComponent(value.iframe[window.$sledge.language]) : null
    switch (value.ratio) {
      case '43':
        link = 'https://www.youtube.com/embed/mM5_T-F1Yn4'
        iframe = code || `<iframe width="420" height="315" src="${link}" frameborder="0" allowfullscreen></iframe>`
        break
      case '169':
        link = 'https://www.youtube.com/embed/WUgvvPRH7Oc'
        vidCont.classList.add('widescreen')
        iframe = code || `<iframe width="560" height="315" src="${link}" frameborder="0" allowfullscreen></iframe>`
        break
      case '256':
        link = 'https://www.youtube.com/embed/bnD9I24EL_4'
        vidCont.classList.add('panorama')
        iframe = code || `<iframe width="1024" height="315" src="${link}" frameborder="0" allowfullscreen></iframe>`
        break
      case '916':
        link = 'https://www.youtube.com/embed/bnD9I24EL_4'
        vidCont.classList.add('vertical')
        iframe = code || `<iframe width="315" height="560" src="${link}" frameborder="0" allowfullscreen></iframe>`
        break
      case '11':
        link = 'https://www.youtube.com/embed/zFwYlWwDQVo'
        vidCont.classList.add('square')
        iframe = code || `<iframe width="300" height="300" src="${link}" frameborder="0" allowfullscreen></iframe>`
        break
      default:
        link = 'https://www.youtube.com/embed/WUgvvPRH7Oc'
        vidCont.classList.add('widescreen')
        iframe = code || `<iframe width="560" height="315" src="${link}" frameborder="0" allowfullscreen></iframe>`
    }
    vidCont.innerHTML = iframe
  }

  if (prop === 'custom') {
    comp.classList.remove('sg-padding-large')
    comp.classList.remove('sg-padding-medium')
    if (value.spacing === 'large' || value.spacing === 'medium') {
      comp.classList.add(`sg-padding-${value.spacing}`)
    }
  }
}
