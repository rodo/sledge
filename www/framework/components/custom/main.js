import {createComponent} from '../../src/init.js'
import receiveProp from './receiveProps.js'

const props = [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      color: '4'
    },
    allow: {
      image: true,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Configuration', en: 'Settings'},
    key: 'custom',
    type: 'custom_content',
    applyTo: 'self'
  },
  {
    name: {fr: 'Fichiers', en: 'Files'},
    key: 'files',
    type: 'files',
    applyTo: 'self'
  }
]

createComponent('custom', {
  properties: props,
  receiveProp: receiveProp
})
