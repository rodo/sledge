import {getImagePath} from '../../src/images.js'

export default function (comp, prop, value, files) {
  if (value.html && value.html[window.$sledge.language]) {
    let htmlContent = decodeURIComponent(value.html[window.$sledge.language]) 
    if (files && files.images) {
      files.images.forEach(image => {
        const tplPath = `media/${image.media}.${image.mediaType}`
        const path = getImagePath(image, true)
        const reg = new RegExp(tplPath, 'g')
        htmlContent = htmlContent.replace(reg, path.path)
      })
    }
    comp.innerHTML = htmlContent
  }

  if (value.css) {
    let tag = comp.querySelector('style')
    let cssContent = decodeURIComponent(value.css)
    if (files && files.images) {
      files.images.forEach(image => {
        const tplPath = `media/${image.media}.${image.mediaType}`
        const path = getImagePath(image, true)
        const reg = new RegExp(tplPath, 'g')
        cssContent = cssContent.replace(reg, path.path)
      })
    }
    if (tag) {
      tag.innerHTML = cssContent
    } else {
      const styleTag = document.createElement('style')
      styleTag.innerHTML = cssContent
      comp.appendChild(styleTag)
    }
  }
}
