export default function custom (comp, value) {
  comp.classList.remove('sd-ip-contain')
  
  if (value.size && value.size !== 'cover') {
    comp.classList.add('sd-ip-contain')
  }
  
}
