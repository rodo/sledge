import {getImagePath} from '../../src/images.js'
import {guidGenerator} from '../../src/utilities.js'

export default function renderMarkup (comp, value) {
  let orbitCont = comp.querySelector('.sd-ip-wrapper')
  let markupOrbit = ''
  let idx = 0
  if (value.images && value.images.length !== 0) {
    value.images.forEach(image => {

      markupOrbit += `${getContainerWithPath(image)}`

      idx++
    })
    orbitCont.innerHTML = markupOrbit
  } else {
    orbitCont.innerHTML = `
    <div class="sd-ip-item"></div>
    <div class="sd-ip-item"></div>
    `
  }
  window.requestAnimationFrame(() => $(document.body).foundation())
}

function getContainerWithPath (image) {
  let path = getImagePath(image)
  let path2 = getImagePath(image, true)
  let imgidx = guidGenerator()
  if (!window.$sledge.isBuilder) {
    window.$sledge.bridge.addHiddenImage(
      path2.largePath,
      (image.text && image.text[window.$sledge.language]) ? decodeURIComponent(image.text[window.$sledge.language]) : '',
      imgidx,
      (image.longText && image.longText[window.$sledge.language]) ? decodeURIComponent(image.longText[window.$sledge.language]) : '',
    )
  }
  if (path.style) {
    return `<div data-effect='modal' data-toggle='${imgidx}' class="sd-ip-item" style="${path.style}">
    ${(image.text && image.text[window.$sledge.language])
      ? `<div class="sd-ip-title">${decodeURIComponent(image.text[window.$sledge.language])}</div>`
      : ''}
    </div>`
  } else {
    return `<div data-effect='modal' data-toggle='${imgidx}' class="sd-ip-item" data-interchange="${path.interchange}">
      ${(image.text && image.text[window.$sledge.language])
      ? `<div class="sd-ip-title">${decodeURIComponent(image.text[window.$sledge.language])}</div>`
      : ''}
    </div>`
  }
}
