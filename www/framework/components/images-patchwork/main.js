import {createComponent} from '../../src/init.js'
import template from './template.js'
import props from './props.js'
import renderMarkup from './render.js'
import custom from './custom.js'

function receiveProp (comp, prop, value) {

console.info(comp, prop, value)

  switch (prop) {
    case 'carousel':
      renderMarkup(comp, value)
      break
    case 'custom':
      custom(comp, value)
      break
    default:
  }
}

createComponent('images-patchwork', {
  render: tag => template,
  properties: props,
  receiveProp: receiveProp,
  onInit: elem => {
    $(elem).foundation()
  }
})
