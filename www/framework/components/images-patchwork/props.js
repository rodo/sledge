export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Images', en: 'Images'},
    key: 'images',
    type: 'carousel',
    hasDesc: true,
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      size: 'cover'
    },
    data: [
      {
        kind: 'size',
        type: 'select',
        name: {fr: 'Remplissage', en: 'Filling'},
        conf: [
          {
            ref: 'sd-size-cover',
            value: 'cover',
            label: {
              fr: 'Image qui remplit le conteneur (tronquée)',
              en: 'Image that fills the container (truncated)'
            }
          },
          {
            ref: 'sd-size-contain',
            value: 'contain',
            label: {
              fr: 'Images toujours complète',
              en: 'Always complete pictures'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
