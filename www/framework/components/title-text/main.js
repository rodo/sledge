import {createComponent} from '../../src/init.js'
import props from './props.js'
import template from './template.js'

createComponent('title-text', {
  render: tag => template,
  properties: props
})
