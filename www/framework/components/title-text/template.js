export default `<h2 class="sg_title">Dreams feel real while we're in them.</h2>
<div class="sg_txt">
<p>I'm going to improvise. Listen, there's something you should know about me... about inception. An idea is like a virus, resilient, highly contagious. The smallest seed of an idea can grow. It can grow to define or destroy you.</p>
</div>`
