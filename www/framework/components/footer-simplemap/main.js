import {createComponent} from '../../src/init.js'
import GetMap from '../../src/sitemap.js'
import props from './props.js'

function receiveProp (comp, prop, value) {
  if (prop === 'custom') {
    if (value.pageColor) {
      let rgLink = /sg-color-scheme-(\d*)-link/
      let matchLink = comp.className.match(rgLink)
      if (matchLink && matchLink[1]) {
        comp.classList.remove(`sg-color-scheme-${matchLink[1]}-link`)
      }
      comp.classList.add(`sg-color-scheme-${value.pageColor}-link`)
    }
    if (value.align) {
      let cont = comp.querySelector('ul')
      cont.classList.remove('align-right')
      cont.classList.remove('align-center')
      if (value.align !== 'left') {
        cont.classList.add(`align-${value.align}`)
      }
    }
  }

  if (prop === 'sitemap') {
    let mapGenerator = new GetMap()
    comp.innerHTML = mapGenerator.buildMap(true, value)
  }
}

createComponent('footer-simplemap', {
  render: tag => {
    let mapGenerator = new GetMap()
    return mapGenerator.buildMap(true)
  },
  properties: props,
  receiveProp: receiveProp
})
