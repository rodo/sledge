export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Options du plan', en: 'Map options'},
    key: 'map',
    type: 'sitemap',
    toggle: false,
    onlyPage: true,
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    data: [
      {
        kind: 'pageColor',
        type: 'themeColor',
        name: {fr: 'Couleur des liens', en: 'Links color'}
      },
      {
        kind: 'align',
        type: 'select',
        name: {fr: 'Alignement', en: 'Align'},
        conf: [
          {
            ref: 'sd-align-right',
            value: 'right',
            label: {
              fr: 'Droite',
              en: 'Right'
            }
          },
          {
            ref: 'sd-align-left',
            value: 'left',
            label: {
              fr: 'Gauche',
              en: 'Left'
            }
          },
          {
            ref: 'sd-align-center',
            value: 'center',
            label: {
              fr: 'Centre',
              en: 'Center'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
