export default function receiveProp (node, type, value, valueImage) {
  node.classList.remove('sg-padding-large')
  node.classList.remove('sg-padding-medium')
  if (type === 'custom') {
    if (value.spacing === 'large' || value.spacing === 'medium') {
      node.classList.add(`sg-padding-${value.spacing}`)
    }
  }

  let img = node.querySelector('img')
  let markup = ''
  img.dataset.toggle = null
  if (type === 'link') {
    switch (value.action) {
      case 'reveal':
        let imgidx = guidGenerator()
        img.dataset.effect = 'modal'
        img.dataset.toggle = imgidx
        let image = {}
        if (valueImage) {
          image = getImagePath(valueImage, true)
        }
        if (!window.$sledge.isBuilder) {
          window.$sledge.bridge.addHiddenImage(image.largePath, image.name, imgidx)
          window.requestAnimationFrame(() => $(document.body).foundation())
        }
        break
      case 'external':
        img.classList.remove('thumbnail')
        markup = `<a href="${decodeURIComponent(value.external)}" class="thumbnail">${img.outerHTML}</a>`
        break
      case 'email':
        img.classList.remove('thumbnail')
        markup = `<a href="mailto:${decodeURIComponent(value.email)}" class="thumbnail">${img.outerHTML}</a>`      
        break
      case 'view':
        let idm = guidGenerator()
        img.dataset.view = value.view
        img.dataset.effect = value.view || 'modal'
        img.dataset.toggle = idm
        if (!window.$sledge.isBuilder) {
          window.$sledge.bridge.addHiddenView(value.view, idm)
          window.requestAnimationFrame(() => $(document.body).foundation())          
        }
        break
      case 'internal':
        let href
        if (window.sledge.isPublish) {
          let pageData = window.$sledge.pages[value.page]
          href = pageData._id === window.$sledge.bridge.projectConf.startPage ? `href="./index.html"` : ` href="./${pageData.name.replace(/\s/g, '_')}.html"`
        } else {
          href = ` href="switchpage://${value.page}"`
        }
        img.classList.remove('thumbnail')
        markup = `<a href="${href}" class="thumbnail">${img.outerHTML}</a>`
        break
      default:
        break
    }
    if (markup !== '') {
      node.innerHTML = markup
    }
    // *** fix safari rendering issue that stop the link click !!
    window.requestAnimationFrame(() => {
      node.style.display = 'none'
      setTimeout(() => {
        node.style.display = 'block'
      }, 50)
    })
  }
}
