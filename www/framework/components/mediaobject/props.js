export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Image', en: 'Image'},
    key: 'image',
    type: 'image',
    applyTo: 'img'
  },
  {
    name: {fr: 'Actions', en: 'Actions'},
    key: 'link',
    type: 'link',
    applyTo: 'self'
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    applyTo: '.sg_title',
    default: {
      hSize: '4'
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription',
    type: 'text_long',
    applyTo: '.sg_txt'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      spacing: 'small'
    },
    data: [
      {
        kind: 'spacing',
        type: 'select',
        default: 'small',
        name: {fr: 'Espacement', en: 'Spacing'},
        conf: [
          {
            ref: 'sd-size-small',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-size-medium',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-size-large',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]


/**
 * 
.sg-padding-medium {
  padding-top: 20px !important;
  padding-bottom: 20px !important; }

.sg-padding-large {
  padding-top: 40px !important;
  padding-bottom: 40px !important; }
 */