export default `
<h6 class="sg_p_title">Progress</h6>
<div class="progress" role="progressbar" tabindex="0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
  <div class="progress-meter" style="width: 50%"></div>
</div>
`
