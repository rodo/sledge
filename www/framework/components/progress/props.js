export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'Titre', en: 'Progress title'},
    key: 'text',
    type: 'text',
    applyTo: '.sg_p_title',
    default: {
      hSize: '6'
    }
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      progress: 50,
      colorKind: 'primary'
    },
    data: [
      {
        kind: 'progress',
        type: 'range',
        default: 50,
        name: {fr: 'Valeur', en: 'Value'},
        conf: {
          min: 0,
          max: 100
        }
      },
      {
        kind: 'colorKind',
        type: 'select',
        default: 'primary',
        name: {fr: 'Thème barre de progression', en: 'Progress bar theme'},
        conf: [
          {
            ref: 'sd-al-sec',
            value: 'secondary',
            label: {
              fr: 'Secondaire',
              en: 'Secondary'
            }
          },
          {
            ref: 'sd-al-suc',
            value: 'success',
            label: {
              fr: 'Succès',
              en: 'Success'
            }
          },
          {
            ref: 'sd-al-war',
            value: 'warning',
            label: {
              fr: 'Information',
              en: 'Warning'
            }
          },
          {
            ref: 'sd-al-al',
            value: 'alert',
            label: {
              fr: 'Alerte',
              en: 'Alert'
            }
          },
          {
            ref: 'sd-al-def',
            value: 'primary',
            label: {
              fr: 'Par défaut',
              en: 'Default'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
