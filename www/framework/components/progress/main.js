import {createComponent} from '../../src/init.js'
import template from './template.js'
import props from './props.js'

createComponent('progress', {
  render: tag => template,
  properties: props,
  receiveProp: (comp, prop, value) => {
    if (prop === 'custom') {
      const range = comp.querySelector('.progress-meter')
      range.style.width = value.progress + '%'

      const pr = comp.querySelector('.progress')
      pr.className = 'progress'
      if (value.colorKind && value.colorKind !== 'primary') {
        pr.classList.add(value.colorKind)
      }
    }
  }
})
