import linkData from '../../src/properties/links.js'

export default (comp, prop, value) => {
  if (prop === 'custom') {

    if (value.quoteColor) {
      let cont = comp.querySelectorAll('path')
      let rgTitle = /sg-color-scheme-(\d*)-stroke/
      ;[].forEach.call(cont, path => {
        let matchTitle = path.getAttribute('class').match(rgTitle)
        if (matchTitle && matchTitle[1]) {
          path.classList.remove(`sg-color-scheme-${matchTitle[1]}-stroke`)
        }
        path.classList.add(`sg-color-scheme-${value.quoteColor}-stroke`)
      })
    }

    if (value.textcolor) {
      let rg = /sg-color-scheme-(\d*)-text/
      let cont = comp.querySelector('a')
      let matchTitle = cont.getAttribute('class').match(rg)
      if (matchTitle && matchTitle[1]) {
        cont.classList.remove(`sg-color-scheme-${matchTitle[1]}-text`)
      }
      cont.classList.add(`sg-color-scheme-${value.textcolor}-text`)
    }

    if (value.textlink) {
      let link = {href: '#', dataset: ''}
      if (value.link) {
        link = linkData(value)
      }
      let linkTag = `<a href="${link.href}" ${link.dataset}>${value.textlink && value.textlink[window.$sledge.language] ? decodeURIComponent(value.textlink[window.$sledge.language]) : ''}</a>`
      comp.innerHTML = linkTag
    }
  }
}
