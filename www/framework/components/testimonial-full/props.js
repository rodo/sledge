export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      backgroundtype: 'bgimage',
      bgimage: 'sd-back-11',
      mask: 'darker',
      maskRange: 3
    },
    allow: {
      image: true,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription',
    type: 'text_long',
    applyTo: '.sg-full-width-testimonial-text'
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription2',
    type: 'text_long',
    applyTo: '.sg-full-width-testimonial-source'
  },
  {
    name: {fr: 'Citation', en: 'Quote logo color'},
    key: 'gencolor',
    type: 'custom',
    data: [
      {
        kind: 'quoteColor',
        type: 'themeColor',
        default: {
          textColor: '#000000'
        },
        name: {fr: 'Citation', en: 'Quotation'}
      }
    ],
    applyTo: '.sg-full-width-testimonial-icon'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    data: [
      {
        kind: 'textlink',
        type: 'text',
        name: {fr: 'Texte', en: 'Text'}
      },
      {
        kind: 'link',
        type: 'link',
        name: {fr: 'Lien', en: 'Link'}
      },
      {
        kind: 'textcolor',
        type: 'themeColor',
        name: {fr: 'Couleur', en: 'Color'}
      }
    ],
    applyTo: '.sg-full-width-testimonial-source-context'
  }
]
