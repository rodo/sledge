export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    default: {
      hSize: '4'
    },
    applyTo: '.sg-map-title'
  },
  {
    name: {fr: 'Carte', en: 'Map'},
    key: 'link',
    type: 'embed',
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      spacing: 'small'
    },
    data: [
      {
        kind: 'spacing',
        type: 'select',
        default: 'small',
        name: {fr: 'Espacement', en: 'Spacing'},
        conf: [
          {
            ref: 'sd-size-small',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-size-medium',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-size-large',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      }
    ],
    applyTo: 'self'
  }
]
