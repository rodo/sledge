import {createComponent} from '../../src/init.js'
import Menu from '../../src/menu.js'

const props = [
  {
    name: {fr: 'Menu', en: 'Menu'},
    key: 'menu',
    type: 'menu',
    applyTo: 'self'
  },
  {
    name: {fr: 'Options du menu', en: 'Menu options'},
    key: 'map',
    type: 'sitemap',
    toggle: false,
    onlyPage: false,
    applyTo: 'self'
  },
  {
    name: {fr: 'Logo', en: 'Logo'},
    key: 'logo',
    type: 'logobackground',
    applyTo: 'self',
    defaultPosition: 'bottomcenter',
    position: [
      {
        key: 'menu',
        text: {
          en: 'In the menu',
          fr: 'Dans le menu'
        }
      },
      {
        key: 'bottomcenter',
        text: {
          en: 'Bottom center',
          fr: 'Centre bas'
        }
      }
    ]
  },
  {
    name: {fr: 'Hauteur', en: 'Height'},
    key: 'height',
    type: 'vh',
    applyTo: 'self',
    default: {
      vh: 80
    }
  },
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    default: {
      color: '#ffffff',
      backgroundtype: 'pattern',
      pattern: 'sd-back-3'
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'text',
    type: 'text',
    applyTo: '.sg-polaroid-txt',
    default: {
      text: {
        fr: 'Salut par ici !',
        en: 'Hello here !'
      }
    }
  },
  {
    name: {fr: 'Photo 1', en: 'Photo 1'},
    key: 'photo1',
    type: 'image',
    applyTo: '.sd-pol-1 .sg-polaroid-img'
  },
  {
    name: {fr: 'Photo 2', en: 'Photo 2'},
    key: 'photo2',
    type: 'image',
    applyTo: '.sd-pol-2 .sg-polaroid-img'
  },
  {
    name: {fr: 'Photo 3', en: 'Photo 3'},
    key: 'photo3',
    type: 'image',
    applyTo: '.sd-pol-3 .sg-polaroid-img'
  }
]

const template = `
<div>  
  <div class="sg-polaroid-cont sd-pol-1">
    <div class="sg-polaroid-img"></div>
    <div class="sg-polaroid-txt"></div>
  </div>
  <div class="sg-polaroid-cont sd-pol-2">
    <div class="sg-polaroid-img"></div>
  </div>
  <div class="sg-polaroid-cont sd-pol-3">
    <div class="sg-polaroid-img"></div>
  </div>
</div>`

createComponent('header-polaroid', {
  render: tag => template,
  properties: props,
  receiveProp: (comp, prop, value, fullValue) => {

    let menuGenerator = new Menu()

    function addScrollWatcher ($cont) {
      window.addEventListener('scroll', e => {
        let distanceY = window.pageYOffset || document.documentElement.scrollTop
        let shrinkOn = 50
        let menu = $cont
        if (distanceY > shrinkOn) {
          menu.addClass('sg-menu-sticky-small')
        } else {
          if (menu.hasClass('sg-menu-sticky-small')) {
            menu.removeClass('sg-menu-sticky-small')
          }
        }
      })
    }

    if (prop === 'menu' && value.asMenu === false) {
      $(comp).find('.sg-menu').remove()
    }

    let sitemapVal = {}
    if (prop === 'sitemap') {
      prop = 'menu'
      sitemapVal = value
      value = fullValue.menu
    } else if (prop === 'menu') {
      sitemapVal = fullValue.sitemap
    }

    if (prop === 'menu' && value.asMenu) {
      let $comp = $(comp)
      if (value.vh) {
        $comp.height(value.vh + 10 + 'vh')
      }
      let addClass = ''
      if (value.menuBack === 'transDark') {
        addClass = 'sg-menu-back-dark'
      }
      if (value.menuBack === 'transLight') {
        addClass = 'sg-menu-back-light'
      }
      if (value.menuBack === 'trans') {
        addClass = 'sg-menu-back-trans'
      }  
      
      $comp.find('.sg-menu').remove()
      let cont = document.createElement('div')
      cont.className = 'sg-menu'
      $comp.removeClass('sg-menu-shrink')
      if (value.menuSticky) {
        if (value.menuShrink) {
          $comp.addClass('sg-menu-shrink')
        }
      }
      if (value.menuSticky && !$sledge.isBuilder) {
        addScrollWatcher($comp)
        $comp.addClass('sg-menu-sticky')
      }
      
      cont.innerHTML = `
        <div class="top-bar ${addClass}">
          <div class="top-bar-title">
            <span data-responsive-toggle="${comp.id + 'topmenu'}" data-hide-for="medium">
              <button class="menu-icon dark" type="button" data-toggle></button>
            </span>
            <strong>${value.menuTitle && value.menuTitle[$sledge.language] ? decodeURIComponent(value.menuTitle[$sledge.language]) : ''}</strong>
          </div>
          <div id="${comp.id + 'topmenu'}" class="sg-top-menu">
            <div class="top-bar-left">
                ${menuGenerator.buildMap(sitemapVal)}
            </div>
            <div class="top-bar-right"></div>
          </div>
        </div>`
      comp.appendChild(cont)
      if (value.colorScheme) {
        $(comp).find('li a').addClass(`sg-color-scheme-${value.colorScheme}-text`)
      }
    }

    if (prop === 'logobackground' && value.asLogo === false) {
      $(comp).find('.sg-logo').remove()
    }

    if (prop === 'logobackground' && value.asLogo) {
      $(comp).find('.sg-logo').remove()
      let appendCont = comp
      let inMenu = false
      if (value.position === 'menu') {
        appendCont = $(comp).find('.top-bar-title').get()[0]
        inMenu = true
      }
      let cont = document.createElement('div')
      cont.className = 'sg-logo'
      if (inMenu) {
        cont.classList.add('sg-logo-menu')
      }
      cont.style.backgroundImage = 'url(./styles/img/logoSledge200.png)'
      let contA = document.createElement('a')
      contA.href = './index.html'
      contA.appendChild(cont)
      $(appendCont).prepend(contA)

      if (!inMenu) {
        switch (value.position) {
          case 'bottomcenter':
            comp.classList.add('sg-logo-bottomcenter')
            break
          case 'menu':
            comp.classList.remove('sg-logo-bottomcenter')
            break              
          default:
            comp.classList.add('sg-logo-bottomcenter')
        }
      } else {
        comp.classList.remove('sg-logo-bottomcenter')
      }

      switch (value.size) {
        case 'medium':
          // default
          break
        case 'large':
          cont.classList.add('sg-large')
          break
        case 'small':
          cont.classList.add('sg-small')
          break
        default:
      }

      if (value.logoHeight) {
        if (value.logoHeight === 201) {
          cont.height = 'auto'
        }
        cont.style.height = value.logoHeight + 'px'
      }

      if (value.logo) {
        let url = window.sledge.isPublish ? '.' : `${window.$sledge.conf.dbServerUrl}/${window.localStorage.getItem('currentProject')}`
        cont.style.backgroundImage = `url(${url}/media/${decodeURIComponent(value.logo)}.${value.mediaType}`
      }
    }
  }
})
