import {createComponent} from '../../src/init.js'
import {guidGenerator} from '../../src/utilities.js'

const props = [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Titre', en: 'Title'},
    key: 'text',
    type: 'text',
    applyTo: '.sg-link-title',
    default: {
      text: {
        fr: 'Titre ici !',
        en: 'Title here !'
      }
    },
    conf: {
      padding: false,
      align: false,
      hSize: false,
      color: true
    }
  },
  {
    name: {fr: 'Logo', en: 'Logo'},
    key: 'logo',
    type: 'marketingicon',
    applyTo: '.sg-link-icon',
    conf: {
      size: false,
      background: false,
      rounded: false
    },
    default: {
      logo: 'fi-torso'
    }
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      rounded: false,
      color: '1'
    },
    data: [
      {
        kind: 'link',
        type: 'link',
        name: {fr: 'Lien', en: 'Link'}
      },
      {
        kind: 'rounded',
        type: 'checkbox',
        name: {fr: 'Coins arrondis', en: 'Rounded'},
        ref: 'sd-prop-rounded'
      },
      {
        kind: 'spacetop',
        type: 'select',
        default: 'small',
        name: {fr: 'Espace en haut', en: 'Top space'},
        conf: [
          {
            ref: 'sd-link-top-s',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-link-top-m',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-link-top-lg',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'spacebottom',
        type: 'select',
        default: 'small',
        name: {fr: 'Espace en bas', en: 'Bottom space'},
        conf: [
          {
            ref: 'sd-link-bot-s',
            value: 'small',
            label: {
              fr: 'Petit',
              en: 'Small'
            }
          },
          {
            ref: 'sd-link-bot-m',
            value: 'medium',
            label: {
              fr: 'Moyen',
              en: 'Medium'
            }
          },
          {
            ref: 'sd-link-bot-lg',
            value: 'large',
            label: {
              fr: 'Grand',
              en: 'Large'
            }
          }
        ]
      },
      {
        kind: 'color',
        type: 'themeColor',
        ref: 'sd-prop-color',
        name: {fr: 'Couleur', en: 'Color'}
      }
    ],
    applyTo: 'self'
  }
]

const template = `<a class="sg-link-card" href="#">
  <i  class="sg-link-icon"></i>
  <h3 class="sg-link-title"></h3>
</a>`

createComponent('link-dashboard', {
  render: tag => template,
  properties: props,
  receiveProp: (comp, prop, value, fullValue) => {
    if (prop === 'custom') {
      let cont = comp.querySelector('.sg-link-card')
      let rg = /sg-color-scheme-(\d*)/
      if (cont.className) {
        let match = cont.className.match(rg)
        if (match && match[1]) {
          cont.classList.remove(`sg-color-scheme-${match[1]}`)
        }
      }
      if (value.color) {
        cont.classList.add(`sg-color-scheme-${value.color}`)
      }
      if (value.rounded) {
        cont.classList.add('sd-rounded')
      } else {
        cont.classList.remove('sd-rounded')
      }
      // **** custom && link
      let href = '#'
      cont.href = href

      if (value.linkType === 'external') {
        href = `${decodeURIComponent(value.external)}`
      }
      if (value.linkType === 'email') {
        href = `mailto:${decodeURIComponent(value.email)}`
      }
      if (value.linkType === 'internal') {
        if (window.sledge.isPublish) {
          let pageData = window.$sledge.pages[value.page]
          href = pageData._id === window.$sledge.bridge.projectConf.startPage ? `./index.html` : ` ./${pageData.name.replace(/\s/g, '_')}.html`
        } else {
          href = `switchpage://${value.page}`
        }
      }
      let idm = guidGenerator()
      if (value.linkType === 'view') {
        cont.dataset.view = value.view
        cont.dataset.effect = value.effect || 'modal'
        cont.dataset.toggle = idm
        if (!window.$sledge.isBuilder) {
          window.$sledge.bridge.addHiddenView(value.view, idm)
        }
      } else {
        cont.dataset.view = null
        cont.dataset.effect = null
        cont.dataset.toggle = null
        cont.href = href
      }
      // ***custom && link
      comp.classList.remove('sd-clink-bot-medium')
      comp.classList.remove('sd-clink-bot-large')
      comp.classList.remove('sd-clink-top-medium')
      comp.classList.remove('sd-clink-top-large')
      if (value.spacebottom && value.spacebottom !== 'small') {
        comp.classList.add('sd-clink-bot-' + value.spacebottom)
      }
      if (value.spacetop && value.spacetop !== 'small') {
        comp.classList.add('sd-clink-top-' + value.spacetop)
      }
    }

    if (prop === 'marketingicon') {
      if (value.logo) {
        let match = comp.className.match(/fi-\w+/g)
        if (match && match[0]) {
          comp.classList.remove(match[0])
        }
        if (value.logo.indexOf('fi-') === 0) {
          comp.classList.add(value.logo)
          let rg = /sg-color-scheme-(\d*)-text/
          let match = comp.className.match(rg)
          if (match && match[1]) {
            comp.classList.remove(`sg-color-scheme-${match[1]}-text`)
          }
          if (value.colorScheme) {
            comp.classList.add(`sg-color-scheme-${value.colorScheme}-text`)
          }
        } else {
          let sizesItem = null
          let urlStart = `${$sledge.conf.dbServerUrl}/${$sledge.currentdb}`
          if (window.sledge.isPublish) {
            urlStart = '.'
          }
          switch (value.numberOfSizes) {
            case 1:
              comp.style.backgroundImage = `url(${urlStart}/media/${value.logo}.${value.mediaType})`
              break
            case 2:
              sizesItem = `
              [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
              [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, medium], 
              [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
              break
            case 3:
              sizesItem = `
              [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
              [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
              [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
              break
            case 4:
              sizesItem = `
              [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
              [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
              [${urlStart}/media/${decodeURIComponent(value.media)}_large.${value.mediaType}, large]`
              break
          }
          if (sizesItem) {
            comp.dataset.interchange = sizesItem
          }
        }
      }

      /*let theme = window.sledge.currentTheme === 'default' ? $sledge.defaultThemeConf : window.sledge.themeConfig
      let bgcolor = theme['foundation-color']['$light-gray']*/
    }
  }
})
