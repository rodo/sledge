export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    }
  },
  {
    name: {fr: 'Image', en: 'Image'},
    key: 'image',
    type: 'image',
    applyTo: '.sg-testimonial-img1',
    onApply: (cont, imgPath) => {
      let node = cont.querySelector('.sg-testimonial-img2')
      if (imgPath.path) {
        node.src = imgPath.path
      } else {
        node.dataset.interchange = imgPath.interchange
      }
    }
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription',
    type: 'text_long',
    default: {
      color: '#000000'
    },
    applyTo: '.sg-testimonial-quot'
  },
  {
    name: {fr: 'Texte', en: 'Text'},
    key: 'textDescription2',
    type: 'text_long',
    applyTo: '.sg-testimonial-aut'
  },
  {
    name: {fr: 'Couleur principale', en: 'Main text Color'},
    key: 'gencolor',
    type: 'custom',
    data: [
      {
        kind: 'textColor',
        type: 'themeColor',
        default: {
          textColor: '#000000'
        },
        name: {fr: 'Citation', en: 'Quotation'}
      }
    ],
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    data: [
      {
        kind: 'textlink',
        type: 'text',
        name: {fr: 'Texte', en: 'Text'}
      },
      {
        kind: 'link',
        type: 'link',
        name: {fr: 'Lien', en: 'Link'}
      },
      {
        kind: 'textcolor',
        type: 'themeColor',
        name: {fr: 'Couleur', en: 'Color'}
      }
    ],
    applyTo: '.subheader'
  }
]
