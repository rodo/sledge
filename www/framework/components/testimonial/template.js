export default `
    <div class="row align-middle sg-testimonial-content">
      <div class="small-12 medium-4 large-4 column hide-for-small-only sg-profile-pic">
        <img class="sg-testimonial-img1" src="https://placeimg.com/300/300/nature">
      </div>
      <div class="small-12 medium-8 large-8 column sg-testimonial-text end">
        <p class="sg-testimonial-quot">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur feugiat velit sed lacinia lacinia.</p>
        <div class="sg-testimonial-author-container">
          <div class="small-profile-pic hide-for-medium">
            <img class="sg-testimonial-img2" src="https://placeimg.com/50/50/nature">
          </div>
          <div class="sg-testimonial-aut-info">
            <div class="sg-testimonial-aut">Lorem ipsum</div>
            <br><i class="subheader">Lorem ipsum Inc.</i>
          </div>
        </div>
      </div>
    </div>
`