import linkData from '../../src/properties/links.js'

export default (comp, prop, value) => {
  if (prop === 'custom') {
    if (value.textColor) {
      let cont = comp.querySelector('.sg-testimonial-quot')
      let rgTitle = /sg-color-scheme-(\d*)-text/
      let matchTitle = cont.className.match(rgTitle)
      if (matchTitle && matchTitle[1]) {
        cont.classList.remove(`sg-color-scheme-${matchTitle[1]}-text`)
      }
      cont.classList.add(`sg-color-scheme-${value.textColor}-text`)
    }

    if (value.textlink) {
      let link = {href: '#', dataset: ''}
      if (value.link) {
        link = linkData(value)
      }
      let linkTag = `<a href="${link.href}" ${link.dataset}>${value.textlink && value.textlink[window.$sledge.language] ? decodeURIComponent(value.textlink[window.$sledge.language]) : '...'}</a>`
      comp.innerHTML = linkTag

      if (value.textcolor) {
        let cont = comp.querySelector('a')
        cont.classList.add(`sg-color-scheme-${value.textcolor}-text`)
      }
    }
  }
}
