import {addMediaForExport} from '../../src/utilities'
import {getImagePath} from '../../src/images'

export default function (comp, prop, value, projName) {
  if (value.sounds) {
    let urlStart = ''
    if (window.sledge.isPublish) {
      urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'
    }
    const sounds = []
    value.sounds.forEach(sound => {
      let imgPath = {}
      if (sound.image) {
        addMediaForExport(sound.image)
        imgPath = getImagePath(sound.image, true)
        if (imgPath.path) {
          imgPath.path
          imgPath.interchange
        }
      }
      if (window.sledge.isPublish) {
        addMediaForExport(sound, true)
        sounds.push(
          {
            file: `${urlStart}/media/${sound.name}.mp3`,
            name: sound.desc || sound.originalName,
            interchange: imgPath.interchange || null,
            imagePath: imgPath.path || null
          }
        )
      } else {
        sounds.push(
          {
            file: `http://localhost:${window.localStorage.getItem('listennerPort')}/${projName}/media/${sound.name}.mp3`,
            name: sound.desc || sound.originalName,
            interchange: imgPath.interchange || null,
            imagePath: imgPath.path || null
          }
        )
      }
    })
    comp.querySelector('.sl-audio').dataset.sounds = JSON.stringify(sounds)
    if (window.$sledge.isBuilder) {
      let audio = comp.querySelector('.sl-audio')
      if (audio.slaudio) {
        audio.slaudio.refresh()
      }
    }
  }

  if (prop === 'custom') {

    let rg = /sg-color-scheme-(\d*)/
    let node = comp.querySelector('.sl-aa-audio-ctrl')
    let match = node.className.match(rg)
    if (match && match[1]) {
      node.classList.remove(`sg-color-scheme-${match[1]}`)
    }
    node.style.backgroundColor = ''
    if (value.color) {
      node.classList.add(`sg-color-scheme-${value.color}`)
    }


    let ctrls = comp.querySelectorAll('i')
    let rgt = /sg-color-scheme-(\d*)-text/
    ;[].forEach.call(ctrls, ctrl => {
      let matchT = ctrl.className.match(rgt)
      if (matchT && matchT[1]) {
        ctrl.classList.remove(`sg-color-scheme-${matchT[1]}-text`)
      }
      if (value.buttonColor) {
        ctrl.classList.add(`sg-color-scheme-${value.buttonColor}-text`)
      }      
    })

    let label = comp.querySelector('label')
    let matchl = label.className.match(rgt)
    if (matchl && matchl[1]) {
      label.classList.remove(`sg-color-scheme-${matchl[1]}-text`)
    }
    if (value.buttonColor) {
      label.classList.add(`sg-color-scheme-${value.buttonColor}-text`)
    }

    label.style.borderTop = '1px solid ' + label.style.color
  }
}
