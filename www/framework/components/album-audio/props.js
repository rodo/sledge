export default [
  {
    name: {fr: 'Arri&egrave;re plan', en: 'Background'},
    key: 'background',
    type: 'background',
    applyTo: 'self',
    allow: {
      image: false,
      video: false,
      color: true
    },
    default: {
      color: '8'
    }
  },
  {
    name: {fr: 'Fichier Audio', en: 'Audio File'},
    key: 'audio',
    type: 'audio',
    applyTo: 'self',
    multiple: true,
    image: true,
    title: true
  },
  {
    name: {fr: 'Espacements', en: 'Spacing'},
    key: 'spacing',
    type: 'container',
    applyTo: 'self'
  },
  {
    name: {fr: 'G&eacute;n&eacute;ral', en: 'Settings'},
    key: 'settings',
    type: 'custom',
    default: {
      hasButton: false,
      hasImgDivider: false
    },
    data: [
      {
        kind: 'color',
        type: 'themeColor',
        ref: 'sd-prop-color',
        name: {fr: 'Couleur de fond zone de contrôle', en: 'Control space background Color'}
      },
      {
        kind: 'buttonColor',
        type: 'themeColor',
        ref: 'sd-prop-color',
        name: {fr: 'Couleur des contrôles', en: 'Controls Color'}
      }
    ],
    applyTo: 'self'
  }
]
