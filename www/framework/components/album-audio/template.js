export default `<div class="sl-audio" data-sounds="[]">
<div class="sl-audio-wrapper">
  <audio controls="controls" src="framework/components/simple-audio/img/sound.mp3">
  No htlm audio support <code>audio</code>.
  </audio>
</div>
<div class="sl-aa-audio-cont">
  <div class="sl-aa-audio-img"></div>
  <div class="sl-aa-audio-ctrl">
    <div class="sl-aa-audio-buttons">
      <button class="sl-audio-prev button clear large"><i class="fi-previous"></i></button>
      <button class="sl-audio-play button clear large"><i class="fi-play"></i></button>
      <button class="sl-audio-next button clear large"><i class="fi-next"></i></button>
    </div>
    <label class="sd_audio_title">Name</label>
  </div>
</div>
</div>
`
