const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {
  entry: {
    runtime: './src/runtime'
  },
  output: {
    path: __dirname,
    filename: './dist/bundle_[name].js'
  },
  module: {
    rules: [
      {
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: './dist/img/',
              publicPath: '.',
              useRelativePath: false
            }
          },
          'image-webpack-loader'
        ]
      },
      {
        test: /\.(css)$/i,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                url: true
              }
            },
            'resolve-url-loader'
          ]
        })
      }
    ]
  },
  devtool: 'cheap-module-source-map',
  resolve: {
    extensions: ['.js'],
    modules: [
      'node_modules'
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '/dist/bundle.css',
      allChunks: true
    })
  ]
}