/* Class Menu that generate a generic responsive menu from the siteMap */

export default class Menu {
  constructor (params) {
    //
  }

  buildMap (settings = {}) {
    let root = window.$sledge.bridge.siteMapManager.getMap(window.$sledge.language)
    const remove = settings.remove || []
    let markup = '<ul class="vertical medium-horizontal menu" data-responsive-menu="drilldown medium-dropdown">'
    root.forEach(obj => {
      if (remove.indexOf(obj.id) === -1) {
        if (obj.kind === 'page') {
          markup += this.getPageItem(obj)
        } else {
          markup += this.explore(obj)
        }
      }
    })
    return markup + '</ul>'
  }

  explore (data) {
    let markup = `<li><a href="#"><span>${data.localName}</span></a><ul class="vertical menu">`
    if (data.children) {
      data.children.forEach(obj => {
        if (obj.kind === 'page') {
          markup += this.getPageItem(obj)
        } else {
          markup += this.explore(obj)
        }
      })
    } else {
      return ''
    }
    markup += `</ul></li>`
    return markup
  }

  getPageItem (data) {
    let markup = ''
    if (window.$sledge.isBuilder) {
      markup += `<li><a>${data.localName}</a></li>`
    } else {
      if (window.sledge.isPublish) {
        let pageData = $sledge.pages[data.id]
        markup += window.$sledge.bridge.projectConf.startPage === pageData._id
        ? `<li><a href="./index.html">${data.localName}</a></li>`
        : `<li><a href="./${data.name.replace(/\s/g, '_')}.html">${data.localName}</a></li>`
      } else {
        markup += `<li><a href="switchpage://${data.id}">${data.localName}</a></li>`
      }
    }
    return markup
  }
}
