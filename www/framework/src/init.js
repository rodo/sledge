import {generateUUID} from './utilities.js'
import {refreshProperty} from './properties/properties.js'

export function createComponent (tagName, conf) {
  class defaultName extends HTMLElement {
    constructor () {
      super()

      if (conf && conf.render) {
        this.render = conf.render
      }

      if (conf && conf.files) {
        if (!window.$sledge.filesAdded) {
          window.$sledge.filesAdded = []
        }

        conf.files.forEach(file => {
          if (window.$sledge.filesAdded.indexOf(file) === -1) {
            window.$sledge.filesAdded.push(file)
            if (!window.sledge.isPublish) {
              const script = document.createElement('script')
              script.type = 'text/javascript'
              script.src = 'framework/components/' + file
              if (conf.filesCallback) {
                script.onload = () => {
                  conf.filesCallback()
                }
              }
              document.head.appendChild(script)
            } 
          }
        })
      }

      if (conf && conf.receiveProp) {
        this.receiveProp = conf.receiveProp
      }

      this.applyProperty = (key, value) => {
        refreshProperty(this, this.properties[key], value)
      }
    }

    childrenReady () {
      this.refreshProps()
    }

    refreshProps () {
      if (conf.properties && conf.properties.length !== 0) {
        conf.properties.forEach(item => refreshProperty(this, item))
      }
    }

    connectedCallback () {
      tagName = tagName.toLowerCase()
      this.ref = tagName
      initComponent(this, conf)
      if (conf) {
        if (conf.cssClass) {
          this.classList.add(conf.cssClass)
        }
        if (conf.properties && conf.properties.length !== 0 && !conf.private) {
          const props = conf.properties
          props.forEach(item => addProperty(this, item))
        }
        if (conf.onInit) {
          conf.onInit(this)
        }
      }
    }

    disconnectedCallback () {
      if (conf && conf.onRemove) {
        conf.onRemove(this)
      }
    }
    }
  window.customElements.define(`sg-${tagName.toLowerCase()}`, defaultName)
  return defaultName
}

export function initComponent (comp, conf) {
  conf = conf || {}
  const ref = comp.ref
  comp.dataset.ref = ref
  comp.classList.add('sg_' + ref)
  if (!conf.container && !conf.private && !comp.dataset.inset) {
    comp.classList.add(ref === 'row' ? 'row' : 'columns')
  }
  if (comp.render) {
    comp.innerHTML = comp.render(comp)
  }
  if (!conf.private) {
    if (!comp.id) {
      comp.id = generateUUID()
      window.$sledge.bridge.initComponent(comp.id, (conf && conf.properties) ? conf.properties : {})
    } else {
      window.$sledge.bridge.initComponent()
    }
  }
}

function addProperty (comp, prop) {
  if (!comp.properties) {
    comp.properties = {}
  }
  comp.properties[prop.key] = prop
  if (!comp.propertiesApply) {
    comp.propertiesApply = {}
  }
  if (!comp.propertiesApply[prop.applyTo]) {
    comp.propertiesApply[prop.applyTo] = []
  }
  comp.propertiesApply[prop.applyTo].push(prop)
  if (prop.applyTo !== 'self') {
    let node = comp.querySelector(prop.applyTo)
    if (node) {
      node.dataset.properties = prop.applyTo
    }
  }

  refreshProperty(comp, prop)
}
