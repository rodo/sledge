export default function treatProp (node, comp, prop, value, fullvalue) {
  node.receiveProp(node, prop.type, value, fullvalue)
  window.requestAnimationFrame(() => node.receiveProp(node, 'logobackground', window.$sledge.properties[comp.id]['logo']))
}
