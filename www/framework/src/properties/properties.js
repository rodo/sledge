import propMenu from './menu.js'
import propCustom from './custom.js'
import propLogobackground from './logobackground.js'
import propMarketingicon from './marketingicon.js'
import propHeadertextblock from './headertextblock.js'
import propImage from './image.js'
import propText from './text.js'
import propBackground from './background.js'
import propContainer from './container.js'

export function refreshProperty (comp, prop, value) {
  if (!window.$sledge.properties || !window.$sledge.properties[comp.id] || !window.$sledge.properties[comp.id][prop.key]) {
    if (!window.$sledge.properties) {
      window.$sledge.properties = {}
    }
    if (!window.$sledge.properties[comp.id]) {
      window.$sledge.properties[comp.id] = {}
    }
    if (!window.$sledge.properties[comp.id][prop.key]) {
      window.$sledge.properties[comp.id][prop.key] = {}
    }
  }

  let projName = window.$sledge.currentdb
  if (!projName) {
    projName = window.localStorage.getItem('currentProject')
  }

  value = value || window.$sledge.properties[comp.id][prop.key]

  let node = comp
  if (prop.applyTo !== 'self') {
    node = comp.querySelector(prop.applyTo)
    if (!node) {
      return
    }
  }

  switch (prop.type) {
    case 'container':
      propContainer(node, value)
      break
    case 'text':
    case 'text_long':
      propText(node, comp, prop, value)
      break
    case 'menu':
      propMenu(node, comp, prop, value, window.$sledge.properties[comp.id])
      break
    case 'carousel':
    case 'embed':
    case 'social':
    case 'links':
      if (node.receiveProp) {
        node.receiveProp(node, prop.type, value)
      }
      break
    case 'custom_content':
      if (node.receiveProp) {
        node.receiveProp(node, prop.type, value, window.$sledge.properties[comp.id]['files'])
      }
      break
    case 'sitemap':
      if (node.receiveProp) {
        node.receiveProp(node, prop.type, value, window.$sledge.properties[comp.id])
        if (window.$sledge.properties[comp.id]['logo']) {
          window.requestAnimationFrame(() => node.receiveProp(node, 'logobackground', window.$sledge.properties[comp.id]['logo']))
        }
      }
      break
    case 'link':
      if (node.receiveProp) {
        node.receiveProp(node, prop.type, value, window.$sledge.properties[comp.id]['image'])
      }
      break
    case 'logobackground':
      propLogobackground(node, prop, value)
      break
    case 'custom':
      propCustom(comp, node, prop, value, window.$sledge.properties[comp.id])
      break
    case 'marketingicon':
      propMarketingicon(node, comp, prop, value)
      break
    case 'vh':
      node.style.minHeight = value.vh + 'vh'
      break
    case 'headertextblock':
      propHeadertextblock(node, prop, value)
      break
    case 'audio':
      if (prop.applyTo === 'self') {
        node.receiveProp(node, prop.type, value, projName)
      } else {
        // propAudio(node, comp, value, prop)
      }
      break
    case 'image':
      if (value.image && value.image[window.$sledge.language]) {
        if (prop.applyTo === 'self') {
          node.receiveProp(node, prop.type, value)
        } else {
          propImage(node, comp, value, prop)
        }
      } else {
        if (node.nodeName !== 'IMG') {
          node.style.backgroundImage = 'url(framework/components/marketing-section/img/chairlift.jpg)'
        } else {
          node.src = 'framework/components/marketing-section/img/chairlift.jpg'
        }
      }
      break
    case 'background':
      propBackground(node, comp, prop, value, projName)
      break
    case 'buttons':
      if (comp && comp.querySelector(prop.applyTo) && comp.querySelector(prop.applyTo).receiveProp) {
        comp.querySelector(prop.applyTo).receiveProp(value)
      }
      break
    default:
  }

  window.requestAnimationFrame(() => window.PubSub.publish('ACTION_ZONE_RESIZE'))
  window.requestAnimationFrame(() => window.PubSub.publish('PROPERTY_UPDATED'))
  // refresh
  window.timeoutRefresh = null
  /*window.addEventListener('resize', () => {
    clearTimeout(window.timeoutRefresh)
    window.timeoutRefresh = setTimeout(() => {
      
    }, 100)
  })*/
}
