import {addMediaForExport} from '../utilities'
import renderMarkup from '../../components/orbit/render'

export default function treatProp (node, comp, prop, value, projName) {
  // color
  let $node = $(node)
  let bgtype = value.backgroundtype || null
  value = value || window.$sledge.properties[comp.id][prop.key]
  let rg = /sg-color-scheme-(\d*)/
  let match = node.className.match(rg)
  if (match && match[1]) {
    node.classList.remove(`sg-color-scheme-${match[1]}`)
  }
  node.style.backgroundColor = ''
  if (value.color) {
    if (value.color.indexOf('#') === -1) {
      node.classList.add(`sg-color-scheme-${value.color}`)
    } else {
      node.style.backgroundColor = value.color
    }
  }

  $node.find('> .sg-bg-mask').remove()
  if (value.mask && value.mask !== 'original') {
    $node.append(`<div class="sg-bg-mask" style="background-color:rgba(${value.mask === 'darker' ? '0,0,0' : '255,255,255'},${value.maskRange / 10});"></div>`)
  }

  // clean
  node.classList.remove('sd-bg-media')
  node.classList.remove('sd-bg-pattern')
  node.classList.remove('sd-bg-repeat')
  let patternClass = node.className.match(/sd-back-(\d)+/)
  if (patternClass && patternClass.length !== 0) {
    node.classList.remove(patternClass[0])
  }
  $node.css('backgroundImage', '')

  if (bgtype === 'bgimage') {
    node.classList.add(value.bgimage)
  }

  if (bgtype === 'media') {
    addMediaForExport(value)
    node.classList.add('sd-bg-media')
    if (value.displayType === 'repeat') {
      node.classList.add('sd-bg-repeat')
    }
    if (value.mediaType.toLowerCase() === 'mov') {
      // node.insertAdjacentHTML('beforeend', `<video class="sd-fullscreen-bg-video" autoplay muted loop src="http://localhost:${window.listennerPort}/${projName}/media/${value.media}.${value.mediaType}"></video>`)
    } else {
      let urlStart = `${window.$sledge.conf.dbServerUrl}/${projName}`
      if (window.sledge.isPublish) {
        urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'
      }        
      if (window.$sledge.currentdb) {
        if (value.sizesItem === 4) {
          node.style.backgroundImage = `url(${urlStart}/media/${value.media}_large.${value.mediaType})`
        } else {
          node.style.backgroundImage = `url(${urlStart}/media/${value.media}.${value.mediaType})`
        }
      } else {
        let sizesItem = null
        switch (value.numberOfSizes) {
          case 1:
            node.style.backgroundImage = `url(${urlStart}/media/${value.media}.${value.mediaType})`
            break
          case 2:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
            break
          case 3:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
            break
          case 4:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_large.${value.mediaType}, large]`
            break
        }

        if (sizesItem) {
          node.dataset.interchange = sizesItem
          if (!window.sledge.isPublish) {
            window.requestAnimationFrame(() => $(document).foundation())
          }
        }
      }
    }
  }

  if (bgtype === 'pattern') {
    node.classList.add('sd-bg-pattern')
    node.classList.add(value.pattern)
  }
}

export function getBGWrapper (value = {}, projName) {
  let bgtype = value.backgroundtype || null
  let markup = document.createElement('div')
  const obj = {}
  obj.classes = []

  if (value.color) {
    if (value.color.indexOf('#') === -1) {
      markup.classList.add(`sg-color-scheme-${value.color}`)
    }
  }

  if (value.mask && value.mask !== 'original') {
    $(renderMarkup).append(`<div class="sg-bg-mask" style="background-color:rgba(${value.mask === 'darker' ? '0,0,0' : '255,255,255'},${value.maskRange / 10});"></div>`)
  }

  if (bgtype === 'bgimage') {
    markup.classList.add(value.bgimage)
  }

  if (bgtype === 'media') {
    addMediaForExport(value)
    markup.classList.add('sd-bg-media')
    if (value.displayType === 'repeat') {
      markup.classList.add('sd-bg-repeat')
    }
    if (value.mediaType.toLowerCase() === 'mov') {
      //
    } else {
      let urlStart = `${window.$sledge.conf.dbServerUrl}/${projName}`
      if (window.sledge.isPublish) {
        urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'
      }        
      if (window.$sledge.currentdb) {
        if (value.sizesItem === 4) {
          markup.style.backgroundImage = `url(${urlStart}/media/${value.media}_large.${value.mediaType})`
        } else {
          markup.style.backgroundImage = `url(${urlStart}/media/${value.media}.${value.mediaType})`
        }
      } else {
        let sizesItem = null
        switch (value.numberOfSizes) {
          case 1:
            markup.style.backgroundImage = `url(${urlStart}/media/${value.media}.${value.mediaType})`
            break
          case 2:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
            break
          case 3:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}.${value.mediaType}, large]`
            break
          case 4:
            sizesItem = `
            [${urlStart}/media/${decodeURIComponent(value.media)}_small.${value.mediaType}, small], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_medium.${value.mediaType}, medium], 
            [${urlStart}/media/${decodeURIComponent(value.media)}_large.${value.mediaType}, large]`
            break
        }

        if (sizesItem) {
          markup.dataset.interchange = sizesItem
        }
      }
    }
  }

  if (bgtype === 'pattern') {
    markup.classList.add('sd-bg-pattern')
    markup.classList.add(value.pattern)
  }
  markup.classList.add('sg_view_wrapper')
  return markup.outerHTML.replace('</div>', '')
}
