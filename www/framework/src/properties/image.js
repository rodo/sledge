import {addMediaForExport} from '../utilities.js'
import {getImagePath} from '../images.js'

export default function treatProp (node, comp, value, prop) {
  let imgObj = null
  if (value.image && value.image[window.$sledge.language]) {
    imgObj = value.image[window.$sledge.language]
  }
  if (!imgObj) {
    return
  }
  addMediaForExport(imgObj)
  let imgPath = getImagePath(imgObj, true)
  
  if (imgPath.path) {
    if (node.nodeName !== 'IMG') {
      node.style.backgroundImage = `url(${imgPath.path})`
    } else {
      node.src = imgPath.path
    }
  } else {
    node.dataset.interchange = imgPath.interchange
  }
  if (prop.onApply) {
    prop.onApply(comp, imgPath)
  }

  if (node.nodeName === 'IMG') {
    const clientRect = node.getClientRects()[0]
    if (!clientRect) return
    const hw = clientRect.height > clientRect.width ? 'height' : 'width'
    if (value.size === 801 || value.size === '801') {
      node.style.maxWidth = null
      node.style.maxHeight = null
    } else {
      if (hw === 'width') {
        node.style.maxWidth = value.size + 'px'
        node.style.height = 'auto'
        node.style.maxHeight = null
      } else {
        node.style.maxWidth = null
        node.style.width = 'auto'
        node.style.maxHeight = value.size + 'px'
      }
    }
  }
}
