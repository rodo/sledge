import {addMediaForExport} from '../utilities.js'

export default function treatProp (node, prop, value) {
  addMediaForExport(value)
  if (node.receiveProp) {
    node.receiveProp(node, prop.type, value)
  }
}
