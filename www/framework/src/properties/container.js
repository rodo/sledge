export default function treatProp (node, value) {
  node.classList.remove('sg-padding-large')
  node.classList.remove('sg-padding-medium')
  node.classList.remove('sg-marginTop-large')
  node.classList.remove('sg-marginTop-medium')
  node.classList.remove('sg-marginBottom-large')
  node.classList.remove('sg-marginBottom-medium')

  if (value.spacing === 'large' || value.spacing === 'medium') {
    node.classList.add(`sg-padding-${value.spacing}`)
  }

  if (value.marginTop === 'large' || value.marginTop === 'medium') {
    node.classList.add(`sg-marginTop-${value.marginTop}`)
  }

  if (value.marginBottom === 'large' || value.marginBottom === 'medium') {
    node.classList.add(`sg-marginBottom-${value.marginBottom}`)
  }
}

