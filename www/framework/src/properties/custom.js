export default function propCustom (comp, node, prop, value, fullVal) {
  if (node.receiveProp) {
    node.receiveProp(node, prop.type, value, fullVal)
  } else if (comp.receiveProp) {
    comp.receiveProp(node, prop.type, value, fullVal)
  }
}
