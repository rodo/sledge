export default function treatProp (node, prop, value) {
  value.rounded ? node.classList.add('sg-rounded') : node.classList.remove('sg-rounded')
  node.classList.remove('sg-transparent-light')
  node.classList.remove('sg-transparent-dark')
  node.classList.remove('sg-textshadow')
  switch (value.background) {
    case 'transDark':
      node.classList.add('sg-transparent-dark')
      break
    case 'transLight':
      node.classList.add('sg-transparent-light')
      break
    case 'transText':
      node.classList.add('sg-textshadow')
      break
    default:
  }
  node.classList.remove('sg-position-left')
  node.classList.remove('sg-position-right')
  node.classList.remove('sg-position-center')
  switch (value.position) {
    case 'left':
      node.classList.add('sg-position-left')
      break
    case 'right':
      // node.classList.add('sg-position-right')
      break
    case 'center':
      node.classList.add('sg-position-center')
      break
    default:
  }
}
