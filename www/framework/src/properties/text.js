export default function treatProp (node, comp, prop, value) {
  if (window.$sledge.properties && window.$sledge.properties[comp.id] && window.$sledge.properties[comp.id][prop.key]) {
    let hSize = window.$sledge.properties[comp.id][prop.key]['hSize']
    let currentName = node.nodeName.toLowerCase()
    if (hSize && currentName !== 'h' + hSize) {
      let markup = node.outerHTML
      markup = markup.replace('<' + currentName, '<h' + hSize)
      markup = markup.replace(currentName + '>', 'h' + hSize + '>')
      let div = document.createElement('div')
      div.innerHTML = markup
      let newNode = div.firstChild
      let cl = node.className
      node.parentNode.replaceChild(newNode, node)
      node = newNode
      node.className = cl
    }

    let col = window.$sledge.properties[comp.id][prop.key]['column']
    if (col && col !== '1') {
      node.classList.add('sg_text_columns')
      node.style.columnCount = parseInt(col, 10)
    } else {
      if (node.classList.contains('sg_text_columns')) {
        node.classList.remove('sg_text_columns')
        node.style.columnCount = 1
      }
    }

    let align = window.$sledge.properties[comp.id][prop.key]['align']
    node.classList.remove('sg-txt-align-right')
    node.classList.remove('sg-txt-align-center')
    node.classList.remove('sg-txt-align-justify')
    if (align && align !== 'left') {
      node.classList.add(`sg-txt-align-${align}`)
    }

    let padding = window.$sledge.properties[comp.id][prop.key]['padding']
    node.classList.remove('sg-padding-medium')
    node.classList.remove('sg-padding-large')
    if (padding && padding !== 'small') {
      node.classList.add(`sg-padding-${padding}`)
    }

    let scheme = window.$sledge.properties[comp.id][prop.key]['colorScheme']
    let rg = /sg-color-scheme-(\d*)-text/
    let match = node.className.match(rg)
    if (match && match[1]) {
      node.classList.remove(`sg-color-scheme-${match[1]}-text`)
    }
    if (scheme) {
      node.classList.add(`sg-color-scheme-${scheme}-text`)
    }
  }

  if (value && value.text && value.text[window.$sledge.language]) {
    node.innerHTML = decodeURIComponent(value.text[window.$sledge.language])
  } else if (window.$sledge.properties && window.$sledge.properties[comp.id] && window.$sledge.properties[comp.id][prop.key] && window.$sledge.properties[comp.id][prop.key]['text'] && window.$sledge.properties[comp.id][prop.key]['text'][window.$sledge.language]) {
    node.innerHTML = decodeURIComponent(window.$sledge.properties[comp.id][prop.key]['text'][window.$sledge.language])
  } else {
    if (window.sledge && window.sledge.nslocal) {
      if (value && value.text) {
        node.innerHTML = window.sledge.nslocal.empty_txt
      }
    }
  }
}
