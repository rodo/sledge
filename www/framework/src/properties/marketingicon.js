export default function treatProp (node, comp, prop, value) {
  if (comp.receiveProp) {
    comp.receiveProp(node, prop.type, value)
  }
}
