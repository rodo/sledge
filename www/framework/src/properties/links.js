import {guidGenerator} from '../utilities.js'

export default (data) => {
  let dataset = ''
  let href = '#'
  let action = data.linkType || data.action
  switch (action) {
    case 'phone':
      href = `tel:${data.phone}`
      break
    case 'email':
      href = `mailto:${decodeURIComponent(data.email)}`
      break
    case 'external':
      href = `${decodeURIComponent(data.external)}`
      break
    case 'internal':
      if (window.sledge.isPublish) {
        let pageData = window.$sledge.pages[data.page]
        href = pageData._id === window.$sledge.bridge.projectConf.startPage ? `./index.html` : `./${pageData.name.replace(/\s/g, '_')}.html`
      } else {
        href = `switchpage://${data.page}`
      }      
      break
    case 'view':
      href = '#'
      let idm = guidGenerator()
      dataset = ` data-view="${data.view}" data-effect="${data.effect || 'modal'}" data-toggle="${idm}" `
      if (!window.$sledge.isBuilder) {
        window.$sledge.bridge.addHiddenView(data.view, idm)
      }
      break
    default:
      href = '#'
  }
  return {dataset, href}
}
