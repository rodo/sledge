import {getColumnSizeFromString, getColumnOffsetFromString} from './utilities'

// global
window.$sledge.equalizerRefresh = viewRef => {
  if ($('[data-equalizer]').length === 0) {
    return
  }
  if (viewRef) {
    equalizer(viewRef)
  } else {
    let views = document.body.querySelectorAll('.sg_view')
    ;[].forEach.call(views, view => {
      equalizer(view)
    })
  }
  window.Foundation.reInit('equalizer')
}

function equalizer (view) {
  let rows = view.querySelectorAll('.sg_row')
  ;[].forEach.call(rows, (row) => {
    equalizerRow(row)
  })
}
window.$sledge.equalizer = equalizer

// global
window.$sledge.equalizerSingleRow = row => {
  let reset = true
  if (row.dataset.equalizer === undefined) {
    reset = false
  }
  equalizerRow(row)
  if (reset) {
    window.requestAnimationFrame(a => window.Foundation.reInit('equalizer'))
  } else {
    $(row).foundation()
  }
}

function equalizerRow (row) {
  let cols = row.querySelectorAll('.columns')
  const $row = $(row)
  $row.find('.columns').height('auto')
  let toAdd = []
  let toRm = []
  let fullSize = 0
  ;[].forEach.call(cols, (col) => {
    let size = getColumnSizeFromString(col.className)
    let offsetSize = getColumnOffsetFromString(col.className)
    let full = size + offsetSize
    fullSize += full
    if (full < 12 && fullSize <= 12) {
      toAdd.push(col)
    } else {
      toRm.push(col)
    }
  })
  if (toAdd.length > 1) {
    row.setAttribute('data-equalizer', '')
    toAdd.forEach(elem => {
      elem.setAttribute('data-equalizer-watch', '')
    })
  }
  if (toRm.length !== 0) {
    toRm.forEach(elem => {
      $(elem).removeAttr('data-equalizer-watch')
    })
  }
}

// resize
let timeout = false
let delay = 250
window.addEventListener('resize', () => {
  clearTimeout(timeout)
  timeout = setTimeout(() => window.$sledge.equalizerRefresh(), delay)
})
