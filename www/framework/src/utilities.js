export function guidGenerator () {
  const date = new Date()
  const S4 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  }
  return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4()) + date.getDate() + date.getDay() + date.getFullYear() + date.getHours() + date.getMinutes() + date.getMilliseconds()
}

export function generateUUID () {
  let d = new Date().getTime()
  if (window.performance && typeof window.performance.now === 'function') {
    d += window.performance.now()
  }
  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
  })
  return uuid
}

export function getColumnOffsetFromString (classes) {
  var reg = new RegExp(`(.*?${window.$sledge.responsiveState}-offset-.*?)([0-9]{1,2})(.*$)`, 'g')
  return parseInt(classes.replace(reg, '$2'), 10)
}

export function getColumnSizeFromString (classes) {
  classes = classes.replace(`${window.$sledge.responsiveState}-offset-`, '')
  var reg = new RegExp(`(.*?${window.$sledge.responsiveState}-.*?)([0-9]{1,2})(.*$)`, 'g')
  return parseInt(classes.replace(reg, '$2'), 10)
}

export function addMediaForExport (media) {
  if (window.sledge.isPublish) {
    if (!window.sledge.media) {
      window.sledge.media = []
      window.sledge.mediaAdded = {}
    }
    if (media.media && !window.sledge.mediaAdded[media.media]) {
      window.sledge.mediaAdded[media.media] = true
      window.sledge.media.push(media)
    } else if (media.originalName && !window.sledge.mediaAdded[media.originalName]) {
      window.sledge.mediaAdded[media.originalName] = true
      window.sledge.media.push(media)
    }
  }
}

export function setColorFromCode (code, set) {
  let div = document.createElement('div')
  div.className = 'sg-color-scheme-' + code
  document.body.appendChild(div)
  window.requestAnimationFrame(e => {
    set(window.getComputedStyle(div).backgroundColor)
    document.body.removeChild(div)
  })
}
