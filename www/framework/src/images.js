import {addMediaForExport} from './utilities.js'

export function getImagePath (image, useMediaQueries) {
  addMediaForExport(image)
  
  let urlStart = `${window.$sledge.conf.dbServerUrl}/${window.localStorage.getItem('currentProject')}`
  if (window.sledge.isPublish) {
    urlStart = window.$sledge.bridge.languages.list.length === 1 ? '.' : '..'
  }
  let style = null
  let path = null
  let sizesItem = null
  let size = window.$sledge.responsiveState

  switch (image.numberOfSizes) {
    case 1:
      path = `${urlStart}/media/${image.media}.${image.mediaType}`
      style = `background-image:url(${path});`
      break
    case 2:
      if (window.$sledge.isBuilder) {
        if (useMediaQueries) {
          if (size === 'small') {
            path = `${urlStart}/media/${image.media}_small.${image.mediaType}`
          } else {
            path = `${urlStart}/media/${image.media}.${image.mediaType}`
          }
        } else {
          path = `${urlStart}/media/${image.media}.${image.mediaType}`
        }
        style = `background-image:url(${path});`
      } else {
        sizesItem = `
        [${urlStart}/media/${decodeURIComponent(image.media)}_small.${image.mediaType}, small], 
        [${urlStart}/media/${decodeURIComponent(image.media)}.${image.mediaType}, medium], 
        [${urlStart}/media/${decodeURIComponent(image.media)}.${image.mediaType}, large]`
      }
      break
    case 3:
      if (window.$sledge.isBuilder) {
        if (useMediaQueries) {
          if (size === 'small' || size === 'medium') {
            path = `${urlStart}/media/${image.media}_${size}.${image.mediaType}`
          } else {
            path = `${urlStart}/media/${image.media}_medium.${image.mediaType}`
          }
        } else {
          path = `${urlStart}/media/${image.media}_medium.${image.mediaType}`
        }
        style = `background-image:url(${path});`
      } else {
        sizesItem = `
        [${urlStart}/media/${decodeURIComponent(image.media)}_small.${image.mediaType}, small], 
        [${urlStart}/media/${decodeURIComponent(image.media)}_medium.${image.mediaType}, medium], 
        [${urlStart}/media/${decodeURIComponent(image.media)}.${image.mediaType}, large]`
      }
      break
    case 4:
      if (window.$sledge.isBuilder) {
        if (useMediaQueries) {
          path = `${urlStart}/media/${image.media}_${size}.${image.mediaType}`
        } else {
          path = `${urlStart}/media/${image.media}_large.${image.mediaType}`
        }
        style = `background-image:url(${path});`
      } else {
        sizesItem = `
        [${urlStart}/media/${decodeURIComponent(image.media)}_small.${image.mediaType}, small], 
        [${urlStart}/media/${decodeURIComponent(image.media)}_medium.${image.mediaType}, medium], 
        [${urlStart}/media/${decodeURIComponent(image.media)}_large.${image.mediaType}, large]`
        break
      }
  }

  return {
    name: image.media + '.' + image.mediaType,
    largePath: `${urlStart}/media/${image.media}.${image.mediaType}`,
    numberOfSizes: image.numberOfSizes,
    style: style,
    path: path,
    interchange: sizesItem
  }
}
