// *** lib
import equalize from './equalize.js'

// *** components
import config from './config.js'
import viewComponent from '../components/view/view.js'
import rowComponent from '../components/row/row.js'
import titleComponent from '../components/title/title.js'
import headerComponent from '../components/header/main.js'
import simpleHeaderComponent from '../components/simple-header/main.js'
import polariodHeaderComponent from '../components/header-polaroid/main.js'
import spaceComponent from '../components/space/space.js'
import buttonSetComponent from '../components/buttonset/main.js'
import featureComponent from '../components/feature/main.js'
import textComponent from '../components/text/main.js'
import linkDashboardComponent from '../components/link-dashboard/main.js'
import thumbnailComponent from '../components/thumbnail/main.js'
import mediaobjectComponent from '../components/mediaobject/main.js'
import cardComponent from '../components/card/main.js'
import simplecardComponent from '../components/simple-card/main.js'
import topcardComponent from '../components/top-card/main.js'
import fullcardComponent from '../components/full-card/main.js'
import videolinkComponent from '../components/video-link/main.js'
import footerSitemapComponent from '../components/footer-sitemap/main.js'
import simplemapComponent from '../components/footer-simplemap/main.js'
import socialComponent from '../components/footer-social/main.js'
import imageComponent from '../components/image/main.js'
import imageFullComponent from '../components/image-full/main.js'
import contactComponent from '../components/contact/main.js'
import mapComponent from '../components/map/main.js'
import mapTitleComponent from '../components/map-title/main.js'
import linksListComponent from '../components/links-list/main.js'
import testimonialComponent from '../components/testimonial/main.js'
import testimonialFullComponent from '../components/testimonial-full/main.js'
import titleTextComponent from '../components/title-text/main.js'
import marketingSectionComponent from '../components/marketing-section/main.js'
import fmarketingSectionComponent from '../components/full-marketing-section/main.js'
import buttonsComponent from '../components/buttons/main.js'
import calloutComponent from '../components/callout/main.js'
import accordionMenuComponent from '../components/accordion-menu/main.js'
import simpleAudioComponent from '../components/simple-audio/main.js'
import defaultAudioComponent from '../components/default-audio/main.js'
import albumAudioComponent from '../components/album-audio/main.js'
import albumAudioListComponent from '../components/album-audio-list/main.js'
import circleHeaderComponent from '../components/circle-header/main.js'
import progressComponent from '../components/progress/main.js'
import imageLinkComponent from '../components/image-link/main.js'
import imagesPatchworkComponent from '../components/images-patchwork/main.js'
import customComponent from '../components/custom/main.js'

// orbit
import orbitComponent from '../components/orbit/main.js'
import orbitUtilities from '../components/orbit/orbit.js'

window.$sledge.foundationReInit = function () {
  window.$sledge.equalizerRefresh()
  window.$sledge.refreshOrbit(true)
  if (window.$sledge.refresh && window.$sledge.refresh['audioComponent']) {
    window.$sledge.refresh['audioComponent']()
  }
}

// *** fix interchnage that does not work at start
$(document).ready(() => {
  if (!window.$sledge.isBuilder) {
    window.requestAnimationFrame(() => $(window).trigger('resize'))
  }
})
