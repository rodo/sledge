/* Class Menu that generate a generic responsive menu from the siteMap */

export default class Menu {
  constructor (params) {
    //
  }

  buildMap (onlyPage, settings = {}) {
    const bridge = window.$sledge.bridge
    const root = bridge.siteMapManager.getMap(window.$sledge.language)
    let markup = ''
    const remove = settings.remove || []
    if (onlyPage) {
      markup = `<ul class="menu">`
      root.forEach(item => {
        if (item.kind === 'page') {
          if (remove.indexOf(item.id) === -1) {
            markup += this.getPageItem(item)
          }
        }
      })
      return markup + '</ul>'
    } else {
      markup = '<div class="sg-map-main">'
      let hasPages = false
      let siteName = window.localStorage.getItem('currentProject')
      const fSiteName = siteName.charAt(0).toUpperCase() + siteName.slice(1)
      const localSiteName = bridge.projectConf.siteName
      let pageMarkup =
      `<div class="sg-map-sub"><ul class="menu vertical">
      <li class="menu-text">${localSiteName || fSiteName}</li>`

      root.forEach(item => {
        if (item.kind === 'page') {
          hasPages = true
          if (remove.indexOf(item.id) === -1) {
            pageMarkup += this.getPageItem(item)
          }
        }
      })
      pageMarkup += '</ul></div>'
      if (hasPages) {
        markup += pageMarkup
      }
      root.forEach(obj => {
        if (obj.kind !== 'page') {
          if (remove.indexOf(obj.id) === -1) {
            markup += this.explore(obj)
          }
        }
      })
      return markup + '</div>'
    }
  }

  explore (data, nested) {
    let markup = ''
    if (nested) {
      markup = `<ul class="nested menu vertical">`
    } else {
      markup = `<div class="sg-map-sub">
      <ul class="menu vertical">`
    }
    markup += `<li class="menu-text">${data.localName}</li>`

    if (data.children) {
      data.children.forEach(obj => {
        if (obj.kind === 'page') {
          markup += this.getPageItem(obj)
        } else {
          markup += this.explore(obj, true)
        }
      })
    } else {
      return ''
    }
    markup += nested ? '</ul>' : '</ul></div>'
    return markup
  }

  getPageItem (data) {
    let markup = ''
    if (window.$sledge.isBuilder) {
      markup += `<li><a>${data.localName}</a></li>`
    } else {
      if (window.sledge.isPublish) {
        let pageData = window.$sledge.pages[data.id]
        markup +=
        window.$sledge.bridge.projectConf.startPage === pageData._id
        ? `<li><a href="./index.html">${data.localName}</a></li>`
        : `<li><a href="./${data.name.replace(/\s/g, '_')}.html">${data.localName}</a></li>`
      } else {
        markup += `<li><a href="switchpage://${data.id}">${data.localName}</a></li>`
      }
    }
    return markup
  }
}
