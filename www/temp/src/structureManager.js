define(function () {
    "use strict";
 	
    function Structure() {
        this.views= {};
        this.init();
    }
    
    Structure.create = function (data) {
        var result = new Structure(data);
        return result;
    };
 	Structure.prototype.constructor = Structure;

    Structure.prototype.init = function () {
    	
        var that = this;	

    	/*if (!_configObject) {
    		console.warn("no _configObject !");
    		return;
    	}*/

    	_configObject.views.forEach(function(elem, index, array) {
			that.views[elem.id] = elem;
 		});

        _b.addApi("getView", function(a){return that.getView(a);}, "struct");
    }

    /** views **/
    Structure.prototype.getView = function (id) {
    	return this.views[id];
    }
 
    Structure.prototype.updateView = function (id) {
    	
    }

    Structure.prototype.addView = function (id) {
    	
    }
	/****/

    return Structure;
});