define(["elementsList", "Utilities"], function (Layout, Utilities) {
    "use strict";
 	
    function elementsList() {
       this.init();
    }
    
    elementsList.create = function () {
        var result = new elementsList();
        return result;
    };
 	elementsList.prototype.constructor = elementsList;

    elementsList.prototype.init = function () {
        
        _b.subscribeTo("onDragElement", function(msg, data){
            this.onElementMove = data;
        });

        this.$ul = $(".b-elementsList");
        this.render();
    };

    elementsList.prototype.render = function () {
    	
        var that = this;
        that.onMove = false;
        
        //handlebar !
        _componentsList.forEach(function(element) {
            _b.componentHash[element.name] = element; 
            console.log(element.name, "cocou")
            that.$ul.append("<li data-type='"+element.name+"' data-tag='"+element.tag+"'>"+element.name+"</li>")
        }, this);

        this.$ul.find("li").draggable({
            cursor: "move",
            refreshPositions: true,
            cursorAt: { top: -5, left: -5 },
            appendTo: "body",
            helper: function( event ) {
                return $( "<div class='b-dragHelper' data-type='"+this.dataset.type+"'>"+this.dataset.type+"</div>" );
            },
            start: function(event, a) {
                PubSub.publish('elementDragStart', {name: this.dataset.type, tag:this.dataset.tag});
                PubSub.publish('onDragElement', true);
            },
            stop: function() {
                PubSub.publish('elementDragStop', true);
                PubSub.publish('onDragElement', false);    
            }
        });
    };

    return elementsList;
});