define(['structureManager', 'viewManager', 'mediaQueries', 'elementsList', 'conf', 'keyboardEvents'], function (Struc, View, Media, Elements, conf, keyboardEvents) {
    "use strict";

    function Main() {
        this.$body = $("#"+conf.bodyid);
        this.init();
    }
    
    Main.create = function () {
        var result = new Main();
        return result;
    };
 	Main.prototype.constructor = Main;

    Main.prototype.componentHash = {};

    Main.prototype.init = function () {
        
        var that = this;    

        setTimeout(function(){
            
            keyboardEvents.init();
            
            var media           = Media.create(),
                elementsList    = Elements.create(),
                stucture        = Struc.create(),
                startView       = View.create("start");

            $(".b-footerRange").change(function() {
                that.$body.css("-webkit-transform", "scale("+this.value+")");
            });

        },0);
    };

    Main.prototype.addApi = function (methodeName, method, namespace) {
        
        if (namespace) {
            if (!this[namespace])
                this[namespace] = {};
            
            this[namespace][methodeName] = method;
        } else {
            this[methodeName] = method;    
        }
        
    };

    Main.prototype.subscribeTo = function (name, method) {
        PubSub.subscribe( name, function(a, b){method.call(_b, a, b);});
    };

    return Main;
});