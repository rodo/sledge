define(["Utilities", "propertiesList"],function (Utilities, propManager) {
    "use strict";
 	
    function Layout(conf, parentView) {
       this.parentView = parentView;
       this.init(conf);
    }
    
    Layout.create = function (conf, parentView) {
        var result = new Layout(conf, parentView);
        return result;
    };
 	Layout.prototype.constructor = Layout;

    Layout.prototype.init = function (conf) {
    	
    	var that = this;

    	this.altKey = false;

		/*conf.forEach(function(rowConf) {
		    that.renderRow(rowConf);
		});*/
		_b.$body.html(conf.html);

		PubSub.subscribe( "elementDragStop", function(a, b) {
			that.elementdragStop();
		});

		PubSub.subscribe( "elementDragStart", function(a, b) {
			that.elementdragStart(b);
		});

		PubSub.subscribe( "altKey", function(a, b) {
			that.altKey = b;
		});
    };

	Layout.prototype.getColumnSizeFromString = function(classes) {
		classes = classes.replace("large-offset-", "");
		var reg = new RegExp("(.*?large-.*?)([0-9]{1,2})(.*$)","g");
		return parseInt(classes.replace(reg, '$2'),10);
	}

	Layout.prototype.getColumnOffsetFromString = function(classes) {
		var reg = new RegExp("(.*?large-offset-.*?)([0-9]{1,2})(.*$)","g");
		return parseInt(classes.replace(reg, '$2'),10);
	};

	Layout.prototype.elementdragStart = function(data) {
		
		this.onDragData = data;

		_b.$body.addClass("b-onDrag");

		//row
		_b.$body.append("<div class='b-insertRow'></div>");
        $("<div class='b-insertRow'></div>").insertBefore($(".row"));

        //col
        var rows = $(".row"),
        	that = this,
        	size = 0;

		$.each(rows, function( index, value ) {
			size = that.isRowFull($(value));
			if (size < 12) {
				that.completeForDrop($(value), size);
			}
		});
	        
	}

	Layout.prototype.elementDragRowUpdate = function($row) {

        var that = this,
        	size = 0;

		$.each($row, function( index, value ) {
			size = that.isRowFull($(value));
			if (size < 12) {
				that.completeForDrop($(value), size);
			}
		});
	        
	}

	Layout.prototype.completeForDrop = function($row, currentsize) {
        
        var fillSize = 12 - currentsize,
        	cols = $row.find(".columns:not(.b-hiddenForDrag)"),
        	offset,
        	that = this;

        $.each(cols, function( index, value ) {
			offset = that.getColumnOffsetFromString(value.className);
			if (!isNaN(offset)) {
				$("<div style='height:"+$row.height()+"px;' class='columns b-insertCol large-"+offset+"'></div>").insertBefore(value);
				$(value).removeClass("large-offset-"+offset).addClass("b-tempOffset");
				value.dataset.offset = offset;
				fillSize = fillSize - offset;
			}
		});

		if (fillSize !== 0) {
			$row.append("<div style='height:"+$row.height()+"px;' class='columns b-insertCol large-"+fillSize+"'></div>");
		}
	}

	Layout.prototype.isRowFull = function($row, withOffset) {
        
        var cols = $row.find(".columns:not(.b-insertCol):not(.b-hiddenForDrag)"),
        	that = this,
        	size = 0,
        	off;

		$.each(cols, function( index, value ) {
			size += that.getColumnSizeFromString(value.className);
			if (withOffset) {
				off = that.getColumnOffsetFromString(value.className);
				if (!isNaN(off))
					size += that.getColumnOffsetFromString(value.className);
			}
		});

		return size;
	}

	Layout.prototype.resetEnd = function($row) {
        $row
        .find(".end")
        .removeClass("end");
        
        var cols = $row.find(".columns");
        cols.last().addClass("end");
	}

	Layout.prototype.elementdragStop = function() {

		_b.$body.removeClass("b-onDrag");

		var ready = $(".b-ready"),
            dragZone = $(".b-dragZone"),
			tag = this.onDragData.tag;

        if (ready.length !== 0) {
			
			dragZone.remove();
            $(".b-ready.b-insertRow")
                .removeClass("b-insertRow")
                .removeClass("b-ready")
                .addClass("row")
                .attr("id", Utilities.guidGenerator())
                .append("<"+tag+" data-name='"+this.onDragData.name+"' id='"+Utilities.guidGenerator()+"' class='columns large-12 medium-12 small-12'></"+tag+">");

            var insertCol = $(".b-ready.b-insertCol");
            if (insertCol.length !== 0) {
	            if (insertCol.next().hasClass("b-tempOffset")) {
	            	insertCol.next().removeClass("b-tempOffset").attr("data-offset", null);
	            }    

	            insertCol
	                .removeClass("b-insertCol")
	                .removeClass("b-ready")
	                .prop( "style", null );

				var cl = insertCol.get()[0].className;
	            
	            insertCol.replaceWith("<"+tag+" data-name='"+this.onDragData.name+"' id='"+Utilities.guidGenerator()+"' class='"+cl+"'></"+tag+">");
	            this.resetEnd(insertCol.parent());	
            }
        
        } else if (dragZone.length !== 0) {

        	var activePart = dragZone.find(".b-active");

        	if (activePart.hasClass("b-horizontal")) {
		   		if (activePart.hasClass("right")) {
                    that.splitHorizontaly(this.parentView.lastOveredTarget, false);
                } else {
                    that.splitHorizontaly(this.parentView.lastOveredTarget, true);
                }
        	} else {
	        	if (activePart.hasClass("b-right")) {
	                this.splitVerticaly(this.parentView.lastOveredTarget, false);
	            } else {
	                this.splitVerticaly(this.parentView.lastOveredTarget, true);
	            }  	
        	}            
            
            dragZone.remove();
        }

        $(".b-insertRow").remove();
        $(".b-insertCol").remove();

        var offsetToAdd = $(".b-tempOffset");
		$.each(offsetToAdd, function( index, value ) {
			$(value).addClass("large-offset-"+value.dataset.offset).removeClass("b-tempOffset");
			delete value.dataset.offset;
		});
	}

    Layout.prototype.splitHorizontaly = function (overed, insertbefore) {

    };

    Layout.prototype.splitVerticaly = function (overed, insertbefore) {

    	var size 	= this.getColumnSizeFromString(overed.attr("class")),
			h 		= overed.height(),
			size1, 
			size2,
			that 	= this,
			newCol;

		if (size%2 === 1) {
			size1 = (size + 1) / 2;
			size2 = size1 - 1;
		} else {
			size1 = size2 = size/2;
		}

		overed.removeClass("large-"+size).addClass("large-"+size1);

		var tag = this.onDragData.tag;

		if (insertbefore) {
			newCol = $("<"+tag+" data-name='"+this.onDragData.name+"' id='"+Utilities.guidGenerator()+"' class='columns large-"+size2+"'></"+tag+">").insertBefore(overed);
		} else {
			newCol = $("<"+tag+" data-name='"+this.onDragData.name+"' id='"+Utilities.guidGenerator()+"' class='columns large-"+size2+"'></"+tag+">").insertAfter(overed);
		}
		
		//newCol.height(h);

		setTimeout(function(){
			that.resetEnd(overed.parent());	
		},0);
		
    };

    Layout.prototype.isOverColumn = function (overed, $target) {
    	
		var lastWidth 	= null,
            goingup 	= false,
            currentUnit = $(".row").width() / 12, //_b.$body.width()
            size 		= this.getColumnSizeFromString(overed.attr("class")),
        	offset 		= this.getColumnOffsetFromString(overed.attr("class")),
        	posElem 	= $target.get()[0].getBoundingClientRect(),
        	that		= this,
        	lastOff,
        	actionZone;

        $(document.body).append("<div class='b-actionZone'></div>");
        actionZone = $(".b-actionZone");
        
		/*actionZone.on("click", function(){
			overed.addClass("bn-selected");
			_b.$body.addClass("on-select");
			actionZone.remove();
			if (!_b.propManager) {
				_b.propManager = propManager.create(overed);
			}
		});*/
		
        actionZone.draggable({
            cursor: "move",
            refreshPositions: true,
            cursorAt: { top: -5, left: -5 },
            appendTo: "body",
            start: function(event, a) {
				
				//keep dataset and markup ! 
				PubSub.publish('elementDragStart', {name:overed[0].dataset.name, tag:overed[0].nodeName});
                PubSub.publish('onDragElement', true);
                
                requestAnimationFrame(function(){
                	overed.addClass("b-hiddenForDrag");	
                	that.elementDragRowUpdate(overed.parent());
                })
                
            },
            stop: function() {
            	$(".b-hiddenForDrag").remove();
            	//créer nouveau à partir des données de l'ancien $(".b-hiddenForDrag")
                PubSub.publish('elementDragStop', true);
                PubSub.publish('onDragElement', false);    
            }
        });

        actionZone.resizable({
            handles: "e, s, w",
            grid: [1,20],
            start: function() {
                that.onLayoutResize = true;
            },
            stop: function() {
                posElem = $target.get()[0].getBoundingClientRect();
                actionZone.css("top", posElem.top);
                actionZone.css("left", posElem.left);
                actionZone.css("width", posElem.width);
                that.onLayoutResize = false;
                $(".large-offset-0").removeClass(".large-offset-0");
            },
            resize: function( event, ui ) {
                newClass = null;
                newSize = null;

                if (overed.height() !== ui.size.height)
					overed.css("height", ui.size.height+"px");

                if (lastWidth === null)
                	lastWidth =	ui.size.width;

                goingup = (lastWidth < ui.size.width) ? true : false;   
                lastWidth = ui.size.width;

                var diff = ui.originalSize.width - ui.size.width,
                	ratio = parseInt(diff / currentUnit),
                	newClass = null,
                	newSize = null,
                	isOne 	= false,
                	is12 	= false,
                	theDiff;

				newSize = parseInt(ui.size.width / currentUnit);
				theDiff = parseFloat(ui.size.width / currentUnit) - newSize

				if (that.getColumnSizeFromString(overed.attr("class")) === 1) {
					isOne = true;
				}

				if (that.getColumnSizeFromString(overed.attr("class")) === 12) {
					is12 = true;
				}

                if (goingup) {

                	if (is12) {
						ui.element.addClass("b-stop");
                	} else {
                		ui.element.removeClass("b-stop");
                		newClass = "large-"+ newSize;	
                	}

                } else {

                	if (isOne) {
                		ui.element.addClass("b-stop");               		
                	} else {
                		ui.element.removeClass("b-stop");
	    				if (0.3 < theDiff && theDiff < 0.5) {
							newClass = "large-"+ newSize;
						}            		
                	}
                	
                }

               	if (newClass === null) return;
                if (overed.hasClass(newClass)) return;

                if (ui.originalPosition.left !== ui.position.left) {

                    if (newClass) {

                        overed.removeClass(function (index, css) {
                            return (css.match (/(^|\s)large-[0-9]+/g) || []).join(' ');
                        }).addClass(newClass);

                        var newOffset;
                        
                        if (isNaN(offset) && that.altKey) {
							var prev = overed.prev(),
								newPrevSize;

							if (prev.length !== 0) {
								var prevSize = that.getColumnSizeFromString(prev.attr("class"));
								if (goingup) {
									newPrevSize = prevSize - 1;
								} else {
									newPrevSize = prevSize + 1;
								}	
								prev.removeClass(function (index, css) {
		                            return (css.match (/(^|\s)large-\S+/g) || []).join(' ');
		                        }).addClass("large-"+newPrevSize);   
							}
                        } else {

	                        if (!isNaN(offset)) {
	                            newOffset = offset + (size - newSize);
	                        } else {
	                            newOffset = size - newSize;
	                        }

	                        overed.removeClass(function (index, css) {
	                            return (css.match (/(^|\s)large-offset-\S+/g) || []).join(' ');
	                        }).addClass("large-offset-"+newOffset);                            	
                        }

                    }

                } else {

	                if (newSize === that.getColumnSizeFromString(overed.attr("class"))) {
	                	return;
	                }

                    if (newClass) {

                    	var nextO = overed.next();
                    	
                    	if (nextO.length !== 0) {

	                    	var nextoffset = that.getColumnOffsetFromString(nextO.attr("class"));
	                    	var nextSize = that.getColumnSizeFromString(nextO.attr("class"));

	                    	if (!isNaN(nextoffset) && nextoffset !== 0) {
		                    	
		                    	if (goingup) {
		                    		nextoffset = nextoffset - 1;
		                    	} else {
									nextoffset = nextoffset + 1;
		                    	}

		                    	var newOffsetClass = "large-offset-"+nextoffset ;
		                    	
		                    	nextO.removeClass(function (index, css) {
		                            return (css.match (/(^|\s)large-offset-[0-9]+/g) || []).join(' ');
		                        }).addClass(newOffsetClass);  	
	                    	
	                    	} else if (!isNaN(nextSize)) {

	                    		if (that.altKey) {

		                    		if (goingup) {
			                    		nextSize = nextSize - 1;
			                    	} else {
										nextSize = nextSize + 1;
			                    	}

			                    	var newNextClass = "large-"+nextSize ;
			                    	
			                    	nextO.removeClass(function (index, css) {
			                            return (css.match (/(^|\s)large-[0-9]+/g) || []).join(' ');
			                        }).addClass(newNextClass);	
	                    		}
	                    		
	                    	}	
                    	}
                        
                        overed.removeClass(function (index, css) {
                            return (css.match (/(^|\s)large-[0-9]+/g) || []).join(' ');
                        }).addClass(newClass);  
                    }
                }  

            }
        });

        actionZone.css("top", posElem.top);
        actionZone.css("left", posElem.left);
        actionZone.css("width", posElem.width);
        actionZone.css("height", posElem.height);

    };

    Layout.prototype.addSplitGhost = function (target) {
    	var pos = target.getBoundingClientRect(),
    		dg;

		$(".b-dragZone").remove();

		$(document.body).append("<div class='b-dragZone'><div class='b-left'></div><div class='b-right'></div></div>"); // b-horizontal
        dg = $(".b-dragZone");
        dg.css("top", pos.top);
        dg.css("left", pos.left);
        dg.css("width", pos.width);
        dg.css("height", pos.height);
        dg.on( "mouseover", function(e) {
            dg.find(".b-active").removeClass("b-active");
            var t = $(e.target);
            t.addClass("b-active");
        });
    }

    Layout.prototype.addStopGhost = function (target) {

    	$(".b-dragZone").remove();

    	var pos = target.getBoundingClientRect(),
    		dg;

		$(document.body).append("<div class='b-dragZone b-stop'></div>");
        dg = $(".b-dragZone");
        dg.css("top", pos.top);
        dg.css("left", pos.left);
        dg.css("width", pos.width);
        dg.css("height", pos.height);

    }

    Layout.prototype.isDragingOverColumn = function (target) {

    	var $target 		= $(target),
    		$row 			= $target.parent(),
    		targetSize;

		targetSize = this.getColumnSizeFromString(target.className);
		
		if (targetSize > 1) {
			this.addSplitGhost(target);
		} else {
			this.addStopGhost(target);
		}

    	//totalColSize 	= this.isRowFull($row, true),
    	//if (totalColSize >= 12) {
    	/*} else {
    		//var allowedSize = 12 - totalColSize;
    		this.addSplitGhost(target);
    	}*/

    };

    Layout.prototype.renderRow = function (rowConf) {
    	_b.$body.append("<div id='"+rowConf.id+"' class='row'>"+this.renderCell(rowConf.cells)+"</div>");
    };

	Layout.prototype.renderCell = function (cellConf) {
		var that = this,
			markup = "",
			end = "",
			cellData,
			tag,
			c = 0;

		cellConf.forEach(function(cell) {
			if (c === cellConf.length -1) {
				end = "end";
			}
			c++;
			cellData = _b.componentHash[cell.type];
			tag = cellData.tag;
			markup += "<"+tag+" id='"+cell.id+"' class='medium-"+cell.size[1]+" small-"+cell.size[0]+" large-"+cell.size[2]+" columns "+end+"'>"+cell.id+"</"+tag+">";
		});

		return markup;
    };
    return Layout;
});