define(function () {
    "use strict";
 	
   	var Utilities = {};

   	Utilities.guidGenerator = function() {

    	var date = new Date();
	    var S4 = function() {
	       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	    };
	    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4()) + date.getDate() + date.getDay() + date.getFullYear() + date.getHours() + date.getMinutes() + date.getMilliseconds();
	}

    return Utilities;
});