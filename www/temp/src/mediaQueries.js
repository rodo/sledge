define(function () {
    "use strict";
 	
    var coreCss = "\n",
        largeCss = "\n",
        mediumCss = "\n",
        smallCss = "\n",
        bpMedium,
        bpSmall,
        bpXSmall,
        bpBig,
        media = {},
        breakpoints = [],
        queriesObj = {},
        points,
        currentSize,
        toggleHidden = false,
        bodyWrapper;

    queriesObj["small"] = [];
    queriesObj["medium"] = [];
    queriesObj["large"] = [];

    function manageQueriesObject(queries) {

        var bpLarge,
            a,
            b,
            tempo,
            numbers = [];

        if (!queries.media || queries.media[0] === "print") {
            return;
        }

        if (!media[queries.media.mediaText]) {
            media[queries.media.mediaText] = [];
        }

        media[queries.media.mediaText].push(queries.cssRules);
       
        points = queries.media.mediaText.match(/\d+\.?\d*/g);

        if (queries.media.mediaText.indexOf("webkit-min-device-pixel-ratio") !== -1) return;
        if (!points) return;

        a = parseInt(points[0],10);

        if (breakpoints.indexOf(a) === -1) {
            if (breakpoints.indexOf(a+1) !== -1) {
                //
            } else if (breakpoints.indexOf(a-1) !== -1) {
                breakpoints.splice(breakpoints.indexOf(a-1), 1);
                breakpoints.push(a);
            } else {
                breakpoints.push(a);
            }
        }

        if (points[1]) {
            b = parseInt(points[1],10);
            if (breakpoints.indexOf(b) === -1) {
                if (breakpoints.indexOf(b+1) !== -1) {
                //
                } else if (breakpoints.indexOf(b-1) !== -1) {
                    breakpoints.splice(breakpoints.indexOf(b-1), 1);
                    breakpoints.push(b);
                } else {
                    breakpoints.push(b);
                }
            }
        }
        
    }

    function parseQueries() {

        var allCss = document.styleSheets,
            foundationCss,
            bp,
            a,
            b,
            sizea,
            sizeb,
            size,
            point;
       
        $.each(allCss, function( index, value ) {
            if (value.ownerNode.id === "foundation") {
                foundationCss = value.cssRules;
                return false;
            }
        });

        $.each(foundationCss, function( index, value ) { // 1 == css 4 == media 7 == keyframe
            if (value.type === 1) {
                coreCss += value.cssText + "\n";
            } else if (value.type === 4) {
                manageQueriesObject(value);
            }
        });

        breakpoints.sort(function(a, b){return a-b});

        bpSmall = breakpoints[0];
        bpMedium = breakpoints[1];
        bpBig = breakpoints[2];

        $.each(media, function( index, value ) {

            bp = index.match(/\d+\.?\d*/g);

            /*if (index === "only screen" || index === "not print") {
                queriesObj["small"] = queriesObj["small"].concat(value);
                queriesObj["medium"] = queriesObj["medium"].concat(value);
                queriesObj["large"] = queriesObj["large"].concat(value);
            }*/

            /*if (index === "only screen and (min-width: 40.0625em)") { //BIG MEDIUM
                console.log("1")
                queriesObj["medium"] = queriesObj["medium"].concat(value);
                queriesObj["large"] = queriesObj["large"].concat(value);
            }*/

            /*if (index === "only screen and (min-width: 64.0625em)") { //BIG
                console.log("2")
                queriesObj["large"] = queriesObj["large"].concat(value);   
            }*/

            /*if (index === "only screen and (max-width: 40em)") { //SMALL
                console.log("3")
                queriesObj["small"] = queriesObj["small"].concat(value);
            }*/
                         
            /*if (index === "only screen and (max-width: 64em) and (min-width: 40.0625em)") { //MEDIUM
                console.log("4")
                queriesObj["medium"] = queriesObj["medium"].concat(value); 
            }*/

            /*if (index === "only screen and (max-width: 90em) and (min-width: 64.0625em)") { //LARGE (|)
                console.log("5")
                queriesObj["large"] = queriesObj["large"].concat(value);
            }

            if (index === "only screen and (max-width: 120em) and (min-width: 90.0625em)") { //XLARGE
                console.log("6")
                queriesObj["large"] = queriesObj["large"].concat(value);
            }

            if (index === "only screen and (min-width: 90.0625em)") { //LARGE XLARGE
                console.log("7")
                queriesObj["large"] = queriesObj["large"].concat(value);
            }

            if (index === "only screen and (max-width: 6249999.9375em) and (min-width: 120.0625em)") { //LARGE XLARGE ++
                console.log("8")
                queriesObj["large"] = queriesObj["large"].concat(value);
            }

            if (index === "only screen and (min-width: 120.0625em)") { //XLARGE
                console.log("9")
                queriesObj["large"] = queriesObj["large"].concat(value);
            }*/
            if (index === "only screen" || index === "not print") {
                queriesObj["small"] = queriesObj["small"].concat(value);
                queriesObj["medium"] = queriesObj["medium"].concat(value);
                queriesObj["large"] = queriesObj["large"].concat(value);
            }
            
            if (bp) {

                if (bp.length === 1) {
                    
                    point = parseInt(bp[0],10);
                    size = getSizeBybreakPoint(point);

                    if(index.indexOf("min") === -1) {
                        //max
                        if (size === "small") {
                            queriesObj["small"] = queriesObj["small"].concat(value);
                        }
                    } else {
                        //min
                        if (size === "small") {
                            queriesObj["medium"] = queriesObj["medium"].concat(value);
                            queriesObj["large"] = queriesObj["large"].concat(value);
                        }
                        if (size === "medium") {
                            queriesObj["large"] = queriesObj["large"].concat(value); 
                        }
                        if (size === "large") {
                            if (bp[0] === "0") {

                            } else {
                                queriesObj["large"] = queriesObj["large"].concat(value);    
                            }
                        }
                    }
                } else {

                    a = bp[0];
                    b = bp[1];
                    sizea = getSizeBybreakPoint(a);
                    sizeb = getSizeBybreakPoint(b);

                    if (sizea === "medium" && sizeb === "small") {
                       queriesObj["medium"] = queriesObj["medium"].concat(value);
                    }
                    if (sizea === "large" && sizeb === "medium") {
                       queriesObj["large"] = queriesObj["large"].concat(value);
                    }
                    if (sizea === "large" && sizeb === "large") {
                        queriesObj["large"] = queriesObj["large"].concat(value);
                    }
                }    
            }
        });

        buildMediaCssFile("large");
        buildMediaCssFile("medium");
        buildMediaCssFile("small");

        removeDefaultCss();
        addStylesheet(coreCss, "coreCss");
        changeMediaCss("large");

    }

    function removeDefaultCss() {
        $("#foundation").remove();
    }

    function addStylesheet(css, id) {

        var head = document.head,
            style = document.createElement('style');

        style.type = 'text/css';
        style.id = id;
        style.appendChild(document.createTextNode(css));
        head.appendChild(style);

    }

    function buildMediaCssFile(type) {

        var data = queriesObj[type],
            theString = "";

        $.each(data, function( index, value ) {
            concatRuleList(value, type);
        });

    }

    function concatRuleList(ruleList, type) {

        $.each(ruleList, function( index, value ) {

            if (type === "large") {
                largeCss += value.cssText + "\n";
            } else if (type === "medium") {
                mediumCss += value.cssText + "\n";
            } else if (type === "small") {
                smallCss += value.cssText + "\n";
            } else if (type === "xsmall") {
                xsmallCss += value.cssText + "\n";
            }

        });
    }

    function getSizeBybreakPoint(val) {
        var size;

        val = parseInt(val, 10);

        switch(val) {
            case bpBig:
                size = "large";
            break;
            case bpMedium:
                size = "medium";
            break;
            case bpSmall:
                size = "small";
            break;
            default :
                size = "large";
        }
        return size;
    }

    function changeMediaCss(type) {
        $("#mediaCss").remove();
        var css,
            sizePx;

        $(".b-footerbutton.b-active").removeClass("b-active");

        switch(type) {
            case "large":
                css = largeCss;
                sizePx = bpBig;
                _b.$body.css("min-width", breakpoints[1]+"em");
                _b.$body.css("max-width", breakpoints[2]+"em");
                $(".b-footerbutton.b-large").addClass("b-active");
            break;
            case "medium":
                css = mediumCss;
                sizePx = bpMedium;
                _b.$body.css("min-width", breakpoints[0]+"em");
                _b.$body.css("max-width", breakpoints[1]+"em");
                $(".b-footerbutton.b-medium").addClass("b-active");
            break;
            case "small":
                css = smallCss;
                sizePx = bpSmall;
                _b.$body.css("min-width", "500px");
                _b.$body.css("max-width", breakpoints[0]+"em");
                $(".b-footerbutton.b-small").addClass("b-active");
            break;
            default:
                css = largeCss;
                _b.$body.css("min-width", breakpoints[2]+"em");
                _b.$body.css("max-width", "none");
                $(".b-footerbutton.b-large").addClass("b-active");
        }

        addStylesheet(css, "mediaCss");
        currentSize = type;

    }

    function Responsive() {
       this.init();
    }
    
    Responsive.create = function () {
        var result = new Responsive();
        return result;
    };
 	Responsive.prototype.constructor = Responsive;

    Responsive.prototype.init = function () {
        parseQueries();
        this.render();
    };

    Responsive.prototype.render = function () {

        $(".b-footerbutton").on("click", function(){
            var elem = $(this);
            if (changeMediaCss("b-active")) return;
            switch (true) {
                case elem.hasClass("b-small"): 
                    changeMediaCss("small");
                break; 
                case elem.hasClass("b-medium"): 
                    changeMediaCss("medium");
                break; 
                case elem.hasClass("b-large"): 
                    changeMediaCss("large");
                break; 
                default:
                    changeMediaCss("large")
            }

        });
    };

    return Responsive;
});