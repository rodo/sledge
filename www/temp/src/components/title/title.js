//define(function () {
  var toto;
  (function () {
  "use strict";

  var proto = Object.create( HTMLElement.prototype );
  
  proto.createdCallback = function() {
      var that = this;
      //this.readAttributes();
      if (this.innerHTML === "") {
          toto = proto
        this.initMarkup();
      }
  };
  
  proto.properties = function() {
    
    var p = {}, 
        that = this;
        
    p.titlecolor = {
      type: "color",
      default: "#000000"
    };
  
    p.backgroundColor = {
      type: "color",
      default: "#ffffff",
      get: function() {
        return that.style.backgroundColor;
      },
      set: function(value) {
        that.style.backgroundColor = value;
      }
    };
    
    return p;
  };
  
  proto.initMarkup = function() {
      this.innerHTML = "<p>This is a title !</p>";
      this.style.backgroundColor = this.properties().backgroundColor.default;
  };
  
  proto.readAttributes = function() {
      //this.hour = this.getAttribute( "hour" ); 
  };
  
  proto.attributeChangedCallback = function( attrName, oldVal, newVal ) {
      //
  };

  document.registerElement( "bn-title", {
      prototype: proto
  });
}());