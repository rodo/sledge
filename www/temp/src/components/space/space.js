//define
(function () {
  "use strict";



  var proto = Object.create( HTMLElement.prototype );
  
  proto.createdCallback = function() {
      var that = this;
      //this.readAttributes();
      if (this.innerHTML === "") {
        this.initMarkup();
      }
  };
  
  proto.initMarkup = function() {
      this.innerHTML = "<div class=''>SPACE<p>Component space</p></div>";
  };
  
  proto.readAttributes = function() {
      //this.hour = this.getAttribute( "hour" ); 
  };
  
  proto.attributeChangedCallback = function( attrName, oldVal, newVal ) {
      //
  };

  document.registerElement( "bn-space", {
      prototype: proto
  });
}());