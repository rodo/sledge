define(function () {
    "use strict";

    var events = {};

    events.init = function() {
        this.altk = false;
        this.altkey();
    };

    events.altkey = function() {

        var that = this;

        $(document).keydown(function(ev) {

            if (ev.altKey) {
                that.altk = true;
                PubSub.publish('altKey', true);
            }

        });

        $(document).keyup(function(ev) {

            if (that.altk) {
                that.altk = false;
                PubSub.publish('altKey', false);
            }

        });        
    };

    return events;
});