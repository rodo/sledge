define(["layoutManager"], function (Layout) {
    "use strict";
 	
    function View(vid) {
       this.init(vid);
    }
    
    View.create = function (vid) {
        var result = new View(vid);
        return result;
    };
 	View.prototype.constructor = View;

    View.prototype.init = function (vid) {
    	
        var that = this;
        this.viewStruc = _b.struct.getView(vid);
    	this.layout = Layout.create(this.viewStruc, this);
        this.lastOveredTarget = null;

        $("aside").on("mouseover", function(){
            $(".b-actionZone").remove();
        });

        _b.$body.on( "mouseover", function(e) {

            if (that.layout.onLayoutResize) return;

            var $target = $(e.target),
                overed;
    
            that.lastOveredTarget = overed = $target;

            if (!_b.onElementMove) {
               
                $(".b-actionZone").remove();
                
                if ($target.hasClass("columns")) {
                    that.layout.isOverColumn(overed, $target);
                } else {
                    $(".b-actionZone").remove();
                }

            } else {
                
                if ($target.hasClass("b-insertCol")) {
                    
                    $(".b-ready").removeClass("b-ready");
                    $target.addClass("b-ready");
                    $(".b-dragZone").remove();

                } else if ($target.hasClass("b-insertRow")) {

                    $(".b-ready").removeClass("b-ready");
                    $target.addClass("b-ready");
                    $(".b-dragZone").remove();

                } else if ($target.hasClass("b-appBody")) {
                    $(".b-dragZone").remove();
                    $(".b-ready").removeClass("b-ready");
                } else {
                    $(".b-ready").removeClass("b-ready");
                }

                if ($target.hasClass("columns") && !$target.hasClass("b-dropped") && !$target.hasClass("b-insertCol")) {
                    that.layout.isDragingOverColumn(e.target);
                }              
            }
        });

    };

    View.prototype.render = function () {
    	
    };

    return View;
});