define(["Utilities"], function (Utilities) {
    "use strict";
 	
    function propertiesList(component) {
       this.init(component);
    }
    
    propertiesList.create = function (component) {
        var result = new propertiesList(component);
        return result;
    };
 	propertiesList.prototype.constructor = propertiesList;

    propertiesList.prototype.init = function (component) {
        console.log(component.get()[0].properties())
    };

    propertiesList.prototype.render = function () {
    	
    };

    return propertiesList;
});