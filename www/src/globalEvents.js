/**
 * a bunch of event that are initialized at startup...
 */

export default function () {
  let delayScroll = 100
  let delayResize = 150

  // click
  document.body.addEventListener('click', (e) => window.PubSub.publish('DOCUMENT_CLICK', e))

  // scroll
  let scrollTM = null
  document.body.addEventListener('mousewheel', e => {
    let ac = document.body.querySelector('.sd-actionZone')
    if (ac) {
      window.PubSub.publish('documentScroll')
    }
    if (scrollTM) {
      clearTimeout(scrollTM)
    }
    scrollTM = window.setTimeout(e => {
      scrollTM = null
    }, delayScroll)
  })

  // resize
  let timeoutRs = null
  window.addEventListener('resize', () => {
    clearTimeout(timeoutRs)
    timeoutRs = setTimeout(() => window.PubSub.publish('documentScroll'), delayResize)
  })

  // mouse Enter header
  const header = document.querySelector('.sd_header')
  header.addEventListener('mouseenter', (e) => window.PubSub.publish('viewMouseOut', e), false)
}
