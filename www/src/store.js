import { createStore } from 'redux'

const initialState = {
  pendingProject: null,
  currentProject: null,
  projects: null,
  editProject: false,
  rootViews: [],
  rootPages: [],
  views: {},
  pages: {},
  sideMenuCurrentItem: {},
  currentController: {},
  pendingNewPage: null,
  pendingNewView: null,
  responsiveState: 'large',
  currentSelectedComponent: null,
  currentEditedProperties: null,
  medias: {},
  projectConf: null,
  currentTheme: 'default',
  themes: {},
  localPublishPath: null,
  languages: {list: ['en']},
  iapState: 0,
  iapStoreData: null,
  iapPlus: false
}

//viewListDragItem: null

let observer = {}

function reducer (state = initialState, action) {
  switch (action.type) {
    case 'SLEDGE_PLUS':
      state.iapPlus = action.data
      return state
    case 'SET_THEMES_LIST':
      state.themes = action.data
      return state
    case 'SET_SETTINGS':
      state.settings = action.data
      return state
    case 'SET_LANGUAGES':
      state.languages = action.data
      return state
    case 'SET_LOCAL_PUBLISH_PATH':
      state.localPublishPath = action.value
      return state
    case 'SET_PROJECT_CONF':
      state.projectConf = action.conf
      return state
    case 'SET_CURRENT_THEME':
      window.sledge.currentTheme = action.themeref
      state.currentTheme = action.themeref
      return state
    case 'SET_MEDIAS':
      state.medias = action.data
      return state
    case 'SET_EDITED_PROPERTIES':
      state.currentEditedProperties = action.value
      return state
    case 'SET_SELECTED_COMPONENT':
      state.currentSelectedComponent = action.value
      return state
    case 'SET_RESPONSIVE_STATE':
      state.responsiveState = action.value
      return state
    case 'PROJECTS':
      const idx = action.projects.indexOf('sledge_db')
      action.projects.splice(idx, 1)
      state.projects = action.projects
      return state
    case 'CURRENT_SIDE_MENU_ITEM':
      state.sideMenuCurrentItem = action.data
      return state
    case 'CURRENT_PROJECT':
      window.localStorage.setItem('currentProject', action.currentProject)
      state.currentProject = action.currentProject
      return state
    case 'PENDING_PROJECT':
      state.pendingProject = action.pendingProject
      return state
    case 'EDIT_PROJECT':
      state.editProject = action.editProject
      return state
    case 'CURRENT_CONTROLLER':
      state.currentController = action.currentController
      return state
    case 'IAP_STATE':
      state.iapState = action.state
      return state
    case 'IAP_DATA':
      state.iapStoreData = action.data
      return state
    case 'SET_PAGES':
      let pages = {}
      if (action.pages.length === 0) {
        state.pages = {}
      } else {
        state.rootPages = action.pages
        action.pages.forEach((item) => {
          pages[item._id] = item
        })
        state.pages = pages
      }
      return state
    case 'UPDATE_PAGE':
      const storeState = store.getState()
      state.pages[action.data._id] = action.data
      let current = storeState.sideMenuCurrentItem
      if (current && current.type === 'page' && current._id === action.data._id) {
        state.sideMenuCurrentItem = action.data
      }
      return state
    case 'PENDING_NEW_PAGE':
      state.pendingNewPage = action.data
      return state
    case 'PENDING_NEW_VIEW':
      state.pendingNewView = action.data
      return state
    case 'SET_VIEWS':
      let views = {}
      if (action.views.length === 0) {
        state.views = {}
      } else {
        state.rootViews = action.views
        action.views.forEach((item) => {
          views[item._id] = item
        })
        state.views = views
      }
      return state
    default:
      return state
  }
}

let store = createStore(reducer)

store.subscribe(() => {
  const st = store.getState()
  if (!st) {
    return
  }
  for (let path in observer) {
    observer[path].forEach(obs => {
      if (JSON.stringify(obs.sender[path]) !== JSON.stringify(st[path])) {
        obs.sender[path] = exportPath(st[path])
        if (obs.callback) {
          obs.callback(obs.sender[path])
        }
        if (obs.sender.render) {
          //obs.sender.render()
        }
      }
    })
  }
})

export function reset () {
  store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {}})
  store.dispatch({type: 'SET_MEDIAS', data: {}})
  store.dispatch({type: 'SET_PAGES', pages: []})
  store.dispatch({type: 'SET_VIEWS', views: []})
  store.dispatch({type: 'SET_PROJECT_CONF', conf: null})
}

/* just copie (duplicate reference) of the given var (obj vs array vs other) */
function exportPath (pathObj) {
  let res
  if (pathObj instanceof Array) {
    res = [...pathObj]
  } else if (pathObj instanceof Object) {
    res = {...pathObj}
  } else {
    res = pathObj
  }
  return res
}

export function connect (path, sender, callback, runNow) {
  let st = store.getState()
  if (!observer[path]) {
    observer[path] = []
  }
  callback = callback || null
  observer[path].push({callback: callback, sender: sender})
  
  sender[path] = exportPath(st[path])
  if (callback && runNow) {
    callback(exportPath(st[path]))
  }
}

export {store}
