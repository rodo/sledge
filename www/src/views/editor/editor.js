import Menu from './sideMenu'
import Workspace from './workspace'
import {setWindowName, updateThemeDial, downloadTheme, removeTheme, getProjConf, getViews, getPages, getMedias, getTheme, getThemeConfig, getLanguages} from '../../interface/interface_mac'
import {store, connect, reset} from '../../store'
import {remove} from '../../utilities'
import * as _langList from '../../../local/languages.js'

export default class Editor {

  constructor (settings) {
    this.mainContent = document.getElementsByClassName('sd_editor')[0]
    this.addListeners()
  }

  addListeners () {
    PubSub.subscribe('ACTION_ZONE_RESIZE', (a, row) => {
      let elem = null
      let sel = document.querySelector('.sd-selection')
      if (row) {
        elem = row
      } else if (sel) {
        elem = sel.parentNode
      }
      if (elem) {
        window.$sledge.equalizerSingleRow(elem)
      }
      window.$sledge.refreshOrbit()
    })
    connect('currentProject', this)
    connect('currentTheme', this, () => this.manageThemeStyleSheet())
    connect('pages', this, () => this.initUI())
    connect('views', this, () => this.initUI())
    connect('languages', this, lang => this.setLangObj(lang))
    connect('editProject', this, edit => {
      if (edit) {
        setWindowName(this.currentProject)
        getMedias(this.currentProject)
        getTheme(this.currentProject)
        getProjConf(this.currentProject)
        getLanguages(this.currentProject, (lang) => this.setLangObj(lang))
        getViews()
        getPages()
      } else {
        setWindowName('Sled')
        this.closeUI()
      }
    })
  }

  setLangObj (lang) {
    let list = lang.list.map(code => _langList[window.nslocal || 'en'][code])
    window.localStorage.setItem('langList', JSON.stringify(list))
  }

  initUI () {
    if (Object.keys(this.views).length !== 0 && Object.keys(this.pages).length !== 0) {
      if (!this.menu) {
        this.menu = new Menu(this.mainContent)
        this.workspace = new Workspace(this.mainContent)
      } else {
        this.menu.open()
        this.workspace.open()
      }
    }
  }

  closeUI (params) {
    this.menu.close()
    this.workspace.close()
    reset()
  }

  updateTheme () {
    let ref = this.currentTheme
    removeTheme(this.currentProject, this.currentTheme)
    store.dispatch({ type: 'SET_CURRENT_THEME', themeref: 'default' })
    window.requestAnimationFrame(() => {
      downloadTheme(this.currentProject, ref, true)
    })
  }

  manageThemeStyleSheet () {
    remove(document.getElementById('foundationTheme'))
    let path = 'defaultTheme/assets/css/appsledge.css'
    if (this.currentTheme !== 'default') {
      path = window.localStorage.documentPath + '/sledgeTheme/' + this.currentProject + '/' + this.currentTheme + '/assets/css/appsledge.css'
      let themes = require(`../../themes.json`)      
      getThemeConfig(this.currentProject, this.currentTheme, conf => {
        window.sledge.themeConfig = conf
        window.localStorage.setItem('themeConfig', JSON.stringify(conf))
        let configObj = themes.find(item => item.ref === this.currentTheme)
        if (configObj.version !== conf.version) {
          updateThemeDial()
        }
      })
    }
    let element = document.createElement('link')
    element.type = 'text/css'
    element.rel = 'stylesheet'
    element.id = 'foundationTheme'
    element.href = path
    document.head.prepend(element)

    remove(document.getElementById('foundationIcons'))
    let path2 = 'defaultTheme/assets/foundation-icons/foundation-icons.css'
    if (this.currentTheme !== 'default') {
      path2 = window.localStorage.documentPath + '/sledgeTheme/' + this.currentProject + '/' + this.currentTheme + '/assets/foundation-icons/foundation-icons.css'
      PubSub.publish('STOP_LOADING')
    }
    let element2 = document.createElement('link')
    element2.type = 'text/css'
    element2.rel = 'stylesheet'
    element2.id = 'foundationIcons'
    element2.href = path2
    document.head.appendChild(element2)
  }
}
