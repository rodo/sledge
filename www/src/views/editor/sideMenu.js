import {appendHtml, html} from '../../utilities'
import {store, connect} from '../../store'

export default class SideMenu {

  constructor (container) {
    connect('iapPlus', this)
    connect('views', this, () => this.updateViewList())
    connect('pages', this, () => this.updatePageList())
    connect('pendingNewPage', this)
    connect('pendingNewView', this)
    connect('currentProject', this, p => {
      let place = document.getElementById('sd-side-settings')
      if (place) {
        place.innerHTML = `<i>${p}</i>`
      }
    })

    this.renderDone = false
    this.mainContent = container
    this.render()

    connect('sideMenuCurrentItem', this, d => {
      if (this.sideMenuCurrentItem && Object.keys(this.sideMenuCurrentItem).length === 0) {
        window.requestAnimationFrame(() => store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: 'settings' }))
        return
      }
      if (this.current && this.current.dataset.id === this.sideMenuCurrentItem._id) {
        // only update display
        this.updateCurrentListItem()
      }
      this.clean()
      if (this.sideMenuCurrentItem._id) {
        this.current = document.querySelector(`[data-id='${this.sideMenuCurrentItem._id}']`)
      } else {
        this.current = document.querySelector(`[data-kind='${this.sideMenuCurrentItem}']`)
      }
      this.current.classList.add('sd_active')
    })
  }

  init () {
    this.element = this.mainContent.querySelector('.sd_editor_side')
    document.getElementsByClassName('sd_toolmenu')[0].addEventListener('click', e => this.manageClick(e.target))
    // event
    this.element.addEventListener('mouseenter', (e) => PubSub.publish('viewMouseOut', e), false)
    // add drag delegate
    const ulViewNode = document.querySelector('.sd_menu_view')
    Sortable.create(ulViewNode, {
      sort: false,
      dragClass: 'sd-view-drag',
      setData: function (dataTransfer, dragEl) {
        dataTransfer.setData('text', dragEl.dataset.id)
      },
      onClone: function (evt) {},
      group: {
        name: 'VIEW_DRAG_GROUP',
        pull: (to, from) => {
          return 'clone'
        }}
    })
    this.open()
    store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: 'settings' })    
  }

  // store.dispatch({ type: 'VIEW_LIST_DRAG', data: item.dataset.id }) !!!
  // store.dispatch({ type: 'VIEW_LIST_DRAG', data: null })

  manageClick (elem) {
    if (!elem.classList.contains('sd_menu_title')) {
      let ds = elem.dataset
      let item = null

      switch (ds.kind) {
        case 'view':
          item = this.views[ds.id]
          break
        case 'page':
          item = this.pages[ds.id]
          break
        default:
          item = ds.kind
      }
      if (ds.kind) {
        store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: item })
      }
    }
  }

  toggleResponsiveSwitch () {
    this.element.classList.toggle('sd_respswitch')
  }

  showResponsiveSwitch () {
    this.element.classList.add('sd_respswitch')
  }

  hideResponsiveSwitch () {
    this.element.classList.remove('sd_respswitch')
  }

  open () {
    window.requestAnimationFrame(() => this.element.classList.add('sd_visible'))
  }

  clean () {
    if (this.current) {
      this.current.classList.remove('sd_active')
      this.current = null
    }
  }

  close () {
    this.clean()
    this.element.classList.remove('sd_visible')
  }

  renderList (items, kind) {
    let markup = ''
    for (let item in items) {
      markup += this.renderListIem(items[item], kind)
    }
    return markup
  }

  renderListIem (view, kind) {
    return `<li data-kind="${kind}" draggable="${kind === 'view'}" data-id="${view._id}">${decodeURIComponent(view.name)}</li>`
  }

  updateCurrentListItem () {
    if (this.sideMenuCurrentItem.name) {
      this.current.innerHTML = this.sideMenuCurrentItem.name
    }
  }

  updateViewList () {
    let loc = window.sledge.nslocal
    html('sd_menu_view', `<li class="sd_menu_title">${loc.gen_views}</li>${this.renderList(this.views, 'view')}`)
    if (this.pendingNewView) {
      store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: this.views[this.pendingNewView] })
      store.dispatch({ type: 'PENDING_NEW_VIEW', data: null })
    } else {
      if (this.sideMenuCurrentItem && this.sideMenuCurrentItem.type === 'view') {
        this.current = document.querySelector("li[data-id='" + this.sideMenuCurrentItem._id + "']")
        if (this.current) {
          this.current.classList.add('sd_active')
        }
      } else if (this.sideMenuCurrentItem && Object.keys(this.sideMenuCurrentItem).length === 0) {
        store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: 'settings' })
      }
    }
  }

  updatePageList () {
    let loc = window.sledge.nslocal
    html('sd_menu_page', `<li class="sd_menu_title">${loc.gen_pages}</li>${this.renderList(this.pages, 'page')}`)
    if (this.pendingNewPage !== null) {
      store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: this.pages[this.pendingNewPage] })
      store.dispatch({ type: 'PENDING_NEW_PAGE', data: null })
    } else {
      if (this.sideMenuCurrentItem && this.sideMenuCurrentItem.type === 'page') {
        this.current = document.querySelector("li[data-id='" + this.sideMenuCurrentItem._id + "']")
        this.current.classList.add('sd_active')
      } else if (this.sideMenuCurrentItem && Object.keys(this.sideMenuCurrentItem).length === 0) {
        store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: 'settings' })
      }
    }
  }

  render () {
    let initDoRun = false
    if (!this.renderDone) {
      this.renderDone = true
      initDoRun = true
      this.previousList = this.views
    } else {

    }
    let loc = window.sledge.nslocal
    let markup = `
      <aside class="sd_editor_side">
        <div class="sd_toolmenu">
          <ul class="sd_menu sd_menu_setting">
              <li class="sd_menu_title">${loc.gen_proj} | <span id="sd-side-settings"><i>${this.currentProject}</i></span></li>
              <li data-kind="settings">${loc.gen_set}</li>
              ${this.iapPlus ? `<li data-kind="siteMap">${loc.gen_sitemap}</li>` : ''}
              <li data-kind="componentsCatalog">${loc.gen_componentsCatalog}</li>
              ${this.iapPlus ? `<li data-kind="languages">${loc.gen_languages}</li>` : ''}
              <li data-kind="theme">${loc.gen_theme}</li>
              <li data-kind="mediaCatalog">${loc.gen_medias}</li>
              <li data-kind="publish">${loc.gen_pub}</li>
          </ul>
          <ul class="sd_menu sd_menu_page">
              <li class="sd_menu_title">${loc.gen_pages}</li>
              ${this.renderList(this.pages, 'page')}
          </ul>
          <ul class="sd_menu sd_menu_view">
              <li class="sd_menu_title">${loc.gen_views}</li>
              ${this.renderList(this.views, 'view')}
          </ul>
        </div>
    </aside>
    `
    if (initDoRun) {
      appendHtml(this.mainContent, markup)
      window.requestAnimationFrame(() => this.init())
    }
  }
}
