import {getHTML, appendHtml} from '../../utilities'
import Navigator from '../navigator/navigator'
import Builder from '../builder/builder'
import {connect, store} from '../../store'
import {deleteView, deletePage, updatePageProp, getViews, getPages, setViewParam} from '../../interface/interface_mac'
import ThemeView from '../themeRoller/main'
import SettingsView from '../settings/main'
import PublishView from '../publish/main'
import LanguagesView from '../languages/main'
import SiteMapView from '../siteMap/main'
import ComponentsCatalogView from '../componentsCatalog/main'
import MediaCatalogView from '../mediaCatalog/main'

export default class Workspace {
 
  constructor (container, data) {
    appendHtml(container, `<div class="sd_editor_work"></div>`)
    this.element = container.querySelector('.sd_editor_work')
    connect('currentProject', this)
    connect('pages', this)
    connect('projectConf', this)
    connect('sideMenuCurrentItem', this)
    PubSub.subscribe('DELETE_VIEW', () => this.deleteView())
    PubSub.subscribe('DELETE_PAGE', () => this.deletePage())
    PubSub.subscribe('RENAME_VIEW', (a, b) => this.renameView(b))
    PubSub.subscribe('RENAME_PAGE', (a, b) => this.renamePage(b))
    this.open()
  }

  open () {
        // store.
    window.requestAnimationFrame(() => this.element.classList.add('sd_visible'))
    if (!this.navigator) {
      this.navigator = new Navigator()
    }
    if (!this.builder) {
      this.builder = new Builder()
    }
    if (!this.theme) {
      this.theme = new ThemeView()
    }
    if (!this.settings) {
      this.settings = new SettingsView()
    }
    if (!this.pubish) {
      this.pubish = new PublishView()
    }
    if (!this.languages) {
      this.languages = new LanguagesView()
    }
    if (!this.siteMap) {
      this.siteMap = new SiteMapView()
    }
    if (!this.componentsCatalog) {
      this.componentsCatalog = new ComponentsCatalogView()
    }
    if (!this.mediaCatalog) {
      this.mediaCatalog = new MediaCatalogView()
    }    

    //window.requestAnimationFrame(() => store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: 'settings' }))
  }

  deleteView () {
    deleteView(this.currentProject, this.sideMenuCurrentItem._id, this.sideMenuCurrentItem._rev, data => this.cleanViewFromPages())
  }

  deletePage () {
    // delete
    deletePage(this.currentProject, this.sideMenuCurrentItem._id, this.sideMenuCurrentItem._rev, data => {
      store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {}})
      this.element.innerHTML = ''
      window.requestAnimationFrame(() => {
        getViews()
        getPages()
      })
    })
  }

  renameView (newName) {
    newName = newName.replace(/[`œ~!@#$%^&*()|+\=÷¿?;:'",.<>\{\}\[\]\\\/]/gi, '')
    newName = newName.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
    setViewParam(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, name: newName},
      data => {
        store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {...this.sideMenuCurrentItem, name: newName, _rev: data.rev}})
      }
    )
  }

  renamePage (newName) {
    newName = newName.replace(/[`œ~!@#$%^&*()|+\=÷¿?;:'",.<>\{\}\[\]\\\/]/gi, '')
    newName = newName.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
    updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, name: newName},
      data => {
        store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {...this.sideMenuCurrentItem, name: newName, _rev: data.rev}})
      }
    )
  }

  cleanViewFromPages () {
    const viewId = this.sideMenuCurrentItem._id
    
    for (let page in this.pages) {
      if (this.pages[page].views) {
        let i = this.pages[page].views.indexOf(viewId)
        if (i !== -1) {
          this.pages[page].views.splice(i, 1)
          updatePageProp(this.currentProject, this.pages[page]._id, this.pages[page])
        }
      }
    }

    for (let page in this.pages) {
      if (this.pages[page].sideViews) {
        let i = this.pages[page].sideViews.indexOf(viewId)
        if (i !== -1) {
          this.pages[page].sideViews.splice(i, 1)
          updatePageProp(this.currentProject, this.pages[page]._id, this.pages[page])
        }
      }
    }

    store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {}})
    this.element.innerHTML = ''
    window.requestAnimationFrame(() => {
      getViews()
      getPages()
    })
  }

  close () {
    this.element.classList.remove('sd_visible')
    this.navigator = null
    this.builder = null
    this.theme = null
    this.settings = null
    this.settings = null
    this.languages = null
    this.siteMap = null
    this.componentsCatalog = null
    this.mediaCatalog = null
  }
}
