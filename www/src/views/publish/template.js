export default (container, localPath) => {
  let loc = window.sledge.nslocal
  let html = `<div class="sd-publish">
    <h1>${loc.gen_pub}</h1>
    <div class="sd-publish-option">
      ${localPath
        ? `<label style="width: auto;">${loc.pub_locpath} :</label>
          <p class="sg-publish-path">${localPath}</p>`
        : ''
      }
      ${localPath
        ? `</br><button data-kind="path">${loc.pub_locpathchange}</button>
            <button data-kind="publish" class="sg-publish-action">${loc.pub_now}</button>
          `
        : `</br><button data-kind="path">${loc.pub_where}</button>`
      }
    </div>
  </div>`
  container.insertAdjacentHTML('beforeend', html)
}

//      <h2>${loc.pub_loc}</h2>
//<button class="sg-help-button"></button>
