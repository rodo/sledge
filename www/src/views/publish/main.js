import {connect} from '../../store'
import render from './template'
import {remove} from '../../utilities'
import {openPublish, getPublishPath} from '../../interface/interface_mac'

export default class Settings {

  constructor (settings) {
    connect('currentProject', this)
    connect('projectConf', this)
    connect('localPublishPath', this, (p) => this.apply(p))
    connect('sideMenuCurrentItem', this, obj => {
      if (obj === 'publish') {
        this.apply()
      } else {
        this.hide()
      }
    })
  }

  updatePathState (path) {
    document.querySelector('.sg-publish-path').innerText = path
    document.querySelector('.sg-publish-action').style.display = 'block'
  }

  apply () {
    if (this.sideMenuCurrentItem !== 'publish') {
      return
    }
    const cont = document.querySelector('.sd_editor_work')
    remove(document.querySelector('.sd-publish'))
    render(cont, this.localPublishPath)
    document.querySelector('.sd-publish').addEventListener('click', e => {
      switch (e.target.dataset.kind) {
        case 'publish':
          window.localStorage.setItem('currentProjName', $sledge.currentdb)
          window.localStorage.setItem('previewKind', 'page')
          window.localStorage.setItem('currentView', this.projectConf.startPage)
          window.localStorage.setItem('firstPublish', true)
          window.localStorage.setItem('pageDone', JSON.stringify({}))
          openPublish()
          break
        case 'path':
          getPublishPath()
          break
        default:
          break
      }
      if (e.target.dataset.kind === 'publish') {
        
      }
    })
  }

  hide () {
    remove(document.querySelector('.sd-publish'))
  }

}
