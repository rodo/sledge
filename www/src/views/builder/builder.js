import {getHTML, appendHtml, remove} from '../../utilities'
import {connect, store} from '../../store'

export default class Builder {

  constructor (container) {
    this.currentViewID = null
    connect('sideMenuCurrentItem', this, () => {
      if (!this.sideMenuCurrentItem.type || this.sideMenuCurrentItem.type !== 'view') {
        PubSub.publish('resetAllBuilderSelection')
        this.destroy()
      }
      if (this.sideMenuCurrentItem.type === 'view') {
        if (this.currentViewID !== this.sideMenuCurrentItem._id) {
          PubSub.publish('resetAllBuilderSelection')
          this.currentViewID = this.sideMenuCurrentItem._id
          this.init()
        }
      }
    })
  };

  init () {
    const tpl = `
    <div class="sd_builder">
      <sd-builder class="sd_builder_workspace"></sd-builder>
      <sd-dockmenu class="sd_builder_side"></sd-dockmenu>
    </div>
    `
    document.getElementsByClassName('sd_editor_work')[0].innerHTML = '' // à voir
    appendHtml(document.getElementsByClassName('sd_editor_work')[0], tpl)
    this.container = document.getElementsByClassName('sd_builder')[0]
    this.workspace = document.getElementsByClassName('sd_builder_workspace')[0]
    this.dockMenu = document.getElementsByClassName('sd_builder_side')[0]

    requestAnimationFrame(() => this.dockMenu.init())

    this.workspace.addEventListener('dragenter', (e) => PubSub.publish('workspaceDragEnter'), false)
    this.workspace.addEventListener('mouseover', (e) => PubSub.publish('viewMouseOver', e), false)
    this.workspace.addEventListener('click', (e) => PubSub.publish('viewClick', e), false)

    connect('responsiveState', this, state => this.setCurrentSize())
    this.setCurrentSize()
  }

  setCurrentSize () {
    switch (this.responsiveState) {
      case 'large':
        this.currentSize = 'l'
        break
      case 'medium':
        this.currentSize = 'm'
        break
      case 'small':
        this.currentSize = 's'
        break
      default:
        this.currentSize = 'l'
    }
    this.manageSizeState()
  }

  manageSizeState () {
    if (this.oldState) {
      this.workspace.classList.remove('sd-f-wrapper-' + this.oldState)
    }
    this.oldState = this.currentSize
    this.workspace.classList.add('sd-f-wrapper-' + this.currentSize)

    window.requestAnimationFrame(() => window.$sledge.foundationReInit())
  }

  destroy () {
    remove('.sd_builder')
    this.currentViewID = null
  }
}

export function checkWorkspaceScroll (e) {
  let y = e.clientY
  let scrollSpeed = 2
  let workspace = document.querySelector('.sd_builder_workspace')
  if (y > window.innerHeight - 20) {
    if (workspace.offsetHeight + workspace.scrollTop !== workspace.scrollHeight) {
      PubSub.publish('workspaceScroll')
      scrollSpeed = (y > window.innerHeight - 10) ? 5 : 2
      workspace.scrollTop = workspace.scrollTop + scrollSpeed
    }
  }
  if (workspace.scrollTop > 0 && y < 70) {
    if (workspace.offsetHeight + workspace.scrollTop !== workspace.scrollHeight) {
      PubSub.publish('workspaceScroll')
      scrollSpeed = (y < 60) ? 5 : 2
      workspace.scrollTop = workspace.scrollTop - scrollSpeed
    }
  }
}
