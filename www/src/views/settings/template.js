export default (container, pages, conf, projName, isPlus) => {
  let favicon = conf && conf.favicon ? conf.favicon : null
  let loc = window.sledge.nslocal  
  let name = conf && conf.siteName ? decodeURIComponent(conf.siteName) : projName
  let domain = conf && conf.domain ? decodeURIComponent(conf.domain) : ''
  let html = `<div class="sd-settings">
    <h1>${loc.gen_set}</h1>
    <br><label>${loc.set_startpage}</label>
    <select data-kind="startPage">
      ${getPageList(pages, conf && conf.startPage ? conf.startPage : null)}
    </select>
    
    <br>
    <label style="margin-top:15px;">${loc.fav_sname}</label>
    <input type="text" value="${name}" data-kind="siteName"/>
    
    ${isPlus
    ? `<br>
    <label style="margin-top:15px;">${loc.fav_domain}</label>
    <input type="text" value="${domain}" data-kind="domain"/>`
    : ''
    }

    ${isPlus
    ? `<hr>
    <label style="margin-top:15px;">${loc.fav_title}</label>

    <div class="sd-fav-wrapper">

    <div class="sd-fav-item">
      <label style="margin-top:15px;" class="sd-full">16x16</label>
      <div class="sd-fav-img">${getFavicoImg('16', favicon)}</div>
      <button data-kind="16">${loc.fav_browse}</button>
    </div>

    <div class="sd-fav-item">
    <label style="margin-top:15px;" class="sd-full">32x32</label>
    <div class="sd-fav-img">${getFavicoImg('32', favicon)}</div>
    <button data-kind="32">${loc.fav_browse}</button>
    </div>

    <div class="sd-fav-item">
    <label style="margin-top:15px;" class="sd-full">96x96</label>
    <div class="sd-fav-img">${getFavicoImg('96', favicon)}</div>
    <button data-kind="96">${loc.fav_browse}</button>
    </div>

    <div class="sd-fav-item">
    <label style="margin-top:15px;" class="sd-full">120x120 (iPhone retina)</label>
    <div class="sd-fav-img">${getFavicoImg('120', favicon)}</div>
    <button data-kind="120">${loc.fav_browse}</button>
    </div>

    <div class="sd-fav-item">
    <label style="margin-top:15px;" class="sd-full">180x180 (iPhone iOS8+)</label>
    <div class="sd-fav-img">${getFavicoImg('180', favicon)}</div>
    <button data-kind="180">${loc.fav_browse}</button>
    </div>

    <div class="sd-fav-item">
    <label style="margin-top:15px;" class="sd-full">152x152 (iPad)</label>
    <div class="sd-fav-img">${getFavicoImg('152', favicon)}</div>
    <button data-kind="152">${loc.fav_browse}</button>
    </div>

    <div class="sd-fav-item">
    <label style="margin-top:15px;" class="sd-full">167x167 (iPad iOS8+)</label>
    <div class="sd-fav-img">${getFavicoImg('167', favicon)}</div>
    <button data-kind="167">${loc.fav_browse}</button>    
    </div>

    </div>`
    : ''
    }

  </div>`
  container.insertAdjacentHTML('beforeend', html)
}

function getFavicoImg (size, list) {
  let urlStart = `${window.$sledge.conf.dbServerUrl}/${window.localStorage.getItem('currentProject')}`
  let mark = (list && list.indexOf(size) !== -1)
  ? `<img src="${urlStart}/settings/favicon_${size}.png"/>`
  : ''
  return mark
}
function getPageList (pages, startPage) {
  let markup = ''
  for (let key in pages) {
    let page = pages[key]
    markup += `<option ${page._id === startPage ? 'selected' : ''} value="${page._id}">${page.name}</option> `
  }
  return markup
}
