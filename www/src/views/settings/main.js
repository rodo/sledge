import {connect} from '../../store'
import render from './template'
import {remove} from '../../utilities'
import {setProjConf, getProjConf, getFavicon} from '../../interface/interface_mac'

export default class Settings {

  constructor (settings) {
    connect('iapPlus', this)
    connect('currentProject', this)
    connect('projectConf', this, () => this.apply())
    connect('pages', this)
    connect('sideMenuCurrentItem', this, obj => {
      if (obj === 'settings') {
        this.apply()
      } else {
        this.hide()
      }
    })
  }

  apply () {
    if (this.sideMenuCurrentItem !== 'settings') {
      return
    }
    const cont = document.querySelector('.sd_editor_work')
    remove(document.querySelector('.sd-settings'))
    render(cont, this.pages, this.projectConf, this.currentProject, this.iapPlus)
    document.querySelector('.sd-settings').addEventListener('change', e => {
      switch (e.target.dataset.kind) {
        case 'startPage':
          this.projectConf.startPage = e.target.value
          setProjConf(this.currentProject, this.projectConf)
          break
        case 'siteName':
          this.projectConf.siteName = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
          setProjConf(this.currentProject, this.projectConf)
          break
        case 'domain':
          let domain = e.target.value
          if (domain[domain.length - 1] === '/') {
            domain = domain.slice(0, -1)
          }
          this.projectConf.domain = domain
          setProjConf(this.currentProject, this.projectConf)
          break          
        default:
          //
      }
    })

    document.querySelector('.sd-settings').addEventListener('click', e => {
      switch (e.target.dataset.kind) {
        case '16':
        case '32':
        case '96':
        case '120':
        case '180':
        case '152':
        case '167':
          let size = e.target.dataset.kind
          getFavicon(parseInt(size, 10), name => {
            getProjConf(this.currentProject, () => {
              if (!this.projectConf.favicon) {
                this.projectConf.favicon = []
              }
              this.projectConf.favicon.push(size)
              setProjConf(this.currentProject, this.projectConf)
            })
          })
          break
        
        default:
          //
      }
    })
  }

  hide () {
    remove(document.querySelector('.sd-settings'))
  }

}
