export default (container, list) => {
  let loc = window.sledge.nslocal
  let html = `
  <div class="sd-mediaCatalog">
    <h1>${loc.gen_medias} <button data-kind="add"></button></h1>
    <div class="sd-media-cont">
      ${getMediasList(list, loc)}
    </div>
  </div>`
  container.insertAdjacentHTML('beforeend', html)
}

function getMediasList (list, loc) {
  let markup = ''
  Object.keys(list).forEach(item => {
    if (item !== '_attachments' && item !== '_id' && item !== '_rev' && item !== 'type') {
      let obj = list[item]
      if (obj.type === 'pdf') {
        markup += `<div class="sd-media-item sd-media-pdf">
        <span>${obj.originalName}.${obj.type}</span>
        <div class="sd-media-mask">
          <button class="sd-media-rdel" data-kind="reqdel"></button>
          <div>
            <button data-ref="${item}" class="sd-media-del" data-kind="del">${loc.cat_del}</button>
            <button class="sd-media-cancel" data-kind="cancel">${loc.gen_cancel}</button>
          </div>
        </div>
      </div>`
      } else if (obj.type === 'mp3') {
        markup += `<div class="sd-media-item sd-media-sound">
          <span>${obj.originalName}.${obj.type}</span>
          <div class="sd-media-mask">
            <button class="sd-media-rdel" data-kind="reqdel"></button>
            <div>
              <button data-ref="${item}" class="sd-media-del" data-kind="del">${loc.cat_del}</button>
              <button class="sd-media-cancel" data-kind="cancel">${loc.gen_cancel}</button>
            </div>
          </div>
        </div>`
      } else {
        markup += `<div class="sd-media-item" style="background-image:url(${window.$sledge.conf.dbServerUrl}/${window.$sledge.currentdb}/media/${item}${obj.numberOfSizes !== 1 ? '_small' : ''}.${obj.type})">
          <div class="sd-media-mask">
            <button class="sd-media-rdel" data-kind="reqdel"></button>
            <div>
              <button data-ref="${item}" class="sd-media-del" data-kind="del">${loc.cat_del}</button>
              <button class="sd-media-cancel" data-kind="cancel">${loc.gen_cancel}</button>
            </div>
          </div>
        </div>`
      }
    }
  })
  if (markup === '') {
    markup = `<p style="margin-left:20px;">${loc.cat_empty}</p>`
  }
  return markup
}

/*markup += `<div class="sd-media-item">
          <video controls width="100%" height="100%" src="http://localhost:${window.listennerPort}/${window.$sledge.currentdb}/media/${item}.${obj.type}"></video>  
          <div class="sd-media-mask sd-media-mask-video">
              <button class="sd-media-rdel" data-kind="reqdel"></button>
              <div>
                <button data-ref="${item}" class="sd-media-del" data-kind="del">${loc.cat_del}</button>
                <button class="sd-media-cancel" data-kind="cancel">${loc.gen_cancel}</button>
              </div>
            </div>
          </div>`*/