import {connect} from '../../store'
import render from './template'
import {remove} from '../../utilities'
import {browseFile, deleteFile, getMedias} from '../../interface/interface_mac'

export default class MediaCatalog {

  constructor (settings) {
    connect('currentProject', this)
    connect('medias', this, () => {
      if (this.sideMenuCurrentItem === 'mediaCatalog') {
        this.apply()
      }
    })
    connect('sideMenuCurrentItem', this, obj => {
      if (obj === 'mediaCatalog') {
        this.apply()
      } else {
        this.hide()
      }
    })
  }

  apply () {
    if (this.sideMenuCurrentItem !== 'mediaCatalog') {
      return
    }

    const cont = document.querySelector('.sd_editor_work')
    remove(document.querySelector('.sd-mediaCatalog'))

    render(cont, this.medias)
    
    document.querySelector('.sd-mediaCatalog').addEventListener('click', e => {
      switch (e.target.dataset.kind) {
        case 'add':
          browseFile(name => getMedias(this.currentProject))
          break
        case 'reqdel':
          e.target.parentNode.classList.add('sd-confirm')
          break
        case 'cancel':
          e.target.parentNode.parentNode.classList.remove('sd-confirm')
          break
        case 'del':
          let ref = e.target.dataset.ref
          deleteFile(ref, this.medias[ref].numberOfSizes)
          break
        default:
          break
      }
    })
  }

  hide () {
    remove(document.querySelector('.sd-mediaCatalog'))
  }

}
