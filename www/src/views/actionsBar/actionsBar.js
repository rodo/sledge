import {html, remove} from '../../utilities'
import {connect, store} from '../../store'
import {openPreview, archive, importArchive} from '../../interface/interface_mac'
import renderLoader from './loader_template'

export default class Editor {

  constructor (settings) {
    connect('iapPlus', this, () => {
      html('sd_header', this.getBaseMarkup())
      this.delegate()
      this.render()
      document.querySelector('.sd_header_editor').addEventListener('keyup', this.handleKey)      
    })
    connect('sideMenuCurrentItem', this, () => this.render())
    connect('currentProject', this)
    connect('pages', this)
    connect('views', this)
    connect('projectConf', this)
    connect('projects', this)
    connect('languages', this)

    html('sd_header', this.getBaseMarkup())
    this.delegate()

    connect('editProject', this, (edit) => this.manageMainState(edit))
    PubSub.subscribe('DOCUMENT_CLICK', (a, e) => this.closeConfirmDialog(e))
    PubSub.subscribe('START_LOADING', (a, message) => this.setLoadingState(message))
    PubSub.subscribe('STOP_LOADING', (a, message) => this.removeLoadingState())
    this.render()
    document.querySelector('.sd_header_editor').addEventListener('keyup', this.handleKey)
  }

  setLoadingState (message) {
    renderLoader(document.querySelector('.sd_actionbar_edit'), message.message)
  }

  removeLoadingState () {
    remove(document.querySelector('.sd-actionBar-loader-cont'))
  }

  handleKey (e) {
    let target = e.target
    let loc = window.sledge.nslocal    
    switch (true) {
      case target.classList.contains('sd_header_rename_text'):

        if (!target.value || target.value.trim() === '') {
          document.querySelector('.sd_header_rename .sd_header_confirm_yes').disabled = true
          document.querySelector('.sd_header_rename div.sd_h_text').innerHTML = loc.acb_enter_name
        } else {
          let newName = target.value
          newName = newName.replace(/[`œ~!@#$%^&*()|+\=÷¿?;:'",.<>\{\}\[\]\\\/]/gi, '')
          newName = newName.normalize('NFD').replace(/[\u0300-\u036f]/g, '')

          let txt = document.querySelector('.sd_header_rename div.sd_h_text')

          if (newName !== target.value) {
            document.querySelector('.sd_header_rename .sd_header_confirm_yes').disabled = true
            txt.innerHTML = `<div class="sd_small">${loc.acb_wrong_name} <i>${newName}</i> ${loc.acb_wrong_name2}</div>`
          } else {
            document.querySelector('.sd_header_rename .sd_header_confirm_yes').disabled = false
            txt.innerHTML = loc.acb_new_name
          }
        }
        break
    }
  }

  delegate () {
    let loc = window.sledge.nslocal    
    document.querySelector('.sd_header').onclick = e => {
      const target = e.target
      let conf = null
      const cl = target.classList
      switch (true) {
        case cl.contains('sd-action-iap'):
          store.dispatch({type: 'IAP_STATE', state: 1})
          break
        case cl.contains('sd_backDash_button'):
          store.dispatch({ type: 'EDIT_PROJECT', editProject: false })
          break
        case cl.contains('sd-action-preview'):
          window.localStorage.setItem('currentProjName', $sledge.currentdb)
          window.localStorage.setItem('previewKind', this.sideMenuCurrentItem.type)
          window.localStorage.setItem('currentView', this.sideMenuCurrentItem._id)
          openPreview()
          break
        case cl.contains('sd-action-preview-full'):
          window.localStorage.setItem('currentProjName', $sledge.currentdb)
          window.localStorage.setItem('previewKind', 'page')
          window.localStorage.setItem('currentView', this.projectConf.startPage)
          openPreview()
          break          
        case cl.contains('sd-action-add-page'):
          PubSub.publish('ADD_PAGE')
          break   
        case cl.contains('sd-action-add-view'):
          PubSub.publish('ADD_VIEW')
          break
        case cl.contains('sd-action-rename-view'):
          e.stopPropagation()
          e.preventDefault()
          document.querySelector('.sd_header_rename .sd_header_confirm_yes').disabled = false
          conf = document.querySelector('.sd_header_rename')
          conf.classList.add('sd_active')
          conf.querySelector('div.sd_h_text').innerHTML = loc.ab_newName
          this.currentAction = 'RENAME_VIEW'
          this.selector = '.sd_header_editor .sd_header_rename'
          let textfiels = document.querySelector('.sd_header_rename_text')
          textfiels.value = decodeURIComponent(this.sideMenuCurrentItem.name)
          textfiels.focus()
          break
        case cl.contains('sd-action-rename-page'):
          e.stopPropagation()
          e.preventDefault()
          document.querySelector('.sd_header_rename .sd_header_confirm_yes').disabled = false
          conf = document.querySelector('.sd_header_rename')
          conf.classList.add('sd_active')
          conf.querySelector('div.sd_h_text').innerHTML = loc.ab_newName
          this.currentAction = 'RENAME_PAGE'
          this.selector = '.sd_header_editor .sd_header_rename'
          let textfiels2 = document.querySelector('.sd_header_rename_text')
          textfiels2.value = decodeURIComponent(this.sideMenuCurrentItem.name)
          textfiels2.focus()
          break          
        case cl.contains('sd-action-delete-view'):
          e.stopPropagation()
          e.preventDefault()
          conf = document.querySelector('.sd_header_editor .sd_header_confirm')
          conf.classList.add('sd_active')
          conf.querySelector('div.sd_h_text').innerHTML = `${loc.gen_del} ${window.decodeURIComponent(this.sideMenuCurrentItem.name)} ?`
          this.currentAction = 'DELETE_VIEW'
          this.selector = '.sd_header_editor .sd_header_confirm'
          break
        case cl.contains('sd-action-delete-page'):
          e.stopPropagation()
          e.preventDefault()
          conf = document.querySelector('.sd_header_editor .sd_header_confirm')
          conf.classList.add('sd_active')
          conf.querySelector('div.sd_h_text').innerHTML = `${loc.gen_del} ${this.sideMenuCurrentItem.name} ?`
          this.currentAction = 'DELETE_PAGE'
          this.selector = '.sd_header_editor .sd_header_confirm'
          break          
        case cl.contains('sd-action-del-proj'):
          e.stopPropagation()
          e.preventDefault()
          conf = document.querySelector('.sd_header_dash .sd_header_confirm')
          conf.classList.add('sd_active')
          conf.querySelector('div.sd_h_text').innerHTML = `${loc.gen_del} ${this.currentProject} ?`
          this.currentAction = 'DELETE_PROJ'
          this.selector = '.sd_header_dash .sd_header_confirm'
          PubSub.publish('DELETE_PROJ_PENDING')
          break   
        case cl.contains('sd-action-arch-proj'):
          e.stopPropagation()
          e.preventDefault()
          archive(this.currentProject)
          break
        case cl.contains('sd-action-imp-arch'):
          e.stopPropagation()
          e.preventDefault()
          importArchive()
          break          
        case cl.contains('sd_header_confirm_no'):
          this.closeConfirmDialog()
          break
        case cl.contains('sd_header_confirm_yes'):
          this.manageConfirmAction()
          this.closeConfirmDialog()
          break
        case cl.contains('sd-action-edit-proj'):
          store.dispatch({ type: 'EDIT_PROJECT', editProject: true })
          break
        default:
          // console.error('no option !')
      }
    }
    document.querySelector('.sd_header').onchange = e => {
      switch (e.target.dataset.kind) {
        case 'language':
          window.PubSub.publish('documentScroll')
          window.$sledge.language = e.target[e.target.selectedIndex].value
          window.$sledge.bridge.refreshAllProps()
          window.PubSub.publish('LANGUAGE_UPDATE')
          break
        default:
      }
    }
  }

  closeConfirmDialog (e) {
    if (!this.currentAction) {
      return
    }
    if (e) {
      if (e.target.nodeName === 'SPAN' || e.target.classList.contains('sd_h_text') || e.target.classList.contains('sd_header_rename_text') || e.target.classList.contains('sd_header_confirm_no') || e.target.classList.contains('sd_header_confirm_yes')) {
        return
      } else {
        this.currentAction = null
        document.querySelector(this.selector).classList.remove('sd_active')
      }
    } else {
      this.currentAction = null
      document.querySelector(this.selector).classList.remove('sd_active')
    } 
  }

  manageConfirmAction () {
    switch (this.currentAction) {
      case 'DELETE_PROJ':
        PubSub.publish('DELETE_PROJ')
        break      
      case 'DELETE_PAGE':
        PubSub.publish('DELETE_PAGE')
        break
      case 'DELETE_VIEW':
        PubSub.publish('DELETE_VIEW')
        break
      case 'RENAME_VIEW':
        let elem = document.querySelector('.sd_header_rename_text')
        if (elem.value && elem.value.trim() !== '') {
          PubSub.publish('RENAME_VIEW', elem.value.trim())
        }
        break        
      case 'RENAME_PAGE':
        let elem2 = document.querySelector('.sd_header_rename_text')
        if (elem2.value && elem2.value.trim() !== '') {
          PubSub.publish('RENAME_PAGE', elem2.value.trim())
        }
        break                
      default:
       // console.error("no action !")
    }
  }

  manageMainState (edit) {
    if (!this.headerDrower) {
      this.headerDrower = document.querySelector('.sd_header_subContent')
    }
    if (edit) {
      this.headerDrower.classList.add('bal_editor')
    } else {
      this.headerDrower.classList.remove('bal_editor')
    }
  }

  getBaseMarkup () {
    let loc = window.sledge.nslocal
    return `<div class="sd_header_logo"></div>
      <div class="sd_header_subContent">
        <div class="sd_header_dash">
          ${this.iapPlus
            ? `<div class="sd-actionBar-low">
                <button class='sd-action-imp-arch'>${loc.ab_impArch}</button>
              </div>`
            : ''
          }      
          <div class="sd-actionBar-high">
              ${this.iapPlus
                ? `<button class='sd-action-arch-proj'>${loc.ab_archProj}</button>`
                : ''
              }    
              <button class='sd-action-del-proj'>${loc.ab_delProj}</button>
          </div>
          <div class="sd-actionBar-vhigh">
              <button class='sd-action-edit-proj'>${loc.ab_editProj}</button>
          </div>
          <div class="sd_header_confirm">
            <div class="sd_h_text">${loc.gen_confirm} ?</div>
            <button class="sd_header_confirm_no">${loc.gen_no}</button>
            <button class="sd_header_confirm_yes">${loc.gen_yes}</button>
          </div>
          ${!this.iapPlus
          ? `
          <div class="sd-actionBar-iap">
            <button class='sd-action-iap'>${loc.iap_update}</button>
          </div>
          `
          : ''
          }
        </div>
        <div class="sd_header_editor">
          <div class="sd_header_confirm">
            <div class="sd_h_text">${loc.gen_confirm} ?</div>
            <button class="sd_header_confirm_no">${loc.gen_no}</button>
            <button class="sd_header_confirm_yes">${loc.gen_yes}</button>
          </div>
          <div class="sd_header_rename">
            <div class="sd_h_text">${loc.ab_newName}</div>
            <input class="sd_header_rename_text" type="text"/>
            <button class="sd_header_confirm_no">${loc.gen_cancel}</button>
            <button class="sd_header_confirm_yes">${loc.gen_confirm}</button>            
            </div>
          <button class="sd_backDash_button">${loc.gen_projs}</button>
          <div class="sd_actionbar_edit"></div>
        </div>
      </div>`
  }

  render () {
    let markup = ''
    let loc = window.sledge.nslocal
    if (this.sideMenuCurrentItem && Object.keys(this.sideMenuCurrentItem).length !== 0) {
      if (
        this.sideMenuCurrentItem === 'settings' ||
        this.sideMenuCurrentItem === 'siteMap' ||
        this.sideMenuCurrentItem === 'componentsCatalog' ||
        this.sideMenuCurrentItem === 'languages' ||
        this.sideMenuCurrentItem === 'theme' ||
        this.sideMenuCurrentItem === 'mediaCatalog' ||
        this.sideMenuCurrentItem === 'publish'
      ) {
        markup = `
        <div class="sd-actionBar-high">
          <button class='sd-action-add-view'>${loc.ab_addView}</button>
          <button class='sd-action-add-page'>${loc.ab_addPage}</button>
          <button class='sd-action-preview-full'>${loc.ab_prevFullProj}</button>
        <div>`
      }
      if (this.sideMenuCurrentItem.type === 'view') {
        markup = `
        <div class="sd-actionBar-low">
          <button class='sd-action-add-view'>${loc.ab_addView}</button>
          <button class='sd-action-add-page'>${loc.ab_addPage}</button>
          <button class='sd-action-preview-full'>${loc.ab_prevFullProj}</button>          
        </div>
        <div class="sd-actionBar-high">
          ${Object.keys(this.views).length !== 1 ? `<button class='sd-action-delete-view'>${loc.ab_delView}</button>` : ''}
          <button class='sd-action-rename-view'>${loc.ab_renView}</button>
          <button class='sd-action-preview'>${loc.ab_prev}</button>
          <sd-responsive-switch></sd-responsive-switch>
          ${this.languages.list.length === 1 ? '' : `<sd-languages-selector data-lang="${window.$sledge.language}"></sd-languages-selector>`}
        <div>`
      }
      if (this.sideMenuCurrentItem.type === 'page') {
        markup = `
        <div class="sd-actionBar-low">
          <button class='sd-action-add-page'>${loc.ab_addPage}</button>
          <button class='sd-action-add-view'>${loc.ab_addView}</button>
          <button class='sd-action-preview-full'>${loc.ab_prevFullProj}</button>           
        </div>
        <div class="sd-actionBar-high">
          ${(Object.keys(this.pages).length !== 1 && this.projectConf.startPage !== this.sideMenuCurrentItem._id) ? `<button class='sd-action-delete-page'>${loc.ab_delPage}</button>` : ''}
          <button class='sd-action-rename-page'>${loc.ab_renPage}</button>
          <button class='sd-action-preview'>${loc.ab_prev}</button>
          ${this.languages.list.length === 1 ? '' : `<sd-languages-selector data-lang="${window.$sledge.language}"></sd-languages-selector>`}
        </div>`
      }
    }
    html('sd_actionbar_edit', markup)
    
    //hack to fix https://stackoverflow.com/questions/33891709/when-flexbox-items-wrap-in-column-mode-container-does-not-grow-its-width
    let ab = document.querySelector('.sd_actionbar_edit .sd-actionBar-low')
    if (ab) {
      let l = ab.querySelectorAll('button').length
      let flex = 1
      if (l % 2 === 0) {
        flex = l / 2
      } else {
        flex = (l + 1) / 2
        ab.style.minWidth = '300px' // to improve for all sizes !
        ab.style.maxWidth = '300px'
      }
      if (flex !== 1) {
        ab.style.flex = flex
      }
    }
  }
}
