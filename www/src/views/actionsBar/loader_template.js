export default (container, message) => {
  let html = ''
  html += `<div class="sd-actionBar-loader-cont"><div class="sd_actionBar-load-text">${message}</div>
  <span class="sd-actionBar-loader"><span class="sd-actionBar-loader-inner"></span></span>
  </div>`
  container.insertAdjacentHTML('beforeend', html)
}
