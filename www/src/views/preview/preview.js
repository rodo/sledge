import {store, connect} from '../../store'
import {remove} from '../../utilities'
import renderBackground from '../../../framework/src/properties/background'

export default class Preview {

  constructor (container) {
    connect('rootViews', this, () => this.setCurrent())
    connect('languages', this, () => this.setCurrent())
    connect('rootPages', this, () => this.setCurrent())
    connect('projectConf', this, () => this.setCurrent())
    connect('currentTheme', this, () => this.setCurrentTheme())
    connect('views', this, () => this.setCurrent())
    connect('pages', this, () => this.setCurrent())
    connect('sideMenuCurrentItem', this, () => this.init())
    connect('currentProject', this)
    if (this.kind === 'view') {
      this.setCurrentView()
    }
  }

  setCurrentTheme () {
    remove(document.getElementById('foundationTheme'))
    let path = 'defaultTheme/assets/css/app.css'
    if (this.currentTheme !== 'default') {
      path = window.localStorage.documentPath + '/sledgeTheme/' + window.localStorage.currentProject + '/' + this.currentTheme + '/assets/css/app.css'
    }
    let element = document.createElement('link')
    element.type = 'text/css'
    element.rel = 'stylesheet'
    element.id = 'foundationTheme'
    element.href = path
    document.head.appendChild(element)
  }

  setCurrent () {
    let realIndex = 0
    let item = {}
    this.kind = window.localStorage.getItem('previewKind')
    if (Object.keys(this.views).length === 0 || this.rootViews.length === 0 || this.projectConf === null) {
      return
    }
    if (this.kind === 'view') {
      const viewId = window.localStorage.getItem('currentView')
      this.rootViews.forEach((elem, idx) => {
        if (elem._id === viewId) {
          realIndex = idx
        }
      })
      native.setViewComboValue(realIndex)
      item = this.views[viewId]
      store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: item })
    }
    
    if (this.kind === 'page') {
      if (Object.keys(this.pages).length === 0 || this.rootPages.length === 0) {
        return
      }
      this.initPage()
      const id = window.localStorage.getItem('currentView')
      if (Object.keys(this.pages).length) {
        this.rootPages.forEach((elem, idx) => {
          if (elem._id === id) {
            realIndex = idx
          }
        })
      }
      native.setViewComboValue(realIndex)

      if (id) {
        store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: this.pages[id] })
      }
    }
  }

  init () {
    if (!this.sideMenuCurrentItem) {
      return
    }
    if (window.localStorage.getItem('previewKind') === 'view') {
      this.applyView(document.body, this.sideMenuCurrentItem)
    }
    if (window.localStorage.getItem('previewKind') === 'page') {
      this.initPage()
    }
    window.requestAnimationFrame(() => window.$sledge.foundationReInit())
  }

  initPage () {
    let page = this.sideMenuCurrentItem
    if (!page) {
      return
    }
    let views = page.views
    switch (page.controller) {
      case 'SINGLE':
        document.body.innerHTML = '' //if no view : ovoid swript in body 
        this.applyView(document.body, this.views[views[0]])
        break
      case 'STRIP':
        this.applyTrip(views)
        break
      case 'SIDE':
        this.applySide(page)
        break
    }
    renderBackground(document.body, null, null, this.sideMenuCurrentItem.settings || {}, window.localStorage.getItem('currentProject'))
    if (this.sideMenuCurrentItem.settings && this.sideMenuCurrentItem.settings.expanded) {
      $(document.body).find('.row').addClass('expanded')
    }
  }

  applySide (page) {
    let name = (page.local && page.local[window.$sledge.language]) ? decodeURIComponent(page.local[window.$sledge.language]) : page.name
    document.body.innerHTML = `


    <div class="off-canvas-wrapper">
      <div class="off-canvas position-left reveal-for-large" id="offCanvas" data-off-canvas></div>
    
      <div class="off-canvas-content" data-off-canvas-content>
      
        <div data-sticky-container>
          <div class="title-bar hide-for-large" data-sticky data-options="marginTop:0;" style="width:100%">
            <button class="menu-icon" type="button" data-open="offCanvas"></button>
            <span class="title-bar-title">${name}</span>
          </div>
        </div>

        <!-- content --> 
      
      </div>
    </div>`

    if (page.views && page.views.length !== 0) {
      let cont = document.querySelector('.off-canvas-content')
      page.views.forEach(viewRef => {
        this.applyView(cont, this.views[viewRef], true)
      })
    }

    if (page.sideViews && page.sideViews.length !== 0) {
      let sideCont = document.querySelector('.off-canvas')
      page.sideViews.forEach(viewRef => {
        this.applyView(sideCont, this.views[viewRef], true)
      })
    }
  }

  applyTrip (views) {
    if (views.length !== 0) {
      document.body.innerHTML = ''
      views.forEach(viewRef => {
        this.applyView(document.body, this.views[viewRef], true)
      })
    }
  }

  applyView (container, elem, doNotClean) {
    if (!elem) {
      return
    }
    if (!doNotClean) {
      container.innerHTML = ''
    }
    let markup = 'snow here'
    if (elem.structure) {
      markup = decodeURIComponent(elem.structure)
    }
    const view = document.createElement('sg-view')
    container.appendChild(view)
    view.applyMarkup(markup, elem.settings, this.currentProject)
  }
}
