export default (container, projects, current) => {
  let html = ''
  html += `<div class="sd-dash-proj-grid">${renderItem(projects, current)}</div>`
  container.insertAdjacentHTML('beforeend', html)
}

function renderItem (projects, current) {
  let loc = window.sledge.nslocal
  let html = `<div class="sd-dash-proj-grid-add"><div>${loc.new_proj}</div></div>`
  projects.forEach(proj => {
    html += `<div class="sd-dash-proj-grid-item ${current && current === proj ? 'sd-current' : ''}" data-ref="${proj}"><div>${proj}</div></div>`
  })
  return html
}

export function marketingPlace (container, src) {
  container.insertAdjacentHTML('beforeend', `<iframe src="${src}"></iframe>`)
}

export function addPopin (conf) {
  let loc = window.sledge.nslocal 
  let markup = `
  <div class="bal_sc_box">
    <p class="bal_sc_welcome">${conf.title1}</p>
    <p class="bal_sc_about">${conf.title2}</p>
    <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="${conf.placeholder}" class="bal medium bal_sc_text" type="text"/>
  
    <p class="bal_sc_langue">${loc.create_lang}</p>
    <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" id="sd_lang_input_create" />
    </div>`
  return markup
}

export function addPopinStart (conf) {
  let loc = window.sledge.nslocal 
  let markup = `
  <div class="bal_sc_box">
    <p class="bal_sc_welcome">${conf.title1}</p>
    <p class="bal_sc_about">${conf.title2}</p>
    <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="${conf.placeholder}" class="bal medium bal_sc_text" type="text"/>
  
    <p class="bal_sc_langue">${loc.create_lang}</p>
    <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" id="sd_lang_input_create" />
    <br>
    <div style="margin-top:10px;">
      <label style="width:auto;">${loc.start_pop1} <a href="http://sled-app.io/${loc.start_pop3}/Quick_Start.html">${loc.start_pop2}</a></label>
    </div>
    <div style="margin-top:10px;">
      <label style="width:auto;">${loc.start_pop4} <a href="http://sled-app.io/${loc.start_pop3}/terms.html">${loc.start_pop5}</a></label>
      <input id="termsCheck" type="checkbox" style="margin:0;"/>
    </div>
    </div>`
  return markup
}

export function updateMess () {
  let loc = window.sledge.nslocal
  return `<div><h2>${loc.up_dial_title}</h2><button>${loc.up_dial_but}</button></div>
  <div>
  <ul>
    <li>${loc.up_dial_1}</li>
  </ul>
  </div>`
}

//  <a class="sd-update-link" href="http://sled-app.io/${window.sledge.languageCode}/release_note.html">${loc.up_dial_8}</a>
