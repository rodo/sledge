import {remove, domUnwrap} from '../../utilities'
import {connect, store} from '../../store'
import renderGrid, {marketingPlace, addPopin, addPopinStart, updateMess} from './templates'
import config from '../../config'
import * as _langList from '../../../local/languages.js'
import {openLink, getUpdateDialState} from '../../interface/interface_mac'

export default class Dashboard {

  constructor (settings) {
    this.initDone = false
    this.projects = []
    this.edit = false
    this.mainContent = document.querySelector('.sd_dashboard')
    marketingPlace(this.mainContent, config.marketingUrl + window.sledge.languageCode + '.html')
    window.PubSub.subscribe('DELETE_PROJ_PENDING', () => { this.closePopin() })
    this.addListeners()
  }

  addListeners () {
    connect('projects', this, (a) => this.projectsInit(a))
    connect('currentProject', this, (projData) => this.setUI(projData))
    connect('editProject', this, (data) => this.switchUI(data))
  }

  softwareUpdateDial () {
    if (this.projects.length !== 0) {
      let root = document.querySelector('.sd_dashboard')
      let mess = document.createElement('div')
      mess.className = 'sd-update-mess'
      mess.innerHTML = updateMess()
      root.appendChild(mess)
      mess.addEventListener('click', e => {
        if (e.target.nodeName === 'BUTTON') {
          remove('.sd-update-mess')
        }
        if (e.target.nodeName === 'A') {
          e.preventDefault()
          openLink(e.target.href)
        }
      })
    }
  }

  projectsInit (proj) {
    if (proj.length === 0) {
      this.loadAddProjectScreen(true)
    } else {
      if (!this.initDone) {
        this.initDone = true
        getUpdateDialState()

      }
      store.dispatch({type: 'PENDING_PROJECT', pendingProject: null})
      store.dispatch({ type: 'CURRENT_PROJECT', currentProject: null})
      store.dispatch({type: 'PENDING_PROJECT', pendingProject: proj[0]})
    }
  }

  setUI (projName) {
    if (projName) {
      this.closePopin()
    }
    if (!projName) {
      return
    }
    remove('.sd-dash-proj-grid')
    renderGrid(this.mainContent, this.projects, this.currentProject)
    let loc = window.sledge.nslocal

    document.querySelector('.sd-action-edit-proj').innerHTML = loc.gen_edit + ' ' + this.currentProject
    document.querySelector('.sd-action-del-proj').innerHTML = loc.gen_del + ' ' + this.currentProject
    domUnwrap('.sd-action-arch-proj', elem => {
      elem.innerHTML = loc.gen_arch + ' ' + this.currentProject
    })

    document.querySelector('.sd-dash-proj-grid').addEventListener('click', e => {
      switch (true) {
        case e.target.classList.contains('sd-dash-proj-grid-item'):
          store.dispatch({ type: 'PENDING_PROJECT', pendingProject: e.target.dataset.ref })
          break
        case e.target.classList.contains('sd-dash-proj-grid-add'):
          sledge.dashboard.loadAddProjectScreen()
          break
        default:
      }
    })

    document.querySelector('.sd-dash-proj-grid').addEventListener('dblclick', e => {
      switch (true) {
        case e.target.classList.contains('sd-dash-proj-grid-item'):
          if (e.target.classList.contains('sd-current')) {
            store.dispatch({ type: 'EDIT_PROJECT', editProject: true })
          }
          break
        default:
      }
    })
  }

  switchUI (edit) {
    if (this.edit === edit) {
      //return
    }
    if (edit) {
      this.hide()
    } else {
      this.show()
    }
  }

  hide () {
    this.mainContent.classList.add('sd_hiden')
  }

  show () {
    this.mainContent.classList.remove('sd_hiden')
  }

  closePopin () {
    if (this.addProjectPopin) {
      this.createProjectPanding = false
      this.addProjectPopin.hide()
      window.setTimeout(() => {
        remove(this.addProjectPopin)
        this.addProjectPopin = null
      }, 200)
    }
  }

  loadAddProjectScreen (first) {
    if (!this.createProjectPanding) {
      this.createProjectPanding = true
    } else {
      return
    }

    let conf
    let data
    let loc = window.sledge.nslocal
    if (first) {
      conf = [{
        name: 'create',
        className: 'sd-pop-create',
        label: loc.gen_create,
        action: function () {
          let input = document.getElementById('sd_lang_input_create')
          if (window.sledge.projects.add(text.value, this.listRef[input.value])) {
            this.addProjectPopin.hide()
            this.createProjectPanding = false
            window.setTimeout(() => {
              remove(this.addProjectPopin)
              this.addProjectPopin = null
            }, 200)
          } else {
            text.value = ''
            console.error('@todo: dashboard ; handle error')
          }
        }
      }]

      data = {
        title1: loc.pop_welcome,
        title2: loc.pop_welcome_about,
        placeholder: loc.pop_placeholder
      }
    } else {
      conf = [{
        name: 'cancel',
        className: 'sd-pop-cancel',
        label: loc.gen_cancel,
        action: this.closePopin
      },
      {
        name: 'create',
        className: 'sd-pop-create',
        label: loc.gen_create,
        action: () => {
          let input = document.getElementById('sd_lang_input_create')
          if (window.sledge.projects.add(text.value, this.listRef[input.value])) {
            this.createProjectPanding = false
            this.addProjectPopin.hide()
            window.setTimeout(() => {
              remove(this.addProjectPopin)
              this.addProjectPopin = null
            }, 200)
          } else {
            text.value = ''
            console.error('@todo: dashboard ; handle error')
          }
        }
      }
      ]

      data = {
        title1: loc.pop_add_new,
        title2: loc.pop_add_about,
        placeholder: loc.pop_placeholder
      }
    }

    this.addProjectPopin = document.createElement('sdg-popin')
    this.addProjectPopin.init(conf, this)
    if (first) {
      this.addProjectPopin.setAttribute('height', 350)
      this.addProjectPopin.addContent(addPopinStart(data))
    } else {
      this.addProjectPopin.setAttribute('height', 300)
      this.addProjectPopin.addContent(addPopin(data))
    }
    this.addProjectPopin.create.disabled = true
    document.body.appendChild(this.addProjectPopin)

    this.nameValid = false
    this.languageValid = false
    this.initLanguageSelect()

    const text = this.addProjectPopin.querySelector('.bal_sc_text')
    const textAbout = this.addProjectPopin.querySelector('.bal_sc_about')
    text.focus()
    text.onkeyup = e => {
      if (!e.target.value || e.target.value === '') {
        textAbout.innerText = loc.pop_add_about
        this.addProjectPopin.create.disabled = true
        this.nameValid = false
      } else {
        let canuse = true
        // exist or not
        this.projects.forEach(element => {
          if (element === e.target.value) {
            textAbout.innerHTML = loc.pop_add_exist
            canuse = false
          }
        })

        let newName = e.target.value
        newName = newName.replace(/[`œ~!@#$%^&*()|+\=÷¿?;:'",.<>\{\}\[\]\\\/]/gi, '')
        newName = newName.replace(/ /g,'_')
        newName = newName.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
        newName = newName.toLowerCase()

        if (!isNaN(parseInt(newName[0]))) {
          newName = 'myProject_' + newName
        }

        if (newName[0] === '_' || newName[0] === '-') {
          newName = 'myProject_' + newName
        }

        if (newName !== e.target.value) {
          textAbout.innerHTML = `${loc.pop_add_car} <i>${newName}</i> ${loc.pop_add_car2}`
          canuse = false
        }
        
        if (canuse) {
          this.nameValid = true
          textAbout.innerHTML = loc.pop_add_valid
        } else {
          this.nameValid = false
        }

        let termsCheck = true
        const terms = document.getElementById('termsCheck')
        if (terms) {
          termsCheck = terms.checked
        }
        this.addProjectPopin.create.disabled = !(termsCheck & canuse && this.languageValid)
      }
    }
    const termsElem = document.getElementById('termsCheck')
    if (termsElem) {
      termsElem.addEventListener('change', e => {
        this.addProjectPopin.create.disabled = !(e.target.checked && this.nameValid && this.languageValid)
      })
    }
    this.addProjectPopin.addEventListener('click', e => {
      if (e.target.nodeName === 'A') {
        e.preventDefault()
        openLink(e.target.href)
      }
    })
    this.addProjectPopin.show()
  }

  initLanguageSelect () {
    let loc = window.sledge.nslocal    
    let input = document.getElementById('sd_lang_input_create')
    let para = document.querySelector('.bal_sc_langue')
    let elem = []
    this.listRef = {}
    let temp = null
    let list = _langList[window.nslocal || 'en']
    Object.keys(list).forEach(d => {
      if (d === window.nslocal) {
        temp = list[d]
      }
      elem.push(list[d])
    })
    Object.keys(list).forEach(d => {
      this.listRef[list[d]] = d
    })

    if (temp) {
      this.languageValid = true
      input.value = temp
    }

    new Awesomplete(input, {
      list: elem
    })

    input.addEventListener('keyup', e => {
      let termsCheck = true
      const terms = document.getElementById('termsCheck')
      if (terms) {
        termsCheck = terms.checked
      }
      if (this.isValid(input.value)) {
        this.languageValid = true
        para.innerHTML = loc.create_lang
        this.addProjectPopin.create.disabled = !(termsCheck && this.nameValid && this.languageValid)
      } else {
        this.languageValid = false
        para.innerHTML = loc.create_lang_no
        this.addProjectPopin.create.disabled = true
      }
    })
  }

  isValid (value) {
    return this.listRef[value]
  }
}
