import {connect} from '../../store'
import render from './template'
import {remove} from '../../utilities'
import _conf from '../../config'
import {setProjConf} from '../../interface/interface_mac'

export default class ComponentsCatalog {

  constructor (settings) {
    this.nonePlusList = [
      'links-list',
      'image-full',
      'image',
      'footer-social',
      'footer-simplemap',
      'video-link',
      'simple-card',
      'thumbnail',
      'link-dashboard',
      'text',
      'space',
      'title',
      'header',
      'simple-header',
      'default-audio',
      'progress',
      'image-link'
    ]

    connect('iapPlus', this)
    connect('currentProject', this)
    connect('projectConf', this, e => {
      if (this.sideMenuCurrentItem === 'componentsCatalog') {
        this.apply()
      }
    })
    connect('localPublishPath', this, (p) => this.apply(p))
    connect('sideMenuCurrentItem', this, obj => {
      if (obj === 'componentsCatalog') {
        this.apply()
      } else {
        this.hide()
      }
    })
  }

  updatePathState (path) {
    //document.querySelector('.sg-publish-path').innerText = path
    //document.querySelector('.sg-publish-action').style.display = 'block'
  }

  apply () {
    if (this.sideMenuCurrentItem !== 'componentsCatalog') {
      return
    }
    this.fullcomplist = []
    this.complist = []
    const cont = document.querySelector('.sd_editor_work')
    remove(document.querySelector('.sd-componentsCatalog'))
    remove(document.querySelector('.sd-componentsCatalog-more'))

    _conf.components.forEach(item => { this.addItem(item) })

    this.currentList =
    (this.projectConf.components && this.projectConf.components.length !== 0)
    ? this.projectConf.components
    : _conf.defaultComponentsList

    this.currentList.forEach((compName) => {
      let data = require(`../../../framework/components/${compName}/package.json`)
      this.complist.push({
        name: data.name[window.sledge.languageCode],
        about: data.description[window.sledge.languageCode],
        ref: data.ref
      })
    })
    this.complist.sort((a, b) => {
      return a.name.localeCompare(b.name)
    })
    render(cont, this.fullcomplist, this.complist, this.currentList, this.iapPlus)
    
    document.querySelector('.sd-componentsCatalog').addEventListener('click', e => {
      let ref = e.target.dataset.ref
      switch (e.target.dataset.kind) {
        case 'rm':
          this.currentList.splice(this.currentList.indexOf(ref), 1)
          this.projectConf.components = this.currentList
          setProjConf(this.currentProject, this.projectConf)          
          break
        case 'add':
          this.currentList.push(ref)
          this.projectConf.components = this.currentList
          setProjConf(this.currentProject, this.projectConf)
          break
        case 'more':
          let pan = document.querySelector('.sd-componentsCatalog-more')
          pan.classList.add('sd-visible')
          break
        default:
          break
      }
    })
    document.querySelector('.sd-componentsCatalog-more').addEventListener('click', e => {
      let panc = document.querySelector('.sd-componentsCatalog-more')
      panc.classList.remove('sd-visible')
    })
  }

  addItem (item) {
    if (!this.iapPlus) {
      if (!this.nonePlusList.includes(item.ref)) {
        return
      }
    }
    const ref = item.ref
    const name = item.name[window.sledge.languageCode]
    const about = item.description[window.sledge.languageCode]
    const nodeName = item.nodeName
    this.fullcomplist.push({ref, name, about, nodeName, category: item.category})
  }

  hide () {
    remove(document.querySelector('.sd-componentsCatalog'))
  }

}
