export default (container, fullcomplist, complist, rootDefaultList, isPlus) => {
  let loc = window.sledge.nslocal
  let html = `
  <div class="sd-componentsCatalog-more">
    <button data-kind="close">${loc.action_close}</button>
  </div>  
  <div class="sd-componentsCatalog">
    <h1>${loc.gen_componentsCatalog}</h1>
    <div class="sd-cc-box">
      <h3>${loc.compcat_current} :</h3>
      <ul id="sd-cl-current">
        ${getCurrentComponents(complist)}
      </ul>
    </div>
    
    <div class="sd-cc-box">
      <h3>${loc.compcat_header}</h3>
      <ul id="sd-cl-header">
        ${getComponentsByCat('header', fullcomplist, complist, rootDefaultList)}
      </ul>
    </div>

    <div class="sd-cc-box">
      <h3>${loc.comp_type_nav}</h3>
      <ul id="sd-cl-txt">
        ${getComponentsByCat('link', fullcomplist, complist, rootDefaultList)}
      </ul>
    </div>

    <div class="sd-cc-box">
    <h3>${loc.comp_type_meters}</h3>
    <ul id="sd-cl-txt">
      ${getComponentsByCat('meters', fullcomplist, complist, rootDefaultList)}
    </ul>
  </div>

    ${isPlus
    ? `<div class="sd-cc-box">
        <h3>${loc.comp_type_testi}</h3>
        <ul id="sd-cl-txt">
          ${getComponentsByCat('testimonial', fullcomplist, complist, rootDefaultList)}
        </ul>
      </div>`
    : ''
    }

      <div class="sd-cc-box">
        <h3>${loc.comp_type_audio}</h3>
        <ul id="sd-cl-txt">
          ${getComponentsByCat('audio', fullcomplist, complist, rootDefaultList)}
        </ul>
      </div>

    <div class="sd-cc-box">
      <h3>${loc.comp_type_footer}</h3>
      <ul id="sd-cl-txt">
        ${getComponentsByCat('footer', fullcomplist, complist, rootDefaultList)}
      </ul>
    </div>

    ${isPlus
      ? `<div class="sd-cc-box">
          <h3>${loc.comp_type_form}</h3>
          <ul id="sd-cl-txt">
            ${getComponentsByCat('form', fullcomplist, complist, rootDefaultList)}
          </ul>
        </div>`
      : ''
    }
    
    <div class="sd-cc-box">
      <h3>${loc.compcat_txt}</h3>
      <ul id="sd-cl-txt">
        ${getComponentsByCat('text', fullcomplist, complist, rootDefaultList)}
      </ul>
    </div>

    ${isPlus
      ? `<div class="sd-cc-box">
          <h3>${loc.comp_type_map}</h3>
          <ul id="sd-cl-txt">
            ${getComponentsByCat('map', fullcomplist, complist, rootDefaultList)}
          </ul>
        </div>`
      : ''
    }

    <div class="sd-cc-box">
    <h3>${loc.comp_type_img}</h3>
    <ul id="sd-cl-header">
      ${getComponentsByCat('image', fullcomplist, complist, rootDefaultList)}
    </ul>
    </div>

    <div class="sd-cc-box">
    <h3>${loc.comp_type_vid}</h3>
    <ul id="sd-cl-header">
      ${getComponentsByCat('video', fullcomplist, complist, rootDefaultList)}
    </ul>
    </div>

    <div class="sd-cc-box">
    <h3>${loc.comp_type_card}</h3>
    <ul id="sd-cl-header">
      ${getComponentsByCat('card', fullcomplist, complist, rootDefaultList)}
    </ul>
    </div>

    ${isPlus
      ? `<div class="sd-cc-box">
        <h3>${loc.comp_type_pres}</h3>
        <ul id="sd-cl-header">
          ${getComponentsByCat('marketing', fullcomplist, complist, rootDefaultList)}
        </ul>
        </div>`
      : ''
    }

    <div class="sd-cc-box">
    <h3>${loc.compcat_ly}</h3>
    <ul id="sd-cl-txt">
      ${getComponentsByCat('layout', fullcomplist, complist, rootDefaultList)}
    </ul>
  </div>
  </div>`
  container.insertAdjacentHTML('beforeend', html)
}

/*function refreshCurrent (fullcomplist, complist, rootDefaultList) {
  document.getElementById('sd-cl-current').innerHTML = getCurrentComponents(complist)
}*/

function getCurrentComponents (list) {
  let markup = ''
  let loc = window.sledge.nslocal
  list.forEach(item => {
    console.info(item)
    markup += `
      <li>
        <div class="sd-cc-img" style="background-image:url(./framework/components/${item.ref}/img/cover.png);"></div>
        <div class="sd-cc-title sd-cc-added">${item.name}</div>
        <div class="sd-cc-about">${item.about}</div>
        <button data-ref="${item.ref}" data-kind="rm" class="sd-cc-action sd-cc-cancel" title="${loc.compcat_rm}"></button>
      </li>
    `
    // <a href="#" data-kind="more">Learn more</a>
  })
  return markup
}

function getComponentsByCat (cat, list, current, rootDefaultList) {
  let markup = ''
  let loc = window.sledge.nslocal
  list.forEach(item => {
    if (item.category === cat) {
      let isCurrent = rootDefaultList.indexOf(item.ref) !== -1
      markup += `
        <li>
          <div class="sd-cc-img" style="background-image:url(./framework/components/${item.ref}/img/cover.png);"></div>
          <div class="sd-cc-title ${isCurrent ? 'sd-cc-added' : ''}">${item.name}</div>
          <div class="sd-cc-about">${item.about}</div>
          ${isCurrent
            ? `<button data-ref="${item.ref}" data-kind="rm" class="sd-cc-action sd-cc-cancel" title="${loc.compcat_rm}"></button`
            : `<button data-ref="${item.ref}" data-kind="add" class="sd-cc-action sd-cc-add" title="${loc.compcat_add}"></button`
          }
        </li>
      `
    }
  })
  return markup
}
