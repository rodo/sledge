import {connect} from '../../store'
import render from './template'
import {remove} from '../../utilities'
import * as _langList from '../../../local/languages.js'
import {setLanguages} from '../../interface/interface_mac'

export default class Langagues {

  constructor (settings) {
    this.langList = _langList[window.nslocal || 'en']  
    connect('currentProject', this)
    connect('projectConf', this)
    connect('languages', this, () => {
      if (this.sideMenuCurrentItem === 'languages') {
        this.apply()
      }
    })
    // connect('localPublishPath', this, (p) => this.apply(p))
    connect('sideMenuCurrentItem', this, obj => {
      if (obj === 'languages') {
        this.apply()
      } else {
        this.hide()
      }
    })
  }

  apply () {
    if (this.sideMenuCurrentItem !== 'languages') {
      return
    }
    const cont = document.querySelector('.sd_editor_work')
    remove(document.querySelector('.sd-languages'))
    render(this, cont, _langList[window.nslocal || 'en'], this.languages)
    const obj = document.querySelector('.sd-languages')
    obj.addEventListener('click', e => {
      if (e.target && e.target.dataset && e.target.dataset.action === 'remove') {
        this.removeLanguage(e.target.parentNode.dataset.code)
      }
    })
    obj.addEventListener('change', e => {
      this.update(e)
    })
  }

  update (e) {
    
    switch (e.target.dataset.kind) {
      case 'title':
        this.languages.title = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        setLanguages(this.currentProject, this.languages)
        break
      case 'keywords':
        this.languages.keywords = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        setLanguages(this.currentProject, this.languages)
        break
      case 'desc':
        this.languages.desc = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        setLanguages(this.currentProject, this.languages)
        break
      case 'author':
        this.languages.author = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        setLanguages(this.currentProject, this.languages)
        break
    }
  }

  hide () {
    remove(document.querySelector('.sd-languages'))
  }

  addLanguage (code) {
    this.languages.list.push(code)
    setLanguages(this.currentProject, this.languages)
  }

  removeLanguage (code) {
    let index = this.languages.list.indexOf(code)
    this.languages.list.splice(index, 1)
    setLanguages(this.currentProject, this.languages)
  }
}
