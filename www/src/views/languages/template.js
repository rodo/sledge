export default (ctrl, container, list, current) => {
  let loc = window.sledge.nslocal
  let title = current.title ? window.decodeURIComponent(current.title) : ''
  let keywords = current.keywords ? window.decodeURIComponent(current.keywords) : ''
  let desc = current.desc ? window.decodeURIComponent(current.desc) : ''
  let author = current.author ? window.decodeURIComponent(current.author) : ''

  let html = `
  <div class="sd-languages">
    <h1>${loc.gen_languages}</h1>
    <div class="sd-languages-cont">
      <div class="sd-languages-list">
        <h3>${loc.ls_curlanguages}</h3>
        <ul>${getCurrentList(list, current)}</ul>
      </div>
      <div class="sd-languages-add">
        <label>${loc.ls_chooselanguages} :</label>
        <input id="sd_lang_input" /><button id="sd_lang_add">${loc.ls_addlanguages}</button>
      </div>
    </div>
    <div class="sd-languages-settings">
      <h3>${loc.gen_language_set}</h3>
      <p><label>${loc.menu_prop_title}</label>
      <input data-kind="title" name="local" type="text" value="${title}"/>
      </p>

      <p><label>${loc.ctrl_key} (${window.$sledge.language})</label>
      <input data-kind="keywords" name="keywords" type="text" value="${keywords}"/>
      </p>

      <p><label>${loc.ctrl_desc} (${window.$sledge.language})</label>
      <textarea data-kind="desc" name="description" rows="5" cols="50">${desc}</textarea>
      </p>

      <p><label>${loc.ctrl_author}</label>
      <input name="author" data-kind="author" type="text" value="${author}"/>
      </p>
    </div>
  </div>`
  container.insertAdjacentHTML('beforeend', html)
  let input = document.getElementById('sd_lang_input')
  let elem = []
  let listRef = {}
  Object.keys(list).forEach(d => {
    if (current.list.indexOf(d) === -1) {
      elem.push(list[d])
    }
  })
  Object.keys(list).forEach(d => {
    listRef[list[d]] = d
  })  
  let ac = new Awesomplete(input, {
    list: elem
  })
  let addButton = document.getElementById('sd_lang_add')
  input.addEventListener('keyup', e => {
    if (isValid(input.value, listRef)) {
      addButton.style.display = 'block'
    } else {
      addButton.style.display = 'none'
    }
  })
  input.addEventListener('awesomplete-selectcomplete', e => {
    addButton.style.display = 'block'
  })
  addButton.addEventListener('click', e => {
    ctrl.addLanguage(listRef[input.value])
  })
}

function isValid (value, listRef) {
  return listRef[value]
}

function getCurrentList (list, current) {
  let markup = ''
  current.list.forEach(code => {
    markup += `<li data-code="${code}">${list[code]} ${current.list.length === 1 ? '' : `<button data-action='remove'></button>`}</li>`
  })
  return markup
}

