import conf from '../../config'

export default (container, themeRef, themes, isPlus) => {
  let loc = window.sledge.nslocal
  let html = `<div class="sd-settings-theme">`
  html += `<h1 class="sd-settings-title">${loc.gen_theme}</h1>`
  let current = getCurrent(themes, themeRef)

  if (Object.keys(themes).length === 0) {
    html += `<div class="sd-settings-error">${loc.the_no}. <button data-kind="rt">${loc.the_rt}</button></div>`
  } else {
    // html += `<div class="sd-settings-about">${loc.the_about}</div>`
    html += `<div class="sd-cc-box" style="max-height:300px;">
    <h3>${loc.the_title_default}</h3>
    <ul id="sd-cl-current">
       <li class="sd-settings-list-item sd-settings-current">
          <div class="sd-settings-list-item-title">${current[0].name}</div>
          <iframe class="sd-settings-list-item-preview" src="${conf.themeRollerUrl + themeRef}/preview.html"></iframe>
          <div class="sd-settings-list-item-about">${current[0] && current[0].description && current[0].description[window.sledge.languageCode] ? current[0].description[window.sledge.languageCode] : '?'}</div>
          <div class="sd-settings-list-item-actions">${loc.the_current}</div>
        </li>
    </ul>
    </div>`

    html += `<div class="sd-cc-box" style="max-height:300px;">
    
    <h3>${loc.theme_blue}</h3>
    <ul id="sd-cl-current">
      ${getThemeByCat(themes, themeRef, 'blue')}
    </ul>
    </div>

    ${
      isPlus
      ? `<div class="sd-cc-box" style="max-height:300px;">
        <h3>${loc.theme_red}</h3>
        <ul id="sd-cl-current">
          ${getThemeByCat(themes, themeRef, 'red')}
        </ul>
        </div>`
      : ''
    }
  
    ${
      isPlus
      ? `<div class="sd-cc-box" style="max-height:300px;">
        <h3>${loc.theme_green}</h3>
        <ul id="sd-cl-current">
          ${getThemeByCat(themes, themeRef, 'green')}
        </ul>
        </div>`
      : ''
    }

    <div class="sd-cc-box" style="max-height:300px;">
    <h3>${loc.theme_grey}</h3>
    <ul id="sd-cl-current">
      ${getThemeByCat(themes, themeRef, 'grey')}
    </ul>
    </div>

    ${
      isPlus
      ? `<div class="sd-cc-box" style="max-height:300px;">
      <h3>${loc.theme_purple}</h3>
      <ul id="sd-cl-current">
        ${getThemeByCat(themes, themeRef, 'purple')}
      </ul>
      </div>`
      : ''
    }
    
    ${
      isPlus
      ? `<div class="sd-cc-box" style="max-height:300px;">
      <h3>${loc.theme_orange}</h3>
      <ul id="sd-cl-current">
        ${getThemeByCat(themes, themeRef, 'orange')}
      </ul>
      </div>`
      : ''
    }
    
    ${
      isPlus
      ? `<div class="sd-cc-box" style="max-height:300px;">
      <h3>${loc.theme_yellow}</h3>
      <ul id="sd-cl-current">
        ${getThemeByCat(themes, themeRef, 'yellow')}
      </ul>
      </div>`
      : ''
    }
    ${
      isPlus
      ? `<div class="sd-cc-box" style="max-height:300px;">
      <h3>${loc.theme_pink}</h3>
      <ul id="sd-cl-current">
        ${getThemeByCat(themes, themeRef, 'pink')}
      </ul>
      </div>`
      : ''
    }
    `
  }
  html += '</div>'
  container.insertAdjacentHTML('beforeend', html)
}

function getCurrent (themes, current) {
  return themes.filter(item => item.ref === current)
}

function getAllTheme (themes, current) {
  let html = ''
  let loc = window.sledge.nslocal
  themes.forEach(theme => {
    if (theme.ref !== current) {
      html +=
      `<li class="sd-settings-list-item ${theme.ref}">
        <div class="sd-settings-list-item-title">${theme.name}</div>
        <iframe class="sd-settings-list-item-preview" src="${conf.themeRollerUrl + theme.ref}/preview.html"></iframe>
        <div class="sd-settings-list-item-about">${theme.description[window.sledge.languageCode]}</div>
        <div class="sd-settings-list-item-actions">
          <button class="sd-settings-list-item-dl" data-ref="${theme.ref}">${loc.the_use}</button>
        </div>
      </li>`
    }
  })
}

function getThemeByCat (themes, current, cat, isPlus) {
  let html = ''
  let loc = window.sledge.nslocal
  themes.forEach(theme => {
    if (theme.ref !== current && cat === theme.category) {
        html +=
        `<li class="sd-settings-list-item ${theme.ref}">
          <div class="sd-settings-list-item-title">${theme.name}</div>
          <iframe class="sd-settings-list-item-preview" src="${conf.themeRollerUrl + theme.ref}/preview.html"></iframe>
          <div class="sd-settings-list-item-about">${theme.description[window.sledge.languageCode]}</div>
          <div class="sd-settings-list-item-actions">
            <button class="sd-settings-list-item-dl" data-ref="${theme.ref}">${loc.the_use}</button>
          </div>
        </li>`
    }
  })
  console.info(html)
  return html
}
