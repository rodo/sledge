import {connect, store} from '../../store'
import render from './template'
import {remove} from '../../utilities'
import {downloadTheme, removeTheme} from '../../interface/interface_mac'

export default class Settings {

  constructor (settings) {
    connect('iapPlus', this)
    connect('currentProject', this)
    connect('themes', this, () => this.apply())
    connect('currentTheme', this, () => this.apply())
    connect('sideMenuCurrentItem', this, obj => {
      if (obj === 'theme') {
        this.apply()
      } else {
        this.hide()
      }
    })
  }

  apply () {
    if (this.sideMenuCurrentItem !== 'theme') {
      return
    }
    const cont = document.querySelector('.sd_editor_work')
    remove(document.querySelector('.sd-settings-theme'))
    render(cont, this.currentTheme, require(`../../themes.json`), this.iapPlus)
    window.requestAnimationFrame(() => {
      document.querySelector('.sd-settings-theme').addEventListener('click', e => {
        switch (true) {
          case e.target.dataset.kind = 'rt':
            // getThemeConf()
            break
          case e.target.classList.contains('sd-settings-list-item-dl'):
            if (e.target.dataset.ref === 'default') {
              removeTheme(this.currentProject, this.currentTheme)
              store.dispatch({ type: 'SET_CURRENT_THEME', themeref: 'default' })
            } else {
              document.querySelector('.sd-settings-theme').classList.add('sd-pending')
              // sending a message to the action bar
              downloadTheme(this.currentProject, e.target.dataset.ref)
            }
            break
          default:
            //
        }
      })
    })
  }

  hide () {
    remove(document.querySelector('.sd-settings-theme'))
  }

}
