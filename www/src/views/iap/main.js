import {connect, store} from '../../store'
import render, {renderSuccess} from './template'
import {remove, setActionMask} from '../../utilities'
import {requestStore, buyProduct, restoreProduct, getSledgePlus, openLink} from '../../interface/interface_mac'

export default class Iap {

  constructor () {
    this.cont = null
    connect('iapState', this, s => this.apply())
    connect('iapStoreData', this, d => this.storeData())
  }

  apply () {
    switch (this.iapState) {
      case 0:
        remove(this.cont)
        this.cont = null
        break
      case 1:
        this.cont = document.createElement('div')
        this.cont.className = 'sd-iap-store'
        document.body.appendChild(this.cont)
        render(this.cont)
        requestStore()
        this.addEvents()
        this.cont.classList.add('sd-visible')
        break
      case 2:
        setActionMask('iap_am_1')
        break
      case 3:
        this.cont.querySelector('.sd-iap-restore').style.display = 'none'
        this.cont.querySelector('.sd-iap-buy').style.display = 'none'
        getSledgePlus()
        renderSuccess(this.cont)
        break
      case 4:
        setActionMask('iap_am_2')
        break
    }
  }

  storeData () {
    let loc = window.sledge.nslocal
    if (this.iapStoreData !== null) {
      remove('.sd-iap-load')
      this.cont.querySelector('.sd-iap-restore').style.display = 'block'
      let buy = this.cont.querySelector('.sd-iap-buy')
      buy.innerHTML = loc.iap_buynow + ' ' + this.iapStoreData.price
      buy.style.display = 'block'
    }
  }

  addEvents () {
    this.cont.addEventListener('click', e => {
      switch (e.target.dataset.kind) {
        case 'close':
          store.dispatch({type: 'IAP_STATE', state: 0})
          store.dispatch({ type: 'IAP_DATA', data: null })
          break
        case 'buy':
          buyProduct()
          store.dispatch({type: 'IAP_STATE', state: 2})
          break
        case 'restore':
          restoreProduct()
          store.dispatch({type: 'IAP_STATE', state: 4})
          break
        default:
          if (e.target.nodeName === 'A') {
            e.preventDefault()
            openLink(e.target.href)
          }
      }
    })
  }
}
