export default function (cont) {
  let loc = window.sledge.nslocal  
  let markup = `
  <div class="sd-iap-main">
    ${getSledgePlus()}
  </div>
  <footer>
    <div class="sd-iap-load">
      ${loc.iap_loading}
      <div class="sd-iap-spinner"></div>
    </div>
    <button data-kind="buy" class="sd-iap-buy" style="display:none;"></button>
    <button data-kind="restore" class="sd-iap-restore" style="display:none;">${loc.iap_restore}</button>
    <button data-kind="close" class="sd-iap-close">${loc.iap_close}</button>
  </footer>`
  cont.innerHTML = markup
}

function getSledgePlus () {
  let loc = window.sledge.nslocal
  return `<div class="sd-iap-store-header">
    <h1>${loc.iap_page_go}</h1>
  </div>

  <div class="sd-iap-store-section-wrapper">
    <div class="sd-iap-store-section sd-iap-store-flag-section">
      <div class="sd-iap-store-section-l">
        <h2>${loc.iap_page_mutli_h2}</h2>
        <p>${loc.iap_page_mutli_p} <a href="http://sled-app.io/${loc.start_pop3}/doc-languages.html">${loc.iap_page_learn}</a></p>
      </div>
      <div class="sd-iap-store-section-r sd-iap-store-flag"></div>
    </div>

    <div class="sd-iap-store-section sd-iap-store-sitemap-section">
      <div class="sd-iap-store-section-l sd-iap-store-sitemap"></div>
      <div class="sd-iap-store-section-r">
        <h2>${loc.iap_page_smap_h2}</h2>
        <p>${loc.iap_page_smap_p} <a href="http://sled-app.io/${loc.start_pop3}/doc-sitemap.html">${loc.iap_page_learn}</a></p>
      </div>
    </div>

    <div class="sd-iap-store-section sd-iap-store-section-full sd-iap-store-archive-section">
      <h2>${loc.iap_page_arch_h2}</h2>
      <p>${loc.iap_page_arch_p}</p>
    </div>

    <div class="sd-iap-store-section">
      <div class="sd-iap-store-section-l">
        <h2>${loc.iap_page_comp_h2}</h2>
        <p>${loc.iap_page_comp_p} <a href="http://sled-app.io/${loc.start_pop3}/sledgeplus_components.html">${loc.iap_page_learn}</a></p>
      </div>
      <div class="sd-iap-store-section-r sd-iap-store-component"></div>
    </div>

    <div class="sd-iap-store-section sd-iap-section-theme">
      <div class="sd-iap-store-section-l sd-iap-store-theme"></div>
      <div class="sd-iap-store-section-r">
      <h2>${loc.iap_page_theme_h2}</h2>
      <p>${loc.iap_page_theme_p}</p>
      </div>
    </div>

    <div class="sd-iap-store-section sd-iap-store-more-section">
      <h2>${loc.iap_page_more_h2}</h2>
      <p>${loc.iap_page_more_p1}</p>
      <p>${loc.iap_page_more_p3}</p>
      <p>${loc.iap_page_more_p2}</p>
      <p>${loc.iap_page_more_p4}</p>
      <p><a href="http://sled-app.io/${loc.start_pop3}/SledPlus.html">${loc.iap_about_learn}</a></p>
    </div>
    
  </div> 
  `
}

export function renderSuccess (cont) {
  let loc = window.sledge.nslocal
  let markup = `<div class="sd-iap-success-wrapper"><div class="sd-iap-success-box"><h1>${loc.iap_success_thx}</h1> <p>${loc.iap_success_p}</p></div></div>`
  cont.querySelector('.sd-iap-main').innerHTML = markup
}
