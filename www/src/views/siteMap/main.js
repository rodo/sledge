import {connect} from '../../store'
import render, {getForm} from './template'
import {remove, guidGenerator} from '../../utilities'
import * as _langList from '../../../local/languages.js'
import {setProjConf} from '../../interface/interface_mac'

export default class Settings {

  constructor () {
    connect('currentProject', this)
    connect('projectConf', this)
    connect('languages', this)
    connect('rootPages', this, a => {
      this.update()
    })
    connect('pages', this)
    connect('sideMenuCurrentItem', this, obj => {
      if (obj === 'siteMap') {
        this.apply()
      } else {
        this.hide()
      }
    })
  }

  apply () {
    if (this.sideMenuCurrentItem !== 'siteMap') {
      return
    }
    if (!this.currentLanguage) {
      this.currentLanguage = this.languages.list[0]
    }
    let sortedPages = this.rootPages.slice(0)
    sortedPages.sort((a, b) => {
      return a.name.localeCompare(b.name)
    })
    let data = []

    if (this.projectConf.siteMap) {
      data = JSON.parse(this.projectConf.siteMap)
    } else {
      sortedPages.forEach(page => {
        let localName =
        (page.local && page.local[this.currentLanguage])
        ? window.decodeURIComponent(page.local[this.currentLanguage])
        : page.name
        data.push({name: localName, pageName: page.name, id: page._id, kind: 'page'})
      })
    }

    const cont = document.querySelector('.sd_editor_work')
    remove(document.querySelector('.sd-siteMap'))
    render(cont, this.currentLanguage, _langList[window.nslocal || 'en'])
    const siteMapCont = document.querySelector('.sd-siteMap')    
    // doc: https://mbraak.github.io/jqTree/#functions-appendnode
    let nodeToRm = []
    let pageAdded = []
    window.requestAnimationFrame(() => {
      let $tree = $('#sd-site-map-tree')

      // init
      $tree.bind(
        'tree.init',
        () => {
          // remove if needed
          if (nodeToRm.length !== 0) {
            nodeToRm.forEach(nodeid => {
              let torm = $tree.tree('getNodeById', nodeid)
              $tree.tree('removeNode', torm)
            })
          }
          // add if needed
          this.rootPages.forEach(page => {
            if (pageAdded.indexOf(page._id) === -1) {
              $tree.tree(
                'appendNode',
                {
                  name: page.name,
                  kind: 'page',
                  id: page._id
                }
              )
            }
          })
          if (this.projectConf.siteMap) {
            this.saveMap()
          }
          $tree.tree('reload')
        }
      )
      
      // init tree
      $tree.tree({
        data: data,
        selectable: true,
        dragAndDrop: true,
        onCanMoveTo: (movedNode, targetNode, position) => {
          if (movedNode.kind === 'page' && targetNode.kind === 'page') {
            if (position === 'after') {
              return true
            }
            if (position === 'before') {
              return true
            }
            if (position === 'inside') {
              return false
            }
          } else if (movedNode.kind === 'menu' && targetNode.kind === 'page') {
              if (position === 'after') {
                return true
              }
              if (position === 'before') {
                return true
              }
              if (position === 'inside') {
                return false
              }
          } else if (movedNode.kind === 'menu' && targetNode.kind === 'menu') {
            return true
          } else if (movedNode.kind === 'page' && targetNode.kind === 'menu') {
            return true
          } else {
            return true
          }
        },
        onCreateLi: (node, $li) => {
          if (node.kind === 'menu') {
            $li.find('.jqtree-element').addClass('sd-side-map-menu')
            let localN =
            (node.local && node.local[this.currentLanguage])
            ? window.decodeURIComponent(node.local[this.currentLanguage])
            : null
            if (localN) {
              $li.find('span').html(localN)
            }
          } else {
            let pageData = this.pages[node.id]
            if (!pageData) {
              nodeToRm.push(node.id)
            } else {
              pageAdded.push(node.id)
              let pageLocalN =
              (pageData.local && pageData.local[this.currentLanguage])
              ? window.decodeURIComponent(pageData.local[this.currentLanguage])
              : pageData.name
              $li.find('span').html(pageLocalN)
            }
          }
        }
      })

      // add action
      document.getElementById('sd-site-map-add').addEventListener('click', e => {
        $tree.tree(
          'appendNode',
          {
            name: window.sledge.nslocal.section_new,
            kind: 'menu',
            id: guidGenerator()
          }
        )
        this.saveMap()
      })

      //remove action
      cont.addEventListener('click', e => {
        if (e.target.dataset.kind === 'remove') {
          let torm = $tree.tree('getNodeById', this.selectedNode)
          if (torm) {
            $tree.tree('removeNode', torm)
            this.saveMap()
            cont.querySelector('.sd-site-map-form').innerHTML = ''
          }
        }
      })

      // click on item
      $tree.bind(
        'tree.click',
        event => {
          this.selectedNode = event.node.id
          getForm(cont, this.currentLanguage, event.node, _langList[window.nslocal || 'en'], this.pages)
        }
      )

      // onchange delegate
      siteMapCont.addEventListener('change', e => {
        if (e.target.dataset.kind === 'language') {
          this.currentLanguage = e.target[e.target.selectedIndex].value
          this.apply()
          return
        }
        if (e.target.dataset.kind === 'local') {
          let node = $tree.tree('getSelectedNode')
          if (!node.local) {
            node.local = {}
          }
          node.local[this.currentLanguage] = window.encodeURIComponent(e.target.value)
          node.element.querySelector('span').innerHTML = e.target.value
          this.saveMap()
        }
      })

      // save after move
      $tree.bind(
        'tree.move',
        event => {
          event.preventDefault()
          event.move_info.do_move()
          this.saveMap()
          getForm(cont, this.currentLanguage, $tree.tree('getNodeById', this.selectedNode), _langList[window.nslocal || 'en'], this.pages)          
        }
      )
      
      // recover selected state if needed
      if (this.selectedNode) {
        let nodeToSel = $tree.tree('getNodeById', this.selectedNode)
        if (nodeToSel) {
          $tree.tree('selectNode', nodeToSel)
          getForm(cont, this.currentLanguage, nodeToSel, _langList[window.nslocal || 'en'], this.pages)
        }
      }

    })
  }

  update () {
    let data = null
    if (this.projectConf && this.projectConf.siteMap) {
      data = JSON.parse(this.projectConf.siteMap)
    } else {
      return
    }
    let node = document.createElement('div')
    node.id = 'sd-site-map-tree-h'
    node.style.position = 'absolute'
    node.style.display = 'none'
    document.body.appendChild(node)

    let nodeToRm = []
    let pageAdded = []
    window.requestAnimationFrame(() => {
      let $tree = $('#sd-site-map-tree-h')

      // init
      $tree.bind(
        'tree.init',
        () => {
          // remove if needed
          if (nodeToRm.length !== 0) {
            nodeToRm.forEach(nodeid => {
              let torm = $tree.tree('getNodeById', nodeid)
              $tree.tree('removeNode', torm)
            })
          }
          // add if needed
          this.rootPages.forEach(page => {
            if (pageAdded.indexOf(page._id) === -1) {
              $tree.tree(
                'appendNode',
                {
                  name: page.name,
                  kind: 'page',
                  id: page._id
                }
              )
            }
          })
          if (this.projectConf && this.projectConf.siteMap) {
            this.saveMap(true)
          }
          $tree.tree('reload')
        }
      )
      
      // init tree
      $tree.tree({
        data: data,
        selectable: true,
        dragAndDrop: true,
        onCreateLi: (node, $li) => {
          if (node.kind === 'menu') {
            $li.find('.jqtree-element').addClass('sd-side-map-menu')
            let localN =
            (node.local && node.local[this.currentLanguage])
            ? window.decodeURIComponent(node.local[this.currentLanguage])
            : null
            if (localN) {
              $li.find('span').html(localN)
            }
          } else {
            let pageData = this.pages[node.id]
            if (!pageData) {
              nodeToRm.push(node.id)
            } else {
              pageAdded.push(node.id)
              let pageLocalN =
              (pageData.local && pageData.local[this.currentLanguage])
              ? window.decodeURIComponent(pageData.local[this.currentLanguage])
              : pageData.name
              $li.find('span').html(pageLocalN)
            }
          }
        }
      })
    })
  }

  saveMap (rm) {
    if (rm) {
      this.projectConf.siteMap = $('#sd-site-map-tree-h').tree('toJson')
      setProjConf(this.currentProject, this.projectConf)
      remove('sd-site-map-tree-h')
    } else {
      this.projectConf.siteMap = $('#sd-site-map-tree').tree('toJson')
      setProjConf(this.currentProject, this.projectConf)
    }
  }

  hide () {
    remove(document.querySelector('.sd-siteMap'))
  }

}
