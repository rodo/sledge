export default (container, language) => {
  let loc = window.sledge.nslocal
  let html = `
  <div class="sd-siteMap">
    <h1>${loc.gen_sitemap}</h1>
    <div class="sd-site-map-cont">
      <div class="sd-site-map-tree">
        <label style="width: auto;">${loc.sm_changelang} : </label>
        <sd-languages-selector data-lang="${language}"></sd-languages-selector>
        <button id="sd-site-map-add">${loc.sm_addsection}</button>
        <div id="sd-site-map-tree"></div>    
      </div>
      <div class="sd-site-map-form"></div>
    </div>
    </div>`
  container.insertAdjacentHTML('beforeend', html)
}

export function getForm (cont, language, node, languageConf, pages) {
  let markup = ''
  let loc = window.sledge.nslocal
  if (node.kind === 'page') {
    let pageData = pages[node.id]
    let local = (pageData.local && pageData.local[language])
    ? window.decodeURIComponent(pageData.local[language])
    : pageData.name

    markup = `
    <p><label class="sd-full">${loc.sm_pname} :</label>
    <span>${pageData.name}</span></p>
    <p><label class="sd-full">${loc.sm_loclabel}</label>
    ${local}<br><span class="sd-info">${loc.sm_updateinfo}</span></p>`
  } else {
    let visibleName = (node.local && node.local[language])
    ? window.decodeURIComponent(node.local[language])
    : node.name
    markup = `
    <p><label class="sd-full">${loc.sm_locsectionlabel} (<i>${languageConf[language]}</i>):</label>
    <input style="margin-top: 5px;" type="text" data-kind="local" value="${visibleName}"/></p>
    ${node.children.length !== 0 ? `<span class="sd-info">${loc.sm_rmsectioninfo}</span>` : `<button class="sd-site-map-remove" data-kind="remove">${loc.sm_rmsection}</button>`}`
  }
  cont.querySelector('.sd-site-map-form').innerHTML = markup
}
