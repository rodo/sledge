import {connect, store} from '../../store'
import {updatePageProp} from '../../interface/interface_mac'
import ControllerSingle from './controllers/single'
import ControllerStrip from './controllers/strip'
import ControllerSide from './controllers/side'
import {remove} from '../../utilities'

import _conf from '../../config'
import render from './template'
import renderBackground from '../../../framework/src/properties/background'

export default class Navigator {

  constructor () {
    let ctrlConf = []
    for (let ctrl in _conf.controllers) {
      ctrlConf.push({label: _conf.controllers[ctrl].label, type: _conf.controllers[ctrl].type})
    }
    this.conf = {
      controller: ctrlConf
    }
    
    connect('iapPlus', this)
    connect('currentProject', this)
    connect('views', this)
    connect('pendingNewPage', this)
    connect('languages', this)
    connect('sideMenuCurrentItem', this, () => {
      if (this.sideMenuCurrentItem.type === 'page') {
        this.apply()
      }
      if (!this.sideMenuCurrentItem.type || this.sideMenuCurrentItem.type !== 'page') {
        this.hide()
      }
    })

    window.PubSub.subscribe('LANGUAGE_UPDATE', () => {
      if (this.sideMenuCurrentItem.type === 'page') {
        this.apply()
      }
    })
  }

  hide () {
    if (this.container) {
      this.container.style.display = 'none'
    }
  }

  show () {
    this.container.style.display = 'block'
  }

  delegate () {

    this.propsWrapper.onclick = e => {
      if (e.target.nodeName === 'LI') {
        this.currentSettingTab = e.target.dataset.tab
        this.propsWrapper.querySelector('.sd-active').classList.remove('sd-active')
        this.propsWrapper.querySelector('.sd-visible').classList.remove('sd-visible')
        e.target.classList.add('sd-active')
        document.getElementById('sg-page-set-' + e.target.dataset.tab).classList.add('sd-visible')
      }
    }
    
    this.propsWrapper.onchange = (e) => {

      /*if (e.target.dataset && e.target.dataset.kind === 'language') {
        this.currentLanguage = e.target[e.target.selectedIndex].value
        this.apply()
        return
      }*/

      const target = e.target
      switch (target.name) {
        case 'local':
        case 'author':
        case 'keywords':
        case 'description':
          if (!this.sideMenuCurrentItem[target.name]) {
            this.sideMenuCurrentItem[target.name] = {}
          }
          this.sideMenuCurrentItem[target.name][window.$sledge.language] = window.encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
          updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, this.sideMenuCurrentItem)
          break
        case 'name':
          updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, name: target.value})
          break
        case 'kind':
          updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, controller: target.value})
          break
        case 'expanded':
          if (!this.sideMenuCurrentItem.settings) {
            this.sideMenuCurrentItem.settings = {}
          }
          this.sideMenuCurrentItem.settings.expanded = e.target.checked
          updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem})
          break
      }
    }

    if (this.controller.apply) {
      this.controller.apply()
    }
  }

  updateAttachedViews () {
    const views = document.querySelectorAll('.sd_view_container')
    let list = []
    ;[].forEach.call(views, view => {
      list.push(view.dataset.id)
    })

    if (this.sideMenuCurrentItem.controller === 'SIDE') {
      const sideViews = document.querySelectorAll('.sd_view_side_container')
      let sideList = []
      ;[].forEach.call(sideViews, view => {
        if (view.dataset.id) {
          sideList.push(view.dataset.id)
        }
      })
      updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, views: list, sideViews: sideList})
    } else {
      updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, views: list})
    }
  }

  renderMenu () {
    let tpl = ''
    this.conf.controller.forEach(item => {
      if (this.sideMenuCurrentItem.controller.toLowerCase() === item.type.toLowerCase()) {
        tpl += `<option value="${item.type}" selected data-kind="${item.type}">${item.label[window.sledge.languageCode]}</option>`
      } else {
        if (!this.iapPlus) {
          if (item.type === 'SINGLE' || item.type === 'STRIP') {
            tpl += `<option value="${item.type}" data-kind="${item.type}">${item.label[window.sledge.languageCode]}</option>`
          }
        } else {
          tpl += `<option value="${item.type}" data-kind="${item.type}">${item.label[window.sledge.languageCode]}</option>`
        }
      }
    })
    return tpl
  }

  renderPage () {
    switch (this.sideMenuCurrentItem.controller) {
      case 'SINGLE':
        this.controller = new ControllerSingle(this)
        break
      case 'STRIP':
        this.controller = new ControllerStrip(this)
        break
      case 'SIDE':
        this.controller = new ControllerSide(this)
        break
      default:
        this.controller = new ControllerSingle(this)
    }

    return this.controller.render(this.sideMenuCurrentItem, this.views)
  }

  applySettings () {
    let comp = document.querySelector('.sd_navigator_page_wrapper-content')
    renderBackground(comp, null, null, this.sideMenuCurrentItem.settings || {}, this.currentProject)

    if (this.iapPlus) {
      let editorBef = window.ace.edit('sd-script-editor-before')
      editorBef.setTheme('ace/theme/monokai')
      editorBef.getSession().setMode('ace/mode/javascript')
  
      let editorAf = window.ace.edit('sd-script-editor-after')
      editorAf.setTheme('ace/theme/monokai')
      editorAf.getSession().setMode('ace/mode/javascript')

      let editorLoad = window.ace.edit('sd-script-editor-load')
      editorLoad.setTheme('ace/theme/monokai')
      editorLoad.getSession().setMode('ace/mode/javascript')
  
      let editorTags = window.ace.edit('sd-script-editor-tags')
      editorTags.setTheme('ace/theme/monokai')
      editorTags.getSession().setMode('ace/mode/html')
      editorTags.getSession().setOption('useWorker', false)

      editorTags.getSession().on('change', e => {
        this.sideMenuCurrentItem.tags = encodeURIComponent(editorTags.getValue()).replace(/[!'()*]/g, escape)
        this.updateScripts()
      })

      editorBef.getSession().on('change', e => {
        if (!this.sideMenuCurrentItem.scripts) {
          this.sideMenuCurrentItem.scripts = {}
        }
        this.sideMenuCurrentItem.scripts.before = encodeURIComponent(editorBef.getValue()).replace(/[!'()*]/g, escape)
        this.updateScripts()
      })
  
      editorAf.getSession().on('change', e => {
        if (!this.sideMenuCurrentItem.scripts) {
          this.sideMenuCurrentItem.scripts = {}
        }
        this.sideMenuCurrentItem.scripts.after = encodeURIComponent(editorAf.getValue()).replace(/[!'()*]/g, escape)
        this.updateScripts()
      })

      editorLoad.getSession().on('change', e => {
        if (!this.sideMenuCurrentItem.scripts) {
          this.sideMenuCurrentItem.scripts = {}
        }
        this.sideMenuCurrentItem.scripts.load = encodeURIComponent(editorLoad.getValue()).replace(/[!'()*]/g, escape)
        this.updateScripts()
      })
    }
  }

  updateScripts () {
    if (this.saveTm) {
      window.clearTimeout(this.saveTm)
    }
    this.saveTm = window.setTimeout(() => {
      this.donorefresh = true
      updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem})
    }, 500)
  }

  apply () {
    if (this.donorefresh) {
      this.donorefresh = false
      return
    }

    render(this, document.querySelector('.sd_editor_work'), window.$sledge.language, this.iapPlus)

    document.querySelector('.sd_navigator_page').addEventListener('click', e => {
      if (e.target.className === 'sd_viewName') {
        remove(e.target.closest('.grid-item'))
        this.updateAttachedViews()
      }
    })

    this.container = document.getElementsByClassName('sd_navigator')[0]
    this.propsWrapper = document.getElementsByClassName('sd_navigator_props_wrapper')[0]
    this.delegate()

    if (this.currentSettingTab) {
      this.propsWrapper.querySelector('.sd-active').classList.remove('sd-active')
      this.propsWrapper.querySelector('.sd-visible').classList.remove('sd-visible')
      document.getElementById('sg-page-set-tab-' + this.currentSettingTab).classList.add('sd-active')
      document.getElementById('sg-page-set-' + this.currentSettingTab).classList.add('sd-visible')
    }
    if (this.pendingNewPage !== null) {
      store.dispatch({ type: 'PENDING_NEW_PAGE', data: null })
    }
  }
}
