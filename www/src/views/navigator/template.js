export default (ctrl, cont, language, isPlus) => {
  const visibleName =
  (ctrl.sideMenuCurrentItem.local && ctrl.sideMenuCurrentItem.local[language])
  ? window.decodeURIComponent(ctrl.sideMenuCurrentItem.local[language])
  : ctrl.sideMenuCurrentItem.name

  const keywords =
  ctrl.sideMenuCurrentItem.keywords && ctrl.sideMenuCurrentItem.keywords[language]
  ? window.decodeURIComponent(ctrl.sideMenuCurrentItem.keywords[language])
  : ''

  const description =
  ctrl.sideMenuCurrentItem.description && ctrl.sideMenuCurrentItem.description[language]
  ? window.decodeURIComponent(ctrl.sideMenuCurrentItem.description[language])
  : ''

  const author =
  ctrl.sideMenuCurrentItem.author
  ? window.decodeURIComponent(ctrl.sideMenuCurrentItem.author)
  : ''

  let loc = window.sledge.nslocal
  const tpl = `
  <div class="sd_navigator">
    <div class="sd_navigator_page_wrapper">
        <div class="sd_navigator_page">
          <div class="sd_navigator_page_wrapper-top"> <div><</div> <div>></div> <span>${visibleName}</span></div>
          ${ctrl.renderPage()}
        </div>
    </div>
    <div class="sd_navigator_props_wrapper">
      <ul>
        <li id="sg-page-set-tab-gen" class="sd-active" data-tab="gen">${loc.page_set_gen}</li>
        <li id="sg-page-set-tab-back" data-tab="back">${loc.page_set_back}</li>
        ${isPlus ? `<li id="sg-page-set-tab-script" data-tab="script">${loc.page_set_script}</li>` : ''}
      </ul>
      <br>
      <div id="sg-page-set-gen" class="sd-page-set-tab sd-visible">
        <p class="help">${loc.ctrl_draginfo}.</p>
        
        <p><label>${loc.ctrl_pagetype}</label>
        <select name='kind'>${ctrl.renderMenu()}</select></p>

        <p><label>${loc.sm_pname}</label>
        <span class="sd-form-span">${ctrl.sideMenuCurrentItem.name}</span></p>

        <p><label>${loc.ctrl_locpagename} (${window.$sledge.language})</label>
        <input name="local" type="text" value="${visibleName}"/>
        </p>

        <p><label>${loc.ctrl_key} (${window.$sledge.language})</label>
        <input name="keywords" type="text" value="${keywords}"/>
        </p>

        <p><label>${loc.ctrl_desc} (${window.$sledge.language})</label>
        <textarea name="description" rows="5" cols="50">${description}</textarea>
        </p>

        <p><label>${loc.ctrl_author}</label>
        <input name="author" type="text" value="${author}"/>
        </p>

        <p><label>${loc.page_set_full}</label>
        <input name="expanded" type="checkbox" ${ctrl.sideMenuCurrentItem.settings && ctrl.sideMenuCurrentItem.settings.expanded ? ' checked ' : ''}/>
        </p>
      </div>

      <div id="sg-page-set-back" class="sd-page-set-tab">
        <sd-viewsettings></sd-viewsettings>
      </div>

      ${isPlus
      ? `
      <div id="sg-page-set-script" class="sd-page-set-tab">
        <label class="sd-full">${loc.page_set_script_af}</label>
        <div id="sd-script-editor-before">${ctrl.sideMenuCurrentItem.scripts && ctrl.sideMenuCurrentItem.scripts.before ? decodeURIComponent(ctrl.sideMenuCurrentItem.scripts.before) : '//your script here'}</div>

        <label class="sd-full">${loc.page_set_script_be}</label>
        <div id="sd-script-editor-after">${ctrl.sideMenuCurrentItem.scripts && ctrl.sideMenuCurrentItem.scripts.after ? decodeURIComponent(ctrl.sideMenuCurrentItem.scripts.after) : '//your script here'}</div>

        <label class="sd-full">${loc.page_set_script_load}</label>
        <div id="sd-script-editor-load">${ctrl.sideMenuCurrentItem.scripts && ctrl.sideMenuCurrentItem.scripts.load ? decodeURIComponent(ctrl.sideMenuCurrentItem.scripts.load) : '//your script here'}</div>

        <label class="sd-full">${loc.ctrl_tags}</label>
        <div id="sd-script-editor-tags"></div>
      </div>
      `
      : ''
      }

    </div>
  </div>`
  cont.innerHTML = tpl
  if (isPlus && ctrl.sideMenuCurrentItem.tags) {
    cont.querySelector('#sd-script-editor-tags').innerText = decodeURIComponent(ctrl.sideMenuCurrentItem.tags)
  }
  ctrl.applySettings()
}
