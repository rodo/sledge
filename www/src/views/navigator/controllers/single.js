class controllerSingle {

  constructor (parent) {
    this.parent = parent
  }

  render (currentItem, views) {
    let loc = window.sledge.nslocal
    let viewID
    let view
    let button = ''

    if (currentItem && currentItem.views && (currentItem.views.length === 0 || currentItem.views[0] === null)) {
      viewID = ''
      view = {
        name: 'drag a view here'
      }
    } else if (currentItem && currentItem.views) {
      viewID = currentItem.views[0]
      view = views[viewID]
      button = `<button class="sd_viewName">${loc.ctrl_edit}</button>`
    }

    return `<div class="sd_navigator_page_wrapper-content sd_controller_single">
        <div data-id="${viewID}" class="sd_view_container">
          <div class="sd_view_actions">
            <div class="sd_viewName">${view.name}</div>
          </div>
        </div>
      </div>
    `
  }

  apply () {
    const viewsContainer = document.querySelector('.sd_view_container')
    viewsContainer.addEventListener('dragover', (e) => e.preventDefault(), false)
    viewsContainer.addEventListener('dragenter', (e) => e.preventDefault(), false)
    viewsContainer.addEventListener('drop', (e) => this.dropOnContainerView(e), false)
  }

  dropOnContainerView (ev) {
    const viewid = ev.dataTransfer.getData('text')
    const cont = ev.target.closest('.sd_view_container')
    if (viewid) {
      cont.dataset.id = viewid
      this.parent.updateAttachedViews()
    }
  }
}

export default controllerSingle
