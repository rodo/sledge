import {domUnwrap, addClass, remove} from '../../../utilities'

class controllerSide {

  constructor (parent) {
    this.parent = parent
  }

  render (currentItem, views) {
    let loc = window.sledge.nslocal
    let view
    let markup = `<div class="sd_navigator_page_wrapper_side_content sd_controller_strip"><div class="grid-stack-side">`
    if (currentItem.sideViews && currentItem.sideViews.length !== 0) {
      currentItem.sideViews.forEach(viewId => {
        view = views[viewId]
        if (view) {
          markup += `<div data-id="${view._id}" class="sd_view_side_container grid-item">
            <div class="grid-item-content">  
              <div class="sd_view_actions">
                <div class="sd_viewName">${view.name}</div>
                <button class="sd_viewName">${loc.ctrl_rm}</button>
              </div>
            </div>
          </div>`
        }
      })
    } else {
      markup += `<div id="NEW-SIDE" class="sd_view_side_container grid-item">
          <div class="grid-item-content">  
            <div class="sd_view_actions">
              <div class="sd_viewName">${loc.ctrl_drag}</div>
            </div>
          </div>
        </div>`
    }

    markup += `</div></div><div class="sd_navigator_page_wrapper-content sd_controller_side">
      <div class="grid-stack">`
    if (currentItem.views && currentItem.views.length !== 0 && currentItem.views[0] !== undefined && currentItem.views[0] !== null) {
      currentItem.views.forEach((viewId) => {
        view = views[viewId]
        markup += `<div data-id="${view._id}" class="sd_view_container grid-item">
          <div class="grid-item-content">  
            <div class="sd_view_actions">
              <div class="sd_viewName">${view.name}</div>
              <button class="sd_viewName">${loc.ctrl_rm}</button>
            </div>
          </div>
        </div>`
      })
    } else {
      markup += `<div id="NEW-STRIP" class="sd_view_container grid-item">
          <div class="grid-item-content">  
            <div class="sd_view_actions">
              <div class="sd_viewName">${loc.ctrl_drag}</div>
            </div>
          </div>
        </div>`
    }
    markup += `</div></div>`
    return markup
  }

  renderControls () {
    // return `<button class="sd_navigator_page_add_view">Add a view</button>`
  }

  apply () {
    const el = document.querySelector('.grid-stack')
    Sortable.create(el, {
      group: {
        name: 'VIEW_DRAG_GROUP'
      },
      onAdd: (evt) => {
        remove(document.getElementById('NEW-STRIP'))
        addClass('.sd_navigator_page_wrapper-content li[data-kind="view"]', 'sd_view_container')
        domUnwrap('.sd_navigator_page_wrapper-content .sd_view_side_container', elem => {
          elem.classList.add('sd_view_container')
          elem.remove('.sd_view_side_container')
        })
        this.parent.updateAttachedViews()
      },
      onClone: (evt) => {},
      onUpdate: () => {
        this.parent.updateAttachedViews()
      }
    })

    const elSide = document.querySelector('.grid-stack-side')
    Sortable.create(elSide, {
      group: {
        name: 'VIEW_DRAG_GROUP'
      },
      onAdd: (evt) => {
        remove(document.getElementById('NEW-SIDE'))
        addClass('.sd_navigator_page_wrapper_side_content li[data-kind="view"]', 'sd_view_side_container')
        domUnwrap('.sd_navigator_page_wrapper_side_content .sd_view_container', elem => {
          elem.classList.remove('sd_view_container')
          elem.classList.add('.sd_view_side_container')
        })
        this.parent.updateAttachedViews()
      },
      onClone: (evt) => {},
      onUpdate: () => {
        this.parent.updateAttachedViews()
      }
    })
  }
}

export default controllerSide
