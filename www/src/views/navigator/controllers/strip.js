import {remove} from '../../../utilities'

class controllerStrip {

  constructor (parent) {
    this.parent = parent
  }

  render (currentItem, views) {
    let loc = window.sledge.nslocal
    let view
    let markup = `
    <div class="sd_navigator_page_wrapper-content sd_controller_strip">
      <div class="grid-stack">`

    if (currentItem && currentItem.views && currentItem.views.length !== 0) {
      currentItem.views.forEach((viewId) => {
        view = views[viewId]
        markup += `<div data-id="${view._id}" class="sd_view_container grid-item">
            <div class="grid-item-content">  
              <div class="sd_view_actions">
                <div class="sd_viewName">${view.name}</div>
                <button class="sd_viewName">${loc.ctrl_rm}</button>
              </div>
            </div>
          </div>`
      })
    } else {
      markup += `<div id="NEW-STRIP" class="sd_view_container grid-item">
          <div class="grid-item-content">  
            <div class="sd_view_actions">
              <div class="sd_viewName">${loc.ctrl_drag}</div>
            </div>
          </div>
        </div>`
    }
    markup += `</div></div>`
    return markup
  }

  renderControls () {
    // return `<button class="sd_navigator_page_add_view">Add a view</button>`
  }

  apply () {
    const el = document.getElementsByClassName('grid-stack')[0]
    Sortable.create(el, {
      group: {
        name: 'VIEW_DRAG_GROUP'
      },
      onAdd: (evt) => {
        remove(document.getElementById('NEW-STRIP'))
        document.querySelectorAll('.sd_navigator_page_wrapper li[data-kind="view"]')[0].classList.add('sd_view_container')
        this.parent.updateAttachedViews()
      },
      onClone: (evt) => {},
      onUpdate: () => {
        this.parent.updateAttachedViews()
      }
    })
  }
}

export default controllerStrip
