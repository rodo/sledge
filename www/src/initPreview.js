import Preview from './views/preview/preview'
import {getPages, getViews, getMedias, getTheme, openLink, getProjConf, getLanguages} from './interface/interface_mac'
import {store} from './store'
import Bridge from './interface/framework_bridge'
const fmBridge = new Bridge()

window.$sledge.bridge = fmBridge

import PublishManager from './publish/PublishManager'

import {initConf} from './config'
initConf()

const prev = new Preview()

window.reloadPreview = () => {
  store.dispatch({ type: 'SET_VIEWS', views: [] })
  store.dispatch({ type: 'SET_PAGES', pages: [] })
  store.dispatch({ type: 'CURRENT_SIDE_MENU_ITEM', data: {} })
  getMedias(window.localStorage.currentProject)
  getTheme(window.localStorage.currentProject)
  getProjConf(window.localStorage.currentProject)
  getPages()
  getViews()
}

window.setLanguage = (index) => {
  window.$sledge.language = fmBridge.languagesCode[index]
  prev.init()
  window.requestAnimationFrame(() => $(document.body).foundation())
  window.requestAnimationFrame(() => window.$sledge.foundationReInit())
}

window.setCurrent = (viewID) => {
  if (window.localStorage.getItem('currentView') === viewID) {
    // return => empeche le setCurrent au publish si plusieurs langues et une seule page, à vérifier si ça pose problème.
  }
  window.localStorage.setItem('currentView', viewID)
  prev.setCurrent()
}

document.addEventListener('click', e => {
  let target = e.target
  if (target.nodeName === 'DIV' || target.nodeName === 'SPAN' || target.nodeName === 'I') {
    if (target.parentNode.nodeName === 'A') {
      manageLinks(e, target.parentNode)
    }
  }
  if (target.nodeName === 'IMG') {
    if (target.parentNode.nodeName === 'A') {
      manageLinks(e, target.parentNode)
    }
  }
  if (target.nodeName === 'A') {
    manageLinks(e, target)
  }
})

function manageLinks (e, target) {
  if (target.href.indexOf('mailto:') !== -1) {
    return
  }
  e.stopPropagation()
  e.preventDefault()
  let action = target.href
  switch (true) {
    case action.indexOf('http') !== -1 :
      openLink(decodeURIComponent(target.getAttribute('href')))
      break
    case action.indexOf('switchpage://') !== -1 :
      window.setCurrent(action.replace('switchpage://', ''))
      break
  }
}

if (window.sledge.isPublish) {
  window.sledge.publishManager = new PublishManager()
}

getMedias(window.localStorage.currentProject)
getLanguages(window.localStorage.currentProject)
getTheme(window.localStorage.currentProject)
getProjConf(window.localStorage.currentProject)
getPages()
getViews(() => {
  if (window.sledge.isPublish) {
    window.setTimeout(() => window.sledge.publishManager.ready(), 3000)
  }
})

