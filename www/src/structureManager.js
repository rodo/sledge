import {appendHtml, insertHtmlBefore, insertHtmlAfter, remove, insertNodeAfter, moveBefore, insertNodeBefore} from './utilities'
import {connect, store} from './store'
import {setViewStructure, getViews, setViewParam} from './interface/interface_mac'

export default class StuctureManager {

  constructor () {
    this.maxRowSize = 12
    this.dragData = null
    this.actionZone = null
    connect('currentProject', this)
    connect('sideMenuCurrentItem', this, (a) => {
      window.requestAnimationFrame(() => {
        this.currentView = document.getElementsByTagName('sg-view')[0]
      })
      this.currentProperties = a.properties || {}
    })
    connect('currentSelectedComponent', this)
    connect('responsiveState', this, () => this.resetActionZone())
    
    PubSub.subscribe('component_drag_start', (a, b) => this.dragStart(a, b))
    PubSub.subscribe('component_drag_stop', (a, b) => this.dragStop(a, b))
    PubSub.subscribe('component_drag_cancel', (a, b) => this.dragCancel(a, b))
    PubSub.subscribe('viewMouseOver', (action, e) => this.onViewOver(e))
    PubSub.subscribe('viewMouseOut', (action, e) => this.onViewOut(e))
    PubSub.subscribe('viewClick', (a, e) => this.onClick(e))
    PubSub.subscribe('removeCurrentComponent', (a, e) => this.removeCurrentComponent(e))
    PubSub.subscribe('readyToSaveStructure', (a, props) => this.trySaveStructure(props))
    PubSub.subscribe('documentScroll', e => this.resetActionZone())
    PubSub.subscribe('resetAllBuilderSelection', () => this.resetActionZone())
    PubSub.subscribe('workspaceScroll', e => {
      if (this.scrollPending && this.ScrollTm) {
        clearTimeout(this.ScrollTm)
      }
      this.scrollPending = true
      this.cleanDrag()
      this.ScrollTm = window.setTimeout(e => { this.scrollPending = false }, 100)
    })

    $(document.body).on('mousemove', '.columns', e => {
      let target = e.target
      if (!target.classList.contains('sd-selection')) {
        this.onViewOver(e)
      }
    })
  }

  getStructure () {
    const rows = this.currentView.querySelectorAll('.sg_row')
    let markup = ''
    let cols = null
    let h = ''
    let name = null

    ;[].forEach.call(rows, row => {
      markup += `<sg-row id="${row.id}">`
      cols = row.querySelectorAll('.columns')
      if (cols.length !== 0) {
        [].forEach.call(cols, col => {
          // h = col.style.height ? `style="height:${col.style.height};"` : ''
          name = col.nodeName.toLowerCase()
          markup += `<${name} class="${col.className}" id="${col.id}"></${name}>`
        })
      }
      markup += `</sg-row>`
    })
    return encodeURIComponent(markup)
  }

  trySaveStructure (props) {
    const rows = document.querySelectorAll('.row')
    ;[].forEach.call(rows, div => {
      if (div.childNodes.length === 0) {
        remove(div)
      }
    })
    if (props) {
      this.pendingSaveProps = props
    }
    if (this.saveTm) {
      clearTimeout(this.saveTm)
    }
    this.saveTm = setTimeout(() => this.saveStructure(), 500)
  }

  saveStructure () {
    window.requestAnimationFrame(() => window.$sledge.foundationReInit())
    if (this.pendingSaveProps) {
      let itemData = {...this.sideMenuCurrentItem, properties: this.pendingSaveProps, structure: this.getStructure()}
      setViewStructure(this.currentProject, this.sideMenuCurrentItem._id, itemData,
        data => {
          getViews()
          store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {...itemData, _rev: data.rev}})
        }
      )
      this.pendingSaveProps = null
      return
    }
    let itemData = {...this.sideMenuCurrentItem, structure: this.getStructure()}
    setViewStructure(this.currentProject, this.sideMenuCurrentItem._id, itemData,
      data => {
        getViews()
        store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {...itemData, _rev: data.rev}})
      }
    )
  }

  onClick (e) {
    e.preventDefault()
    e.stopPropagation()
    let target = e.target
    if (this.actionZone && this.actionZone.selected && !target.classList.contains('selected')) {
      this.resetActionZone()
    }
  }

  removeCurrentComponent () {
    let col = this.actionZone.ref
    this.resetActionZone()
    let row = col.parentNode
    remove(col)
    if (row.querySelectorAll('.columns').length === 0) {
      remove(row)
    } else {
      this.resetEnd(row)
    }
    this.cleanAll()

    // remove properties & save structure
    if (this.currentProperties[this.currentSelectedComponent]) {
      delete this.currentProperties[this.currentSelectedComponent]
    }
    setViewParam(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, properties: this.currentProperties},
      data => {
        store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {...this.sideMenuCurrentItem, properties: this.currentProperties, _rev: data.rev}})
        window.requestAnimationFrame(() => this.trySaveStructure())
      }
    )
  }

  dragStart (event, item) {
    this.dragData = item
    this.currentDragOver = null
    this.currentView.classList.add('sd-onDrag')
    this.currentView.addEventListener('dragover', (e) => this.onViewDragOver(e), false)
        // store.currentView.addEventListener("dragenter", (e) => this.onViewEnter(e), false);
    this.currentView.addEventListener('drop', (e) => this.onViewDrop(e), false)

    const rowTpl = "<div class='sd-insertRow'></div>"
    appendHtml(this.currentView, rowTpl)

    let rows = this.currentView.querySelectorAll('.sg_row:not(.sd-emptyForDrag)')
    let size = 0

    ;[].forEach.call(rows, (element) => {
      insertHtmlBefore(element, rowTpl)
      size = this.getRowSize(element, false)
      if (size < this.maxRowSize) {
        this.completeForDrop(element, size)
      }
    })
  }

  manageMoveNodeParam (movedItem, oldSize, newSize, moveOldoffset, breakpoint) {
    let bp = breakpoint || this.responsiveState
    let cl = movedItem.classList
    cl.remove('sd-hiddenForDrag')
    cl.remove(`${bp}-offset-${moveOldoffset}`)
    cl.remove('end')
    cl.remove(`${bp}-${oldSize}`)
    cl.add(`${bp}-${newSize}`)
    cl.add(`${bp}-offset-0`)
  }

  dragCancel (event, item) {
    this.currentView.classList.remove('sd-onDrag')
    this.currentView.removeEventListener('dragover', (e) => this.onViewDragOver(e))
    this.currentView.removeEventListener('dragenter', (e) => this.onViewEnter(e))
    this.currentView.removeEventListener('drop', (e) => this.onViewDrop(e))
    this.cleanAll()
    
    this.cleanDragZone()
    if (item.isMove) {
      item.item.classList.remove('sd-hiddenForDrag')
    }
  }

  dragStop (event, item) {
    this.currentView.classList.remove('sd-onDrag')
    this.currentView.removeEventListener('dragover', (e) => this.onViewDragOver(e))
    this.currentView.removeEventListener('dragenter', (e) => this.onViewEnter(e))
    this.currentView.removeEventListener('drop', (e) => this.onViewDrop(e))
    switch (true) {
      case (this.lastReady !== null && this.lastReady !== undefined):

        const nodeName = this.dragData.nodeName
        this.cleanDragZone()
        let oldRow

        if (this.lastReady.classList.contains('sd-insertRow')) {
          if (item.isMove) {
            let newRow = document.createElement('sg-row')
            insertNodeBefore(this.lastReady.parentNode, this.lastReady, newRow)
            newRow.appendChild(item.item)

            let moveOldSize = this.getColumnSizesFromString(item.item.className)
            this.manageMoveNodeParam(item.item, moveOldSize.large, this.maxRowSize, this.getColumnOffsetFromString(item.item.className), 'large')
            this.manageMoveNodeParam(item.item, moveOldSize.medium, this.maxRowSize, this.getColumnOffsetFromString(item.item.className), 'medium')
            this.manageMoveNodeParam(item.item, moveOldSize.small, this.maxRowSize, this.getColumnOffsetFromString(item.item.className), 'small')
          } else {
            insertHtmlBefore(
              this.lastReady,
              `<sg-row>
                <${nodeName} class='large-${this.maxRowSize} medium-${this.maxRowSize} small-${this.maxRowSize} small-offset-0 large-offset-0 medium-offset-0'></${nodeName}>
              </sg-row>`
            )
          }
          remove(this.lastReady)
          this.cleanAll()
        }

        if (this.lastReady && this.lastReady.classList.contains('sd-insertCol')) {
          let row = this.lastReady.parentNode
          let nextSibling = this.lastReady.nextSibling
          if (nextSibling && nextSibling.classList.contains('sd-tempOffset')) {
            nextSibling.classList.remove('sd-tempOffset')
            delete nextSibling.dataset.offset
          }
          this.lastReady.classList.remove('sd-insertCol')
          this.lastReady.classList.remove('sd-ready')

          if (item.isMove) {
            let oldSize = this.getColumnSizesFromString(item.item.className)
            let newSize = this.getColumnSizeFromString(this.lastReady.className)
            oldRow = item.item.parentNode
            moveBefore(this.lastReady.parentNode, item.item, this.lastReady)
            this.manageMoveNodeParam(item.item, oldSize.large, newSize, this.getColumnOffsetFromString(item.item.className), 'large')
            this.manageMoveNodeParam(item.item, oldSize.medium, newSize, this.getColumnOffsetFromString(item.item.className), 'medium')
            this.manageMoveNodeParam(item.item, oldSize.small, newSize, this.getColumnOffsetFromString(item.item.className), 'small')
          } else {
            insertHtmlBefore(this.lastReady, `<${nodeName} class='${this.lastReady.className}'></${nodeName}>`)
          }

          this.lastReady.parentElement.removeChild(this.lastReady)
          this.cleanAll()
          this.manageTempOffset()
          if (oldRow) {
            this.resetEnd(oldRow)
          }
          this.resetEnd(row)
        }
        break
      case (this.dzActive !== null && this.dzActive !== undefined):
        let activePart = this.dzActive

        if (activePart.classList.contains('sd-right')) {
          this.splitVerticaly(this.currentDragOver, item, false)
        } else {
          this.splitVerticaly(this.currentDragOver, item, true)
        }

        this.cleanDragZone()
        this.cleanAll()

        break
      default:
        this.cleanAll()
        if (item.isMove) {
          item.item.classList.remove('sd-hiddenForDrag')
        }
    }

    let emptyToClean = document.querySelector('.sd-emptyForDrag')
    if (emptyToClean) {
      if (emptyToClean.childNodes.length === 0) {
        remove(emptyToClean)
      } else {
        emptyToClean.classList.remove('sd-emptyForDrag')
      }
    }
  }

  manageTempOffset () {
    let offsetToAdd = document.getElementsByClassName('sd-tempOffset')
    if (offsetToAdd) {
      [].forEach.call(offsetToAdd, (element) => {
        element.classList.add(`${this.responsiveState}-offset-${element.dataset.offset}`)
        element.classList.remove('sd-tempOffset')
        delete element.dataset.offset
      })
    }
  }

  cleanAll () {
    this.cleanLastReady()
    this.cleanDropSpace()
    this.dragData = null
  }

  splitVerticaly (overed, item, insertbefore) {
    let size = this.getColumnSizesFromString(overed.className)
    let h
    let rect
    let tag
    let size1
    let size2
    let nodeName

    rect = overed.getBoundingClientRect()
    h = rect.bottom - rect.top

    if (size[this.responsiveState] % 2 === 1) {
      size1 = (size[this.responsiveState] + 1) / 2
      size2 = size1 - 1
    } else {
      size1 = size2 = size[this.responsiveState] / 2
    }

    overed.classList.remove('large-' + size.large)
    overed.classList.add('large-' + size1)

    overed.classList.remove('medium-' + size.medium)
    overed.classList.add('medium-' + size1)

    overed.classList.remove('small-' + size.small)
    overed.classList.add('small-' + size1)

    nodeName = this.dragData.nodeName

    if (item && item.isMove) {
      if (insertbefore) {
        insertNodeBefore(overed.parentNode, overed, item.item)
      } else {
        insertNodeAfter(overed.parentNode, overed, item.item)
      }
      let oldSizes = this.getColumnSizesFromString(item.item.className)
      this.manageMoveNodeParam(item.item, oldSizes.large, size2, this.getColumnOffsetFromString(item.item.className), 'large')
      this.manageMoveNodeParam(item.item, oldSizes.medium, size2, this.getColumnOffsetFromString(item.item.className), 'medium')
      this.manageMoveNodeParam(item.item, oldSizes.small, size2, this.getColumnOffsetFromString(item.item.className), 'small')
    } else {
      tag = `<${nodeName} data-name='${this.dragData.name}' class='columns small-offset-0 medium-offset-0 large-offset-0 large-${size2} medium-${size2} small-${size2}'></${nodeName}>`
      if (insertbefore) {
        insertHtmlBefore(overed, tag)
      } else {
        insertHtmlAfter(overed, tag)
      }
    }
    setTimeout(() => this.resetEnd(overed.parentNode), 0)
  }

  getRowSize (row, withOffset) {
    const cols = row.querySelectorAll('.columns:not(.sd-insertCol):not(.sd-hiddenForDrag)')
    let	size = 0
    let off

    [].forEach.call(cols, (element) => {
      size += this.getColumnSizeFromString(element.className)
      if (withOffset) {
        off = this.getColumnOffsetFromString(element.className)
        if (!isNaN(off)) {
          size += off
        }
      }
    })

    return size
  }

  getColumnOffsetFromString (classes) {
    var reg = new RegExp(`(.*?${this.responsiveState}-offset-.*?)([0-9]{1,2})(.*$)`, 'g')
    return parseInt(classes.replace(reg, '$2'), 10)
  }

  getColumnSizeFromString (classes) {
    classes = classes.replace(`${this.responsiveState}-offset-`, '')
    var reg = new RegExp(`(.*?${this.responsiveState}-.*?)([0-9]{1,2})(.*$)`, 'g')
    return parseInt(classes.replace(reg, '$2'), 10)
  }

  getColumnOffsetsFromString (classes) {
    let res = {}

    classes = classes.replace('large-offset-', '') // ???
    classes = classes.replace('medium-offset-', '')
    classes = classes.replace('small-offset-', '')

    let regLarge = new RegExp('(.*?large-offset-.*?)([0-9]{1,2})(.*$)', 'g')
    let regMedium = new RegExp('(.*?medium-offset-.*?)([0-9]{1,2})(.*$)', 'g')
    let regSmall = new RegExp('(.*?small-offset-.*?)([0-9]{1,2})(.*$)', 'g')

    res.large = parseInt(classes.replace(regLarge, '$2'), 10)
    res.medium = parseInt(classes.replace(regMedium, '$2'), 10)
    res.small = parseInt(classes.replace(regSmall, '$2'), 10)

    return res
  }

  getColumnSizesFromString (classes) {
    let res = {}

    classes = classes.replace('large-offset-', '')
    classes = classes.replace('medium-offset-', '')
    classes = classes.replace('small-offset-', '')

    let regLarge = new RegExp('(.*?large-.*?)([0-9]{1,2})(.*$)', 'g')
    let regMedium = new RegExp('(.*?medium-.*?)([0-9]{1,2})(.*$)', 'g')
    let regSmall = new RegExp('(.*?small-.*?)([0-9]{1,2})(.*$)', 'g')

    res.large = parseInt(classes.replace(regLarge, '$2'), 10)
    res.medium = parseInt(classes.replace(regMedium, '$2'), 10)
    res.small = parseInt(classes.replace(regSmall, '$2'), 10)

    return res
  }

  completeForDrop (row, currentsize) {
    let fillSize = this.maxRowSize - currentsize
    const cols = row.querySelectorAll('.columns:not(.sd-hiddenForDrag)')
    let offset
    let height
    let rect

    rect = row.getBoundingClientRect()
    height = rect.bottom - rect.top;

    [].forEach.call(cols, (element) => {
      offset = this.getColumnOffsetFromString(element.className)
      console.info(typeof offset, offset)
      if (!isNaN(offset) && offset !== 0) {
        insertHtmlBefore(
          element,
          `<div 
          style='height:${height}px;' 
          class='columns sd-insertCol large-${offset} medium-${offset} small-${offset}' small-offset-0 large-offset-0 medium-offset-0></div>`
        )
        element.classList.remove(`${this.responsiveState}-offset-${offset}`)
        element.classList.add('sd-tempOffset')
        element.dataset.offset = offset
        fillSize = fillSize - offset
      }
    })

    if (fillSize !== 0) {
      appendHtml(row, `<div style='height:${height}px;' class='columns sd-insertCol large-${fillSize} medium-${fillSize} small-${fillSize} small-offset-0 large-offset-0 medium-offset-0'></div>`)
    }
  }

  cleanDragZone () {
    remove('.sd-dragZone')
    this.dzActive = null
  }

  cleanDropSpace () {
    remove('.sd-insertCol')
    remove('.sd-insertRow')
    const cols = this.currentView.querySelectorAll('.sd-tempOffset');
    [].forEach.call(cols, (element) => {
      if (element.dataset.offset) {
        element.classList.remove('sd-tempOffset')
        element.classList.add(`${this.responsiveState}-offset-${element.dataset.offset}`)
        delete element.dataset.offset
      }
    })
  }

  cleanLastReady () {
    if (this.lastReady) {
      this.lastReady.classList.remove('sd-ready')
      this.lastReady = null
    }
  }

  setLastReady (target) {
    this.cleanLastReady()
    target.classList.add('sd-ready')
    this.lastReady = target
  }

  onViewDrop (event) {
    event.preventDefault()
    event.stopPropagation()
  }

  onViewEnter (event) {
    event.preventDefault()
    event.stopPropagation()
  }

  onViewOut (event) {
    if (this.actionZone && this.actionZone.selected) {
      return
    }

    if (!event.target.classList.contains('columns')) {
      this.resetActionZone()
    }
  }

  onViewOver (event) {
    event.preventDefault()

    let target = event.target
    let posElem

    if (target.nodeName === 'DIV' || (this.actionZone && this.actionZone.selected) || (this.actionZone && this.actionZone.isResizing())) {
      return
    }

    if (target.classList.contains('columns')) {
      this.resetActionZone()
      posElem = target.getBoundingClientRect()
      this.actionZone = document.createElement('sd-actionzone')
      document.body.appendChild(this.actionZone)
      this.actionZone.init(target, this)
    } else {
      this.resetActionZone()
    }
  }

  resetActionZone () {
    if (this.actionZone) {
      remove('.sd-prop-ghost')
      window.PubSub.publish('hideProperties')
      remove(this.actionZone)
      this.actionZone = null
      store.dispatch({type: 'SET_SELECTED_COMPONENT', value: null})
      let selected = document.querySelector('.sd-selection')
      if (selected) {
        selected.classList.remove('sd-selection')
      }
    }
  }

  onViewDragOver (event) {
    let target = event.target

    if (this.scrollPending) {
      return false
    }

    if (this.currentDragOver === event.target) {
      event.preventDefault()
      event.stopPropagation()
      return false
    }

    if (target.classList.contains('sd-insertCol') || target.classList.contains('sd-insertRow')) {
      event.preventDefault()
      event.stopPropagation()
      this.currentDragOver = event.target
      this.setLastReady(target)
      this.cleanDragZone()
    } else if (target.classList.contains('sg_view')) {
      this.cleanDrag()
    } else if (target.classList.contains('columns')) {
      event.preventDefault()
      event.stopPropagation()
      this.currentDragOver = event.target
      this.cleanLastReady()
      let targetSize = this.getColumnSizeFromString(target.className)
      if (targetSize > 1) {
        this.addSplitGhost(target)
      } else {
        this.addStopGhost(target)
      }
    } else if (target.classList.contains('sg_row')) {
      this.cleanDrag()
    }
  }

  cleanDrag () {
    this.currentDragOver = null
    this.cleanDragZone()
    this.cleanLastReady()
  }

  resetEnd (row) {
    window.PubSub.publish('ACTION_ZONE_RESIZE', row)
    const end = row.getElementsByClassName('end')[0]
    if (end) {
      end.classList.remove('end')
    }
    const cols = row.getElementsByClassName('columns')
    cols[cols.length - 1].classList.add('end')
    this.trySaveStructure()
  }

  addSplitGhost (target) {
    let pos = target.getBoundingClientRect()

    this.cleanDragZone()

    appendHtml(document.body, "<div class='sd-dragZone'><div class='sd-left'></div><div class='sd-right'></div></div>")
    let dg = document.getElementsByClassName('sd-dragZone')[0]

    let top = pos.top < 50 ? 50 : pos.top
    let h = pos.top < 50 ? pos.height + (pos.top - 50) : pos.height
    if ((top + h) > window.innerHeight) {
      h = h - ((top + h) - window.innerHeight)
    }
    dg.style.top = top + 'px'
    dg.style.left = pos.left + 'px'
    dg.style.width = pos.width + 'px'
    dg.style.height = h + 'px'
    dg.addEventListener('dragover', (e) => {
      e.preventDefault()
      e.stopPropagation()
      if (!e.target.classList.contains('sd-active')) {
        let elem = dg.getElementsByClassName('sd-active')
        if (elem && elem[0]) {
          elem[0].classList.remove('sd-active')
        }
        e.target.classList.add('sd-active')
        this.dzActive = e.target
      }
    })
    dg.addEventListener('drop', (e) => {
      e.stopPropagation()
      e.preventDefault()
    }, false)
  }

  addStopGhost (target) {
    this.cleanDragZone()

    let pos = target.getBoundingClientRect()
    let dg

    appendHtml(document.body, "<div class='sd-dragZone sd-stop'></div>")
    dg = document.getElementsByClassName('sd-dragZone')[0]
    dg.style.top = pos.top + 'px'
    dg.style.left = pos.left + 'px'
    dg.style.width + pos.width + 'px'
    dg.style.height = pos.height + 'px'
  }
}
