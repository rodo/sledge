import {deleteReferenceOnDisk, deleteProject, addDoc, getViews, getPages, getProjects, openProject, createProject, addView, addPage} from './interface/interface_mac'
import {store, connect} from './store'
import conf from './conf'
import _conf from './config'

export default class ProjectManager {

  constructor (settings) {
    PubSub.subscribe('DELETE_PROJ', () => this.deleteCurrentProject())
    this.projDocumentConfID = 'Configuration'
    this.singleControllerConfID = _conf.controllers['SINGLE'].dbref
    this.loc = window.sledge.nslocal
    connect('projects', this)
    connect('currentProject', this)
    connect('pages', this, () => {
      // page update
    })
    connect('pendingProject', this, (projName) => {
      if (!projName) {
        return
      }
      this.pending = projName
      openProject(projName, () => {
        store.dispatch({ type: 'CURRENT_PROJECT', currentProject: projName })
      })
    })

    PubSub.subscribe('ADD_VIEW', () => {
      this.addNewView()
    })
    PubSub.subscribe('ADD_PAGE', () => {
      this.addNewPage()
    })
  }

  setCurrent (name) {
    this.current = name
  }

  deleteCurrentProject () {
    let old = this.currentProject
    deleteProject(this.currentProject, () => {
      deleteReferenceOnDisk(old)
      this.getProjects()
    })
  }

  getProjects () {
    getProjects(a => store.dispatch({ type: 'PROJECTS', projects: a }))
  }

  generateUniqueName (from, name) {
    let splitted
    let currentIdx
    let currentPagesIdx = []

    Object.keys(from).forEach((key) => {
      splitted = from[key].name.split(name)
      if (splitted[1]) {
        currentIdx = parseInt(splitted[1], 10)
        if (currentIdx) {
          currentPagesIdx.push(currentIdx)
        }
      }
    })

    return currentPagesIdx.length === 0 ? 1 : (Math.max(...currentPagesIdx) + 1)
  }

  addNewView () {
    // @todo generate indexed name
    addView(this.currentProject, {...conf.defaultViewConf, name: window.sledge.nslocal.newview}, res => {
      store.dispatch({ type: 'PENDING_NEW_VIEW', data: res.id })
      getViews()
    })
  }

  addNewPage () {
    //let idex = this.generateUniqueName(this.pages, 'New Page')

    const pageConf = {
      'type': 'page',
      'name': `${this.loc.gen_addpagename}`,
      'controller': 'SINGLE',
      'views': []
    }
    addPage(this.currentProject, pageConf, res => {
      store.dispatch({ type: 'PENDING_NEW_PAGE', data: res.id })
      getPages()
    })
  }

  add (name, languageCode) {
    let canuse = true

    this.projects.forEach(element => {
      if (element === name) {
        canuse = false
        return true
      }
    })

    if (canuse) {
      createProject(name, resProject => {
        addDoc(name, {
          'type': 'theme',
          '_id': 'theme',
          'current': 'default'
        }, res => {})
        
        addDoc(name, {
          'type': 'media',
          '_id': 'media'
        }, res => {})

        addDoc(name, {
          'type': 'languages',
          '_id': 'languages',
          'list': [
            languageCode
          ]
        }, res => {})

        const viewConf = {...conf.defaultViewConf, name: this.loc.gen_fisrtview}
        addView(name, viewConf, resView => {
          const pageConf = {
            'type': 'page',
            'name': this.loc.gen_mainpage,
            'controller': 'SINGLE',
            'views': [resView.id]
          }
          addPage(name, pageConf, resPage => {
            addDoc(name, {
              'type': 'settings',
              '_id': 'settings',
              'startPage': resPage.id
            }, res => this.getProjects())
          })
        })
      })
    }
    return canuse
  }
}
