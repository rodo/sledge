import {appendHtml} from './utilities'

export default class Resizable {

  constructor (config) {
    this.onDragElement = null
    this.initialPosX = null
    this.domElement = config.domElement
    this.resizePanding = false

    const markup = `<div data-draggable='true' data-direction='right' class='sd-actionZone-right'></div>
                    <div data-draggable='true' data-direction='left' class='sd-actionZone-left'></div>`

    // <div data-draggable='true' data-direction='bottom' class='sd-actionZone-bottom'></div>

    window.requestAnimationFrame(() => {
      appendHtml(this.domElement, markup)

      this.onResize = config.onResize ? config.onResize : null
      this.onStartResize = config.onStartResize ? config.onStartResize : null
      this.onStopResize = config.onStopResize ? config.onStopResize : null

      this.domElement.onmousedown = (e) => this.mouseDown(e)
    })
  }

  mouseMove (e) {
    e.preventDefault()
    e.stopPropagation()
    
    const x = e.pageX
    const y = e.pageY

    switch (this.dragDirection) {
      case 'right':
        this.domElement.style.width = this.initialSizes.width + (x - this.initialMousePositionX) + 'px'
        break
      case 'left':
        this.domElement.style.width = this.initialSizes.width - (x - this.initialMousePositionX) + 'px'
        this.domElement.style.left = this.initialSizes.left + (x - this.initialMousePositionX) + 'px'
        break
      case 'bottom':
        this.domElement.style.height = this.initialSizes.height + (y - this.initialMousePositionY) + 'px'
        break
      default:
    }

    if (this.onResize) {
      this.onResize({
        elem: this.domElement,
        currentSize: this.domElement.getBoundingClientRect(),
        initialPos: this.initialSizes,
        direction: this.dragDirection
      })
    }
  }

  mouseDown (e) {
    if (e.target.dataset.draggable) {
      e.preventDefault()
      e.stopPropagation()

      this.resizePanding = true
      e.target.classList.add('sd-resizePanding')
      this.onDragElement = e.target
      this.dragDirection = this.onDragElement.dataset.direction
      this.initialSizes = this.domElement.getBoundingClientRect()
      this.initialMousePositionX = e.pageX
      this.initialMousePositionY = e.pageY

      this.upHandler = e => this.mouseUp(e)
      this.moveHandler = e => this.mouseMove(e)
      document.body.addEventListener('mousemove', this.moveHandler)
      document.body.addEventListener('mouseup', this.upHandler)

      if (this.onStartResize) {
        this.onStartResize(e)
      }
    }
  }

  mouseUp (e) {
    e.preventDefault()
    e.stopPropagation()

    if (this.onDragElement) {
      document.body.removeEventListener('mousemove', this.moveHandler)
      document.body.removeEventListener('mouseup', this.upHandler)
      this.resizePanding = false
      this.upHandler = null
      this.moveHandler = null
      this.onDragElement = null
      this.initialSizes = null
      this.initialMousePositionX = null
      this.initialMousePositionY = null

      if (this.onStopResize) {
        this.onStopResize({
          currentSize: this.domElement.getBoundingClientRect()
        })
        }
    }
  }

}
