import Projects from './projectManager'
import Dashboard from './views/dashboard/dashboard'
import Editor from './views/editor/editor'
import actionsBar from './views/actionsBar/actionsBar'
import Structure from './structureManager'
import Iap from './views/iap/main'
import {getSledgePlus} from './interface/interface_mac'

// load components
require('./components/responsiveSwitch')
require('./components/actionMask')
require('./components/popin/popin')
require('./components/dockMenu/dockMenu')
require('./components/dockPanel/dockPanel')
require('./components/componentsList/componentsList')
require('./components/viewSettings')
require('./components/builder/builder')
require('./components/actionZone/actionZone')
require('./components/languageSelector/main')
require('./components/colorSelector/main')
require('./components/imageSelector/main')
require('./components/longTextEditor/main')

// properties
require('./components/propertiesPanel/propertiesPanel')
require('./components/properties/text/main')
require('./components/properties/text_long/main')
require('./components/properties/link/main')
require('./components/properties/background/main')
require('./components/properties/buttons/main')
require('./components/properties/headertextblock/main')
require('./components/properties/vh/main')
require('./components/properties/logobackground/main')
require('./components/properties/menu/main')
require('./components/properties/image/main')
require('./components/properties/radiolist/main')
require('./components/properties/custom/main')
require('./components/properties/marketingicon/main')
require('./components/properties/carousel/main')
require('./components/properties/embed/main')
require('./components/properties/links/main')
require('./components/properties/social/main')
require('./components/properties/container/main')
require('./components/properties/sitemap/main')
require('./components/properties/audio/main')
require('./components/properties/custom_content/main')
require('./components/properties/files/main')

import globalEvents from './globalEvents'
import * as _local from '../local/nslocal'
import Bridge from './interface/framework_bridge'
import {initConf} from './config'

//localStorage.getItem("osxMode") @ici

window.sledge.nslocal = _local[window.nslocal || 'en']
window.sledge.languageCode = window.nslocal

const fmBridge = new Bridge()
window.$sledge.bridge = fmBridge
window.$sledge.isBuilder = true

initConf()

let addInst = (name, Obj) => { sledge[name] = new Obj() }

document.body.appendChild(document.createElement('sd-propertiespanel'))

globalEvents()

getSledgePlus(() => {
  addInst('actionsBars', actionsBar)
  addInst('projects', Projects)
  addInst('dashboard', Dashboard)
  addInst('editor', Editor)
  addInst('structure', Structure)
  addInst('iad', Iap)
  window.sledge.projects.getProjects()
})
