export function guidGenerator () {
  const date = new Date()
  const S4 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  }
  return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4()) + date.getDate() + date.getDay() + date.getFullYear() + date.getHours() + date.getMinutes() + date.getMilliseconds()
}

export function getHTML (path, data) {
  const postTemplate = JST[path]
  if (!data) data = {}
  return postTemplate(data)
}

export function appendHtml (container, html) {
  container.insertAdjacentHTML('beforeend', html)
}

export function prependHtml (container, html) {
  container.insertAdjacentHTML('afterbegin', html)
}

export function insertHtmlBefore (container, html) {
  container.insertAdjacentHTML('beforebegin', html)
}

export function insertNodeBefore (container, ref, node) {
  container.insertBefore(node, ref)
}

export function insertNodeAfter (container, ref, node) {
  container.insertBefore(node, ref.nextSibling)
}

export function insertHtmlAfter (container, html) {
  container.insertAdjacentHTML('afterend', html)
}

export function html (containerClass, html) {
  const container = document.getElementsByClassName(containerClass)[0]
  container.innerHTML = html
}

export function moveBefore (ref, nodeRef, nodeTarget) {
  ref.insertBefore(nodeRef, nodeTarget)
}

export function remove (elem) {
  if (!elem) return
  if (typeof elem === 'string') {
    let elems = document.querySelectorAll(elem);
    [].forEach.call(elems, (element) => {
      element.parentElement.removeChild(element)
    })
  } else {
    if (elem.parentElement) {
      elem.parentElement.removeChild(elem)
    }
  }
}

export function domUnwrap (elem, action) {
  if (typeof elem === 'string') {
    elem = document.querySelector(elem)
  }
  if (elem) {
    action(elem)
  }
}

export function addClass (elem, theClassName) {
  let sel = document.querySelector(elem)
  if (sel) {
    sel.classList.add(theClassName)
  }
}

export function removeClass (elem, theClassName) {
  if (typeof elem === 'string') {
    elem = document.querySelector(elem)
  }
  if (elem) {
    elem.classList.remove(theClassName)
  }
}

export function elementsFromPoint (x, y) {
  const elements = []
  const previousPointerEvents = []
  let current, i, d

  // get all elements via elementFromPoint, and remove them from hit-testing in order
  while ((current = document.elementFromPoint(x, y)) && elements.indexOf(current) === -1 && current != null) {
    // push the element and its current style
    elements.push(current)
    previousPointerEvents.push({
      value: current.style.getPropertyValue('pointer-events'),
      priority: current.style.getPropertyPriority('pointer-events')
    }
  )
  // add "pointer-events: none", to get to the underlying element
    current.style.setProperty('pointer-events', 'none', 'important')
  }

  // restore the previous pointer-events values
  for (i = previousPointerEvents.length; d = previousPointerEvents[--i];) {
    elements[i].style.setProperty('pointer-events', d.value ? d.value : '', d.priority)
  }

  return elements
}

export function setActionMask (mess) {
  let loc = window.sledge.nslocal
  let panel = document.createElement('sd-actionmask')
  panel.dataset.message = loc[mess]
  document.body.appendChild(panel)
}
