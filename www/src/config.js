let conf = {
  dbURL: 'http://lite.couchbase./',
  // dbURL: "http://MacBook-Pro-de-Rodolphe.local:49479/",
  themeRollerUrl: 'https://sledge-app.github.io/theme-',
  marketingUrl: "https://sledge-app.github.io/web/marketing/marketing_",
  controllers: {
    'SINGLE': {
      type: 'SINGLE',
      label: {
        fr: 'Page &agrave; une seule vue',
        en: 'Single view page'
      },
      dbref: 'ctrlSingle'
    },
    'STRIP': {
      type: 'STRIP',
      label: {
        en: 'Multi-views strip page',
        fr: 'Page multi-vues par bandes'
      },
      dbref: 'ctrlStrip'
    },
    'SIDE': {
      type: 'SIDE',
      label: {
        fr: 'Page mutli-vues avec panneau lat&eacute;ral de type hamburger',
        en: 'Multi-views page with hamburger type side panel'
      },
      dbref: 'ctrlSide'
    }
  },
  components: [],
  defaultComponentsList: ['space', 'title', 'text', 'simple-header', 'image']
}

export function initConf () {
  const components = [
    'accordion-menu',
    'callout',
    'buttons',
    'full-marketing-section',
    'marketing-section',
    'title-text',
    'testimonial-full',
    'testimonial',
    'links-list',
    'map-title',
    'map',
    'contact',
    'image-full',
    'image',
    'footer-social',
    'footer-simplemap',
    'footer-sitemap',
    'video-link',
    'full-card',
    'top-card',
    'simple-card',
    'card',
    'mediaobject',
    'thumbnail',
    'orbit',
    'link-dashboard',
    'text',
    'space',
    'view',
    'row',
    'title',
    'header',
    'simple-header',
    'header-polaroid',
    'feature',
    'simple-audio',
    'default-audio',
    'album-audio',
    'album-audio-list',
    'circle-header',
    'progress',
    'image-link',
    'images-patchwork',
    'custom'
  ]
  components.forEach((compName) => {
    let data = require(`../framework/components/${compName}/package.json`)
    if (data.category !== 'sd-internal') {
      conf.components.push(data)
    }
  })
  if (window.sledge && window.sledge.languageCode) {
    conf.components.sort((a, b) => {
      return a.name[window.sledge.languageCode].localeCompare(b.name[window.sledge.languageCode])
    })
  }
}

export default conf
