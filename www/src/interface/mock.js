var mock = {};

mock.getAllViews = function() {
    window.nativeCallBack([
        "coucou",
        "testproj"
    ]); 
}

mock.getLocal = function() {
    
    const obj = '{Optional({"component-buttons-name":"Boutons","component-title-about":"Un titre","component-list-panel-title":"Composants","component-buttons-about":"Des boutons","backDashboardbutton":"Retour tableau","component-title-name":"Titre","hello":"bonjour"})}';
    window.nativeCallBack(obj); 
}


mock.openProject = function (projName) {
    window.nativeCallBack('{\"_id\":\"-7Y3gNowaKIuwV4ti5nqMQR\",\"_rev\":\"1-8b8a41e34f4fcc8055cff59659dbf222\",\"navigation\":\"single\",\"type\":\"view\",\"kind\":\"main\",\"name\":\"main view\",\"_local_seq\":1}'); 
}

export default mock;