import {connect} from '../store'
import {getMediasPath} from './interface_mac'
import SiteMapManager from '../siteMapManager'
import defTheme from '../../defaultTheme/assets/sledge/config.json'
import {getBGWrapper} from '../../framework/src/properties/background'

class Bridge {
  constructor () {
    this.currentId = null
    this.listennerPort = window.listennerPort
    $sledge.defaultThemeConf = defTheme
    $sledge.responsiveState = 'large'
    connect('views', this)
    connect('languages', this, () => {
      this.languagesCode = this.languages.list
      window.$sledge.language = this.languages.list[0]      
    })      
    connect('pages', this, () => {
      $sledge.pages = this.pages
    })
    connect('responsiveState', this, () => {
      $sledge.responsiveState = this.responsiveState
      window.requestAnimationFrame(() => $(window).trigger('changed.zf.mediaquery'))
    })
    connect('currentProject', this)
    connect('projectConf', this, d => {
      this.projectConf = d
    })
    connect('sideMenuCurrentItem', this, obj => {
      if (!obj) {
        return
      }

      if (obj) {
        this.currentProperties = obj.properties || {}
      } else {
        this.currentProperties = {}
      }

      if (!window.$sledge.properties) {
        window.$sledge.properties = {}
      }
      
      let loaded = false
      if (this.currentId === obj._id) {
        loaded = true
      } else {
        this.currentId = obj._id
      }

      /*if (!this.currentLanguage) {
        this.currentLanguage = window.$sledge.language
      }
      if (this.currentLanguage !== window.$sledge.language) {
        this.currentLanguage = window.$sledge.language
        loaded = false
      }*/

      if (obj.type === 'view' && !window.sledge.isPublish && !loaded) { //
        window.requestAnimationFrame(() => $(document.body).foundation())
      }
      
      if (obj.type === 'page') {
        if (!obj.views) {
          return
        }
        
        obj.views.forEach(view => {
          let viewObj = this.views[view]
          if (viewObj && viewObj.properties) {
            window.$sledge.properties = {...window.$sledge.properties, ...viewObj.properties}
          }
        })

        if (obj.sideViews) {
          obj.sideViews.forEach(view => {
            let viewObj = this.views[view]
            if (viewObj && viewObj.properties) {
              window.$sledge.properties = {...window.$sledge.properties, ...viewObj.properties}
            }
          })          
        } 

        if (!window.sledge.isPublish) {
          window.requestAnimationFrame(() => $(document.body).foundation())
        }
      } else {
        if (obj && obj.properties) {
          $sledge.properties = {...$sledge.properties, ...obj.properties}
        }
      }
    })

    this.siteMapManager = new SiteMapManager()
  }

  getMediasUrl (name, callback) {
    getMediasPath(name, callback)
  }

  refreshAllProps () {
    PubSub.publish('LANGUAGE_UPDATE')
    let elems = document.querySelectorAll('.columns')
    ;[].forEach.call(elems, elem => {
      elem.refreshProps()
    })
    // !!
    window.requestAnimationFrame(() => $(document.body).foundation())
    window.requestAnimationFrame(() => window.$sledge.foundationReInit())
  }

  initComponent (componentID, props) {

    if (!componentID) {
      return
    }

    if (!props || (props && Object.keys(props).length === 0)) {
      PubSub.publish('readyToSaveStructure')
      return
    }
    props.forEach(element => {
      if (element.default) {
        let p = JSON.parse(JSON.stringify(element.default))
        if (!this.currentProperties[componentID]) {
          this.currentProperties[componentID] = {}
        }
        if ((element.type === 'text' || element.type === 'text_long') && element.default.text) {
          Object.keys(p.text).forEach(key => {
            p.text[key] = encodeURIComponent(p.text[key])
          })
        }
        this.currentProperties[componentID][element.key] = p
      }
    })
    if (!$sledge.properties) {
      $sledge.properties = {}
    }
    $sledge.properties = {...$sledge.properties, ...this.currentProperties}

    document.getElementById(componentID).refreshProps()
    PubSub.publish('readyToSaveStructure', this.currentProperties)
  }

  saveComponent () {
    PubSub.publish('readyToSaveStructure')
  }

  addHiddenImage (imgPath, name, imgidx, desc) {
    document.body.insertAdjacentHTML('afterbegin', `
    <div class="full reveal" id="${imgidx}" data-reveal>
    <p>${name}</p>  
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
      ${
        desc
        ? decodeURIComponent(desc)
        : ''
      }
      <div class="sd-img-reveal-full">
      <img src="${imgPath}"/>
      </div>
    </div>`)
  }

  addHiddenView (viewID, id) {
    let view = this.views[viewID]
    if (view && view.structure) {
      if (view.properties) {
        window.$sledge.properties = {...window.$sledge.properties, ...view.properties}
      }
      const wrapper = getBGWrapper(view.settings, window.localStorage.getItem('currentProject'))
      //${view.name}
      document.body.insertAdjacentHTML('afterbegin', `
        <div class="full reveal" id="${id}" data-reveal>
          <p></p>
          <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
          </button>
          ${wrapper}
          ${decodeURIComponent(view.structure)}
          </div>
        </div>`)
    }
  }
}

export default Bridge
