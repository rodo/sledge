/*
http://developer.couchbase.com/documentation/mobile/1.1.0/develop/references/couchbase-lite/rest-api/index.html
*/
import mock from './mock'
import {store} from '../store'
import _conf from '../config'
import {setActionMask, remove} from '../utilities'

const url = _conf.dbURL

export function getThemeConf () {
  const path = _conf.themeRollerUrl + 'list'
  $.getJSON(path, (res) => {
    //window.sledge.themeList = res
    store.dispatch({ type: 'SET_THEMES_LIST', data: res })
  })
  .fail(() => {
    console.error('error geting theme from server')
  })
}

export function send (restAPI, method, callBackSuccess, data) {
  var param = {
    url: url + restAPI,
    method: method
  }

  if (data) {
    param.data = data
    param.dataType = 'json'
  }

  $.ajax(param).done(response => callBackSuccess(response))
}

export function downloadTheme (projName, ref, update) {
  let loc = window.sledge.nslocal
  PubSub.publish('START_LOADING', {message: update ? loc.update_theme : loc.download_theme})
  native.downloadTheme(projName, ref)
}
export function removeTheme (projName, ref) {
  native.removeTheme(projName, ref)
}

export function updateThemeDial () {
  native.updateThemeDial()
}

window.updateCurrentTheme = function () {
  setActionMask('update_theme')
  window.sledge.editor.updateTheme()
}

window.downloadThemeStart = function (ref) {
  //
}

window.downloadThemeDone = function (ref) {
  setTimeout(() => { //time to ensure write on disk is ok (specialy in case of restauration)
    PubSub.publish('STOP_LOADING')
    window.actionReset()
    store.dispatch({ type: 'SET_CURRENT_THEME', themeref: ref })
  }, 800)
  
}

 // because jquery send headers that make a 406 error
export function sendXHR (restAPI, method, callBackSuccess, data) {
  let xhr = new XMLHttpRequest()
  xhr.open(method, url + restAPI, true)
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.onreadystatechange = function (e) {
    if (xhr.readyState == 4 && (xhr.status === 201 || xhr.status === 200)) {
      if (callBackSuccess)
        callBackSuccess(JSON.parse(xhr.responseText))
    }
  }

  if (data) {
    xhr.send(JSON.stringify(data))
  } else {
    xhr.send()
  }
}

export function getProjects (success) {
  send('_all_dbs', 'GET', success)
}

export function deleteProject (proj, success) {
  send(proj, 'DELETE', success)
}

export function deleteReferenceOnDisk (proj) {
  native.deleteReferenceOnDisk(proj)
}

export function importArchive () {
  setActionMask('action_importAr')
  native.importAchiveDial()
}

export function archive (proj) {
  setActionMask('action_arch')
  native.archive(proj)
}

window.importDone = function () {
  window.actionReset()
  window.sledge.projects.getProjects()
}

window.actionReset = function () {
  remove(document.querySelector('sd-actionmask'))
}

export function createProject (name, success) {
  send(name, 'PUT', success)
}

export function openProject (projName, callBack, sender) {
  window.nativeCallBack = data => {
    $sledge.currentdb = projName
    data = (data) ? JSON.parse(data) : null
    callBack(data)
        // window.nativeCallBack = null;
  }
  (typeof native !== 'undefined') ? native.openProject(projName) : mock.openProject(projName)
}

export function addDoc (name, conf, success) {
  sendXHR(name, 'POST', success, conf)
}

export function addView (name, conf, success) {
  sendXHR(name, 'POST', success, conf)
}

export function addPage (name, conf, success) {
  sendXHR(name, 'POST', success, conf)
}

export function setProjectConf (name, id, conf, success) {
  sendXHR(name + '/' + id, 'PUT', success, conf)
}

export function updatePageProp (name, id, conf) {
  sendXHR(name + '/' + id, 'PUT', (data) => {
    conf._rev = data.rev
    store.dispatch({ type: 'UPDATE_PAGE', data: conf })
  }, conf)
}

export function setViewStructure (name, id, conf, callback) { //to rm - use setViewParam
  sendXHR(name + '/' + id, 'PUT', callback, conf)
}

export function setViewParam (name, id, conf, callback) {
  sendXHR(name + '/' + id, 'PUT', callback, conf)
}

export function setLanguages (name, conf) {
  sendXHR(name + '/languages', 'PUT', () => getLanguages(name), conf)
}

export function get (name, id, success) {
  sendXHR(name + '/' + id, 'GET', success)
}

export function deleteView (projName, viewId, rev, success) {
  sendXHR(projName + '/' + viewId + '?rev=' + rev, 'DELETE', success)
}

export function deletePage (projName, viewId, rev, success) {
  sendXHR(projName + '/' + viewId + '?rev=' + rev, 'DELETE', success)
}

window.attachmentsToProceed = {}

window.attachmentPath = (data) => {
  data = data.replace('Optional(', '') // @tocheck
  data = data.replace(')', '')
  const res = JSON.parse(data)
  window.attachmentsToProceed[res.name](res.path)
}

export function getMediasPath (name, callBack) {
  window.attachmentPath[name] = callBack
  native.getAttachmentPath(name)
}

export function setWindowName (name) {
  native.setWindowName(name)
}

export function getMedias (name, senderCallBack) {
  function callBack (data) {
    store.dispatch({ type: 'SET_MEDIAS', data: data })
    if (senderCallBack) {
      window.requestAnimationFrame(() => senderCallBack())
    }
  }
  get(name, 'media', callBack)
}

export function deleteFile (ref, numberOfItems) {
  native.deleteFile(ref, numberOfItems)
}

window.mediaUpdate = function () {
  getMedias(window.$sledge.currentdb)
}

export function getLanguages (name, senderCallBack) {
  function callBack (data) {
    store.dispatch({ type: 'SET_LANGUAGES', data: data })
    if (senderCallBack) {
      window.requestAnimationFrame(() => senderCallBack(data))
    }
  }
  get(name, 'languages', callBack)
}

export function getProjConf (name, senderCallBack) {
  function callBack (data) {
    store.dispatch({ type: 'SET_PROJECT_CONF', conf: data })
    if (senderCallBack) {
      window.requestAnimationFrame(() => senderCallBack())
    }
  }
  get(name, 'settings', callBack)
}

export function setProjConf (name, conf) {
  sendXHR(name + '/settings', 'PUT', () => getProjConf(name), conf)
}

window.setLocalPublishPath = function (path) {
  store.dispatch({ type: 'SET_LOCAL_PUBLISH_PATH', value: path })
}

export function getTheme (name, senderCallBack) {
  function callBack (data) {
    store.dispatch({ type: 'SET_CURRENT_THEME', themeref: data.current })
    if (senderCallBack) {
      // window.requestAnimationFrame(() => senderCallBack())
    }
  }
  get(name, 'theme', callBack)
}

export function checkForTheme (projName, ref) {
  native.checkForTheme(projName, ref)
}

window.useDefaultTheme = function () {
  store.dispatch({ type: 'SET_CURRENT_THEME', themeref: 'default' })  
}

window.restTheme = function (conf) {
  setActionMask('rest_theme')
  let loc = window.sledge.nslocal
  store.dispatch({ type: 'SET_CURRENT_THEME', themeref: 'default' })
  PubSub.publish('START_LOADING', {message: loc.rest_theme})
}

export function openLink (link) {
  native.openLink(link)
}

export function getThemeConfig (projName, themeRef, callback) {
  window.setThemeConfig = callback
  native.getThemeConfig(projName, themeRef)
}

export function getViews (callBack, sender) {
  window.nativeCallBack = data => {
    if (data.indexOf('Optional') !== -1) {
      data = data.replace('{Optional(', '') // @tocheck
      data = data.replace(')}', '')
      console.info(data)
      data = (data) ? JSON.parse(data) : null
    }
    if (callBack) {
      callBack(data)
    }
    store.dispatch({ type: 'SET_VIEWS', views: data })
    window.nativeCallBack = null
  }
  (typeof native !== 'undefined') ? native.getAllViews() : mock.getAllViews()
    // native.getAllViews();
}

export function getPages (callBack, sender) {
  window.nativeCallBack = data => {
    if (data.indexOf('Optional') !== -1) {
      data = data.replace('{Optional(', '') // @tocheck
      data = data.replace(')}', '')
      data = (data) ? JSON.parse(data) : null
    }
    store.dispatch({ type: 'SET_PAGES', pages: data })
    window.nativeCallBack = null
  }
  typeof native !== 'undefined' ? native.getAllPages() : mock.getAllPages()
    // native.getAllViews();
}

export function getLocal (callBack, sender) {
  window.nativeCallBack = data => {
    if (data.indexOf('Optional') !== -1) {
      data = data.replace('{Optional(', '') // @tocheck
      data = data.replace(')}', '')
      data = (data) ? JSON.parse(data) : null
    }
    callBack.call(sender, data)
        // window.nativeCallBack = null;
  }
    // native.getLocal();
  (typeof native !== 'undefined') ? native.getLocal() : mock.getLocal()
}

window.publishReady = function () {
  if (window.sledge && window.sledge.publishManager) {
    window.sledge.publishManager.ready()
  }
}

window.nextPage = function () {
  if (window.sledge && window.sledge.publishManager) {
    window.sledge.publishManager.nextPage()
  }
}

export function saveImages (array = [], projName, favicon = [], siteMap = '', siteMapPath = '') {
  native.saveImages(JSON.stringify(array), projName, JSON.stringify(favicon), siteMap, siteMapPath)
}

export function getPublishPath () {
  native.getPublishPath()
}

export function publishPage (projName, name, markup, status, language, files) {
  native.publishPage(projName, name, markup, status, language, files)
}

export function initPublish (name, themeName) {
  native.initPublish(name, themeName)
}

export function openPreview () {
  native.openPreview()
}

export function openPublish () {
  native.openPublish()
}

export function getFavicon (size, callback) {
  window.nativeCallBack = name => {
    callback(name)
  }
  window.native.getFavicon(size)
}

export function browseFile (callback, onlySound = false, onlyImage = false, onlyImagePlus = false) {
  window.nativeCallBack = (name) => {
    name = name.replace('Optional(', '') // @tocheck
    name = name.replace(')', '')
    callback(JSON.parse(name))
  }
  window.native.getFile(onlySound, onlyImage, onlyImagePlus)
}

window.setIapProduct = function (products) {
  store.dispatch({ type: 'IAP_DATA', data: JSON.parse(products) })
}

window.cancelIap = function () {
  store.dispatch({type: 'IAP_STATE', state: 0})
  store.dispatch({ type: 'IAP_DATA', data: null })
  window.actionReset()
}

window.successIap = function () {
  store.dispatch({type: 'IAP_STATE', state: 3})
  window.actionReset()
}

export function getSledgePlus (callback) {
  window.nativeCallBack = v => {
    store.dispatch({type: 'SLEDGE_PLUS', data: v})
    if (callback) {
      callback()
    }
  }
  native.getPlus()
}

export function requestStore () {
  native.requestStore()
}

export function buyProduct () {
  native.buyProduct()
}

export function restoreProduct () {
  native.restoreProduct()
}

window.importFile = function (fileName) {
  importArchive()
}


window.openUpdateDial = function() {
  window.sledge.dashboard.softwareUpdateDial()
}

export function getUpdateDialState () {
  window.native.getUpdateDialState()
}
