define(["classGenerator", "pubsub", "utilities"], function (gengen, pubsub, util) {
	"use strict";

	var Class = gen.create.call(this, Class),
		proto = gen.getPrototype(Class);

    proto.init = function () {
    	//init code here
    }

    return Class;
});