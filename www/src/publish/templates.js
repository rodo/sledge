export default (pageName, name, markup, languages, kind, scriptBefore, scriptAfter, scriptLoad, projName, favicon, meta = {}, files, domain) => {
  let root = languages.length !== 1 ? '..' : '.'
  let empty = 'sd_empty_line'
  let fullmarkup = `
<!DOCTYPE HTML>
<html lang="${window.$sledge.language}">
<head>
<title>${window.decodeURIComponent(name)} | ${projName}</title>

${
meta.tags
? meta.tags
: empty
}

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0">
${alternate(languages, kind, pageName, domain)}
<meta name = "viewport" content = "width = device-width, initial-scale = 1, viewport-fit=cover">
${
meta.keywords
? `<meta name="keywords" content="${window.decodeURIComponent(meta.keywords)}">`
: empty
}
${
meta.description
? `<meta name="description" content="${window.decodeURIComponent(meta.description)}">`
: empty
}
${
meta.author
? `<meta name="author" content="${window.decodeURIComponent(meta.author)}">`
: empty
}

${getFaviconLink(root, favicon, '16')}
${getFaviconLink(root, favicon, '32')}
${getFaviconLink(root, favicon, '96')}
${getFaviconLink(root, favicon, '120', true)}
${getFaviconLink(root, favicon, '152', true)}
${getFaviconLink(root, favicon, '180', true)}
${getFaviconLink(root, favicon, '167', true)}

<!--*** stylesheet ***-->
<link id="foundationIcons" rel="stylesheet" type="text/css" href="${root}/theme/assets/foundation-icons/foundation-icons.css"/>      
<link rel="stylesheet" type="text/css" href="${root}/theme/assets/css/app.css"/>
<link rel="stylesheet" type="text/css" href="${root}/framework/dist/bundle.css"/>

<style>
  .sg_view {
    position: relative;
  }
</style>      
${
  scriptBefore
  ? `<script>${scriptBefore}</script>`
  : empty
}
${
  scriptAfter
  ? `<script>document.addEventListener("DOMContentLoaded", function(event) { ${scriptAfter} });</script>`
  : empty
}
${
  scriptLoad
  ? `<script>document.addEventListener("load", function(event) { ${scriptLoad} });</script>`
  : empty
}
${componentsLib(files, root)}
</head>
${markup}
<script language="JavaScript" src="${root}/theme/assets/js/app.js"></script>
<script language="JavaScript" src="${root}/framework/dist/bundle_runtime.js"></script>  



<script>
  // Patch for a Bug in v6.3.1
  $(window).on('changed.zf.mediaquery', function() {
    $('.invisible').removeClass('invisible');
  });
</script>
<script>
  window.addEventListener("load", function(event) {
    $(window).trigger('resize'); //fix equalizer at start
  });
</script>
</html>
    `
  return fullmarkup
}

function getFaviconLink (root, favicon, size, apple) {
  let rel = apple ? 'apple-touch-icon' : 'icon'
  if (favicon && favicon.indexOf(size) !== -1) {
    return `<link rel="${rel}" type="image/png" href="${root}/media/favicon_${size}.png" sizes="${size}x${size}">`
  }
  return 'sd_empty_line'
}

export function languageRoot (projectName, languages, langRef, siteName, favicon, domain) {
  let root = '.'
  return `
  <!DOCTYPE HTML>
      <html lang="x-default">
      <head>
      ${
        languages.title
        ? `<title>${window.decodeURIComponent(languages.title)}</title>`
        : `<title>${siteName || projectName}</title>`
      }
      
      ${
      languages.keywords
      ? `<meta name="keywords" content="${window.decodeURIComponent(languages.keywords)}">`
      : ''
      }
      ${
      languages.desc
      ? `<meta name="description" content="${window.decodeURIComponent(languages.desc)}">`
      : ''
      }
      ${
      languages.author
      ? `<meta name="author" content="${window.decodeURIComponent(languages.author)}">`
      : ''
      }
      
      
      <meta http-equiv="Content-Type" content="text/html;" />
      <META charset="UTF-8">      
      <META http-equiv="Cache-Control" content="no-cache">
      <META http-equiv="Pragma" content="no-cache">
      <META http-equiv="Expires" content="0">
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      ${alternateRoot(languages.list, domain)}
      
      ${getFaviconLink(root, favicon, '16')}
      ${getFaviconLink(root, favicon, '32')}
      ${getFaviconLink(root, favicon, '96')}
      ${getFaviconLink(root, favicon, '120', true)}
      ${getFaviconLink(root, favicon, '152', true)}
      ${getFaviconLink(root, favicon, '180', true)}
      ${getFaviconLink(root, favicon, '167', true)}

      <style>
        .header {
          font-size: 19px;
          height: 200px;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          background: rgb(246, 246, 255);          
        }

        .content {
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
        }

        .content a {
          display: flex;
          flex-direction: column-reverse;
          align-items: center;
          margin: 20px;
        }

        .footer {
          margin-top: 50px;
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        .content img {
          width: 150px;
          height: 100px;
        }
      </style>
      <script language="JavaScript" src="./lib/jquery/jquery.min.js"></script>
      <script language="JavaScript">
        window.langRef = ${JSON.stringify(langRef)};
        window.languages = {
          en: {
            'wel': 'Welcome to',
            'cho': 'Please choose a language',
            'fl': 'Flags provided by',
            'made': 'Made with'
          },
          fr: {
            'wel': 'Bienvenue sur',
            'cho': 'Merci de choisir une langue',
            'fl': 'Drapeaux fournis par',
            'made': 'Fait avec'            
          }
        }
        var userLang = navigator.language || navigator.userLanguage; 
        window.usrlanguage = 'en'

        switch (true) {
          case userLang.indexOf('fr-') !== -1 :
            window.usrlanguage = 'fr'
            break;
          default:
            break;
        }
        
      </script>
      </head>
    <body style="margin:0">
    <div class="header">
      <span><span id="sl-wel"></span> <b>${siteName || projectName}</b> </span> 
      <span><span id="sl-cho"></span> :</span>
    </div>  
    <div class="content">
      ${getLinks(languages.list, langRef)}
    </div>
    <div class="footer">
        <span class="flag"><span id="sl-fl">Flags provided by</span> <a href="http://www.geonames.org">geonames</a></span>
        <span class="made"><span id="sl-made">Made with </span> <a href="http://sled-app.io">Sled</a></span>
    </div>  
    </body>

    <script language="JavaScript">
        document.getElementById('sl-wel').innerHTML = window.languages[window.usrlanguage].wel
        document.getElementById('sl-cho').innerHTML = window.languages[window.usrlanguage].cho
        document.getElementById('sl-fl').innerHTML = window.languages[window.usrlanguage].fl
        document.getElementById('sl-made').innerHTML = window.languages[window.usrlanguage].made
        if (window.usrlanguage !== 'en') {
          var links = document.querySelectorAll(".sl-link");
          [].forEach.call(links, function(link) {
            link.querySelector('span').innerHTML = window.langRef[window.usrlanguage][link.dataset.code]
          });
        }
    </script>    
    </html>  
  `
}

function getLinks (languages, langRef) {
  let ref = {
    en: 'gb'
  }
  
  let markup = ''
  languages.forEach(code => {
    markup += `<br><a href="./${code}/index.html" class="sl-link" data-code="${code}"><span>${langRef['en'][code]}</span>
    <img src="http://www.geonames.org/flags/x/${ref[code] || code}.gif"/></a>`
  })
  return markup
}

function alternate (languages, kind, name, domain) {
  const root = domain || '..'
  if (languages.length === 1) {
    return ''
  } else {
    let markup = `<link rel="alternate" href="${root}/index.html" hreflang="x-default">`
    let pagname = kind === 'root' ? 'index.html' : `${name}.html`
    let current = window.$sledge.language
    languages.forEach(code => {
      if (code !== current) {
        if (pagname === 'index.html') {
          markup += `<link rel="alternate" href="${root}/${code}/index.html" hreflang="${code}" />`
        } else {
          markup += `<link rel="alternate" href="${root}/${code}/${pagname}" hreflang="${code}" />`
        }
      }
    })
    return markup
  }
}

function alternateRoot (languages, domain) {
  const root = domain || '.'
  if (languages.length === 1) {
    return ''
  } else {
    let markup = ''
    languages.forEach(code => {
      markup += `<link rel="alternate" href="${root}/${code}/index.html" hreflang="${code}" />`
    })
    return markup
  }
}

function componentsLib (files, root) {
  let markup = ''
  if (files.length !== 0) {
    files.forEach(file => {
      markup += `<script language="JavaScript" src="${root}/componentsLib/${file}"></script>`
    })
  }
  return markup
}
