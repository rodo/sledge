import {connect} from '../store'
import {initPublish, publishPage, saveImages} from '../interface/interface_mac'
import renderPage, {languageRoot} from './templates'
import * as _langList from '../../local/languages.js'

export default class PublishManager {
  constructor () {
    connect('rootViews', this)
    connect('projectConf', this)
    connect('rootPages', this)
    connect('pages', this)
    connect('currentTheme', this)
    connect('languages', this)
    this.pageDone = JSON.parse(window.localStorage.getItem('pageDone'))
    this.languageDone = {}
    this.canRun = false
  }

  ready () {
    if (!this.canRun) {
      this.canRun = true
    }
    let projectName = window.localStorage.getItem('currentProject')
    this.publishProjectName = projectName.replace(/\s/g, '_')
    let first = window.localStorage.getItem('firstPublish')
    if (first === 'true') {
      window.localStorage.setItem('firstPublish', false)
      initPublish(this.publishProjectName, this.currentTheme)
    } else {
      this.savePage()
    }
  }

  nextPage () {
    let alldone = true
    this.rootPages.forEach(page => {
      if (!this.pageDone[page._id]) {
        window.setCurrent(page._id)
        alldone = false
      }
    })
    if (alldone) {
      let languageok = true
      if (this.languages.list.length === 1) {
        const xml = this.siteMapGenerator()
        const smPath = this.projectConf.domain ? `Sitemap: ${this.projectConf.domain}/sitemap.xml` : ''
        saveImages(window.sledge.media, this.publishProjectName, this.projectConf.favicon, xml, smPath)
        window.sledge.media = null
        window.localStorage.setItem('pagesPath', JSON.stringify([]))
        window.localStorage.setItem('files', JSON.stringify([]))
      } else {
        this.languages.list.forEach(lg => {
          if (!this.languageDone[lg]) {
            this.pageDone = {}
            languageok = false
            window.localStorage.setItem('pageDone', JSON.stringify({}))
            window.localStorage.setItem('files', JSON.stringify([]))
            window.$sledge.language = lg
            window.setCurrent(this.rootPages[0]._id)
          }
        })
        if (languageok) {
          const xml = this.siteMapGenerator()
          const smPath = this.projectConf.domain ? `Sitemap: ${this.projectConf.domain}/sitemap.xml` : ''
          saveImages(window.sledge.media, this.publishProjectName, this.projectConf.favicon, xml, smPath)
          window.sledge.media = null
          window.localStorage.setItem('pagesPath', JSON.stringify([]))
          let rootMarkup = languageRoot(window.localStorage.getItem('currentProject'), this.languages, _langList, this.projectConf.siteName, this.projectConf.favicon, this.projectConf.domain)
          rootMarkup = rootMarkup.replace(/sd_empty_line\n/g, '')
          publishPage(this.publishProjectName, 'sg-index-lang', rootMarkup, 'root', 'default', JSON.stringify([]))
        } else {
          this.savePage()
        }
      }
    } else {
      this.savePage()
    }
  }

  savePage () {
    this.languageDone[window.$sledge.language] = true
    let pageID = window.localStorage.getItem('currentView')
    this.pageDone[pageID] = true

    window.localStorage.setItem('pageDone', JSON.stringify(this.pageDone))

    let pageData = this.pages[pageID]

    // clean html from unwanted dom node
    $('[data-publish="no"]').remove()

    let currentMarkup = document.body.outerHTML
    const comps = document.body.querySelectorAll('.columns')
    const componentsName = []
    const files = []
    const filesToCopy = []
    ;[].forEach.call(comps, comp => {
      componentsName.push(comp.dataset.ref)
    })
    
    let allFiles = window.localStorage.getItem('files')
    if (allFiles) {
      allFiles = JSON.parse(allFiles)
    }
    if (!allFiles || allFiles.length === 0) {
      allFiles = []
    }

    componentsName.forEach(compName => {
      let data = require(`../../framework/components/${compName}/package.json`)
      if (data.files) {
        data.files.forEach(file => {
          if (files.indexOf(file) === -1) {
            files.push(file)
            if (allFiles.indexOf(file) === -1) {
              filesToCopy.push(file)
              allFiles.push(file)
            }
          }
        })
      }
    })

    window.localStorage.setItem('files', JSON.stringify(allFiles))

    currentMarkup = currentMarkup.replace(/(^|\W|\b)<sg-(\w+)/g, (_, $1, $2) => { return $1 + '<div' })
    currentMarkup = currentMarkup.replace(/(^|\W|\b)<div-(\w+)/g, (_, $1, $2) => { return $1 + '<div' })
    currentMarkup = currentMarkup.replace(/(^|\W|\b)<div-(\w+)/g, (_, $1, $2) => { return $1 + '<div' })
    currentMarkup = currentMarkup.replace(/(^|\W|\b)<\/sg-(\w+)/g, (_, $1, $2) => { return $1 + '</div' })
    currentMarkup = currentMarkup.replace(/(^|\W|\b)<\/div-(\w+)/g, (_, $1, $2) => { return $1 + '</div' })
    currentMarkup = currentMarkup.replace(/(^|\W|\b)<\/div-(\w+)/g, (_, $1, $2) => { return $1 + '</div' })
    currentMarkup = currentMarkup.replace(/file:(.+?)www/g, this.languages.list.length !== 1 ? '..' : '.')
    currentMarkup = currentMarkup.replace(/file:(.+?)Contents\/Resources/g, this.languages.list.length !== 1 ? '..' : '.')

    let kind = 'default'
    if (this.projectConf.startPage === pageData._id) {
      kind = 'root'
    }
    let scriptBefore = pageData.scripts && pageData.scripts.before ? decodeURIComponent(pageData.scripts.before) : null
    let scriptAfter = pageData.scripts && pageData.scripts.after ? decodeURIComponent(pageData.scripts.after) : null
    let scriptLoad = pageData.scripts && pageData.scripts.load ? decodeURIComponent(pageData.scripts.load) : null

    const meta = {}
    if (pageData.author) {
      meta.author = pageData.author
    }
    if (pageData.description && pageData.description[window.$sledge.language]) {
      meta.description = window.decodeURIComponent(pageData.description[window.$sledge.language])
    }
    if (pageData.keywords && pageData.keywords[window.$sledge.language]) {
      meta.keywords = window.decodeURIComponent(pageData.keywords[window.$sledge.language])
    }
    const projName = window.localStorage.getItem('currentProject')
    let markup = renderPage(pageData.name, pageData.local && pageData.local[window.$sledge.language] ? pageData.local[window.$sledge.language] : pageData.name, currentMarkup, this.languages.list, kind, scriptBefore, scriptAfter, scriptLoad, this.projectConf.siteName || projName, this.projectConf.favicon, meta, files, this.projectConf.domain)
    markup = markup.replace(/sd_empty_line\n/g, '')

    this.pageDone[pageData._id] = true
    const language = this.languages.list.length !== 1 ? window.$sledge.language : 'default'

    let pagesPath = window.localStorage.getItem('pagesPath')
    if (pagesPath) {
      pagesPath = JSON.parse(pagesPath)
    } else {
      pagesPath = []
    }

    if (pagesPath.indexOf(kind === 'root' ? 'index' : pageData.name.replace(/\s/g, '_')) === -1) {
      pagesPath.push(kind === 'root' ? 'index' : pageData.name.replace(/\s/g, '_'))
    }

    window.localStorage.setItem('pagesPath', JSON.stringify(pagesPath))
    publishPage(this.publishProjectName, pageData.name.replace(/\s/g, '_'), markup, kind, language, JSON.stringify(filesToCopy))
  }

  siteMapGenerator () {
    let pagesPath = window.localStorage.getItem('pagesPath')
    if (pagesPath) {
      pagesPath = JSON.parse(pagesPath)
    } else {
      pagesPath = []
    }
    let markup = ''
    if (this.projectConf.domain) {
      if (this.languages.list.length === 1) {
        markup += `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`
        pagesPath.forEach(page => {
          markup += `<url>
            <loc>${this.projectConf.domain}/${page}.html</loc>
            <priority>${page === 'index' ? '1' : '0.8'}</priority>
          </url>\n`
        })
        markup += '</urlset>'
      } else {
        markup += `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
        <url>
        <loc>${this.projectConf.domain}/index.html</loc>
        `
        this.languages.list.forEach(code => {
          markup += `<xhtml:link rel="alternate" hreflang="${code}" href="${this.projectConf.domain}/${code}/index.html" />\n`
        })
        markup += '</url>\n'
        if (pagesPath) {
          pagesPath.forEach(page => {
            this.languages.list.forEach(code => {
              markup += `<url>
              <loc>${this.projectConf.domain}/${code}/${page}.html</loc>
              <priority>${page === 'index' ? '1' : '0.8'}</priority>\n`
              this.languages.list.forEach(code2 => {
                markup += `<xhtml:link rel="alternate" hreflang="${code2}" href="${this.projectConf.domain}/${code2}/${page}.html" />\n`
              })
              if (page === 'index') {
                markup += `<xhtml:link rel="alternate" hreflang="x-default" href="${this.projectConf.domain}/index.html" />\n`
              }
              markup += '</url>\n'
            })
          })
        }   
        markup += '</urlset>'
      }
    }
    return markup
  }
}
