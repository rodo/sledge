export default class DockPanel extends HTMLElement {
  constructor () {
    super()
  }
  connectedCallback () {
    this.classList.add('sd_dockpanel')
    this.isActive = false
  }

  init (conf, sender) {
    const senderOffset = sender.getBoundingClientRect()
    const right = senderOffset.right - senderOffset.left + 1
    this.style.right = right + 'px'
    this.style.top = (senderOffset.top - 50) + 'px'
    this.refID = conf.id
    if (conf.size) {
      this.style.height = conf.size.height + 'px'
      this.style.width = conf.size.width + 'px'
    }
    if (conf.content) {
      this.innerHTML = conf.content
    }
    this.show()
  }

  toggle () {
    this.isActive ? this.hide() : this.show()
  }

  show () {
    console.log(this)
    this.classList.add('sd_active')
    this.isActive = true
  }

  hide () {
    this.classList.remove('sd_active')
    this.isActive = false
  }
}

window.customElements.define('sd-dockpanel', DockPanel)
