import * as _langList from '../../../local/languages.js'
import {connect} from '../../store'
import render from './template'

export default class languagesSelector extends HTMLElement {
  constructor () {
    super()
  }
  connectedCallback () {
    connect('languages', this)
    render(this, _langList[window.nslocal || 'en'], this.languages.list, this.dataset.lang)
  }

}

window.customElements.define('sd-languages-selector', languagesSelector)
