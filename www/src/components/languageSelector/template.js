export default (sender, list, projectList, current) => {
  sender.innerHTML = `
  <select data-kind="language">
    ${getMarkup(list, projectList, current)}
  </select>`
}

function getMarkup (list, projectList, current) {
  let markup = ''
  projectList.forEach(code => {
    markup += `<option value="${code}" ${current === code ? 'selected' : ''}>${list[code]}</option>`
  })
  return markup
}
