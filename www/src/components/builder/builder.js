import {connect} from '../../store'

export default class BuilderComponent extends HTMLElement {
  constructor () {
    super()
    connect('sideMenuCurrentItem', this, v => {
      if (this.view) {
        this.view.refresh(this.sideMenuCurrentItem.settings, this.currentProject)
      }
    })
    connect('currentProject', this)
  }

  connectedCallback () {
    this.classList.add('sd-f-wrapper')
    let markup = `<sg-row>
      <sg-title class="small-12 medium-12 large-12 small-offset-0 large-offset-0 medium-offset-0 columns">This is a title</sg-title>
    </sg-row>`
    if (this.sideMenuCurrentItem.structure) {
      markup = decodeURIComponent(this.sideMenuCurrentItem.structure)
    }
    this.view = document.createElement('sg-view')
    this.appendChild(this.view)
    this.view.applyMarkup(markup, this.sideMenuCurrentItem.settings, this.currentProject)
  }
}

window.customElements.define('sd-builder', BuilderComponent)
