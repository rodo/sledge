export default (sender) => {
  let loc = window.sledge.nslocal
  let name = sender.dataset.kind
  let markup = `<br><label class="sd-full sd-label-small">${loc.prop_color_theme} <span class="sd-color-sheme-rm" data-name="${name}" data-kind="rmColor">${loc.prop_color_rmColor}</span></label>
  <div class="sd-color-scheme-cont">
    <div class="sd-color-scheme-wrapper">
      <div data-name="${name}" data-kind="scheme" data-value="1" class="sg-color-scheme-1"></div>  
    </div>
    <div class="sd-color-scheme-wrapper">
      <div data-name="${name}" data-kind="scheme" data-value="2" class="sg-color-scheme-2"></div>  
      <div data-name="${name}" data-kind="scheme" data-value="3" class="sg-color-scheme-3"></div>  
      <div data-name="${name}" data-kind="scheme" data-value="4" class="sg-color-scheme-4"></div>  
      <div data-name="${name}" data-kind="scheme" data-value="5" class="sg-color-scheme-5"></div>
    </div>
    <div class="sd-color-scheme-wrapper">
      <div data-name="${name}" data-kind="scheme" data-value="6" class="sg-color-scheme-6"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="7" class="sg-color-scheme-7"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="8" class="sg-color-scheme-8"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="9" class="sg-color-scheme-9"></div>      
    </div>
    <div class="sd-color-scheme-wrapper">
      <div data-name="${name}" data-kind="scheme" data-value="10" class="sg-color-scheme-10"></div>       
      <div data-name="${name}" data-kind="scheme" data-value="11" class="sg-color-scheme-11"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="12" class="sg-color-scheme-12"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="13" class="sg-color-scheme-13"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="14" class="sg-color-scheme-14"></div>      
    </div>
    <div class="sd-color-scheme-wrapper">
      <div data-name="${name}" data-kind="scheme" data-value="15" class="sg-color-scheme-15"></div>          
      <div data-name="${name}" data-kind="scheme" data-value="16" class="sg-color-scheme-16"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="17" class="sg-color-scheme-17"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="18" class="sg-color-scheme-18"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="19" class="sg-color-scheme-19"></div>      
    </div>
    <div class="sd-color-scheme-wrapper">
      <div data-name="${name}" data-kind="scheme" data-value="20" class="sg-color-scheme-20"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="21" class="sg-color-scheme-21"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="22" class="sg-color-scheme-22"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="23" class="sg-color-scheme-23"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="24" class="sg-color-scheme-24"></div>      
    </div>
    <div class="sd-color-scheme-wrapper">
      <div data-name="${name}" data-kind="scheme" data-value="25" class="sg-color-scheme-25"></div>          
      <div data-name="${name}" data-kind="scheme" data-value="26" class="sg-color-scheme-26"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="27" class="sg-color-scheme-27"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="28" class="sg-color-scheme-28"></div>      
      <div data-name="${name}" data-kind="scheme" data-value="29" class="sg-color-scheme-29"></div>      
    </div>
  </div>`
  sender.innerHTML = markup
}
