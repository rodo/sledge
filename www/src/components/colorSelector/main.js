import render from './template'

export default class colorSelector extends HTMLElement {
  constructor () {
    super()
  }
  connectedCallback () {
    render(this)
  }

}

window.customElements.define('sd-color-selector', colorSelector)
