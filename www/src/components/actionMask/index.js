export default class ActionMask extends HTMLElement {
  constructor () {
    super()
  }

  connectedCallback () {
    document.querySelector('.sd_main').classList.add('sd-action-mask-body')
    this.className = 'sd-action-mask'
    this.innerHTML = `
    <div class="sampleContainer">
      <div class="loader">
          <span class="dot dot_1"></span>
          <span class="dot dot_2"></span>
          <span class="dot dot_3"></span>
          <span class="dot dot_4"></span>
      </div>
    </div>
    <p>${this.dataset.message}</p>`
  }

  disconnectedCallback () {
    document.querySelector('.sd_main').classList.remove('sd-action-mask-body')
  }
}

window.customElements.define('sd-actionmask', ActionMask)
