export default (params, root) => {
  root.innerHTML = `<ul class="sd_dock_menu">
  ${
    params.map(item => `<li data-id="${item.id}" class="${item.className} sd_dock_item"></li>`).join('')
  }
  </ul>`
}
