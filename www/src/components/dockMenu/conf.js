export default [
  {
    name: 'components',
    id: 'components',
    className: 'sd-comp-panel-button',
    content: '<sd-componentlist></sd-componentlist>',
    size: {
      height: 400,
      width: 350
    }
  },
  {
    name: 'components',
    id: 'settings',
    className: 'sd-comp-panel-settings',
    content: '<sd-viewsettings></sd-viewsettings>',
    size: {
      height: 470,
      width: 400
    }
  }
]
