import render from './template'
import conf from './conf'
import {remove} from '../../utilities'

export default class DockMenu extends HTMLElement {

  constructor () {
    super()
  }

  connectedCallback () {
    this.classList.add('sd_dockmenu')
  }

  init () {
    render(conf, this)

    this.map = {}

    const clickOnItem = (e) => {
      if (e.target && e.target.dataset.id) {
        PubSub.publish('documentScroll')
        this.panelManager(this.map[e.target.dataset.id], e.target)
      }
    }

    let confCopy = JSON.parse(JSON.stringify(conf))
    confCopy.forEach(item => { this.map[item.id] = item })

    this.ul = document.getElementsByClassName('sd_dock_menu')[0]
    if (this.ul) {
      this.ul.addEventListener('click', clickOnItem)
    }

    PubSub.subscribe('dockMenuCloseCurrent', () => {
      if (this.currentItemRef) {
        this.currentItemRef.toggle()
        this.currentItemRef = null
      }
      this.removeSelectedState()
    })

    this.addEventListener('mouseenter', (e) => PubSub.publish('viewMouseOut', e), false)
  }

  removeSelectedState () {
    const elem = document.querySelector('.sd_dock_item.sd-selected')
    if (elem) {
      elem.classList.remove('sd-selected')
      if (elem.dataset.id === 'settings') {
        remove(this.map['settings'].ref)
        this.map['settings'].ref = null
        this.settingsClose = true
      }
    }
  }

  panelManager (conf, sender) {
    this.removeSelectedState()
    if (this.currentItemRef && this.currentItemRef.isActive && this.currentItemRef.refID !== conf.id) {
      this.currentItemRef.toggle()
    }

    if (conf) {
      if (this.map[conf.id].ref) {
        this.map[conf.id].ref.toggle()
      } else {
        if (!this.settingsClose) {
          this.map[conf.id].ref = document.createElement('sd-dockpanel')
          const cont = document.getElementsByClassName('sd_builder')[0]
          cont.appendChild(this.map[conf.id].ref)
          this.map[conf.id].ref.init(conf, sender)
        } else {
          this.settingsClose = false
        }
      }
    }

    if (this.map[conf.id].ref && this.map[conf.id].ref.isActive) {
      sender.classList.add('sd-selected')
      this.currentItemRef = this.map[conf.id].ref
    }
  }
}

window.customElements.define('sd-dockmenu', DockMenu)
