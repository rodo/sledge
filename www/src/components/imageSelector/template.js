import iconsConf from '../../../defaultTheme/assets/foundation-icons/.fontcustom-data.json'
import currentTheme from '../../../defaultTheme/assets/sledge/config.json'

export default (sender, medias, noImgTheme) => {
  let loc = window.sledge.nslocal
  let name = sender.dataset.kind
  let markup = `
  <h2>${loc.prop_prop_img}</h2>
  
  <ul>
    <li data-kind="menu" class="sd-current" data-menu="0">${loc.prop_prop_img_cur}</li>
    <li data-kind="menu" data-menu="1">${loc.prop_prop_img_cat}</li>
    ${noImgTheme ? '' : `<li data-kind="menu" data-menu="2">${loc.prop_prop_img_t}</li>`}
    <li data-kind="menu" data-menu="3">${loc.prop_prop_img_lm}</li>
  </ul>

  <div class="sg-image-sel-wrapper">
    <div class="sg-image-sel-cur sd-current"></div>
    <div class="sg-image-sel-cat">${getMediaList(medias)}</div>
    ${noImgTheme ? '' : `<div class="sg-image-sel-img">${getThemeMediaImage()}</div>`}
    <div class="sg-image-sel-logo">${getIconsList()}</div>
  </div>
  `
  sender.innerHTML = markup
}

function getThemeMediaImage () {
  let theme = window.sledge.currentTheme === 'default' ? currentTheme : sledge.themeConfig
  let markup = `<div class="sd-prop-library">`
  if (!theme.bgimage) {
    return ''
  }
  for (let item in theme.bgimage) {
    markup += `<div data-kind="imgTheme" class="sd-prop-list-theme-img ${theme.bgimage[item]}" data-name="${theme.bgimage[item]}"><span>${item}</span></div>`
  }
  markup += '</div>'
  return markup
}

function getIconsList () {
  let markup = ''
  iconsConf.glyphs.forEach(cl => {
    markup += `<div class="sd-glyphs-cat"><i data-icn="${cl}" data-kind="sd-func-cat" class="${cl}"></i></div>`
  })
  return markup
}

function getMediaList (list) {
  let attachements = []
  let supportedType = ['png', 'jpg']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div data-kind="media" class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div data-kind="media" class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}

export function renderCurrent (sender, type, image) {
  let loc = window.sledge.nslocal  
  let cont = sender.querySelector('.sg-image-sel-cur')
  let style = ''
  let prevClass = ''
  let i = ''

  if (type === 'media') {
    let fileName = image
    if (fileName) {
      if (sender.dataset.imgsize !== '1') {
        fileName = fileName + '_small'
      }
      style = `background-image:url(${window.$sledge.conf.dbServerUrl}/${window.$sledge.currentdb}/media/${fileName}.${sender.dataset.mediatype}); `
    }
  }

  if (type === 'logo') {
    i = `<i class="${image}"></i>`
  }

  let markup = `<div class="sd-prop-preview-block">
    <div class="sd-prop-preview ${prevClass}" style="${style}">
      ${i}
    </div>
  </div>
  <button data-kind="browse">${loc.prop_prop_img_find}</button>
  `

  cont.innerHTML = markup
}
