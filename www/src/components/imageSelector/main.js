import render, {renderCurrent} from './template'
import {connect} from '../../store'

export default class imageSelector extends HTMLElement {
  constructor () {
    super()
  }

  static get observedAttributes () {
    return ['data-image', 'data-type']
  }

  connectedCallback () {
    connect('medias', this)
    connect('currentProject', this)
    this.addEventListener('click', e => this.updateClick(e))
    this.renderAll()  
  }

  renderAll () {
    render(this, this.medias, this.dataset.noimgtheme)
    this.current = this.querySelector('ul li')
    this.currentPanel = this.querySelector('.sg-image-sel-cur')
  }

  updateClick (e) {
    switch (e.target.dataset.kind) {
      case 'menu':
        this.manageTab(e)
        break 
      default:
    }
  }

  manageTab (e) {
    this.current.classList.remove('sd-current')
    this.currentPanel.classList.remove('sd-current')
    this.current = e.target
    this.current.classList.add('sd-current')

    switch (e.target.dataset.menu) {
      case '0':
        this.currentPanel = this.querySelector('.sg-image-sel-cur')
        this.currentPanel.classList.add('sd-current')
        break
      case '1':
        this.currentPanel = this.querySelector('.sg-image-sel-cat')
        this.currentPanel.classList.add('sd-current')
        break
      case '2':
        this.currentPanel = this.querySelector('.sg-image-sel-img')
        this.currentPanel.classList.add('sd-current')
        break
      case '3':
        this.currentPanel = this.querySelector('.sg-image-sel-logo')
        this.currentPanel.classList.add('sd-current')
        break
      default:
    }
  }

  attributeChangedCallback (attributeName, oldValue, newValue, namespace) {
    if (newValue) {
      let sp = newValue.split('/')
      renderCurrent(this, sp[0], sp[1])
    }
  }

}

window.customElements.define('sd-image-selector', imageSelector)
