import {connect, store} from '../../store'
import render from './template'

export default class ResponsiveSwitch extends HTMLElement {

  connectedCallback () {
    this.classList.add('sd_responsiveswitch')
    render(this)
    connect('responsiveState', this, state => {
      this.defineResponsiveState(state)
    })
    this.addEventListener('click', (e) => this.changeResponsiveState(e), false)
    this.defineResponsiveState(this.responsiveState)
  }

  changeResponsiveState (e) {
    const target = e.target
    let v = 'large'
    switch (true) {
      case (target.className.indexOf('small') !== -1):
        v = 'small'
        break
      case (target.className.indexOf('medium') !== -1):
        v = 'medium'
        break
      case (target.className.indexOf('large') !== -1):
        v = 'large'
        break
    }
      
    store.dispatch({type: 'SET_RESPONSIVE_STATE', value: v})
  }

  defineResponsiveState (state) {
    const current = this.getElementsByClassName('sd_active')
    if (current[0]) {
      current[0].classList.remove('sd_active')
    }
    const newElem = this.getElementsByClassName('sd_r' + state)[0]
    newElem.classList.add('sd_active')

    //window.requestAnimationFrame(() => window.$sledge.equalizerRefresh())
  }

}

window.customElements.define('sd-responsive-switch', ResponsiveSwitch)
