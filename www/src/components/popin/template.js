export default (container, conf) => {
  let markup = `
  <div class="bal_popinContent"></div>
  <div class="bal_popinActions">
    ${getButtons(conf)}
  </div>
  `
  container.insertAdjacentHTML('beforeend', markup)
}

function getButtons (conf) {
  let markup = ``
  conf.forEach((element, index, array) => {
    markup += `<button data-name="${element.name}" class="${element.className}">${element.label}</button>`
  })
  return markup
}
