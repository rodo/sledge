import render from './template'

export default class Popin extends HTMLElement {

  createdCallback (conf, parent) {
  }
  static observedAttributes = ['height']
  init (conf, parent) {
    render(this, conf)
    this.classList.add('bal_popin')
    this.contentBox = this.getElementsByClassName('bal_popinContent')[0]
    conf.forEach((element, index, array) => {
      this[element.name] = this.getElementsByClassName(element.className)[0]
      this[element.name].addEventListener('click', function () {
        element.action.call(parent)
      })
    })
  }

  	readAttributes () {
    	// this.hour = this.getAttribute( "hour" );
  	}

  	attributeChangedCallback (attrName, oldVal, newVal) {
      if (attrName === 'height') {
        this.style.height = newVal + 'px'
        this.style.top = '-' + (parseInt(newVal, 10) - 50) + 'px'
      }
  	}

  setAutoHeight () {
    var that = this
    autoHeight(this)

    window.addEventListener('resize', function () {
      autoHeight(that)
    })
  };

  show () {
    var that = this
    window.requestAnimationFrame(function () {
      that.classList.add('visible')
    })
  }

  hide () {
    this.classList.remove('visible')
  }

  delete () {

  }

  addContent (tpl) {
    this.contentBox.innerHTML = tpl
  };

  autoHeight (ref) {
    var h = window.innerHeight,
      newHeight = h - 53 - 20
    ref.style.height = newHeight + 'px'
    ref.style.top = '-' + (parseInt(newHeight, 10) - 50) + 'px'
  }

}

window.customElements.define('sdg-popin', Popin)
