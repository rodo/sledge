(function () {
 	"use strict";

  	var proto = Object.create(HTMLElement.prototype);
  
  	proto.createdCallback = function() {
      	var that = this;
      	//this.readAttributes();
      	if (this.innerHTML === "") {
        	this.initMarkup();
      	}
  	};
  
  	proto.initMarkup = function() {
    	
        var post = {
            title: 'Projects',
        };
    
        var postTemplate = JST['src/components/singleMenu/singleMenu.hbs'];
        var html = postTemplate(post);
  	};
  
  	proto.readAttributes = function() {
    	//this.hour = this.getAttribute( "hour" ); 
  	};
  
  	proto.attributeChangedCallback = function( attrName, oldVal, newVal ) {
      	//
  	};

  	document.registerElement( "bal-singleMenu", {
      	prototype: proto
  	});
}());