import render from './template'
import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('click', e => this.updateClick(e))
  }

  init (propController, props, value) {
    this.value = value || {}
    this.ctrl = propController
    this.key = props.key
    render(this, this.value)
  }

  updateOrder (data) {
    this.value.networks = data
    this.setProp()
  }

  updateClick (e) {
    if (e.target.dataset.kind === 'rm') {
      let li = e.target.parentNode
      this.value.networks.splice(li.dataset.idx, 1)
      this.setProp()
    }
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'add':
        let newNetwork = e.target[e.target.selectedIndex].value
        if (!this.value.networks) {
          this.value.networks = []
        }
        this.value.networks.push({
          name: newNetwork,
          url: ''
        })
        this.setProp()
        break
      case 'url':
        let li = e.target.parentNode
        this.value.networks[li.dataset.idx].url = e.target.value
        this.setProp()
        break
      default:
      //
    }
    // this.ctrl.setPropertyValue(this.key, e.target.value)
  }

  setProp () {
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

}

window.customElements.define('sd-properties-social', Property)
