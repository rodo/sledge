export default (sender, param = {}) => {

  let networks = [
    '500px',
    'adobe',
    'amazon',
    'android',
    'apple',
    'behance',
    'bing',
    'blogger',
    'delicious',
    'designer-news',
    'deviant-art',
    'digg',
    'dribbble',
    'drive',
    'dropbox',
    'evernote',
    'facebook',
    'flickr',
    'forrst',
    'foursquare',
    'game-center',
    'github',
    'google-plus',
    'hacker-news',
    'hi5',
    'instagram',
    'joomla',
    'lastfm',
    'linkedin',
    'medium',
    'myspace',
    'orkut',
    'path',
    'picasa',
    'pinterest',
    'rdio',
    'reddit',
    'skillshare',
    'skype',
    'smashing-mag',
    'snapchat',
    'spotify',
    'squidoo',
    'stack-overflow',
    'steam',
    'stumbleupon',
    'treehouse',
    'tumblr',
    'twitter',
    'vimeo',
    'windows',
    'xbox-20',
    'yahoo',
    'yelp',
    'youtube',
    'zerply',
    'zurb'
  ]

  let currents = []
  if (param && param.networks) {
    currents = param.networks.map(item => item.name)
  }

  let loc = window.sledge.nslocal

  let markup = `
  <label>${loc.prop_soc_add}</label>
  <select data-kind="add">
    ${getNetworks(networks, currents)}
  </select>
  <br><label class="sd-full">${loc.prop_soc_current}</label>
  <ul class="sg-prop-social">
    ${getList(param.networks || [])}
  </ul>
  `
  sender.innerHTML = markup

  $('.sg-prop-social').sortable({update: (a, b) => {
    let lis = a.target.querySelectorAll('li')
    let res = []
    ;[].forEach.call(lis, li => {
      res.push({
        name: li.dataset.name,
        url: li.dataset.url
      })
    })
    sender.updateOrder(res)
  }})
}

function getNetworks (list, currents) {
  let loc = window.sledge.nslocal
  let markup = `<option>${loc.prop_soc_choose}</option>`
  list.forEach(net => {
    if (currents.indexOf(net) === -1) {
      markup += `<option value="${net}">${net}</option>`
    }
  })
  return markup
}

function getList (currents) {
  let markup = ''
  let idx = 0
  let loc = window.sledge.nslocal
  if (currents.lenght !== 0) {
    currents.forEach(net => {
      markup += `<li data-idx="${idx}" data-url="${net.url || ''}" data-name="${net.name}">
        <span>${net.name}</span>
        <input placeholder="${loc.prop_soc_link}" data-kind="url" type="text" value="${net.url || ''}"/>
        <button title="${loc.prop_soc_rm}" data-kind="rm"></button>
      </li>`
      idx++
    })
  }
  return markup
}