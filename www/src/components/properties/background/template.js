import currentTheme from '../../../../defaultTheme/assets/sledge/config.json'

export default (sender, param, medias, props) => {
  let loc = window.sledge.nslocal
  let style = ''
  let prevClass = ''
  let fileName = null
  let allow =
  props.allow ||
    {
      image: true,
      video: true,
      color: true
    }

  param = param || {}

  if (param.backgroundtype === 'media') {
    let fileName = param ? param.media : null
    if (fileName) {
      if (param.numberOfSizes !== 1) {
        fileName = param.media + '_small'
      }
      style = `background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${fileName}.${param.mediaType}); `
    }
  }

  if (param.backgroundtype === 'pattern') {
    prevClass = param.pattern
  }

  if (param.backgroundtype === 'bgimage') {
    prevClass = param.bgimage
  }

  let attachements = []
  if (Object.keys(medias).length !== 0) {
    for (let fileName2 in medias) {
      attachements.push({...medias[fileName2], name: fileName2})
    }
  }
  let bgcolor = '#ffffff'
  if (param.color) {
    bgcolor = param.color
  }
  
  param = param || {}
  // <input class="sd-prop-bg-color" type="color" value="${bgcolor}" data-kind="color"/>
  let markup = `
  ${allow.color
    ? `<label>${loc.prop_back_color}</label>
      <sd-color-selector data-kind="colorScheme"></sd-color-selector>`
    : ''}`

  if (allow.image) {
    markup += `<label>${loc.prop_back_img}</label>
    <div class="sd-prop-preview-block">
      <div class="sd-prop-preview-block-buttons">
        <button class="sd-prop-remove-media">${loc.prop_back_rm}</button>        
        <select data-kind="displaymode" class="sd-prop-media-display">
          <option ${param.displayType ? '' : 'selected'} value="default">${loc.prop_back_full}</option>
          <option ${param.displayType ? 'selected' : ''} value="repeat">${loc.prop_back_rp}</option>
        </select>
      </div>
      <div class="sd-prop-preview ${prevClass}" style="${style}"></div>
    </div>
    <div class="sd-prop-buttons-cont">
      <button class="sd-prop-browse-media">${loc.prop_back_find}</button>
      <button data-kind="cat" data-logo="${param.logo}">${loc.prop_choose_cat}</button>
      <button data-kind="pat">${loc.prop_back_pat}</button>
      <button data-kind="img">${loc.prop_back_timg}</button>
    </div>
    ${maskMarkup(param)}`
  }

  let markupFull = `
  <div class="sd-but-catalog">
    <button class="sd-but-catalog-close" data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getMediaList(medias)}
    </div>
  </div>
  <div class="sd-but-patterns">
    <button data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getThemeMediaPattern(param)}
    </div>
  </div>  
  <div class="sd-but-img">
    <button data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getThemeMediaImage(param)}
    </div>
  </div>    
  <div class="sd-but-cont">${markup}</div>`

  sender.innerHTML = markupFull

  if (allow.color) {
    /*let theme = window.sledge.currentTheme === 'default' ? window.$sledge.defaultThemeConf : window.sledge.themeConfig

    $('.sd-prop-bg-color').spectrum({
      change: color => sender.setBackgroundColor(color.toHexString()),
      showPaletteOnly: true,
      togglePaletteOnly: true,
      togglePaletteMoreText: 'more',
      togglePaletteLessText: 'less',
      showSelectionPalette: true,
      palette: theme.fullColorList
    })*/
  }

  if (allow.image) {
    if (param.backgroundtype !== 'media' && param.mediaType !== 'mov') {
      sender.querySelector('.sd-prop-media-display').style.display = 'none'
    }

    if (param.backgroundtype !== 'media' && param.backgroundtype !== 'pattern' && param.backgroundtype !== 'bgimage') {
      sender.querySelector('.sd-prop-preview').style.display = 'none'
      sender.querySelector('.sd-prop-remove-media').style.display = 'none'
    }

    if (attachements.length === 0 || (attachements.length === 1 && fileName)) {
      sender.querySelector('.sd-prop-library-label').style.display = 'none'
      sender.querySelector('.sd-prop-library').style.display = 'none'
    }
  }
}

function getMediaList (list) {
  let attachements = []
  let supportedType = ['png', 'jpg', 'jpeg']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}

function maskMarkup (param) {
  let pos = param && param.mask ? param.mask : 'original'
  let loc = window.sledge.nslocal
  let markup = `
    <label>${loc.prop_back_dl}</label>
    <div class="sd-prop-custom-cont"><div class="sd-prop-custom-range"><select data-kind="mask">
      <option value="original" ${pos === 'original' ? 'selected' : ''}>Original</option>
      <option value="darker" ${pos === 'darker' ? 'selected' : ''}>darker</option> 
      <option value="lighter" ${pos === 'lighter' ? 'selected' : ''}>lighter</option> 
    </select>
  `
  if (pos !== 'original') {
    markup += `<span style="margin-right: 10px; margin-left: 10px;">${loc.gen_op}</span><input data-kind="maskRange" type="range" min="1" max="9" value="${param.maskRange || 5}"/></div></div>`
  } else {
    markup += '</div></div>'
  }

  return markup
}

function getThemeMediaPattern () {
  let theme = window.sledge.currentTheme === 'default' ? currentTheme : window.sledge.themeConfig

  let markup = `<div class="sd-prop-library">`
  if (!theme.pattern) {
    return ''
  }
  for (let item in theme.pattern) {
    markup += `<div class="sd-prop-list-theme ${theme.pattern[item]}" data-name="${theme.pattern[item]}"><span>${item}</span></div>`
  }
  markup += '</div>'
  return markup
}

function getThemeMediaImage () {
  let theme = window.sledge.currentTheme === 'default' ? currentTheme : sledge.themeConfig
  let markup = `<div class="sd-prop-library">`
  if (!theme.bgimage) {
    return ''
  }
  for (let item in theme.bgimage) {
    markup += `<div class="sd-prop-list-theme-img ${theme.bgimage[item]}" data-name="${theme.bgimage[item]}"><span>${item}</span></div>`
  }
  markup += '</div>'
  return markup
}
