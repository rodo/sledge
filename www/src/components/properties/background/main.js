import render from './template'
import {browseFile, getMedias} from '../../../interface/interface_mac'
import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('medias', this)
    connect('currentProject', this)
    this.value = {}
    this.addEventListener('click', e => this.update(e))
    this.addEventListener('change', e => this.updateChange(e))
  }

  updateChange (e) {
    switch (e.target.dataset.kind) {
      case 'mask':
        this.value.mask = e.target[e.target.selectedIndex].value
        if (this.value.mask === 'original') {
          delete this.value.maskRange
        } else {
          this.value.maskRange = 5
        }

        const wrapper = document.querySelector('.sd-but-cont')
        window.tempScrollTop = wrapper.scrollTop
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'maskRange':
        this.value.maskRange = e.target.value
        window.sledge.donorefresh = true
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
    }
  }

  openCatalog (id) {
    this.currentOpen = document.querySelector('.sd-but-catalog')
    this.currentOpen.classList.add('visible')
  }

  openPatternCatalog (id) {
    this.currentOpen = document.querySelector('.sd-but-patterns')
    this.currentOpen.classList.add('visible')
  }

  openImgCatalog (id) {
    this.currentOpen = document.querySelector('.sd-but-img')
    this.currentOpen.classList.add('visible')
  }

  closeCatalog () {
    this.currentOpen.classList.remove('visible')
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'displaymode':
        let v = e.target[e.target.selectedIndex].value
        if (v === 'default') {
          delete this.value.displayType
        } else {
          this.value.displayType = v
        }
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'scheme':
        this.value.color = e.target.dataset.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'rmColor':
        this.value.color = null
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'img':
        this.openImgCatalog()
        break
      case 'imgclose':
        this.closeCatalog()
        break
      case 'cat':
        this.openCatalog()
        if (e.target.dataset.logo) {
          $('.sd-prop-list-prev').css('display', 'block')
          $(`.sd-but-catalog-list div[data-name="${e.target.dataset.logo}"]`).css('display', 'none')
        }
        break
      case 'pat':
        this.openPatternCatalog()
        break
    }

    switch (true) {
      case e.target.classList.contains('sd-prop-browse-media'):
        browseFile(name => this.setBackgroundImage(name), false, true)
        break
      case e.target.classList.contains('sd-prop-remove-media'):
        this.removeBackgroundImage()
        break
      case e.target.classList.contains('sd-prop-list-theme-img'):
        delete this.value.displayType
        delete this.value.media
        delete this.value.numberOfSizes
        delete this.value.mediaType
        this.value.backgroundtype = 'bgimage'
        this.value.bgimage = e.target.dataset.name
        this.closeCatalog()
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case e.target.classList.contains('sd-prop-list-theme'):
        delete this.value.bgimage
        delete this.value.displayType
        delete this.value.media
        delete this.value.numberOfSizes
        delete this.value.mediaType
        this.value.backgroundtype = 'pattern'
        this.value.pattern = e.target.dataset.name
        this.closeCatalog()
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case e.target.classList.contains('sd-prop-list-prev'):
        let name = e.target.dataset.name
        delete this.value.bgimage
        delete this.value.pattern
        delete this.value.displayType
        this.value.backgroundtype = 'media'
        this.value.media = encodeURIComponent(name)
        this.value.numberOfSizes = this.medias[name].numberOfSizes
        this.value.mediaType = this.medias[name].type
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      default:
      //
    }
    // this.ctrl.setPropertyValue(this.key, e.target.value)
  }

  setBackgroundColor (color) {
    this.value.color = color
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  setBackgroundImage (data) {
    getMedias(this.currentProject, () => {
      delete this.value.pattern
      delete this.value.displayType
      delete this.value.bgimage
      this.value.backgroundtype = 'media'
      this.value.media = encodeURIComponent(data.name)
      if (data.numberOfSizes) {
        this.value.numberOfSizes = data.numberOfSizes
      }
      this.value.mediaType = data.type
      this.ctrl.applyProperty(this.key, this.value)
      this.ctrl.setPropertyValue(this.key, this.value)
    })
  }

  removeBackgroundImage () {
    delete this.value.pattern
    delete this.value.media
    delete this.value.bgimage
    delete this.value.numberOfSizes
    delete this.value.mediaType
    delete this.value.backgroundtype
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, this.value, this.medias, props)
    console.info("this.scrollTop")
    console.info(this.scrollTop)

    if (window.tempScrollTop) {
      window.requestAnimationFrame(() => {
        const wrapper = document.querySelector('.sd-but-cont')
        wrapper.scrollTop = window.tempScrollTop
        window.tempScrollTop = null
      })
    }
  }

}

window.customElements.define('sd-properties-background', Property)
