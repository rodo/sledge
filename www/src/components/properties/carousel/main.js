import render from './template'
import {guidGenerator} from '../../../utilities'
import {connect} from '../../../store'
import {browseFile, getMedias} from '../../../interface/interface_mac'

export default class PropertyCarousel extends HTMLElement {

  connectedCallback () {
    this.initDone = false
    connect('pages', this)
    connect('views', this)
    connect('medias', this)
    connect('currentProject', this)
    connect('sideMenuCurrentItem', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('click', e => this.onClick(e))
  }

  onClick (e) {
    switch (true) {
      case e.target.classList.contains('sd-prop-list-prev'):
        let mediaData = this.medias[e.target.dataset.name]
        mediaData.name = e.target.dataset.name
        this.saveImage(mediaData)
        this.closeCatalog()
        break
      case e.target.classList.contains('sd-prop-but-add'):
        this.value.images.push({
          id: guidGenerator()
        })
        this.setProp()
        break
      case e.target.classList.contains('sd-prop-but-rm'):
        this.removeImage(e.target.dataset.ref)
        break
      case e.target.dataset.kind === 'cat':
        this.openCatalog(e.target.dataset.ref)
        if (e.target.dataset.media) {
          $('.sd-prop-list-prev').css('display', 'block')
          $(`.sd-but-catalog-list div[data-name="${e.target.dataset.media}"]`).css('display', 'none')
        }
        break
      case e.target.dataset.kind === 'browse':
        this.currentEditedImage = e.target.dataset.ref
        browseFile(name => this.setBackgroundImage(name), false, true)
        break
      case e.target.dataset.kind === 'imgclose':
        this.closeCatalog()
        break        
      default:
      //  
    }
  }

  saveImage (data) {
    let index = this.getIndexFromRef(this.currentEditedImage)
    if (!this.value.images[index].id) {
      this.value.images[0].id = guidGenerator()
    }
    this.value.images[index]['media'] = encodeURIComponent(data.name)
    if (data.numberOfSizes) {
      this.value.images[index]['numberOfSizes'] = data.numberOfSizes
    }
    this.value.images[index]['mediaType'] = data.type
    this.setProp()
  }

  setBackgroundImage (data) {
    getMedias(this.currentProject, () => this.saveImage(data))
  }

  update (e) {
    let index = null
    switch (e.target.dataset.kind) {
      case 'text':
        index = this.getIndexFromId(e)
        if (!this.value.images[index].text) {
          this.value.images[index].text = {}
        }
        this.value.images[index].text[window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        window.sledge.donorefresh = true
        this.setProp(true)
        break
      default:
      //
    }
  }

  setText (val, ref) {
    console.info(ref)
    const index = this.getIndexFromRef(ref)
    console.info(index)
    if (!this.value.images[index].longText) {
      this.value.images[index].longText = {}
    }
    console.info(val)
    this.value.images[index].longText[window.$sledge.language] = val
    window.sledge.donorefresh = true
    this.setProp(true)
  }

  openCatalog (id) {
    this.currentEditedImage = id
    document.querySelector('.sd-but-catalog').classList.add('visible')
  }

  closeCatalog () {
    document.querySelector('.sd-but-catalog').classList.remove('visible')
  }

  removeImage (id) {
    let index = this.value.images.findIndex(item => item.id === id)
    this.value.images.splice(index, 1)
    this.setProp()
  }

  updateOrder (newOrder) {
    let newRes = []
    let index = 0
    newOrder.forEach(newId => {
      index = 0
      index = this.value.images.findIndex(item => item.id === newId)
      newRes.push(this.value.images[index])
    })
    this.value.images = newRes
    this.setProp()
  }

  getIndexFromId (e) {
    let id = e.target.dataset.ref
    if (id === 'null') {
      id = this.checkId()
    }
    let idx
    if (!this.value.images) {
      idx = 0
      this.value.images = []
    } else {
      idx = this.value.images.findIndex(item => item.id === id)
    }
    if (!this.value.images[idx]) {
      this.value.images[idx] = {}
    }
    return idx
  }

  getIndexFromRef (id) {
    let idx
    if (!this.value.images) {
      idx = 0
      this.value.images = []
    } else {
      idx = this.value.images.findIndex(item => item.id === id)
    }
    if (!this.value.images[idx]) {
      this.value.images[idx] = {}
    }
    return idx
  }

  checkId () {
    let id = guidGenerator()
    this.value.images[0].id = id
    return id
  }

  setProp (all) {
    this.ctrl.setPropertyValue(this.key, this.value)
    this.ctrl.applyProperty(this.key, this.value, all)
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    this.props = props
    this.value = value || {}
    console.info(this.value)
    if (!this.initDone) {
      render(this, value, window.$sledge.language, this.medias, props)
      this.initDone = true
    }
  }

}

window.customElements.define('sd-properties-carousel', PropertyCarousel)
