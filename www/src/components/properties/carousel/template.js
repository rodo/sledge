export default (sender, param, language, media, props) => {
  let isSingle = true
  let markup = ''
  let loc = window.sledge.nslocal
  if (!param || !param.images) {
    param = {}
    param.images = [{id: null}]
  }
  if (param.images && param.images.length !== 1) {
    isSingle = false
  }
  markup = `
  <div class="sd-but-catalog">
    <button data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getMediaList(media)}
    </div>
  </div>
  <div class="sd-txt-full">
    <button class="sd-lte-close" data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
    </div>
  </div>
  <div class="sd-but-cont">
  ${
    isSingle
    ? ''
    : `<div class='sd-prop-order-cont'>
      <label>${loc.img_ord}</label>
      <div class='sd-prop-but-order-wrapper sd-props-images-order'>
        <ul>
          ${getOrderMarkup(param.images, language)}
        <ul>
      </div>
    </div>`
  }
  ${props.single ? '' : `<button class='sd-prop-but-add'>${loc.prop_img_add}</button>`}
  <div class='sd-prop-but-wrapper ${props.single ? 'sd-prop-but-single' : ''}'>
    ${getMarkup(param.images, props, language, isSingle, media)}
  </div>
  </div>
  `
  sender.innerHTML = markup

  const longTxt = sender.querySelectorAll('sd-txt-editor')
  let idx = 0
  ;[].forEach.call(longTxt, txt => {
    txt.init(param.images[idx], props, sender.querySelector('.sd-txt-full'), (val, ref) => {
      sender.setText(val, ref)
    })
    idx++
  })

  if (!isSingle) {
    $('.sd-prop-but-order-wrapper ul').sortable({update: (a, b) => {
      let lis = a.target.querySelectorAll('li')
      let res = []
      ;[].forEach.call(lis, li => {
        res.push(li.dataset.ref)
      })
      sender.updateOrder(res)
    }})
  }
}

function getOrderMarkup (images, language) {
  let markup = ''
  images.forEach(image => {
    let style = getImageStylePath(image)
    markup += `<li class="sd-prop-images-move" style="${style}" data-ref="${image.id || null}"></li>`
  })
  return markup
}

function getMarkup (images, props, language, isSingle, media) {
  let markup = ''
  if (images) {
    images.forEach(image => {
      markup += getBaseMarkup(image, props, language, isSingle)
    })
  }
  return markup
}

function getMediaList (list) {
  let attachements = []
  let supportedType = ['png', 'jpg', 'jpeg']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}

function getBaseMarkup (image, props, language, isSingle) {
  let loc = window.sledge.nslocal
  let text = (image.text && image.text[language]) ? decodeURIComponent(image.text[language]) : ''
  return `
    <div class='sd-prop-but-item'>
      <button data-ref="${image.id || null}" class='sd-prop-but-rm'></button>
    <div>
      <label>${loc.prop_img_cap} (${window.$sledge.language})</label>
      <input data-kind="text" data-ref="${image.id || null}" type="text" class="sd-prop-but-text" value="${text}"/>
      </div>
      <div>
      ${
        props.hasDesc
        ? `<div>
          <label>${loc.prop_img_desc} (${window.$sledge.language})</label>
          <sd-txt-editor data-button="Update description" data-ref="${image.id || null}"></sd-txt-editor>
        </div>`
        : ''
      }
      </div>
    ${getImgMarkup(image)}          
  </div>`
}

function getImageStylePath (param) {
  let style = ''
  let fileName = param ? param.media : null
  if (fileName) {
    if (param.numberOfSizes !== 1) {
      fileName = param.media + '_small'
    }
    style = `background-image:url(${window.$sledge.conf.dbServerUrl}/${window.$sledge.currentdb}/media/${fileName}.${param.mediaType}); `
  }
  return style
}

function getImgMarkup (param) {
  let loc = window.sledge.nslocal
  let style = getImageStylePath(param)
  let markup =
  `<label>${loc.but_img_current}</label>
  <div class="sd-prop-preview-block">
    <div class="sd-prop-preview-block-buttons">
      <button data-kind="browse" class="sd-prop-browse-media" data-ref="${param.id || null}">${loc.but_img_new}</button>
      <button data-kind="cat" data-media="${param.media}" data-ref="${param.id || null}">${loc.prop_choose_cat}</button>
    </div>
    <div class="sd-prop-preview" style="${style}"></div>
  </div>`
  return markup
}
