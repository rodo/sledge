export default (sender, param, medias, props) => {
  sender.innerHTML = renderBackManager(param) + renderPositionManager(param, props)
}

function renderBackManager (param) {
  let loc = window.sledge.nslocal
  let markup = ''
  let back = param && param.background ? param.background : 'trans'
  let rounded = param && param.rounded ? param.rounded : false
  markup += `
    <label>${loc.gen_back}</label>
    <select data-kind="background">
      <option value="trans" ${back === 'trans' ? 'selected' : ''}>${loc.back_trs}</option>
      <option value="transText" ${back === 'transText' ? 'selected' : ''}>${loc.back_txt_st}</option> 
      <option value="transDark" ${back === 'transDark' ? 'selected' : ''}>${loc.back_trs_dk}</option> 
      <option value="transLight" ${back === 'transLight' ? 'selected' : ''}>${loc.back_trs_li}</option>
    </select>
    <br>
    <label>${loc.gen_rounded}</label>
    <input type='checkbox' data-kind="rounded" ${rounded ? 'checked' : ''}/>
  `
  return markup
}

function renderPositionManager (param, props) {
  if (props.position === false) {
    return ''
  }
  let loc = window.sledge.nslocal
  let markup = ''
  let pos = param && param.position ? param.position : 'right'
  markup += `
    <br>
    <label>${loc.proplogo_pos}</label>
    <select data-kind="position">
      <option value="right" ${pos === 'right' ? 'selected' : ''}>${loc.back_pos_right}</option>
      <option value="left" ${pos === 'left' ? 'selected' : ''}>${loc.back_pos_left}</option> 
      <option value="center" ${pos === 'center' ? 'selected' : ''}>${loc.back_pos_center}</option> 
    </select>
  `
  return markup
}