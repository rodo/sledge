import render from './template'
import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    this.value = {}
    //this.addEventListener('click', e => this.update(e))
    this.addEventListener('change', e => this.update(e))
    // this.addEventListener('keyup', e => this.update(e))
  }

  update(e) {
    switch (e.target.dataset.kind) {
      case 'background':
        this.value.background = e.target[e.target.selectedIndex].value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'position':
        this.value.position = e.target[e.target.selectedIndex].value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'rounded':
        this.value.rounded = e.target.checked
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      default:
    }
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, value, this.medias, props)
  }

}

window.customElements.define('sd-properties-headertextblock', Property)
