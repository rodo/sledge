export default (sender, param = {}) => {

    let loc = window.sledge.nslocal

    const spacing = param.spacing || 'small'
    const marginTop = param.marginTop || 'small' 
    const marginBottom = param.marginBottom || 'small' 

    sender.innerHTML =  `<br><label>${loc.prop_h_space}</label>
    <select data-kind="spacing">
      <option value="small" ${spacing === 'small' ? 'selected' : ''}>${loc.gen_size_sm}</option> 
      <option value="medium" ${spacing === 'medium' ? 'selected' : ''}>${loc.gen_size_med}</option> 
      <option value="large" ${spacing === 'large' ? 'selected' : ''}>${loc.gen_size_lg}</option>
    </select>
    
    <br><label>${loc.prop_h_margintop}</label>
    <select data-kind="marginTop">
      <option value="small" ${marginTop === 'small' ? 'selected' : ''}>${loc.gen_size_def}</option> 
      <option value="medium" ${marginTop === 'medium' ? 'selected' : ''}>${loc.gen_size_med}</option> 
      <option value="large" ${marginTop === 'large' ? 'selected' : ''}>${loc.gen_size_lg}</option>
    </select>
    
    <br><label>${loc.prop_h_marginbottom}</label>
    <select data-kind="marginBottom">
      <option value="small" ${marginBottom === 'small' ? 'selected' : ''}>${loc.gen_size_def}</option> 
      <option value="medium" ${marginBottom === 'medium' ? 'selected' : ''}>${loc.gen_size_med}</option> 
      <option value="large" ${marginBottom === 'large' ? 'selected' : ''}>${loc.gen_size_lg}</option>
    </select>`
}
