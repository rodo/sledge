import render from './template'

export default class Property extends HTMLElement {

  connectedCallback () {
    this.value = {}
    this.addEventListener('change', e => this.updateChange(e))
  }

  updateChange (e) {
    switch (e.target.dataset.kind) {
      case 'spacing':
      case 'marginBottom':
      case 'marginTop':
        this.value[e.target.dataset.kind] = e.target[e.target.selectedIndex].value
        window.sledge.donorefresh = true
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
    }
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, this.value)
  }

}

window.customElements.define('sd-properties-container', Property)
