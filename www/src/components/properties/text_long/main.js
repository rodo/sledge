import render from './template'

export default class PropertiesText extends HTMLElement {

  connectedCallback () {
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('click', e => this.updateClick(e))
    //this.addEventListener('keyup', e => this.updateKey(e))
  }
  updateClick (e) {
    switch (e.target.dataset.kind) {
      case 'left':
      case 'right':
      case 'center':
      case 'justify':
        this.value.align = e.target.dataset.kind
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      default:
    }
  }
  updateKey (e) {
    switch (e.target.dataset.kind) {
      case 'text':
        if (!this.value.text) {
          this.value.text = {}
        }
        this.value.text[window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        this.ctrl.applyProperty(this.key, this.value.text)
        break
      default:
    }
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  setText (txt) {
    if (!this.value.text) {
      this.value.text = {}
    }    
    this.value.text[window.$sledge.language] = txt
    this.ctrl.setPropertyValue(this.key, this.value)
    this.ctrl.applyProperty(this.key, this.value)
    /*window.requestAnimationFrame(() => {
      PubSub.publish('ACTION_ZONE_RESIZE')
    })*/
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'col':
        this.value.column = e.target[e.target.selectedIndex].value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        /*window.requestAnimationFrame(() => {
          window.PubSub.publish('ACTION_ZONE_RESIZE')
        })*/
        break
      default:
    }
  }

  init (propController, props, values) {
    this.value = values || {}
    this.ctrl = propController
    this.key = props.key
    render(this, this.value, props, window.$sledge.language)
  }

}

window.customElements.define('sd-properties-text_long', PropertiesText)
