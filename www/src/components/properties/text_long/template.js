/*
var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],
  ['link']
  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction

  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean']                                         // remove formatting button
]
*/


export default (sender, params, props, language) => {

  let theme = window.sledge.currentTheme === 'default' ? window.$sledge.defaultThemeConf : window.sledge.themeConfig

  const autoSize = props.autoSize || false
  var toolbarOptions = []
  if (autoSize) {
    toolbarOptions = [
      ['bold', 'italic', 'underline'],
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'color': theme.fullColorList }, { 'background': theme.fullColorList }],
      ['link'],
      ['clean']
    ]  
  } else {
    toolbarOptions = [
      ['bold', 'italic', 'underline'],
      ['blockquote', 'code-block'],
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'align': [] }],  
      [{ 'script': 'sub'}, { 'script': 'super' }],
      [{ 'size': ['small', false, 'large', 'huge'] }],
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      [{ 'color': theme.fullColorList }, { 'background': theme.fullColorList }],
      ['link'],
      ['clean']
    ]
  }
  
  let loc = window.sledge.nslocal
  params = params || {}
  let text = ''
  if (params.text) {
    text = decodeURIComponent(params.text[language])
  }
  let col = params.column || '1'
  let align = params.align || 'left'

  sender.innerHTML = `
  <label style="width:180px;">${loc.prop_text_col}</label>
  <select data-kind='col'>
    <option value="1" ${col === '1' ? 'selected' : ''}>1</option>
    <option value="2" ${col === '2' ? 'selected' : ''}>2</option>
    <option value="3" ${col === '3' ? 'selected' : ''}>3</option>
    <option value="4" ${col === '4' ? 'selected' : ''}>4</option>
  </select>
  ${
    !autoSize
    ? `<br><label style="width:180px;">${loc.prop_txt_align}</label>
    <div class="sd-txt-align-cont">
      <button data-kind="left" class="sd-txt-align-l ${(align === 'left') ? 'sd-selected' : ''}"></button>
      <button data-kind="center" class="sd-txt-align-c ${(align === 'center') ? 'sd-selected' : ''}"></button>
      <button data-kind="justify" class="sd-txt-align-j ${(align === 'justify') ? 'sd-selected' : ''}"></button>
      <button data-kind="right" class="sd-txt-align-r ${(align === 'right') ? 'sd-selected' : ''}"></button>
    </div>`
    : ''
  }
  <br><label>${loc.gen_txt} (${language})</label>
  <div class="sd-rich-text-edit" id="sd-prop-txt-edit" data-kind="text">
    ${text}
  </div>
  `
  /* fix scroll issue with cmd v */
  const Clipboard = Quill.import('modules/clipboard')

  class MyClipboard extends Clipboard {
    onPaste(e) {
      const wrapper = document.querySelector('#sd-prop-txt-edit').parentNode.parentNode
      var scrollTop = wrapper.scrollTop
      super.onPaste(e)
      setTimeout(() => {
        wrapper.scrollTop = scrollTop
      }, 10)
    }
  }

  Quill.register('modules/clipboard', MyClipboard, true)
  /* end fix scroll issue with cmd v */

  let quill = new Quill('#sd-prop-txt-edit', {
    theme: 'snow',
    modules: {
      toolbar: toolbarOptions
    }
  })
  let panel = document.querySelector('#sd-prop-txt-edit .ql-editor')
  quill.on('text-change', () => {
    sender.setText(encodeURIComponent(panel.innerHTML).replace(/[!'()*]/g, escape))
  })
}
