export default (sender, param, language, media, props) => {
  let isSingle = true
  let markup = ''
  let loc = window.sledge.nslocal
  if (!param || !param.images) {
    param = {}
    param.images = [{id: null}]
  }
  if (param.images && param.images.length !== 1) {
    isSingle = false
  }
  markup = `
  <div class="sd-but-catalog">
    <button data-kind="imgclose" class="sd-but-catalog-close"></button>
    <div class="sd-but-catalog-list">
      ${getMediaList(media)}
    </div>
  </div>
  <div class="sd-txt-full">
    <button class="sd-lte-close" data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
    </div>
  </div>
  
  <div class="sd-but-cont">
  <label class="sd-full">${loc.prop_cus_about}</label>
  ${props.single ? '' : `<button class='sd-prop-but-add'>${loc.prop_file_add}</button>`}
  <div class='sd-prop-but-wrapper ${props.single ? 'sd-prop-but-single' : ''}'>
    ${getMarkup(param.images, props, language, isSingle, media)}
  </div>
  </div>
  `
  sender.innerHTML = markup

  const longTxt = sender.querySelectorAll('sd-txt-editor')
  let idx = 0
  ;[].forEach.call(longTxt, txt => {
    txt.init(param.images[idx], props, sender.querySelector('.sd-txt-full'), (val, ref) => {
      sender.setText(val, ref)
    })
    idx++
  })
}

function getMarkup (images, props, language, isSingle, media) {
  let markup = ''
  if (images) {
    images.forEach(image => {
      markup += getBaseMarkup(image, props, language, isSingle)
    })
  }
  return markup
}

function getMediaList (list) {
  let attachements = []
  let supportedType = ['png', 'jpg', 'jpeg', 'pdf']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    if (obj.type === 'pdf') {
      noItem = false
      markup += `<div class="sd-prop-list-prev sd-prop-list-pdf" data-name="${obj.name}"></div>`
    } else if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}

function getBaseMarkup (image, props, language, isSingle) {
  let loc = window.sledge.nslocal
  
  let text = image.media ? `media/${image.media}.${image.mediaType}` : ''
  return `
    <div class='sd-prop-but-item'>
      <button data-ref="${image.id || null}" class='sd-prop-but-rm'></button>
    <div>
      <label class="sd-full">${loc.prop_cus_path}</label>
      <input disabled data-ref="${image.id || null}" type="text" style="width:95%;" value="${text}"/>
    </div>
    ${getImgMarkup(image)}          
  </div>`
}

function getImageStylePath (param) {
  let style = ''
  let fileName = param ? param.media : null
  if (fileName) {
    if (param.numberOfSizes !== 1) {
      fileName = param.media + '_small'
    }
    style = `background-image:url(${window.$sledge.conf.dbServerUrl}/${window.$sledge.currentdb}/media/${fileName}.${param.mediaType}); `
  }
  return style
}

function getImgMarkup (param) {
  let loc = window.sledge.nslocal
  let style = getImageStylePath(param)
  let markup =
  `<label>${loc.but_file_current}</label>
  <div class="sd-prop-preview-block">
    <div class="sd-prop-preview-block-buttons">
      <button data-kind="browse" class="sd-prop-browse-media" data-ref="${param.id || null}">${loc.but_file_new}</button>
      <button data-kind="cat" data-media="${param.media}" data-ref="${param.id || null}">${loc.prop_choose_file}</button>
    </div>
    <div class="sd-prop-preview" style="${style}"></div>
  </div>`
  return markup
}