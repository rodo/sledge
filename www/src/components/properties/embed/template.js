export default (sender, param) => {
  let loc = window.sledge.nslocal
  param = param || {}
  let ratio = param.ratio || '169'
  let code = param.iframe && param.iframe[window.$sledge.language] ? window.decodeURIComponent(param.iframe[window.$sledge.language]) : ''
  let markup = `
  <label class="sd-full">${loc.prop_vid_emded} (${window.$sledge.language})</label>
  <textarea data-kind="iframe" rows="6" style="width:90%;">${code}</textarea>

  <br><label>${loc.prop_vid_choose}</label>
  <select data-kind='ratio'>
    <option value="43" ${ratio === '43' ? 'selected' : ''}>4:3</option>
    <option value="169" ${ratio === '169' ? 'selected' : ''}>16:9 (${loc.prop_vid_w})</option>
    <option value="916" ${ratio === '916' ? 'selected' : ''}>9:16 (${loc.prop_vid_v})</option>
    <option value="256" ${ratio === '256' ? 'selected' : ''}>256:81 (${loc.prop_vid_pa})</option>
    <option value="11" ${ratio === '11' ? 'selected' : ''}>1:1 (${loc.prop_vid_sq})</option>
  </select>`

  sender.innerHTML = markup
}
