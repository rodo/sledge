import render from './template'
// import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    // connect('sideMenuCurrentItem', this)    
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    // this.addEventListener('keyup', e => this.update(e))
  }

  init (propController, props, value) {
    this.value = value || {}
    this.ctrl = propController
    this.key = props.key
    render(this, this.value)
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'language':
        //this.currentLangage = e.target[e.target.selectedIndex].value
        //render(this, this.value, this.currentLangage, this.pages, this.views, this.sideMenuCurrentItem)
        break
      case 'iframe':
        if (!this.value.iframe) {
          this.value.iframe = {}
        }
        this.value.iframe[window.$sledge.language] = window.encodeURIComponent(e.target.value)
        this.setProp()
        break
      case 'ratio':
        this.value.ratio = e.target[e.target.selectedIndex].value
        this.setProp()
        /*window.requestAnimationFrame(() => {
          window.PubSub.publish('ACTION_ZONE_RESIZE')
        })*/
        break
      default:
      //
    }
    // this.ctrl.setPropertyValue(this.key, e.target.value)
  }

  setProp () {
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

}

window.customElements.define('sd-properties-embed', Property)
