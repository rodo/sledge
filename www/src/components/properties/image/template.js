export default (sender, param = {}, medias, nodeName) => {
  let style = ''
  let prevClass = ''
  let loc = window.sledge.nslocal
  let fileName = null
  let ig = {}

  if (param.image && param.image[window.$sledge.language]) {
    ig = param.image[window.$sledge.language]
    fileName = ig.media
  }

  if (fileName) {
    if (ig.numberOfSizes !== 1) {
      fileName = ig.media + '_small'
    }
    style = `background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${fileName}.${ig.mediaType}); `
  }

  let attachements = []
  if (Object.keys(medias).length !== 0) {
    for (let fileName2 in medias) {
      attachements.push({...medias[fileName2], name: fileName2})
    }
  }

  const markup = `
  <div class="sd-but-catalog">
    <button class="sd-but-catalog-close" data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getMediaList(medias)}
    </div>
  </div>
  <div class="sd-but-cont">
    <label>${loc.prop_img_cur} (${window.$sledge.language})</label>
    <div class="sd-prop-preview-block">
      <div class="sd-prop-preview-block-buttons">
        <button data-kind="browse" class="sd-prop-browse-media">${loc.but_img_new}</button>
        <button data-kind="cat" data-media="${ig.media}" data-ref="${param.id || null}">${loc.prop_choose_cat}</button>        
      </div>
      ${style !== ''
      ? `<div class="sd-prop-preview ${prevClass}" style="${style}"></div>`
      : ``
      }
    </div>
    ${nodeName === 'IMG'
    ? getSizeMarkup(param)
    : ''
    }
  </div>`

  sender.innerHTML = markup
}

function getSizeMarkup (param) {
  let loc = window.sledge.nslocal
  let val = param.size || '801'
  let markup = `<br><label>${loc.proplogo_sz}</label>
  <div class="sd-prop-custom-cont">
  <div class="sd-prop-custom-range"><input value="${val}" type="range" data-kind="size" min="50" max="801">`
  markup += `<span>Auto</span></div></div>`
  markup += '</div></div>'
  return markup
}

function getMediaList (list) {
  let attachements = []
  let supportedType = ['png', 'jpg', 'jpeg']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}
