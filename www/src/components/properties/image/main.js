import render from './template'
import {browseFile, getMedias} from '../../../interface/interface_mac'
import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('medias', this)
    connect('currentProject', this)
    this.value = {}
    this.addEventListener('click', e => this.update(e))
    this.addEventListener('change', e => this.updateChange(e))
  }

  updateChange (e) {
    switch (e.target.dataset.kind) {
      case 'size':
        this.value.size = e.target.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      default:
      //
    }
  }

  update (e) {
    switch (true) {
      case e.target.dataset.kind === 'cat':
        this.openCatalog(e.target.dataset.ref)
        if (e.target.dataset.media) {
          $('.sd-prop-list-prev').css('display', 'block')
          $(`.sd-but-catalog-list div[data-name="${e.target.dataset.media}"]`).css('display', 'none')
        }
        break
      case e.target.dataset.kind === 'imgclose':
        this.closeCatalog()
        break           
      case e.target.classList.contains('sd-prop-browse-media'):
        browseFile(name => this.setBackgroundImage(name), false, true)
        break
      case e.target.classList.contains('sd-prop-list-prev'):
        this.saveImage(e)
        break
      default:
      //
    }
  }

  saveImage (e) {
    let name = e.target.dataset.name
    if (!this.value.image) {
      this.value.image = {}
    }
    if (!this.value.image[window.$sledge.language]) {
      this.value.image[window.$sledge.language] = {}
    }
    const ig = this.value.image[window.$sledge.language]
    ig.media = encodeURIComponent(name)
    ig.numberOfSizes = this.medias[name].numberOfSizes
    ig.mediaType = this.medias[name].type
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  openCatalog (id) {
    this.currentEditedImage = id
    document.querySelector('.sd-but-catalog').classList.add('visible')
  }

  closeCatalog () {
    document.querySelector('.sd-but-catalog').classList.remove('visible')
  }

  setBackgroundImage (data) {
    getMedias(this.currentProject, () => {
      if (!this.value.image) {
        this.value.image = {}
      }
      if (!this.value.image[window.$sledge.language]) {
        this.value.image[window.$sledge.language] = {}
      }
      const ig = this.value.image[window.$sledge.language]
      ig.media = encodeURIComponent(data.name)
      if (data.numberOfSizes) {
        ig.numberOfSizes = data.numberOfSizes
      }
      ig.mediaType = data.type
      this.ctrl.applyProperty(this.key, this.value)
      this.ctrl.setPropertyValue(this.key, this.value)
    })
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    let nodeName = null
    const comp = document.getElementById(this.ctrl.currentSelectedComponent)
    if (comp) {
      const el = comp.querySelector(props.applyTo)
      if (el) {
        nodeName = el.nodeName
      }
    }
    render(this, this.value, this.medias, nodeName)
  }

}

window.customElements.define('sd-properties-image', Property)
