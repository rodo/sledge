export default (sender, params, language, props) => {
  let loc = window.sledge.nslocal  
  params = params || {}
  const autoSize = props.autoSize || false
  let text = ''
  if (params.text && params.text[language]) {
    text = decodeURIComponent(params.text[language])
  }
  let align = params.align || 'left'
  let hSize = params.hSize || '1'
  let padding = params.padding || 'small'

  let hasPadding = (props.conf && props.conf.padding !== null) ? props.conf.padding : true
  let hasAlign = (props.conf && props.conf.align !== null) ? props.conf.align : true
  let hasHsize = (props.conf && props.conf.hSize !== null) ? props.conf.hSize : true
  let hasColor = (props.conf && props.conf.color !== null) ? props.conf.color : true

  let markup = `
  <label>${loc.gen_text} (${language})</label>
  <input type="text" data-kind="text" value="${text}"/>`

  if (hasHsize && !autoSize) {
    markup += `<br><label>${loc.prop_h_size}</label>
    <select data-kind='hSize'>
      <option value="1" ${hSize === '1' ? 'selected' : ''}>1</option>
      <option value="2" ${hSize === '2' ? 'selected' : ''}>2</option>
      <option value="3" ${hSize === '3' ? 'selected' : ''}>3</option>
      <option value="4" ${hSize === '4' ? 'selected' : ''}>4</option>
      <option value="5" ${hSize === '5' ? 'selected' : ''}>5</option>
      <option value="6" ${hSize === '6' ? 'selected' : ''}>6</option>  
    </select>`
  }

  if (hasPadding && !autoSize) {
    markup += `<br><label>${loc.prop_h_space}</label>
    <select data-kind='padding'>
      <option value="small" ${padding === 'small' ? 'selected' : ''}>${loc.gen_size_sm}</option>
      <option value="medium" ${padding === 'medium' ? 'selected' : ''}>${loc.gen_size_med}</option>
      <option value="large" ${padding === 'large' ? 'selected' : ''}>${loc.gen_size_lg}</option>
    </select>`
  }

  if (hasAlign && !autoSize) {
    markup += `<br><label  class="sd-full">${loc.prop_txt_align}</label>
    <div class="sd-txt-align-cont">
      <button data-kind="left" class="sd-txt-align-l ${(align === 'left') ? 'sd-selected' : ''}"></button>
      <button data-kind="center" class="sd-txt-align-c ${(align === 'center') ? 'sd-selected' : ''}"></button>
      <button data-kind="right" class="sd-txt-align-r ${(align === 'right') ? 'sd-selected' : ''}"></button>
    </div>`
  }

  if (hasColor) {
    markup += `<sd-color-selector data-kind="colorScheme"></sd-color-selector>`
  }

  sender.innerHTML = markup
}
