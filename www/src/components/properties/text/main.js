import render from './template'

export default class PropertiesText extends HTMLElement {

  connectedCallback () {
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('keyup', e => this.updateKey(e))
    this.addEventListener('click', e => this.updateClick(e))
  }

  updateClick (e) {    
    switch (e.target.dataset.kind) {
      case 'left':
      case 'right':
      case 'center':
        this.value.align = e.target.dataset.kind
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'scheme':
        this.value.colorScheme = e.target.dataset.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'rmColor':
        this.value.colorScheme = null
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break        
      default:
    }    
  }

  updateKey (e) {
    switch (e.target.dataset.kind) {
      case 'text':
        if (!this.value.text) {
          this.value.text = {}
        }
        this.value.text[window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        this.applyProp(this.value)
        this.setProp()
        break
      default:
    }
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'hSize':
        let v = e.target[e.target.selectedIndex].value
        this.value.hSize = v
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'padding':
        let val = e.target[e.target.selectedIndex].value
        this.value.padding = val
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      default:
    }
  }

  setProp () {
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  applyProp (val) {
    this.ctrl.applyProperty(this.key, this.value)
  }

  init (propController, props, values) {
    this.ctrl = propController
    this.key = props.key
    this.props = props
    this.value = values || {}
    render(this, this.value, window.$sledge.language, props)
  }

}

window.customElements.define('sd-properties-text', PropertiesText)
