import iconsConf from '../../../../defaultTheme/assets/foundation-icons/.fontcustom-data.json'

export default (sender, props, param, media) => {
  param = param || {}
  let loc = window.sledge.nslocal

  let back = (props.conf && props.conf.background !== null) ? props.conf.background : true
  let rounded = (props.conf && props.conf.rounded !== null) ? props.conf.rounded : true
  let size = (props.conf && props.conf.size !== null) ? props.conf.size : true

  sender.innerHTML = `
  <div class="sd-but-catalog">
    <button class="sd-but-catalog-close" data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getMediaList(media)}
    </div>
  </div>
  <div class="sd-foundation-icons-catalog">
    <button data-kind="fiClose"></button>
    <div class="sd-foundation-catalog-list">
    ${getIconsList()}
    </div>
  </div>
  <div class="sd-but-cont">  
    <h2>Icon settings</h2>
    <div>
    ${renderLogoManager(param)}
    </div>
    ${
      back
      ? `<div>
        <label>${loc.gen_back}</label>
        <input data-kind="background" type="checkbox" ${param.background ? 'checked' : ''}/>
      </div>`
      : ''
    }
    ${
      rounded
      ? `<div>
        <label>${loc.gen_rounded}</label>
        <input data-kind="rounded" type="checkbox" ${param.rounded ? 'checked' : ''}/>
      </div>`
      : ''
    }
    ${
      size
      ? `<div>
        <label>${loc.proplogo_sz}</label>
        <select data-kind="size">
          <option value="small" ${param.size === 'small' ? 'selected' : ''}>${loc.gen_size_sm}</option> 
          <option value="medium" ${param.size === 'medium' ? 'selected' : ''}>${loc.gen_size_med}</option> 
          <option value="large" ${param.size === 'large' ? 'selected' : ''}>${loc.gen_size_lg}</option>     
        </select>
      </div>`
      : ''
    }
  </div> 
  `
}

function getIconsList () {
  let markup = ''
  iconsConf.glyphs.forEach(cl => {
    markup += `<div class="sd-glyphs-cat"><i data-icn="${cl}" data-kind="sd-func-cat" class="${cl}"></i></div>`
  })
  return markup
}

function getMediaList (list) {
  let attachements = []
  let supportedType = ['png', 'jpg']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}

function renderLogoManager (param) {
  let style = null
  let loc = window.sledge.nslocal
  
  if (param.logo && param.logo.indexOf('fi-') === -1) {
    style = `style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${decodeURIComponent(param.logo)}.${param.mediaType});"`
  }
  let markup = `<br><label>${loc.proplogo_current}</label>
  <div class="sd-prop-preview-block">
    <div class="sd-prop-preview-block-buttons">
      <button data-kind="browse" class="sd-prop-browse-media">${loc.proplogo_find}</button>
      <button data-kind="cat" data-logo="${param.logo}">${loc.prop_choose_cat}</button>
      <button data-kind="fiCat" data-logo="${param.logo}">${loc.prop_choose_icn}</button>
    </div>
    <div class="sd-prop-preview" ${style || ''}>
      ${(param.logo && param.logo.indexOf('fi-')) === 0 ? `<i class="${param.logo}"></i>` : ''}
    </div>
  </div>
  ${(param.logo && param.logo.indexOf('fi-')) === 0
  ? `<sd-color-selector data-kind="colorScheme"></sd-color-selector>`
  : ''}`
  return markup
}
