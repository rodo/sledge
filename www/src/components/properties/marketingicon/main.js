import render from './template'
import {browseFile, getMedias} from '../../../interface/interface_mac'
import {connect} from '../../../store'

export default class Marketingicon extends HTMLElement {

  connectedCallback () {
    this.value = {}
    connect('currentProject', this)
    connect('medias', this)
    this.addEventListener('click', e => this.onClick(e))
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('keyup', e => this.update(e))
  }

  onClick (e) {
    if (e.target.classList.contains('sd-prop-list-prev')) {
      let mediaData = this.medias[e.target.dataset.name]
      mediaData.name = e.target.dataset.name
      this.saveImage(mediaData)
      this.closeCatalog()
      return
    }

    switch (e.target.dataset.kind) {
      case 'scheme':
        this.value.colorScheme = e.target.dataset.value
        this.setProp()
        break
      case 'rmColor':
        this.value.colorScheme = null
        this.setProp()
        break
      case 'sd-func-cat':
        this.value.logo = e.target.dataset.icn
        this.setProp()
        this.closeFiCatalog()
        break
      case 'browse':
        this.currentEditedButton = e.target.dataset.ref
        browseFile(name => this.setLogo(name), false, true)
        break
      case 'imgclose':
        this.closeCatalog()
        break
      case 'cat':
        this.openCatalog()
        if (e.target.dataset.logo) {
          $('.sd-prop-list-prev').css('display', 'block')
          $(`.sd-but-catalog-list div[data-name="${e.target.dataset.logo}"]`).css('display', 'none')
        }
        break
      case 'fiClose':
        this.closeFiCatalog()
        break
      case 'fiCat':
        document.querySelector('.sd-foundation-icons-catalog').classList.add('visible')
        break        
      default:
        //
    }
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'background':
        this.value.background = e.target.checked
        break
      case 'rounded':
        this.value.rounded = e.target.checked
        break
      case 'size':
        this.value.size = e.target[e.target.selectedIndex].value
        break
    }
    this.setProp()
    /*window.requestAnimationFrame(() => {
      PubSub.publish('ACTION_ZONE_RESIZE')
    })*/
  }

  openCatalog (id) {
    document.querySelector('.sd-but-catalog').classList.add('visible')
  }

  closeFiCatalog () {
    document.querySelector('.sd-foundation-icons-catalog').classList.remove('visible')    
  }

  closeCatalog () {
    document.querySelector('.sd-but-catalog').classList.remove('visible')
  }

  setProp () {
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  saveImage (data) {
    this.value.logo = encodeURIComponent(data.name)
    if (data.numberOfSizes) {
      this.value.numberOfSizes = data.numberOfSizes
    }
    this.value.mediaType = data.type
    this.setProp()
  }

  setLogo (data) {
    getMedias(this.currentProject, () => this.saveImage(data))
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    this.value = value
    render(this, props, value, this.medias)
  }

}

window.customElements.define('sd-properties-marketingicon', Marketingicon)
