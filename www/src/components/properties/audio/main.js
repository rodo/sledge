import render from './template'
import {guidGenerator} from '../../../utilities'
import {connect} from '../../../store'
import {browseFile, getMedias} from '../../../interface/interface_mac'
import { generateUUID } from '../../../../framework/src/utilities'

export default class PropertyAudio extends HTMLElement {

  connectedCallback () {
    this.initDone = false
    connect('pages', this)
    connect('views', this)
    connect('medias', this)
    connect('currentProject', this)
    connect('sideMenuCurrentItem', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('click', e => this.onClick(e))
  }

  onClick (e) {
    switch (true) {
      case e.target.classList.contains('sd-prop-list-prev'):
        let mediaData = this.medias[e.target.dataset.name]
        mediaData.name = e.target.dataset.name
        this.saveImage(mediaData)
        this.closeCatalog()
        break
      case e.target.classList.contains('sd-prop-list-sound'):
        let mediaData2 = this.medias[e.target.dataset.name]
        mediaData2.name = e.target.dataset.name
        this.saveSound(mediaData2)
        this.closeCatalog()
        break        
      case e.target.classList.contains('sd-prop-sound-add'):
        this.value.sounds.push({
          description: {},
          image: {},
          id: guidGenerator(),
          sound: {}
        })
        this.setProp()
        break
      case e.target.classList.contains('sd-prop-but-rm'):
        this.removeSound(e.target.dataset.ref)
        break
      case e.target.dataset.kind === 'imgRm':
        this.removeImg(e.target.dataset.ref)
        break
        case e.target.dataset.kind === 'catSound':
        this.openSoundCatalog(e.target.dataset.ref)
        break
      case e.target.dataset.kind === 'catImg':
        this.openCatalog(e.target.dataset.ref)
        break
      case e.target.dataset.kind === 'browse':
        this.currentEditedButton = e.target.dataset.ref
        browseFile(name => this.setSound(name), true, false)
        break
      case e.target.dataset.kind === 'browseImg':
        this.currentEditedButton = e.target.dataset.ref
        browseFile(name => this.setBackgroundImage(name), false, true)
        break        
      case e.target.dataset.kind === 'imgclose':
        this.closeCatalog()
        break        
      default:
      //  
    }
  }

  saveSound (data) {
    const index = this.getIndexFromRef(this.currentEditedButton)
    data.id = this.currentEditedButton
    if (!this.value.sounds) {
      this.value.sounds = []
    }
    this.value.sounds[index] = data
    this.setProp()
  }

  saveImage (data) {
    const index = this.getIndexFromRef(this.currentEditedButton)
    const im = this.value.sounds[index].image = {}
    im.media = encodeURIComponent(data.name)
    im.numberOfSizes = data.numberOfSizes
    im.mediaType = data.type
    this.setProp()
  }

  setSound (data) {
    getMedias(this.currentProject, () => this.saveSound(data))
  }

  setBackgroundImage (data) {
    getMedias(this.currentProject, () => this.saveImage(data))
  }

  update (e) {
    let index = null
    switch (e.target.dataset.kind) {
      case 'desc':
        index = this.getIndexFromId(e)
        this.value.sounds[index]['desc'] = e.target.value
        this.setProp()
        break
      case 'action':
        index = this.getIndexFromId(e)
        this.value.buttons[index]['action'] = e.target[e.target.selectedIndex].value
        if (this.value.buttons[index]['action'] === 'view') {
          this.value.buttons[index]['link'] = false
          this.setProp()
        } else {
          this.ctrl.setPropertyValue(this.key, this.value)
        }
        break
      case 'internal':
        index = this.getIndexFromId(e)
        this.value.buttons[index]['internal'] = e.target[e.target.selectedIndex].value
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'external':
      case 'email':
        index = this.getIndexFromId(e)
        this.value.buttons[index][e.target.dataset.kind] = encodeURIComponent(e.target.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'text':
        index = this.getIndexFromId(e)
        if (!this.value.buttons[index]) {
          this.value.buttons[index] = {}
        }
        if (!this.value.buttons[index].text) {
          this.value.buttons[index].text = {}
        }
        this.value.buttons[index].text[window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        this.setProp()
        break
      case 'size':
      case 'role':
      case 'page':
      case 'view':
      case 'effect':
        this.manageSelect(e)
        break
      case 'link':
      case 'hollow':
      case 'uppercase':
      case 'expanded':
        window.sledge.donorefresh = true
        index = this.getIndexFromId(e)
        this.value.buttons[index][e.target.dataset.kind] = e.target.checked
        this.setProp()
        break
      case 'img':
        index = this.getIndexFromId(e)
        this.value.buttons[index][e.target.dataset.kind] = e.target.checked
        this.setProp()
        break
      case 'round':
        window.sledge.donorefresh = true
        index = this.getIndexFromId(e)
        this.value.buttons[index]['round'] = e.target.checked
        this.value.buttons[index]['radius'] = false
        this.setProp()
        break
      case 'radius':
        window.sledge.donorefresh = true;
        index = this.getIndexFromId(e)
        this.value.buttons[index]['radius'] = e.target.checked
        this.value.buttons[index]['round'] = false
        this.setProp()
        break
      default:
      //
    }
  }

  openCatalog (id) {
    this.currentEditedButton = id
    document.querySelector('.sd-but-catalog').classList.add('visible')
  }

  openSoundCatalog (id) {
    this.currentEditedButton = id
    document.querySelector('.sd-but-catalogSound').classList.add('visible')
  }

  closeCatalog () {
    document.querySelector('.sd-but-catalog').classList.remove('visible')
    document.querySelector('.sd-but-catalogSound').classList.remove('visible')
  }

  removeSound (id) {
    let index = this.value.sounds.findIndex(item => item.id === id)
    this.value.sounds.splice(index, 1)
    this.setProp()
  }

  removeImg (id) {
    let index = this.value.sounds.findIndex(item => item.id === id)
    this.value.sounds[index].image = null
    this.setProp()
  }

  updateOrder (newOrder) {
    let newRes = []
    let index = 0
    newOrder.forEach(newId => {
      index = 0
      index = this.value.sounds.findIndex(item => item.id === newId)
      newRes.push(this.value.sounds[index])
    })
    this.value.sounds = newRes
    this.setProp()
  }

  manageSelect (e) {
    let index = this.getIndexFromId(e)
    this.value.buttons[index][e.target.dataset.kind] = e.target[e.target.selectedIndex].value
    this.setProp()
  }

  getIndexFromId (e) {
    let id = e.target.dataset.ref
    if (id === 'null') {
      id = this.checkId()
    }
    return this.value.sounds.findIndex(item => item.id === id)
  }

  getIndexFromRef (id) {
    return this.value.sounds.findIndex(item => item.id === id)
  }

  checkId () {
    let id = guidGenerator()
    this.value.sounds[0].id = id
    return id
  }

  setProp () {
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    this.value = value || {}
    if (!this.value.sounds) {
      this.value.sounds = [{
        id: generateUUID(),
        name: '',
        originalName: ''
      }]
    }
    if (!this.initDone) {
      render(this, value, window.$sledge.language, this.pages, this.views, this.sideMenuCurrentItem, this.medias, props)
      this.initDone = true
    }
  }

}

window.customElements.define('sd-properties-audio', PropertyAudio)
