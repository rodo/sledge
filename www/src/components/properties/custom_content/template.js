export default (sender, param, language) => {
  sender.innerHTML = renderHeightManager(param, language)
}

function renderHeightManager (param, language) {
  let loc = window.sledge.nslocal
  let markup = ''
  
  const cssValue = param.css ? decodeURIComponent(param.css) : ''

  markup += `
    <label class="sd-full">HTML (${language})</label>
    <div id="sd-props-custom-html"></div>
    
    <label class="sd-full">CSS</label>
    <div id="sd-props-custom-css">${cssValue}</div>    
  `
  return markup
}

