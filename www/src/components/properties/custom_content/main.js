import render from './template'
import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'vh':
        this.value.vh = e.target.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        /*window.requestAnimationFrame(() => {
          PubSub.publish('ACTION_ZONE_RESIZE')
        })*/
        break
      default:
    }
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, this.value, window.$sledge.language)

    const editorTags = window.ace.edit('sd-props-custom-html')
    editorTags.setTheme('ace/theme/monokai')
    editorTags.getSession().setMode('ace/mode/html')
    editorTags.getSession().setOption('useWorker', false)
    if (this.value.html && this.value.html[window.$sledge.language]) {
      editorTags.getSession().setValue(decodeURIComponent(this.value.html[window.$sledge.language]))
    }

    const editorCss = window.ace.edit('sd-props-custom-css')
    editorCss.setTheme('ace/theme/monokai')
    editorCss.getSession().setMode('ace/mode/css')
    editorCss.getSession().setOption('useWorker', false)

    editorTags.getSession().on('change', e => {
      if (!this.value.html) {
        this.value.html = {}
      }
      this.value.html[window.$sledge.language] = encodeURIComponent(editorTags.getValue()).replace(/[!'()*]/g, escape)
      this.updateScripts()
    })

    editorCss.getSession().on('change', e => {
      this.value.css = encodeURIComponent(editorCss.getValue()).replace(/[!'()*]/g, escape)
      this.updateScripts()
    })
  }

  updateScripts () {
    if (this.saveTm) {
      window.clearTimeout(this.saveTm)
    }
    this.saveTm = window.setTimeout(() => {
      window.sledge.donorefresh = true
      this.ctrl.applyProperty(this.key, this.value)
      this.ctrl.setPropertyValue(this.key, this.value)
    }, 500)
  }

}

window.customElements.define('sd-properties-custom_content', Property)
