import render from './template'
import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
  }

  update(e) {
    switch (e.target.dataset.kind) {
      case 'vh':
        this.value.vh = e.target.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        /*window.requestAnimationFrame(() => {
          PubSub.publish('ACTION_ZONE_RESIZE')
        })*/
        break
      default:
    }
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, value, this.medias)
  }

}

window.customElements.define('sd-properties-vh', Property)
