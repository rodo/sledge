export default (sender, param, medias) => {
  sender.innerHTML = renderHeightManager(param)
}

function renderHeightManager (param) {
  let loc = window.sledge.nslocal
  let markup = ''
  let vh = param && param.vh ? param.vh : '80'
  markup += `
    <label>${loc.prop_back_h} : ${vh} (vh)</label>
    <input type="range" min="10" max="100" step="1" data-kind="vh" value="${vh}"/>
  `
  return markup
}
