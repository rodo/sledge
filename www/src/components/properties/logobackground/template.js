export default (sender, param, medias, fullValues, props) => {
  let loc = window.sledge.nslocal
  let logo = param ? param.asLogo : null
  if (props.permanent) {
    logo = true
  }
  if (!logo) {
    logo = false
  }
  let attachements = []
  if (Object.keys(medias).length !== 0) {
    for (let fileName2 in medias) {
      attachements.push({...medias[fileName2], name: fileName2})
    }
  }
  
  sender.innerHTML = `
  ${!props.permanent
  ? `<br><label>${loc.proplogo_add}</label>
  <input type="checkbox" data-kind="asLogo" ${logo ? 'checked' : ''}/>`
  : ''
  }
  ${logo
    ? renderLogoManager(param) +
      renderPositionManager(param, fullValues, props) +
      renderSizeManager(param, props) +
      getMediaList(attachements, param.media)
    : ''}
  `
}

function renderLogoManager (param) {
  let style = null
  let loc = window.sledge.nslocal
  
  if (param.media) {
    style = `style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${decodeURIComponent(param.media)}.${param.mediaType});"`
  }
  let markup = `<br><label>${loc.proplogo_current}</label>
  <div class="sd-prop-preview-block">
    <div class="sd-prop-preview-block-buttons">
      <button data-kind="browse" class="sd-prop-browse-media">${loc.proplogo_find}</button>
    </div>
    <div class="sd-prop-preview" ${style || ''}></div>
  </div>`
  return markup
}

function renderPositionManager (param, fullValues, props) {
  let markup = ''
  let loc = window.sledge.nslocal
  
  if (fullValues && fullValues.asMenu && props.positionWithMenu === false) {
    return ''
  }
  if (props.position === false) {
    return ''
  }

  markup += `
  <br><label>${loc.proplogo_pos}</label>
  <select data-kind="position">`
  let pos = param && param.position ? param.position : props.defaultPosition
  props.position.forEach(item => {
    markup += `<option value="${item.key}" ${pos === item.key ? 'selected' : ''}>${item.text[window.sledge.languageCode]}</option>`
  })
  markup += '</select>'
 
  return markup
}

function renderSizeManager (param, props) {
  if (props.fixedSize) {
    return ''
  }
  let markup = ''
  let loc = window.sledge.nslocal
  const pos = param && param.size ? param.size : 'medium'
  markup += `
    <br><label>${loc.proplogo_sz}</label>
    <select data-kind="size">
      <option value="small" ${pos === 'small' ? 'selected' : ''}>${loc.gen_size_sm}</option>
      <option value="medium" ${pos === 'medium' ? 'selected' : ''}>${loc.gen_size_med}</option> 
      <option value="large" ${pos === 'large' ? 'selected' : ''}>${loc.gen_size_lg}</option> 
    </select>
  `
  const logoHeight = param && param.logoHeight ? param.logoHeight : 201
  markup += `<br><label>${loc.prop_back_lh}</label>
  <div class="sd-prop-custom-cont"><div class="sd-prop-custom-range"><input value="${logoHeight}" type="range" data-kind="logoHeight" min="20" max="201">`

  markup += `<span>Auto</span></div></div>`

  return markup
}

/*function renderRatioManager (param) {
  let markup = ''
  let loc = window.sledge.nslocal
  let pos = param && param.ratio ? param.ratio : 'square'
  markup += `
    <br><label>Ratio</label>
    <select data-kind="ratio">
      <option value="square" ${pos === 'square' ? 'selected' : ''}>Square</option>
      <option value="large" ${pos === 'large' ? 'selected' : ''}>Large</option> 
    </select>
  `
  return markup
}*/

function getMediaList (list, paramName) {
  let loc = window.sledge.nslocal
  let markup = `<br><label class="sd-prop-library-label sd-full">${loc.proplogo_prev}</label><div class="sd-prop-library">`
  let noItem = true
  if (list.length === 0) {
    return ''
  }
  let supportedType = ['png', 'jpg']
  list.forEach(obj => {
    if (supportedType.indexOf(obj.type) !== -1 && obj.numberOfSizes && obj.name !== decodeURIComponent(paramName)) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div data-kind="prev" class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div data-kind="prev" class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  markup += '</div>'
  return markup
}