import render from './template'
import {connect} from '../../../store'
import {browseFile, getMedias} from '../../../interface/interface_mac'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    connect('medias', this)
    this.value = {}
    this.addEventListener('change', e => this.updateChange(e))
    this.addEventListener('click', e => this.update(e))
  }

  updateChange (e) {
    switch (e.target.dataset.kind) {
      case 'position':
      case 'size':
      case 'ratio':
        this.value[e.target.dataset.kind] = e.target[e.target.selectedIndex].value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'logoHeight':
        this.value[e.target.dataset.kind] = e.target.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'asLogo':
        this.value.asLogo = e.target.checked
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break        
      default:
    }
  }
  
  update (e) {
    switch (e.target.dataset.kind) {
      case 'prev':
        let name = e.target.dataset.name
        this.value.media = encodeURIComponent(name)
        this.value.numberOfSizes = this.medias[name].numberOfSizes
        this.value.mediaType = this.medias[name].type
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'browse':
        browseFile(name => this.setLogo(name), false, true)
        break
      default:
    }
  }

  setLogo (data) {
    getMedias(this.currentProject, () => {
      this.value.media = encodeURIComponent(data.name)
      if (data.numberOfSizes) {
        this.value.numberOfSizes = data.numberOfSizes
      }
      this.value.mediaType = data.type
      this.ctrl.applyProperty(this.key, this.value)
      this.ctrl.setPropertyValue(this.key, this.value)
    })
  }

  init (propController, props, value, fullValues) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, value, this.medias, fullValues.menu, props)
  }

}

window.customElements.define('sd-properties-logobackground', Property)
