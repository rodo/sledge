import render from './template'

export default class Radiolist extends HTMLElement {

  connectedCallback () {
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('keyup', e => this.update(e))
  }

  update (e) {
    this.value.position = e.target.value
    this.ctrl.setPropertyValue(this.key, this.value)
    this.ctrl.applyProperty(this.key, this.value)
    /*window.requestAnimationFrame(() => {
      PubSub.publish('ACTION_ZONE_RESIZE')
    })*/
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    this.value = value
    render(this, props, value)
  }

}

window.customElements.define('sd-properties-radiolist', Radiolist)
