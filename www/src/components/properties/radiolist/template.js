export default (sender, props, param) => {
  param = param || {}
  if (!param.position) {
    param.position = 'side'
  }
  sender.innerHTML = `
  ${getRadio(props, param)}
  `
}

function getRadio (props, param) {
  let markup = ''
  props.data.forEach(item => {
    markup += `<label class="sd-full" for="${item.ref}">${item.label[window.sledge.languageCode]}</label>
    <input ${param.position === item.value ? 'checked' : ''} name="${props.key}" type="radio" id="${item.ref}" value="${item.value}" >`
  })
  return markup
}
