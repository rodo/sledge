import render from './template'
import {connect} from '../../../store'
import {browseFile, getMedias} from '../../../interface/interface_mac'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('click', e => this.updateClick(e))
  }

  updateClick (e) {    
    switch (e.target.dataset.kind) {
      case 'scheme':
        this.value.colorScheme = e.target.dataset.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'rmColor':
        this.value.colorScheme = null
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break        
      default:
    }    
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'asMenu':
        this.value.asMenu = e.target.checked
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'menuSticky':
        this.value.menuSticky = e.target.checked
        if (!this.value.menuSticky) {
          this.value.menuShrink = false
        }
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'menuAnim':
      case 'menuShrink':
        this.value[e.target.dataset.kind] = e.target.checked
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'menuTitle':
        if (!this.value.menuTitle) {
          this.value.menuTitle = {}
        }
        this.value.menuTitle[window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'menuBack':
      case 'position':
        this.value[e.target.dataset.kind] = e.target[e.target.selectedIndex].value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      default:
    }
  }

  /*setLogo (data) {
    getMedias(this.currentProject, () => {
      this.value.logo = encodeURIComponent(data.name)
      if (data.numberOfSizes) {
        this.value.numberOfSizes = data.numberOfSizes
      }
      this.value.mediaType = data.type
      this.ctrl.applyProperty(this.key, this.value)
      this.ctrl.setPropertyValue(this.key, this.value)
    })
  }*/

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, value, window.$sledge.language, props)
  }

}

window.customElements.define('sd-properties-menu', Property)
