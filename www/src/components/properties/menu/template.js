export default (sender, param, language, props) => {
  let menu = param ? param.asMenu : null
  if (!menu) {
    menu = false
  }
  let loc = window.sledge.nslocal
  sender.innerHTML = `<br><label>${loc.menu_prop_add}</label>
  <input type="checkbox" data-kind="asMenu" ${menu ? 'checked' : ''}/>
  ${menu
    ? renderTitleManager(param, language, props) +
    renderPositionManager(param, props) +
    renderBackgroundManager(param) +
      renderStickyManager(param, props) +
      `<sd-color-selector data-kind="menuColor"></sd-color-selector>`
    : ''}
  `
}

function renderTitleManager (param, language, props) {
  if (props.title === false) {
    return ''
  }
  let title = param.menuTitle && param.menuTitle[language] ? decodeURIComponent(param.menuTitle[language]) : ''
  let loc = window.sledge.nslocal
  let markup = `<br><label>${loc.menu_prop_title}</label>
  <input type="text" data-kind="menuTitle" value="${title}"/>`
  return markup
}

function renderPositionManager (param, props) {
  if (!props.position) {
    return ''
  }
  const loc = window.sledge.nslocal
  let markup = `<br><label>${loc.menu_prop_pos}</label>
  <select data-kind="position">
    <option value="top" ${param.position === 'top' ? 'selected' : ''}>${loc.menu_at_top}</option>
    <option value="bottom" ${param.position === 'bottom' ? 'selected' : ''}>${loc.menu_at_bottom}</option> 
  </select>`
  
  return markup
}

function renderStickyManager (param, props) {
  let loc = window.sledge.nslocal
  if (props.sticky === false) {
    return ''
  }
  let check = param.menuSticky || false
  let markup = `<br><label>${loc.menu_prop_sticky}</label>
  <input type="checkbox" data-kind="menuSticky" ${check ? 'checked' : ''}/>`
  if (check) {
    let check2 = param.menuShrink || false
    markup += `<br><label>${loc.menu_prop_shrink}</label>
    <input type="checkbox" data-kind="menuShrink" ${check2 ? 'checked' : ''}/>`
  }
  return markup
}

/*function renderAnimateManager (param) {
  let check = param.menuAnim || false
  console.info(check)
  let markup = `<br><label>Open with Animation (small)</label>
  <input type="checkbox" data-kind="menuAnim" ${check ? 'checked' : ''}/>`
  return markup
}*/

function renderBackgroundManager (param) {
  let loc = window.sledge.nslocal
  let markup = ''
  let pos = param && param.menuBack ? param.menuBack : 'default'
  markup += `
    <br><label>${loc.menu_prop_back}</label>
    <select data-kind="menuBack">
      <option value="default" ${pos === 'default' ? 'selected' : ''}>${loc.menu_prop_backdef}</option>
      <option value="trans" ${pos === 'trans' ? 'selected' : ''}>${loc.menu_prop_backTrans}</option>
      <option value="transLight" ${pos === 'transLight' ? 'selected' : ''}>${loc.menu_prop_backlight}</option> 
      <option value="transDark" ${pos === 'transDark' ? 'selected' : ''}>${loc.menu_prop_backdark}</option> 
    </select>
  `
  return markup
}
