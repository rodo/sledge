import render from './template'
import {guidGenerator} from '../../../utilities'
import {connect} from '../../../store'
import {browseFile, getMedias} from '../../../interface/interface_mac'

export default class PropertyButtons extends HTMLElement {

  connectedCallback () {
    this.initDone = false
    connect('pages', this)
    connect('views', this)
    connect('medias', this)
    connect('currentProject', this)
    connect('sideMenuCurrentItem', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('click', e => this.onClick(e))
  }

  onClick (e) {
    switch (true) {
      case e.target.classList.contains('sd-prop-list-prev'):
        let mediaData = this.medias[e.target.dataset.name]
        mediaData.name = e.target.dataset.name
        this.saveImage(mediaData)
        this.closeCatalog()
        break
      case e.target.classList.contains('sd-prop-but-add'):
        this.value.buttons.push({
          text: {fr: 'Salut !', en: 'Hello !'},
          id: guidGenerator()
        })
        this.setProp()
        break
      case e.target.classList.contains('sd-prop-but-rm'):
        this.removeButton(e.target.dataset.ref)
        break
      case e.target.dataset.kind === 'cat':
        this.openCatalog(e.target.dataset.ref)
        if (e.target.dataset.media) {
          $('.sd-prop-list-prev').css('display', 'block')
          $(`.sd-but-catalog-list div[data-name="${e.target.dataset.media}"]`).css('display', 'none')
        }
        break
      case e.target.dataset.kind === 'browse':
        this.currentEditedButton = e.target.dataset.ref
        browseFile(name => this.setBackgroundImage(name), false, true)
        break
      case e.target.dataset.kind === 'imgclose':
        this.closeCatalog()
        break        
      default:
      //  
    }
  }

  saveImage (data) {
    let index = this.getIndexFromRef(this.currentEditedButton)
    
    if (!this.value.buttons[index].image) {
      this.value.buttons[index].image = {}
    }
    if (!this.value.buttons[index]['image'][window.$sledge.language]) {
      this.value.buttons[index]['image'][window.$sledge.language] = {}
    }
    
    let ig = this.value.buttons[index]['image'][window.$sledge.language]
    ig.media = encodeURIComponent(data.name)
    if (ig.numberOfSizes) {
      ig.numberOfSizes = data.numberOfSizes
    }
    ig.mediaType = data.type
    this.setProp()
  }

  setBackgroundImage (data) {
    getMedias(this.currentProject, () => this.saveImage(data))
  }

  update (e) {
    let index = null
    switch (e.target.dataset.kind) {
      case 'action':
        index = this.getIndexFromId(e)
        this.value.buttons[index]['action'] = e.target[e.target.selectedIndex].value
        if (this.value.buttons[index]['action'] === 'view') {
          this.value.buttons[index]['link'] = false
          this.setProp()
        } else {
          this.ctrl.setPropertyValue(this.key, this.value)
        }
        break
      case 'internal':
        index = this.getIndexFromId(e)
        this.value.buttons[index]['internal'] = e.target[e.target.selectedIndex].value
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'external':
      case 'email':
        index = this.getIndexFromId(e)
        this.value.buttons[index][e.target.dataset.kind] = encodeURIComponent(e.target.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'text':
        index = this.getIndexFromId(e)
        if (!this.value.buttons[index]) {
          this.value.buttons[index] = {}
        }
        if (!this.value.buttons[index].text) {
          this.value.buttons[index].text = {}
        }
        this.value.buttons[index].text[window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        this.setProp()
        break
      case 'size':
      case 'role':
      case 'page':
      case 'view':
      case 'effect':
        this.manageSelect(e)
        break
      case 'link':
      case 'hollow':
      case 'uppercase':
      case 'expanded':
        window.sledge.donorefresh = true;
        index = this.getIndexFromId(e)
        this.value.buttons[index][e.target.dataset.kind] = e.target.checked
        this.setProp()
        break
      case 'img':
        index = this.getIndexFromId(e)
        this.value.buttons[index][e.target.dataset.kind] = e.target.checked
        this.setProp()
        break
      case 'round':
        window.sledge.donorefresh = true;
        index = this.getIndexFromId(e)
        this.value.buttons[index]['round'] = e.target.checked
        this.value.buttons[index]['radius'] = false
        this.setProp()
        break
      case 'radius':
        window.sledge.donorefresh = true;
        index = this.getIndexFromId(e)
        this.value.buttons[index]['radius'] = e.target.checked
        this.value.buttons[index]['round'] = false
        this.setProp()
        break
      default:
      //
    }
  }

  openCatalog (id) {
    this.currentEditedButton = id
    document.querySelector('.sd-but-catalog').classList.add('visible')
  }

  closeCatalog () {
    document.querySelector('.sd-but-catalog').classList.remove('visible')
  }

  removeButton (id) {
    let index = this.value.buttons.findIndex(item => item.id === id)
    this.value.buttons.splice(index, 1)
    this.setProp()
  }

  updateOrder (newOrder) {
    let newRes = []
    let index = 0
    newOrder.forEach(newId => {
      index = 0
      index = this.value.buttons.findIndex(item => item.id === newId)
      newRes.push(this.value.buttons[index])
    })
    this.value.buttons = newRes
    this.setProp()
  }

  manageSelect (e) {
    let index = this.getIndexFromId(e)
    this.value.buttons[index][e.target.dataset.kind] = e.target[e.target.selectedIndex].value
    this.setProp()
  }

  getIndexFromId (e) {
    let id = e.target.dataset.ref
    if (id === 'null') {
      id = this.checkId()
    }
    return this.value.buttons.findIndex(item => item.id === id)
  }

  getIndexFromRef (id) {
    return this.value.buttons.findIndex(item => item.id === id)
  }

  checkId () {
    let id = guidGenerator()
    this.value.buttons[0].id = id
    return id
  }

  setProp () {
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    this.value = value || {}
    if (!this.initDone) {
      render(this, value, window.$sledge.language, this.pages, this.views, this.sideMenuCurrentItem, this.medias, props)
      this.initDone = true
    }
  }

}

window.customElements.define('sd-properties-buttons', PropertyButtons)
