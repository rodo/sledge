import currentTheme from '../../../../defaultTheme/assets/sledge/config.json'

export default (sender, param, language, pages, views, sideMenuCurrentItem, media, props) => {
  let isSingle = true
  let loc = window.sledge.nslocal
  if (!param.buttons) {
    return
  }
  if (param.buttons.length !== 1) {
    isSingle = false
  }
  sender.innerHTML = `
  <div class="sd-but-catalog">
    <button data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getMediaList(media)}
    </div>
  </div>
  <div class="sd-but-cont">
  ${
    isSingle
    ? ''
    : `<div class='sd-prop-order-cont'>
      <label>${loc.but_ord}</label>
      <div class='sd-prop-but-order-wrapper'>
        <ul>
          ${getOrderMarkup(param.buttons, language)}
        <ul>
      </div>
    </div>`
  }
  ${props.single ? '' : `<button class='sd-prop-but-add'>${loc.prop_but_add}</button>`}
  <div class='sd-prop-but-wrapper ${props.single ? 'sd-prop-but-single' : ''}'>
    ${getMarkup(param.buttons, language, isSingle, pages, views, sideMenuCurrentItem._id, media, props)}
  </div>
  </div>
  `
  if (!isSingle) {
    $('.sd-prop-but-order-wrapper ul').sortable({update: (a, b) => {
      let lis = a.target.querySelectorAll('li')
      let res = []
      ;[].forEach.call(lis, li => {
        res.push(li.dataset.ref)
      })
      sender.updateOrder(res)
    }})
  }
}

function getOrderMarkup (buttons, language) {
  let markup = ''
  buttons.forEach(button => {
    markup += `<li data-ref="${button.id || null}">${decodeURIComponent(button.text[language])}</li>`
  })
  return markup
}

function getMarkup (buttons, language, isSingle, pages, views, currentViewID, media, props) {
  let markup = ''
  buttons.forEach(button => {
    markup += getBaseMarkup(button, language, isSingle, pages, views, currentViewID, media, props)
  })
  return markup
}

function getMediaList (list, current) {
  let attachements = []
  let supportedType = ['png', 'jpg', 'svg', 'jpeg']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    console.info(obj)
    if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}

function getBaseMarkup (button, language, isSingle, pages, views, currentViewID, media, props) {
  let loc = window.sledge.nslocal
  return `
    <div class='sd-prop-but-item'>
    ${isSingle ? '' : `<button data-ref="${button.id || null}" class='sd-prop-but-rm'></button>`}
    <div>
      <label>${loc.but_img}</label>
      <input data-kind="img" data-ref="${button.id || null}" type="checkbox" ${button.img ? 'checked' : ''}/>      
    </div>
    ${!button.img ? `<div>
    <label>${loc.gen_txt} (${window.$sledge.language})</label>
      <input data-kind="text" data-ref="${button.id || null}" type="text" class="sd-prop-but-text" value="${decodeURIComponent(button.text[language])}"/>
    </div>` : ''}
    ${
      !button.img &&
      (!button.action ||
      button.action !== 'view')
    ? `<div>
        <label>${loc.but_link}</label>
        <input data-kind="link" data-ref="${button.id || null}" type="checkbox" ${button.link ? 'checked' : ''}/>
      </div>`
    : ''
    }
    ${props.noAction
      ? ''
      : `<div class="sd-prop-but-action ${(button.action && button.action !== 'none') ? 'sd-prop-multi' : ''}">
      <div><label>Action</label>
      ${getActionMarkup(button)}
      ${getActionDetailMarkup(button, pages, views, currentViewID)}
    </div>`
    }
    ${button.link
      ? ''
      : `<div>
          <label>${loc.proplogo_sz}</label>
          ${getSizeMarkup(button)}
        </div>
        ${button.img ? `${getImgMarkup(button)}`
        : `<div>
            <label>Role</label>
              ${getRoleMarkup(button)}
            </div>
          <div>
            <label>${loc.gen_rounded}</label>
            <input data-kind="round" data-ref="${button.id || null}" type="checkbox" ${button.round ? 'checked' : ''}/>
          </div>
          <div>
            <label>${loc.gen_rad}</label>
            <input data-kind="radius" data-ref="${button.id || null}" type="checkbox" ${button.radius ? 'checked' : ''}/>
          </div>
          <div>
            <label>${loc.gen_uppercase}</label>
            <input data-kind="uppercase" data-ref="${button.id || null}" type="checkbox" ${button.uppercase ? 'checked' : ''}/>
          </div>        
          <div>
            <label>${loc.gen_hollow}</label>
            <input data-kind="hollow" data-ref="${button.id || null}" type="checkbox" ${button.hollow ? 'checked' : ''}/>
          </div>`
          }`
    }
    <div>
      <hr/>
      <label>id</label>
      <input readonly type="text" value="${button.id || null}"/>      
    </div>            
  </div>`
}

function getImgMarkup (param) {
  let loc = window.sledge.nslocal
  let style = ''
  let fileName = null
  let ig = {}

  if (param.image && param.image[window.$sledge.language]) {
    ig = param.image[window.$sledge.language]
    fileName = ig.media
  }
  
  if (fileName) {
    if (ig.numberOfSizes && ig.numberOfSizes !== 1) {
      fileName = ig.media + '_small'
    }
    style = `background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${fileName}.${ig.mediaType}); `
  }
  let markup =
  `<label>${loc.but_img_current} (${window.$sledge.language})</label>
  <div class="sd-prop-preview-block">
    <div class="sd-prop-preview-block-buttons">
      <button data-kind="browse" class="sd-prop-browse-media" data-ref="${param.id || null}">${loc.but_img_new}</button>
    </div>
    <div class="sd-prop-preview" style="${style}"></div>
  </div>
  <button data-kind="cat" data-media="${ig.media}" data-ref="${param.id || null}">Choose from catalog</button>`
  return markup
}

function getViewMarkup (param, views, currentViewID) {
  let loc = window.sledge.nslocal

  let markup = `<select data-kind="view" data-ref="${param.id || null}">`
  if (!param.view || param.view === 'none') {
    markup += `<option value="none" selected>${loc.prop_actions_vsel}</option>`
  }
  for (let item in views) {
    if (currentViewID !== item) {
      if (param.view && param.view === item) {
        markup += `<option value="${item}" selected>${views[item].name}</option>`
      } else {
        markup += `<option value="${item}">${views[item].name}</option>`
      }
    }
  }
  markup += '</select></div>'

  const effect = [
      {value: 'modal', label: loc.prop_actions_mod},
      {value: 'fullscreen', label: loc.prop_actions_full},
      {value: 'papertop', label: loc.prop_actions_pap},
      {value: 'sandwitch', label: loc.prop_actions_sand}
    ]

    /*markup += `<div><label>${loc.prop_actions_eff}</label>`
    markup += `<select style="margin-left:4px;" data-kind="effect" data-ref="${param.id || null}">`

    effect.forEach(item => {
      if ((!param.effect || param.effect === 'modal') && item.value === 'modal') {
        markup += `<option value="${item.value}" selected>${item.label}</option>`
      } else if (param.effect && param.effect === item.value && item.value !== 'modal') {
        markup += `<option value="${item.value}" selected>${item.label}</option>`
      } else {
        markup += `<option value="${item.value}">${item.label}</option>`
      }
    })
    markup += '</select></div>'*/
  //}

  return markup
}

function getPageMarkup (param, pages) {
  let loc = window.sledge.nslocal

  let markup = `<select data-kind="page" data-ref="${param.id || null}">`
  if (!param.page || param.page === 'none') {
    markup += `<option value="none" selected>${loc.prop_actions_nop}</option>`
  }
  for (let item in pages) {
    if (param.page && param.page === item) {
      markup += `<option value="${item}" selected>${pages[item].name}</option>`
    } else {
      markup += `<option value="${item}">${pages[item].name}</option>`
    }
  }
  markup += '</select>'
  return markup
}

function getActionDetailMarkup (param, pages, views, currentViewID) {
  let loc = window.sledge.nslocal
  let markup = ''
  switch (param.action) {
    case 'internal':
      markup += `<div><label>${loc.prop_links_int}</label>
      ${getPageMarkup(param, pages)}</div>`
      break
    case 'email':
      markup += `<div><label>${loc.prop_links_em}</label>
      <input type="text" data-kind='email' data-ref="${param.id || null}" value="${param.email ? decodeURIComponent(param.email) : ''}"/></div>`
      break
    case 'external':
      markup += `<div><label>${loc.prop_links_ex}</label>
      <input type="text" data-kind='external' data-ref="${param.id || null}" value="${param.external ? decodeURIComponent(param.external) : ''}"/></div>`
      break
    case 'view':
      markup += `<div><label>${loc.prop_links_vw}</label>
      ${getViewMarkup(param, views, currentViewID)}`
      break
    default:
      //         
  }
  return markup
}

function getActionMarkup (param) {
  let loc = window.sledge.nslocal
  const action = [
    {value: 'none', label: loc.prop_actions_noac},
    {value: 'internal', label: loc.prop_actions_int},
    {value: 'external', label: loc.prop_actions_ex},
    {value: 'email', label: loc.prop_actions_em},
    {value: 'view', label: loc.prop_actions_vw}
  ]
  let markup = `<select data-kind="action" data-ref="${param.id || null}">`

  action.forEach(item => {
    if ((!param.action || param.action === 'none') && item.value === 'none') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else if (param.action && param.action === item.value && item.value !== 'none') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else {
      markup += `<option value="${item.value}">${item.label}</option>`
    }
  })
  markup += '</select></div>'
  return markup
}


function getSizeMarkup (param) {
  let loc = window.sledge.nslocal
  const sizes = [
    {value: 'tiny', label: loc.gen_size_vsm},
    {value: 'small', label: loc.gen_size_sm},
    {value: 'default', label: loc.gen_size_def},
    {value: 'large', label: loc.gen_size_lg}
  ]
  let markup = `<select data-kind="size" data-ref="${param.id || null}">`

  sizes.forEach(item => {
    if ((!param.size || param.size === 'default') && item.value === 'default') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else if (param.size && param.size === item.value && item.value !== 'default') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else {
      markup += `<option value="${item.value}">${item.label}</option>`
    }
  })
  markup += '</select>'
  return markup
}

function getRoleMarkup (param) {
  let palette = currentTheme['foundation-palette']
  let markup = `<select data-kind="role" data-ref="${param.id || null}">`

  for (let item in palette) {
    if ((!param.role || param.role === 'primary') && item === 'primary') {
      markup += `<option value="${item}" selected>${palette[item].label[window.sledge.languageCode]}</option>`
    } else if (param.role && param.role === item && item !== 'primary') {
      markup += `<option value="${item}" selected>${palette[item].label[window.sledge.languageCode]}</option>`
    } else {
      markup += `<option value="${item}">${palette[item].label[window.sledge.languageCode]}</option>`
    }
  }
  markup += '</select>'
  return markup
}
