import render from './template'
import {connect} from '../../../store'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    connect('pages', this)
    connect('views', this)
    connect('sideMenuCurrentItem', this)    
    this.value = {}
    this.addEventListener('change', e => this.update(e))
  }

  init (propController, props, value) {
    this.value = value
    this.ctrl = propController
    this.key = props.key
    render(this, value, this.pages, this.views, this.sideMenuCurrentItem)
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'action':
        this.value['action'] = e.target[e.target.selectedIndex].value
        if (this.value['action'] === 'view') {
          this.value['link'] = false
          this.setProp()
        } else {
          this.ctrl.setPropertyValue(this.key, this.value)
        }
        break
      case 'page':
      case 'view':
        this.value[e.target.dataset.kind] = e.target[e.target.selectedIndex].value
        this.setProp()
        break
      case 'internal':
        this.value['internal'] = e.target[e.target.selectedIndex].value
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'effect':
        this.value['effect'] = e.target[e.target.selectedIndex].value
        this.ctrl.setPropertyValue(this.key, this.value)
        break        
      case 'external':
      case 'email':
        this.value[e.target.dataset.kind] = encodeURIComponent(e.target.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      default:
      //
    }
    // this.ctrl.setPropertyValue(this.key, e.target.value)
  }

  setProp () {
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

}

window.customElements.define('sd-properties-link', Property)
