import render from './template'

export default class Property extends HTMLElement {

  connectedCallback () {
    this.value = {}
    this.addEventListener('change', e => this.updateChange(e))
  }

  updateChange (e) {
    switch (e.target.dataset.kind) {
      case 'toggle':
        this.value.toggle = e.target.checked
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'item':
        if (!this.value.remove) {
          this.value.remove = []
        }
        if (e.target.checked) {
          this.value.remove.push(e.target.dataset.id)
        } else {
          const index = this.value.remove.indexOf(e.target.dataset.id)
          this.value.remove.splice(index, 1)
        }
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
    }
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    if (value) {
      this.value = {...value}
    } else {
      this.value = {}
    }
    render(this, this.value, props)
  }

}

window.customElements.define('sd-properties-sitemap', Property)
