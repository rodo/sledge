export default (sender, param = {}, props = {}) => {
  const loc = window.sledge.nslocal
  let markup = ''
  if (props.toggle) {
    markup += `<label class="sd-full">${loc.prop_sitemap_toggle}</label>
    <input data-kind='toggle' type="checkbox" ${param.toggle ? 'checked' : ''}/>`
  }
  markup += getRootMenu(param, props)
  sender.innerHTML = markup
}

function getRootMenu (param, props) {
  const loc = window.sledge.nslocal
  let root = window.$sledge.bridge.siteMapManager.getMap(window.$sledge.language)
  let markup = `<br><label class="sd-full">${loc.prop_sitemap_set}</label><ul class="sg_props_menu">`
  const remove = param.remove || []
  root.forEach(obj => {
    if (obj.kind === 'page') {
      markup += `<li><input ${remove.indexOf(obj.id) !== -1 ? 'checked' : ''} data-kind='item' data-id="${obj.id}" type="checkbox"/> Page | ${obj.localName}</li>`
    } else if (!props.onlyPage) {
      markup += `<li><input ${remove.indexOf(obj.id) !== -1 ? 'checked' : ''} data-kind='item' data-id="${obj.id}" type="checkbox"/> Section | ${obj.localName}</li>`
    }
  })
  return markup + '</ul>'
}
