import render from './template'
import {connect} from '../../../store'

export default class Custum extends HTMLElement {

  connectedCallback () {
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('keyup', e => this.update(e))
    this.addEventListener('click', e => this.updateClick(e))
    connect('pages', this)
    connect('sideMenuCurrentItem', this)
    connect('views', this)
  }

  update (e) {
    let done = false
    switch (e.target.dataset.kind) {
      case 'linkType':
        this.value.linkType = e.target[e.target.selectedIndex].value
        this.setProp()
        done = true
        break
      case 'page':
      case 'view':
      case 'effect':
        this.value[e.target.dataset.kind] = e.target[e.target.selectedIndex].value
        this.ctrl.setPropertyValue(this.key, this.value)
        done = true
        break
      case 'external':
        this.value.linkType = 'external'
        this.value[e.target.dataset.kind] = encodeURIComponent(e.target.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        done = true
        break
      case 'email':
        this.value[e.target.dataset.kind] = encodeURIComponent(e.target.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        done = true
        break
      default:
    }

    if (done) {
      return
    }

    switch (e.target.type) {
      case 'range':
        this.value[e.target.dataset.kind] = e.target.value
        break
      case 'checkbox':
        this.value[e.target.dataset.kind] = e.target.checked
        break
      case 'radio':
        this.value[e.target.dataset.kind] = e.target.value
        break
      case 'email':
        this.value[e.target.dataset.kind] = encodeURIComponent(e.target.value)
        break
      case 'text':
        if (!this.value[e.target.dataset.kind]) {
          this.value[e.target.dataset.kind] = {}
        }
        this.value[e.target.dataset.kind][window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        break
      case 'select-one':
        this.value[e.target.dataset.kind] = e.target[e.target.selectedIndex].value
        break
      default:
    }
    this.setProp()
  }

  setProp () {
    this.ctrl.setPropertyValue(this.key, this.value)
    this.ctrl.applyProperty(this.key, this.value)
  }

  updateClick (e) {
    switch (e.target.dataset.kind) {
      case 'scheme':
        this.value[e.target.dataset.name] = e.target.dataset.value
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
      case 'rmColor':
        this.value[e.target.dataset.name] = null
        this.ctrl.applyProperty(this.key, this.value)
        this.ctrl.setPropertyValue(this.key, this.value)
        break
    }
  }

  init (propController, props, value) {
    this.ctrl = propController
    this.key = props.key
    this.value = value || {}
    render(this, props, value, this.pages, this.views, this.sideMenuCurrentItem, window.$sledge.language)
  }
}

window.customElements.define('sd-properties-custom', Custum)
