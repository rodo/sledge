export default (sender, props, param, pages, views, sideMenuCurrentItem, languages) => {
  param = param || {}
  if (!param.position) {
    param.position = 'side'
  }
  sender.innerHTML = `
  ${getMarkup(props, param, pages, views, sideMenuCurrentItem, languages)}
  `
}

function getMarkup (props, param, pages, views, sideMenuCurrentItem, language) {
  let loc = window.sledge.nslocal
  let markup = ''
  props.data.forEach(item => {
    switch (item.type) {
      case 'text':
        markup += getText(item, param, language)
        break
      case 'radioList':
        markup += getRadio(item, param)
        break
      case 'select':
        markup += getSelect(item, param)
        break
      case 'checkbox':
        markup += getCheck(item, param)
        break
      case 'themeColor':
        markup += getThemeColor(item, param)
        break
      case 'range':
        markup += getRange(item, param)
        break
      case 'link':
        markup += getLink(item, param, pages, views, sideMenuCurrentItem._id)
        break
      case 'color':
        markup += getColor(item, param)
        break
      default:
    }
  })
  return markup
}

function getText (props, param, language) {
  let conf = props.conf || {}
  let val = ''
  let type = props.special || 'text'
  if (type === 'email') {
    val = param[props.kind] || conf.default || ''
  } else {
    val = param[props.kind] ? param[props.kind][language] : ''
  }
  
  if (val && val !== '') {
    val = decodeURIComponent(val)
  }
  let markup = `<br>
  <label>${props.name[window.sledge.languageCode]} (${language})</label>
  <input data-kind="${props.kind}" value="${val}" type="${type}"/>`
  return markup
}

function getRange (props, param) {
  let conf = props.conf
  let val = param[props.kind] || conf.default
  let markup = `<br><label>${props.name[window.sledge.languageCode]}</label>
  <div class="sd-prop-custom-cont"><div class="sd-prop-custom-range"><input value="${val}" type="range" data-kind="${props.kind}" min="${conf.min}" max="${conf.max}">`

  if (conf.label && conf.label.max) {
    markup += `<span>${conf.label.max[window.sledge.languageCode]}</span></div></div>`
  } else {
    markup += '</div></div>'
  }
  return markup
}

function getSelect (props, param) {
  let size = param[props.kind] || props.default || 'small'
  let markup = `<br><label>${props.name[window.sledge.languageCode]}</label>
  <select data-kind="${props.kind}">`
  for (let item in props.conf) {
    markup += `<option value="${props.conf[item].value}" ${size === props.conf[item].value ? 'selected' : ''}>${props.conf[item].label[window.sledge.languageCode]}</option>`
  }
  markup += '</select></div>'
  return markup
}

function getLink (props, param, pages, views, currentViewID) {
  let loc = window.sledge.nslocal
  
  let linkType = param.linkType || 'external'
  let markup = `<br><label>${loc.pro_cus_link}</label>
  <select data-kind="linkType">
    <option value="external" ${linkType === 'external' ? 'selected' : ''}>${loc.prop_actions_ex}</option> 
    <option value="internal" ${linkType === 'internal' ? 'selected' : ''}>${loc.prop_actions_int}</option> 
    <option value="view" ${linkType === 'view' ? 'selected' : ''}>${loc.prop_actions_vw}</option>     
    <option value="email" ${linkType === 'email' ? 'selected' : ''}>${loc.prop_actions_em}</option>
  </select>
  `
  switch (linkType) {
    case 'internal':
      markup += `<div><label>${loc.prop_links_int}</label>
      ${getPageMarkup(param, pages)}</div>`
      break
    case 'email':
      markup += `<div><label>${loc.prop_links_em}</label>
      <input type="text" data-kind='email' data-ref="${param.id || null}" value="${param.email ? decodeURIComponent(param.email) : ''}"/></div>`
      break
    case 'external':
      markup += `<div><label>${loc.prop_links_ex}</label>
      <input type="text" data-kind='external' data-ref="${param.id || null}" value="${param.external ? decodeURIComponent(param.external) : ''}"/></div>`
      break
    case 'view':
      markup += `<div><label>${loc.prop_links_vw}</label>
      ${getViewMarkup(param, views, currentViewID)}`
      break
    default:
      //         
  }
  return markup
}

function getViewMarkup (param, views, currentViewID) {
  let loc = window.sledge.nslocal
  
  let markup = `<select data-kind="view" data-ref="${param.id || null}">`
  if (!param.view || param.view === 'none') {
    markup += `<option value="none" selected>${loc.prop_actions_vsel}</option>`
  }
  for (let item in views) {
    if (currentViewID !== item) {
      if (param.view && param.view === item) {
        markup += `<option value="${item}" selected>${views[item].name}</option>`
      } else {
        markup += `<option value="${item}">${views[item].name}</option>`
      }
    }
  }
  markup += '</select></div>'

  /*const effect = [
      {value: 'modal', label: loc.prop_actions_mod},
      {value: 'fullscreen', label: loc.prop_actions_full},
      {value: 'papertop', label: loc.prop_actions_pap},
      {value: 'sandwitch', label: loc.prop_actions_sand}
    ]

  markup +=   `<div><label>${loc.prop_actions_eff}</label>`
  markup += `<select style="margin-left:4px;" data-kind="effect" data-ref="${param.id || null}">`

  effect.forEach(item => {
    if ((!param.effect || param.effect === 'modal') && item.value === 'modal') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else if (param.effect && param.effect === item.value && item.value !== 'modal') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else {
      markup += `<option value="${item.value}">${item.label}</option>`
    }
  })
  markup += '</select></div>'*/
  //}

  return markup
}

function getPageMarkup (param, pages) {
  let loc = window.sledge.nslocal
  
  let markup = `<select data-kind="page" data-ref="${param.id || null}">`
  if (!param.page || param.page === 'none') {
    markup += `<option value="none" selected>${loc.prop_actions_nop}</option>`
  }
  for (let item in pages) {
    if (param.page && param.page === item) {
      markup += `<option value="${item}" selected>${pages[item].name}</option>`
    } else {
      markup += `<option value="${item}">${pages[item].name}</option>`
    }
  }
  markup += '</select>'
  return markup
}

function getThemeColor (props, param) {
  return `<br><label class="sd-full">${props.name[window.sledge.languageCode]}</label><sd-color-selector data-kind="${props.kind}"></sd-color-selector>`
}

function getRadio (props, param) {
  let markup = ''
  props.conf.forEach(item => {
    markup += `<br><label for="${item.ref}">${item.label[window.sledge.languageCode]}</label>
    <input ${param[props.kind] === item.value ? 'checked' : ''} name="${props.kind}" data-kind="${props.kind}" type="radio" id="${item.ref}" value="${item.value}" >`
  })
  return markup
}

function getCheck (props, param) {
  let markup = ''
  markup += `<br><label for="${props.ref}">${props.name[window.sledge.languageCode]}</label>
  <input ${param[props.kind] ? 'checked' : ''} data-kind="${props.kind}" type="checkbox" id="${props.ref}">`
  return markup
}
