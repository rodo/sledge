import render from './template'
import {connect} from '../../../store'
import {browseFile, getMedias} from '../../../interface/interface_mac'
import {guidGenerator} from '../../../utilities'

import renderLink from './templateLink'

export default class Property extends HTMLElement {

  connectedCallback () {
    connect('currentProject', this)
    connect('medias', this)
    connect('pages', this)
    connect('views', this)
    connect('sideMenuCurrentItem', this)
    this.value = {}
    this.addEventListener('change', e => this.update(e))
    this.addEventListener('keyup', e => this.updateKey(e))
    this.addEventListener('click', e => this.updateClick(e))
  }

  init (propController, props, value) {
    this.value = value || {}
    this.ctrl = propController
    this.key = props.key
    // fix issue with old system without idx
    if (this.value.links && this.value.links[0] && !this.value.links[0].idx) {
      this.value.links.forEach(obj => {
        obj.idx = guidGenerator()
      })
    }
    render(this, this.value, window.$sledge.language, this.views, this.pages)
  }

  updateOrder (newOrder) {
    let newRes = []
    let index = 0
    newOrder.forEach(newId => {
      index = 0
      index = this.getLinkIndexByIdx(newId)
      newRes.push(this.value.links[index])
    })
    this.value.links = newRes
    this.setProp()
  }

  getLinkIndexByIdx (idx) {
    return this.value.links.findIndex(item => item.idx === idx)
  }

  closeCatalog () {
    document.querySelector('.sd-but-catalog').classList.remove('visible')
  }

  updateClick (e) {
    switch (e.target.dataset.kind) {
      case 'browse':
        browseFile(name => this.setBackgroundImage(name), false, true)
        break
      case 'link':
        this.currentIdx = this.getLinkIndexByIdx(e.target.dataset.idx)
        let el = this.querySelector('.sd-but-link-manager')
        el.classList.add('visible')
        renderLink(el.querySelector('.sd-but-catalog-list'), this.value.links[this.currentIdx], this.pages, this.views, this.sideMenuCurrentItem)
        break
      case 'linkclose':
        render(this, this.value, window.$sledge.language, this.views, this.pages)
        break
      case 'img':
        this.currentIdx = this.getLinkIndexByIdx(e.target.dataset.idx)
        let elem = this.querySelector('sd-image-selector')
        this.querySelector('.sd-but-catalog').classList.add('visible')
        let name = e.target.dataset.image
        if (e.target.dataset.type === 'media') {
          elem.setAttribute('data-imgsize', this.medias[name].numberOfSizes)
          elem.setAttribute('data-mediatype', this.medias[name].type)
        }
        elem.setAttribute('data-image', e.target.dataset.type + '/' + name)
        break
      case 'imgclose':
        this.closeCatalog()
        break
      case 'add':
        if (!this.value.links) {
          this.value.links = []
        }
        this.value.links.push({
          name: '',
          url: '',
          urlType: 'external',
          image: 'fi-list',
          imageType: 'logo',
          idx: guidGenerator()
        })
        this.setProp()
        break
      case 'rm':
        //let li = e.target.parentNode
        let theID = this.getLinkIndexByIdx(e.target.dataset.idx)
        this.value.links.splice(theID, 1)
        this.setProp()
        break
      case 'sd-func-cat':
        //this.currentIdx = this.getLinkIndexByIdx(e.target.dataset.idx)
        let obj = this.value.links[this.currentIdx]
        obj.image = e.target.dataset.icn
        obj.imageType = 'logo'
        this.closeCatalog()
        this.setProp()
        break
      case 'imgTheme':
        let objs = this.value.links[this.currentIdx]
        objs.imageType = 'imgTheme'
        objs.image = e.target.dataset.name
        this.closeCatalog()
        this.setProp()
        break
      case 'media':
        let mediaName = e.target.dataset.name
        let imgObj = this.value.links[this.currentIdx]
        imgObj.imageType = 'media'
        imgObj.media = encodeURIComponent(mediaName)
        imgObj.numberOfSizes = this.medias[mediaName].numberOfSizes
        imgObj.mediaType = this.medias[mediaName].type
        this.closeCatalog()
        this.setProp()
        break
    }
  }

  setBackgroundImage (data) {
    getMedias(this.currentProject, () => {
      let obj = this.value.links[this.currentIdx]
      obj.imageType = 'media'
      obj.media = encodeURIComponent(data.name)
      if (data.numberOfSizes) {
        obj.numberOfSizes = data.numberOfSizes
      }
      obj.mediaType = data.type
      this.closeCatalog()
      this.setProp()
    })
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'action':
        let obj = this.value.links[this.currentIdx]
        obj.action = e.target[e.target.selectedIndex].value
        let el = this.querySelector('.sd-but-link-manager')
        window.sledge.donorefresh = true
        this.ctrl.setPropertyValue(this.key, this.value)
        renderLink(el.querySelector('.sd-but-catalog-list'), this.value.links[this.currentIdx], this.pages, this.views, this.sideMenuCurrentItem)        
        break
      case 'add':
        let newNetwork = e.target[e.target.selectedIndex].value
        if (!this.value.networks) {
          this.value.networks = []
        }
        this.value.networks.push({
          name: newNetwork,
          url: ''
        })
        this.setProp()
        break
      case 'phone':
        let objP = this.value.links[this.currentIdx]
        objP.phone = e.target.value
        window.sledge.donorefresh = true
        this.setProp()
        break
      case 'email':
      case 'external':
        let obje = this.value.links[this.currentIdx]
        obje[e.target.dataset.kind] = encodeURIComponent(e.target.value)
        window.sledge.donorefresh = true
        this.setProp()
        break
      case 'page':
      case 'view':
        let objv = this.value.links[this.currentIdx]
        objv[e.target.dataset.kind] = e.target[e.target.selectedIndex].value
        window.sledge.donorefresh = true
        this.setProp()
        break
      case 'effect':
        let objef = this.value.links[this.currentIdx]
        objef.effect = e.target[e.target.selectedIndex].value
        window.sledge.donorefresh = true
        this.ctrl.setPropertyValue(this.key, this.value)
        break                   
      default:
      //
    }
  }

  updateKey (e) {
    switch (e.target.dataset.kind) {
      case 'name':
        let theIdx = this.getLinkIndexByIdx(e.target.dataset.idx)
        let objn = this.value.links[theIdx]
        if (!objn.name) {
          objn.name = {}
        }
        objn.name[window.$sledge.language] = encodeURIComponent(e.target.value).replace(/[!'()*]/g, escape)
        window.sledge.donorefresh = true
        this.setProp()
        break
      default:
      //
    }
  }

  setProp () {
    this.ctrl.applyProperty(this.key, this.value)
    this.ctrl.setPropertyValue(this.key, this.value)
  }

}

window.customElements.define('sd-properties-links', Property)
