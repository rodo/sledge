export default (sender, param, pages, views, currentView) => { 
  param = param || {}

  let markup = `
  ${getActionMarkup(param)}
  <hr>
  ${getActionDetailMarkup(param, pages, views, currentView._id)}  
  `
  sender.innerHTML = markup
}

function getActionDetailMarkup (param, pages, views, currentViewId) {
  let loc = window.sledge.nslocal  
  let markup = ''
  switch (param.action) {
    case 'internal':
      markup += `<div><label>${loc.prop_links_int}</label>
      ${getPageMarkup(param, pages)}</div>`
      break
    case 'email':
      markup += `<div><label>${loc.prop_links_em}</label>
      <input type="text" data-kind='email' data-ref="${param.id || null}" value="${param.email ? decodeURIComponent(param.email) : ''}"/></div>`
      break
    case 'phone':
      markup += `<div><label>${loc.prop_links_pho}</label>
      <input type="tel" data-kind='phone' data-ref="${param.id || null}" value="${param.phone || ''}"/></div>`
      break
    case 'external':
      markup += `<div><label>${loc.prop_links_ex}</label>
      <input type="text" data-kind='external' data-ref="${param.id || null}" value="${param.external ? decodeURIComponent(param.external) : ''}"/></div>`
      break
    case 'view':
      markup += `<div><label>${loc.prop_links_vw}</label>
      ${getViewMarkup(param, views, currentViewId)}`
      break
    default:
      markup = ''
  }
  return markup
}

function getActionMarkup (param) {
  let loc = window.sledge.nslocal
  const action = [
    {value: 'none', label: loc.prop_actions_noac},
    {value: 'internal', label: loc.prop_actions_int},
    {value: 'external', label: loc.prop_actions_ex},
    {value: 'email', label: loc.prop_actions_em},
    {value: 'phone', label: loc.prop_actions_pho},
    {value: 'view', label: loc.prop_actions_vw}
  ]
  let markup = `<label class="sd-full">${loc.prop_actions_add}</label>
  <select data-kind="action" data-ref="${param.id || null}">`

  action.forEach(item => {
    if ((!param.action || param.action === 'none') && item.value === 'none') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else if (param.action && param.action === item.value && item.value !== 'none') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else {
      markup += `<option value="${item.value}">${item.label}</option>`
    }
  })
  markup += '</select></div>'
  return markup
}

function getViewMarkup (param, views, currentViewID) {
  let loc = window.sledge.nslocal  
  let markup = `<select data-kind="view" data-ref="${param.id || null}">`
  if (!param.view || param.view === 'none') {
    markup += `<option value="none" selected>${loc.prop_actions_vsel}</option>`
  }
  for (let item in views) {
    if (currentViewID !== item) {
      if (param.view && param.view === item) {
        markup += `<option value="${item}" selected>${views[item].name}</option>`
      } else {
        markup += `<option value="${item}">${views[item].name}</option>`
      }
    }
  }
  markup += '</select></div>'

  /*const effect = [
    {value: 'modal', label: loc.prop_actions_mod},
    {value: 'fullscreen', label: loc.prop_actions_full},
    {value: 'papertop', label: loc.prop_actions_pap},
    {value: 'sandwitch', label: loc.prop_actions_sand}
  ]

  markup += `<div><label>${loc.prop_actions_eff}</label>`
  markup += `<select style="margin-left:4px;" data-kind="effect" data-ref="${param.id || null}">`

  effect.forEach(item => {
    if ((!param.effect || param.effect === 'modal') && item.value === 'modal') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else if (param.effect && param.effect === item.value && item.value !== 'modal') {
      markup += `<option value="${item.value}" selected>${item.label}</option>`
    } else {
      markup += `<option value="${item.value}">${item.label}</option>`
    }
  })
  markup += '</select></div>'*/
  return markup
}

function getPageMarkup (param, pages) {
  let loc = window.sledge.nslocal
  let markup = `<select data-kind="page" data-ref="${param.id || null}">`
  if (!param.page || param.page === 'none') {
    markup += `<option value="none" selected>${loc.prop_actions_nop}</option>`
  }
  for (let item in pages) {
    if (param.page && param.page === item) {
      markup += `<option value="${item}" selected>${pages[item].name}</option>`
    } else {
      markup += `<option value="${item}">${pages[item].name}</option>`
    }
  }
  markup += '</select>'
  return markup
}
