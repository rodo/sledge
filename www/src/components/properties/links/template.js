
export default (sender, param = {}, language, views, pages) => {
  let loc = window.sledge.nslocal
  let lists = param.links || []
  let markup = `
  <div class="sd-prop-lists sd-but-catalog">
    <button class="sd-but-catalog-close" data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      <sd-image-selector data-image="" data-imgsize="" data-mediatype="" data-noimgtheme="true"></sd-image-selector>    
    </div>
  </div>
  <div class="sd-but-link-manager">
    <button class="sd-but-catalog-close" data-kind="linkclose"></button>
    <div class="sd-but-catalog-list">
    </div>
  </div>
  <div class="sd-but-cont">
    <button data-kind="add">${loc.prop_links_add}</button>
    <br><label class="sd-full">${loc.prop_links_lab}</label>
    <ul class="sg-prop-links">
      ${getList(lists, language, views, pages)}
    </ul>
  </div>
  `
  sender.innerHTML = markup

  $('.sg-prop-links').sortable({update: (a, b) => {
    let lis = a.target.querySelectorAll('li')
    let res = []
    ;[].forEach.call(lis, li => {
      res.push(li.dataset.idx)
    })
    sender.updateOrder(res)
  }})
}

function getList (currents, language, views, pages) {
  let markup = ''
  let loc = window.sledge.nslocal
  if (currents.lenght !== 0) {
    currents.forEach(net => {
      let name = (net.name && net.name[language]) ? decodeURIComponent(net.name[language]) : ''
      let url = ''
      switch (net.action) {
        case 'phone':
          url = net.phone
          break
        case 'email':
          url = decodeURIComponent(net.email)
          break
        case 'external':
          url = decodeURIComponent(net.external)
          break
        case 'internal':
          url = pages[net.page] ? pages[net.page].name : ''
          break
        case 'view':
          url = views[net.view] ? views[net.view].name : ''
          break
        default:
          url = ''
      }
      markup += `<li data-idx="${net.idx}" data-name="${name}" data-url="${url || ''}">
        <div>
        <label>${loc.prop_links_name} (${language})</label>
        <input data-idx="${net.idx}" data-kind="name" type="text" value="${name}"/>      
        </div>
        <div>
        <label>${loc.prop_links_link}</label>
        <span>${url}</span>
        <button data-idx="${net.idx}" data-kind="link">${loc.prop_links_change}</button>
        </div>
        <div>
        <label>${loc.prop_links_logo}</label><span class="sd-prop-list-spi">${getImagePreview(net)}</span><button data-idx="${net.idx}" data-image="${net.imageType === 'media' ? net.media : net.image}" data-type="${net.imageType || 'logo'}" data-kind="img">${loc.prop_links_change}</button>
        <button data-idx="${net.idx}" class="sd-prop-links-rm" title="${loc.prop_soc_rm}" data-kind="rm"></button>
        </div>
      </li>`
    })
  }
  return markup
}

function getImagePreview (data) {
  let markup = ''
  if (data.imageType === 'logo') {
    markup += `<i class="${data.image}"></i>`
  }
  if (data.imageType === 'imgTheme') {
    markup += `<div class="sd-prop-links-imp-prev ${data.image}"></div>`
  }

  if (data.imageType === 'media') {
    let fileName = data.media
    let style = ''
    if (fileName) {
      if (data.numberOfSizes !== 1) {
        fileName = fileName + '_small'
      }
      style = `background-image:url(${window.$sledge.conf.dbServerUrl}/${window.$sledge.currentdb}/media/${fileName}.${data.mediaType}); `
    }

    markup += `<div class="sd-prop-links-imp-prev" style="${style}"></div>`
  }
  return markup
}
