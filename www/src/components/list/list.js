(function () {
 	"use strict";

  	var proto = Object.create(HTMLElement.prototype);
  
  	proto.createdCallback = function() {
      	this.init();
  	};
  
    proto.init = function() {
        this.classList.add("sd_list");   
    };

  	proto.readAttributes = function() {
    	//this.hour = this.getAttribute( "hour" ); 
  	};
  
  	proto.attributeChangedCallback = function(attrName, oldVal, newVal) {

  	};

    proto.delete = function() {
    
    };

    proto.clean = function() {
        this.innerHTML = "";
    };

    proto.populate = function(data) {
        this.clean();
        var tpl = JST['src/components/list/list.hbs'];
        this.innerHTML = tpl(data);
    };

  	document.registerElement( "sdg-list", {
      	prototype: proto
  	});
}());