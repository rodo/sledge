import {remove, domUnwrap, elementsFromPoint, removeClass} from '../../utilities'
import Resizable from '../../resizable'
import {connect, store} from '../../store'
//import { currentId } from 'async_hooks';
// import {checkWorkspaceScroll} from '../builder/builder' ?? marche pas avec celle ci ?

export default class ActionZone extends HTMLElement {
  constructor (conf, parent) {
    super()
  }

  connectedCallback () {
    let loc = window.sledge.nslocal

    this.classList.add('sd-actionZone')
    this.resizable = true
    this.onDragElement = null
    this.setAttribute('draggable', true)
        // drag
    this.addEventListener('dragstart', this.handleDragStart, false)
    this.addEventListener('dragend', this.handleDragStop, false)
    this.addEventListener('drag', this.checkWorkspaceScroll, false)
        
        // actions
    this.innerHTML = `<div class="sd-component-rm">
                        <div class="sd-component-rm-main"></div>
                        <div class="sd-component-rm-conf">${loc.comp_rm}</div>
                      </div>
                      <div class="sd-component-edit"></div>`

    this.querySelector('.sd-component-rm').addEventListener('click', e => this.removeComponent(e))
    this.querySelector('.sd-component-edit').addEventListener('click', e => this.editProp())

    // resize
    this.resizable = new Resizable({
      domElement: this,
      onResize: (d) => this.onResize(d),
      onStopResize: (d) => this.onStopResize(d),
      onStartResize: () => this.onStartResize()
    })
    connect('currentSelectedComponent', this, id => { this.selectedComponent = document.getElementById(id) })
    connect('currentEditedProperties', this, p => {
      if (!p) {
        remove('.sd-prop-ghost')
        removeClass(this.querySelector('.sd-selected'), 'sd-selected')
        this.ghost = null
      }
    })
    connect('responsiveState', this)
    connect('currentEditedProperties', this, (a) => {
      if (a === null) {
        this.resetSelection()
      }
    })
  }

  init (ref, strManager) {
    this.ref = ref
    this.strManager = strManager

    // pre-load image for drag
    this.ghostMove = document.createElement('img')
    this.ghostMove.src = `./framework/components/${ref.dataset.ref}/icn.png`

    let posElem = ref.getBoundingClientRect()
    let top = posElem.top < 50 ? 50 : posElem.top
    let h = posElem.top < 50 ? posElem.height + (posElem.top - 50) : posElem.height
    if ((top + h) > window.innerHeight) {
      h = h - ((top + h) - window.innerHeight)
    }
    this.style.top = top + 'px'
    this.style.left = posElem.left + 'px'
    this.style.width = posElem.width + 'px'
    this.style.height = h + 'px'

    this.lastWidth = null
    this.selected = false
    this.currentUnit = this.ref.parentNode.getBoundingClientRect().width / 12
    this.addEventListener('click', this.onSelect)
    window.PubSub.subscribe('PROPERTY_UPDATED', a => {
      domUnwrap(this.selectedComponent, elem => {
        this.resizeGhost(elem.querySelector(this.currentEditedProperties))
      })
    })
    window.PubSub.subscribe('ACTION_ZONE_RESIZE', a => this.resizeZone())
  }

  resizeZone () {
    let posElem = this.ref.getBoundingClientRect()
    
    let top = posElem.top < 50 ? 50 : posElem.top
    let h = posElem.top < 50 ? posElem.height + (posElem.top - 50) : posElem.height
    if ((top + h) > window.innerHeight) {
      h = h - ((top + h) - window.innerHeight)
    }
    this.style.top = top + 'px'
    this.style.height = h + 'px'
  }

  removeComponent (e) {
    let target = e.target

    if (target.classList.contains('sd-component-rm-main')) {
      if (!this.removeState) {
        this.resetSelection()
        this.removeState = true
        this.classList.add('sd-cancel')
        this.actionPending = true
        PubSub.publish('hideProperties')
      } else {
        this.cancelRemoveState()
      }
    }

    if (target.classList.contains('sd-component-rm-conf')) {
      PubSub.publish('removeCurrentComponent')
    }  
  }

  cancelRemoveState () {
    if (this.removeState) {
      this.removeState = false
      this.classList.remove('sd-cancel')
      this.actionPending = false
    }
  }

  resetSelection () {
    remove(this.ghost)
    this.ghost = null
    let button = this.querySelector('.sd-component-edit.sd-selected')
    if (button) {
      button.classList.remove('sd-selected')
    }
  }

  onSelect (e) {
    if (!e.target.classList.contains('sd-component-rm-main') && !e.target.classList.contains('sd-component-rm-conf')) {
      this.cancelRemoveState()
    }
    if (e.srcElement.className.indexOf('sd-actionZone-') !== -1) {
      return
    }
    if (e.target.classList.contains('sd_selected')) {
      if (this.ghost) {
        store.dispatch({type: 'SET_EDITED_PROPERTIES', value: null})
        remove(this.ghost)
        this.ghost = null
        return
      }

      if (this.currentEditedProperties === 'self') {
        store.dispatch({type: 'SET_EDITED_PROPERTIES', value: null})
        let sel = this.querySelector('.sd-component-edit.sd-selected')
        if (sel) {
          this.querySelector('.sd-component-edit.sd-selected').classList.remove('sd-selected')
        }
      }
      return
    }
    e.preventDefault()
    e.stopPropagation()
    this.selected = true
    this.classList.add('sd_selected')
    this.addEventListener('mousemove', e => this.onOver(e))
    this.ref.classList.add('sd-selection')
    store.dispatch({type: 'SET_SELECTED_COMPONENT', value: this.ref.id})
  }

  editProp (elem) {
    this.cancelRemoveState()
    let value
    if (!elem) {
      value = 'self'
    } else {
      value = $(elem).data('properties')
    }
    store.dispatch({type: 'SET_EDITED_PROPERTIES', value: value})
    if (this.ghost) {
      this.ghost.classList.add('sd-selected')
    } else {
      this.querySelector('.sd-component-edit').classList.add('sd-selected')
    }
  }

  onOver (e) {    
    if (this.currentEditedProperties === 'self') {
      return
    }
    if (this.actionPending) {
      return
    }
    let elems = elementsFromPoint(e.clientX, e.clientY)
    
    elems = elems.filter(elem => {
      if (elem.className.indexOf('sd-actionZone') === -1 && elem.nodeName !== 'SG-ROW' && $(elem).data('properties')) { // accès .dataset qui ne marche pas ?!!
        return true
      }
      return false
    })

    if (elems.length !== 0) {
      if (this.ghost && this.ghost.dataset.selector !== elems[0].dataset.properties && !this.ghost.classList.contains('sd-selected')) {
        remove(this.ghost)
        this.ghost = null
      }

      if (!this.ghost) {
        this.ghost = document.createElement('div')
        this.ghost.dataset.selector = elems[0].dataset.properties
        this.ghost.className = 'sd-prop-ghost'
        this.resizeGhost(elems[0])
        this.ghost.addEventListener('click', () => this.editProp(elems[0]))
        document.body.appendChild(this.ghost)
        this.ghost.addEventListener('mousemove', e => this.onOver(e))
      }
    } else {
      if (this.ghost && !this.ghost.classList.contains('sd-selected')) {
        remove(this.ghost)
        this.ghost = null
      }
    }
  }

  resizeGhost (elem) {
    if (!this.ghost || !elem) {
      return
    }
    const sizes = elem.getBoundingClientRect()
    const padding = 0
    // const editSpace = 30
    this.ghost.style.width = sizes.width + 'px'
    this.ghost.style.top = sizes.top + padding + 'px'
    this.ghost.style.left = sizes.left + 'px'
    this.ghost.style.height = sizes.height + 'px'

    const selectedSizes = this.selectedComponent.getBoundingClientRect()
    if (sizes.width > selectedSizes.width) {
      this.ghost.style.left = selectedSizes.left + 'px'
      this.ghost.style.width = selectedSizes.width + 'px'
    }
  }

  onStartResize () {
    this.cancelRemoveState()
    this.removeEventListener('click', this.onSelect)
  }

  onResize (data) {
    let overed = this.ref
    let newClass = null

    const direction = data.direction
    const ghost = data.elem
    // const originalSize = data.initialPos
    // const currentSize = data.currentSize
    // const diff = originalSize.width - currentSize.width
    const currentW = data.currentSize.width
    const mod = currentW % this.currentUnit
    let newUnitNumber = (currentW - mod) / this.currentUnit

    if (mod >= (this.currentUnit / 2)) {
      newUnitNumber++
    }

    ghost.classList.remove('sd-stop')

    newUnitNumber = Math.round(newUnitNumber)

    if (direction === 'right') {
      if (newUnitNumber > 12 || newUnitNumber < 1) {
        ghost.classList.add('sd-stop')
      }

      let nextO = overed.nextSibling
      if (nextO && nextO.nodeName === '#text') {
        nextO = nextO.nextSibling
      }
      const oldUnitNumber = this.strManager.getColumnSizeFromString(overed.className)
      const unitDiff = newUnitNumber - oldUnitNumber

      if (newUnitNumber < 13 && newUnitNumber >= 1) {
        if (nextO) {
          const nextoOffsetNumber = this.strManager.getColumnOffsetFromString(nextO.className)
          if (nextoOffsetNumber !== 0 && newUnitNumber > oldUnitNumber) {
            this.replaceClass(nextO, 'offset', this.responsiveState + '-offset-' + (nextoOffsetNumber - unitDiff))
          }
        }
        newClass = this.responsiveState + '-' + newUnitNumber
        this.replaceClass(overed, this.responsiveState, newClass)
      }
    } else { //left
      let prev = overed.previousSibling
      if (prev && prev.nodeName === '#text') {
        prev = prev.previousSibling
      }
      const oldUnitNumber = this.strManager.getColumnSizeFromString(overed.className)
      const oldOffsetNumber = this.strManager.getColumnOffsetFromString(overed.className)

      if (newUnitNumber > 12 || newUnitNumber <= 1) {
        ghost.classList.add('sd-stop')
      }
      if (newUnitNumber < 13 && newUnitNumber >= 1) {
        newClass = this.responsiveState + '-' + newUnitNumber
        
        const unitDiff = oldUnitNumber - newUnitNumber
        const newOffset = oldOffsetNumber + unitDiff

        if (newOffset < 0) {
          if (prev) {
            const prevUnitNumber = this.strManager.getColumnSizeFromString(prev.className)
            if (prevUnitNumber > 1) {
              this.replaceClass(overed, this.responsiveState, newClass)
              this.replaceClass(prev, this.responsiveState, this.responsiveState + '-' + (prevUnitNumber - (newUnitNumber - oldUnitNumber)))     
            } else {
              ghost.classList.add('sd-stop')
            }
          } else {
            ghost.classList.add('sd-stop')
          }
        } else {
          this.replaceClass(overed, this.responsiveState, newClass) 
          this.replaceClass(overed, 'offset', this.responsiveState + '-offset-' + newOffset)
        }
      }
    }

    window.PubSub.publish('readyToSaveStructure')
  }

  replaceClass (elem, type, newClass) {
    let regexOffset
    let regex
    
    switch (this.responsiveState) {
      case 'large':
        regexOffset = /large-offset-(\d*)/
        regex = /large-(\d*)/
        break
      case 'medium':
        regexOffset = /medium-offset-(\d*)/
        regex = /medium-(\d*)/
        break
      case 'small':
        regexOffset = /small-offset-(\d*)/
        regex = /small-(\d*)/
        break
      default:
        regexOffset = /large-offset-(\d*)/
        regex = /large-(\d*)/
    }

    let classes = elem.className

    if (type !== 'offset') {
      classes = classes.replace(this.responsiveState + '-offset-', '')
    }

    let rg = (type === 'offset') ? regexOffset : regex
    let match = classes.match(rg)
    if (match) {
      elem.classList.remove(match[0].trim())
    }
    elem.classList.add(newClass)
  }

  onStopResize (data) {
    const currentSize = this.ref.getBoundingClientRect()

    this.lastWidth = null
    let style = this.style
    style.top = currentSize.top + 'px'
    style.left = currentSize.left + 'px'
    style.width = currentSize.width + 'px'

    /*let rmZeroOffset = document.getElementsByClassName(this.responsiveState + '-offset-0')
    if (rmZeroOffset && rmZeroOffset[0]) {
      rmZeroOffset[0].classList.remove(this.responsiveState + '-offset-0')
    }*/

    this.addEventListener('click', this.onSelect)
  }

  isResizing () {
    return this.resizable.resizePanding
  }

  handleDragStart (e) {
    const item = this.ref
    let componentRef = item.dataset.ref

    item.classList.add('sd-hiddenForDrag')

    const obj = {
      item: item,
      ref: componentRef,
      nodeName: item.nodeName,
      isMove: true
    }
    this.currentDrag = obj

    this.ghostMove = document.createElement('img')
    this.ghostMove.src = `./framework/components/${componentRef}/icn.png`
    e.dataTransfer.setDragImage(this.ghostMove, 0, 0)
    window.setTimeout(() => {
      this.style.top = '-5000px'
    }, 0)

    if (item.childNodes.length === 0) {
      item.parentNode.classList.add('sd-emptyForDrag')
    }
    PubSub.publish('component_drag_start', obj)
  }

  handleDragStop (e) {
    if (e.dataTransfer.dropEffect === 'none') {
      PubSub.publish('component_drag_cancel', this.currentDrag)
    } else {
      PubSub.publish('component_drag_stop', this.currentDrag)
    }
    this.ghostMove = null
    this.currentDrag = null
  }

  checkWorkspaceScroll (e) {
    let y = e.clientY
    let scrollSpeed = 2
    let workspace = document.querySelector('.sd_builder_workspace')
    if (y > window.innerHeight - 20) {
      if (workspace.offsetHeight + workspace.scrollTop !== workspace.scrollHeight) {
        PubSub.publish('workspaceScroll')
        scrollSpeed = (y > window.innerHeight - 10) ? 5 : 2
        workspace.scrollTop = workspace.scrollTop + scrollSpeed
      }
    }
    if (workspace.scrollTop > 0 && y < 70) {
      if (workspace.offsetHeight + workspace.scrollTop !== workspace.scrollHeight) {
        PubSub.publish('workspaceScroll')
        scrollSpeed = (y < 60) ? 5 : 2
        workspace.scrollTop = workspace.scrollTop - scrollSpeed
      }
    }
  }
}

window.customElements.define('sd-actionzone', ActionZone)
