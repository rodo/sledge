import render, {renderBase} from './template'

export default class textEditor extends HTMLElement {
  constructor () {
    super()
  }
  connectedCallback () {
  }

  init (params, props, place, setText) {
    this.classList.add('sd-text-editor')
    this.ref = params.id
    console.info("pop", params)
    this.saveTextHandler = setText
    this.params = params
    this.place = place
    this.props = props
    renderBase(this, this.dataset.button)
    window.requestAnimationFrame(() => {
      this.querySelector('button.sd-lte-open').addEventListener('click', e => {
        this.open()
      })
    })
    this.place.querySelector('.sd-lte-close').addEventListener('click', e => {
      this.place.classList.remove('visible')
    })
  }

  open () {
    this.place.classList.add('visible')
    render(this, this.params, this.props, this.place.querySelector('.sd-but-catalog-list'))
  }

  saveText (txt) {
    console.info("longTextEdit", this.ref)
    this.saveTextHandler(txt, this.ref)
  }

}

window.customElements.define('sd-txt-editor', textEditor)
