import {connect} from '../../store'
import _conf from '../../config'
import render from './template'
import {checkWorkspaceScroll} from '../../views/builder/builder'

export default class ComponentsList extends HTMLElement {

  connectedCallback () {
    connect('projectConf', this)
    let loc = window.sledge.nslocal
    this.classList.add('sd_componentList')
    this.conf = {}
    this.conf.list = []
    this.conf.name = loc.cl_title

    this.currentList =
    (this.projectConf.components && this.projectConf.components.length !== 0)
    ? this.projectConf.components
    : _conf.defaultComponentsList

    this.currentList.forEach((compName) => {
      let data = require(`../../../framework/components/${compName}/package.json`)
      this.conf.list.push({
        name: data.name[window.sledge.languageCode],
        about: data.description[window.sledge.languageCode],
        nodeName: data.nodeName,
        ref: data.ref
      })
    })
    this.conf.list.sort((a, b) => {
      return a.name.localeCompare(b.name)
    })
    render(this.conf, this)
    const ulNode = this.getElementsByTagName('ul')[0]
    const handleDragStart = e => {
      if (e.target && e.target.nodeName === 'LI') {
        const item = e.target
        const obj = {
          item: item,
          ref: item.dataset.ref,
          nodeName: item.dataset.node
        }
        this.currentDrag = obj

            // create drag ghost
        let ghost = document.createElement('img')
        //ghost.src = item.style.backgroundImage.replace('url(', '').replace(')', '')
        ghost.src = './styles/img/icons8-led_diode_filled.png'
        /*ghost.width = '100'
        let div = document.createElement('div')
        div.appendChild(ghost)
        document.querySelector('body').appendChild(div)*/
        e.dataTransfer.setDragImage(ghost, 0, 0)
        //e.dataTransfer.setDragImage(div, -10, -10)

        PubSub.publish('component_drag_start', obj)
      }
    }

    const handleDragStop = e => {
      if (e.dataTransfer.dropEffect === 'none') {
        PubSub.publish('component_drag_cancel', this.currentDrag)
      } else {
        PubSub.publish('component_drag_stop', this.currentDrag)
      }
      this.currentDrag = null
    }

    ulNode.addEventListener('dragstart', handleDragStart, false)
    ulNode.addEventListener('dragend', handleDragStop, false)
    ulNode.addEventListener('drag', checkWorkspaceScroll, false)

    PubSub.subscribe('workspaceDragEnter', () => {
      if (this.currentDrag) {
        PubSub.publish('dockMenuCloseCurrent')
      }
    })
  }

  addItem (item) {
    const ref = item.ref
    const name = item.name[window.sledge.languageCode]
    const about = item.description[window.sledge.languageCode]
    const nodeName = item.nodeName

    this.conf.list.push({ref, name, about, nodeName})
  }
}

window.customElements.define('sd-componentlist', ComponentsList)
