export default (params, root) => {
  root.innerHTML = `<div class="sd_dockmenu_top">${params.name}</div>
    <div class="sd_dockmenu_content">
    <ul>
      ${
        params.list.map(item => {
          return `<li style="background-image:url(framework/components/${item.ref}/img/cover.png);" draggable="true" data-node="${item.nodeName}" data-ref="${item.ref}" class="sd_componentList_item">
            <div class="sd_componentList_item_title">${item.name}</div>
            <div class="sd_componentList_item_desc">${item.about}</div>
          </li>`
        }).join('')
      }
    </ul>
  </div>`
}
