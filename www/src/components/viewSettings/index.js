import {connect, store} from '../../store'
import render from './template'
import {updatePageProp, setViewParam, browseFile, getMedias} from '../../interface/interface_mac'

export default class Settings extends HTMLElement {

  connectedCallback () {
    connect('projectConf', this)
    connect('medias', this)
    connect('currentProject', this)
    connect('sideMenuCurrentItem', this, v => {
      this.renderAll()
    })

    let loc = window.sledge.nslocal
    this.renderAll()
    this.addEventListener('click', e => this.update(e))
  }

  renderAll () {
    this.value = this.sideMenuCurrentItem.settings || {}
    render(this, this.value, this.medias, {}, this.sideMenuCurrentItem.type)
  }

  openCatalog (id) {
    this.currentOpen = this.querySelector('.sd-but-catalog')
    this.currentOpen.classList.add('visible')
  }

  openPatternCatalog (id) {
    this.currentOpen = this.querySelector('.sd-but-patterns')
    this.currentOpen.classList.add('visible')
  }

  openImgCatalog (id) {
    this.currentOpen = this.querySelector('.sd-but-img')
    this.currentOpen.classList.add('visible')
  }

  closeCatalog () {
    this.currentOpen.classList.remove('visible')
  }

  update (e) {
    switch (e.target.dataset.kind) {
      case 'displaymode':
        let v = e.target[e.target.selectedIndex].value
        if (v === 'default') {
          delete this.value.displayType
        } else {
          this.value.displayType = v
        }
        break
      case 'scheme':
        this.value.color = e.target.dataset.value
        this.setValue()
        break
      case 'img':
        this.openImgCatalog()
        break
      case 'imgclose':
        this.closeCatalog()
        break
      case 'cat':
        this.openCatalog()
        if (e.target.dataset.logo) {
          $('.sd-prop-list-prev').css('display', 'block')
          $(`.sd-but-catalog-list div[data-name="${e.target.dataset.logo}"]`).css('display', 'none')
        }
        break
      case 'pat':
        this.openPatternCatalog()
        break
      case 'browse':
        browseFile(name => this.setBackgroundImage(name), false, true)
        break
      case 'rmColor':
        this.value.color = null
        this.setValue()
        break
      case 'media':
        let name = e.target.dataset.name
        delete this.value.bgimage
        delete this.value.pattern
        delete this.value.displayType
        this.value.backgroundtype = 'media'
        this.value.media = encodeURIComponent(name)
        this.value.numberOfSizes = this.medias[name].numberOfSizes
        this.value.mediaType = this.medias[name].type
        this.closeCatalog()
        this.setValue()
        break
      case 'themeMedia':
        delete this.value.displayType
        delete this.value.media
        delete this.value.numberOfSizes
        delete this.value.mediaType
        this.value.backgroundtype = 'bgimage'
        this.value.bgimage = e.target.dataset.name
        this.closeCatalog()
        this.setValue()        
        break
      case 'pattern':
        delete this.value.bgimage
        delete this.value.displayType
        delete this.value.media
        delete this.value.numberOfSizes
        delete this.value.mediaType
        this.value.backgroundtype = 'pattern'
        this.value.pattern = e.target.dataset.name
        this.closeCatalog()
        this.setValue()
        break
      case 'rmMedia':
        this.removeBackgroundImage()
        break
      default:
      //
    }
  }

  setValue () {
    if (this.sideMenuCurrentItem.type === 'page') {
      updatePageProp(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, settings: this.value})
    } else {
      setViewParam(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, settings: this.value},
        data => {
          store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {...this.sideMenuCurrentItem, settings: this.value, _rev: data.rev}})
        }
      )
    }
  }

  setBackgroundColor (color) {
    this.value.color = color
  }

  setBackgroundImage (data) {
    getMedias(this.currentProject, () => {
      delete this.value.pattern
      delete this.value.displayType
      delete this.value.bgimage
      this.value.backgroundtype = 'media'
      this.value.media = encodeURIComponent(data.name)
      if (data.numberOfSizes) {
        this.value.numberOfSizes = data.numberOfSizes
      }
      this.value.mediaType = data.type
      this.setValue()      
    })
  }

  removeBackgroundImage () {
    delete this.value.pattern
    delete this.value.media
    delete this.value.bgimage
    delete this.value.numberOfSizes
    delete this.value.mediaType
    delete this.value.backgroundtype
    this.setValue()
  }
}

window.customElements.define('sd-viewsettings', Settings)
