import currentTheme from '../../../defaultTheme/assets/sledge/config.json'

export default (root, param, medias, props, type) => {
  let loc = window.sledge.nslocal
  let style = ''
  let prevClass = ''
  let fileName = null
  let allow =
  props.allow ||
    {
      image: true,
      video: true,
      color: true
    }

  param = param || {}
  if (param.backgroundtype === 'media') {
    fileName = param ? param.media : null
    if (fileName) {
      if (param.numberOfSizes !== 1) {
        fileName = param.media + '_small'
      }
      style = `background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${fileName}.${param.mediaType}); `
    }
  }

  if (param.backgroundtype === 'pattern') {
    prevClass = param.pattern
  }

  if (param.backgroundtype === 'bgimage') {
    prevClass = param.bgimage
  }

  let attachements = []
  if (Object.keys(medias).length !== 0) {
    for (let fileName2 in medias) {
      attachements.push({...medias[fileName2], name: fileName2})
    }
  }
  let bgcolor = '#ffffff'
  if (param.color) {
    bgcolor = param.color
  }

  param = param || {}
  let markup = `
  
  ${allow.color
    ? `<label>${loc.prop_back_color}</label>
      <sd-color-selector data-kind="colorScheme"></sd-color-selector>`
    : ''}`

    if (allow.image) {
    markup += `<label>${loc.prop_back_img}</label>
    ${
      prevClass !== '' || fileName !== null
      ? `<div class="sd-prop-preview-block">
        <div class="sd-prop-preview-block-buttons">
          <button data-kind="rmMedia" class="sd-prop-remove-media">${loc.prop_back_rm}</button>
        </div>
        <div class="sd-prop-preview ${prevClass}" style="${style}"></div>
      </div>`
      : ''
    }
    <div class="sd-prop-buttons-cont">
      <button data-kind="browse" class="sd-prop-browse-media">${loc.prop_back_find}</button>
      <button data-kind="cat" data-logo="${param.logo}">${loc.prop_choose_cat}</button>
      <button data-kind="pat">${loc.prop_back_pat}</button>
      <button data-kind="img">${loc.prop_back_timg}</button>
    </div>`
  }

  let markupFull = `
  ${
    type === 'page'
    ? ``
    : `<div class="sd_dockmenu_top">${loc.settings_view}</div>`
  }
  <div class="sd_dockmenu_content">
  <div class="sd-but-catalog">
    <button class="sd-but-catalog-close" data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getMediaList(medias)}
    </div>
  </div>
  <div class="sd-but-patterns">
    <button data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getThemeMediaPattern(param)}
    </div>
  </div>  
  <div class="sd-but-img">
    <button data-kind="imgclose"></button>
    <div class="sd-but-catalog-list">
      ${getThemeMediaImage(param)}
    </div>
  </div>    
  <div class="sd-but-cont">${markup}</div>`

  root.innerHTML = markupFull + '</div>'
}


function getMediaList (list) {
  let attachements = []
  let supportedType = ['png', 'jpg']
  if (Object.keys(list).length !== 0) {
    for (let fileName2 in list) {
      if (supportedType.indexOf(list[fileName2].type) !== -1) {
        attachements.push({...list[fileName2], name: fileName2})
      }
    }
  }
  let markup = ''
  let noItem = true
  if (attachements.length === 0) {
    return ''
  }
  attachements.forEach(obj => {
    if (obj.numberOfSizes) {
      if (obj.numberOfSizes !== 1) {
        noItem = false
        markup += `<div data-kind="media" class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}_small.${obj.type})"></div>`
      } else {
        noItem = false
        markup += `<div data-kind="media" class="sd-prop-list-prev" data-name="${obj.name}" style="background-image:url(${$sledge.conf.dbServerUrl}/${$sledge.currentdb}/media/${obj.name}.${obj.type})"></div>`
      }
    }
  })
  if (noItem) {
    return ''
  }
  return markup
}

function getThemeMediaPattern () {
  let theme = window.sledge.currentTheme === 'default' ? currentTheme : window.sledge.themeConfig

  let markup = `<div class="sd-prop-library">`
  if (!theme.pattern) {
    return ''
  }
  for (let item in theme.pattern) {
    markup += `<div data-kind="pattern" class="sd-prop-list-theme ${theme.pattern[item]}" data-name="${theme.pattern[item]}"><span>${item}</span></div>`
  }
  markup += '</div>'
  return markup
}

function getThemeMediaImage () {
  let theme = window.sledge.currentTheme === 'default' ? currentTheme : sledge.themeConfig
  let markup = `<div class="sd-prop-library">`
  if (!theme.bgimage) {
    return ''
  }
  for (let item in theme.bgimage) {
    markup += `<div data-kind="themeMedia" class="sd-prop-list-theme-img ${theme.bgimage[item]}" data-name="${theme.bgimage[item]}"><span>${item}</span></div>`
  }
  markup += '</div>'
  return markup
}

