export default (sender, params) => {
  let loc = window.sledge.nslocal

  sender.innerHTML = `<div class="sd_pp_dragHandle">
    ${loc.prop_pan_title}
    <button></button>
  </div>
  <ul class="sd_pp_menu"></ul>
  <div class="sd_pp_content"></div>`
}
