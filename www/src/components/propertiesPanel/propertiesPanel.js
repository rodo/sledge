import {connect, store} from '../../store'
import render from './template.js'
import {setViewParam, getViews} from '../../interface/interface_mac'

export default class PropertiesPanel extends HTMLElement {

  connectedCallback () {
    this.currentProperties = {}
    this.currentPropIndex = null
    connect('currentProject', this)
    connect('currentEditedProperties', this, s => this.display(s))
    connect('sideMenuCurrentItem', this, obj => {
      getViews()
      this.currentProperties = (obj && obj.properties) ? obj.properties : {}
      if (this.currentPropIndex !== null && this.properties && this.properties[this.currentPropIndex]) {
        this.displayProperty(this.properties[this.currentPropIndex])
      }
    })
    connect('currentSelectedComponent', this, id => { this.selectedComponent = document.getElementById(id) })
    this.classList.add('sd_propertiesPanel')
    render(this)
    window.requestAnimationFrame(() => {
      this.menu = this.querySelector('.sd_pp_menu')
      this.menu.addEventListener('click', e => this.menuAction(e))
      this.content = this.querySelector('.sd_pp_content')

      this.content.addEventListener('mousewheel', e => {
        e.stopPropagation()
      })

      this.querySelector('.sd_pp_dragHandle button').addEventListener('click', (e) => this.hide())
      const $cont = $(this)
      $cont.draggable({handle: '.sd_pp_dragHandle', containment: 'window'})
      $cont.resizable()
    })
    
    window.PubSub.subscribe('LANGUAGE_UPDATE', (a, e) => {
      if (this.currentPropIndex !== null && this.properties && this.properties[this.currentPropIndex]) {
        this.displayProperty(this.properties[this.currentPropIndex])
      }
    })
  
    window.PubSub.subscribe('hideProperties', () => this.hide())
  }

  display (selector) {
    let isSelf = false
    if (!selector) {
      this.hide()
    }
    if (selector === 'self') {
      isSelf = true
    }
    const parentElement = document.getElementById(this.currentSelectedComponent)

    if (!parentElement) {
      return
    }
    if (parentElement && parentElement.propertiesApply && !parentElement.propertiesApply[selector]) {
      return
    }
    this.properties = parentElement.propertiesApply[selector]

    if (this.properties) {
      if (this.properties.length > 1) {
        this.createMenu(this.properties)
      } else {
        this.removeMenu()
      }

      this.currentPropIndex = 0
      this.displayProperty(this.properties[0])

      const ghost = isSelf ? parentElement : document.querySelector('.sd-prop-ghost')
      this.classList.add('sd-visible')

      const pos = ghost.getBoundingClientRect()
      const panelSizes = this.getBoundingClientRect()
      const pH = panelSizes.height
      const pW = panelSizes.width
      if ((pW + pos.left + 300) > window.innerWidth) {
        this.style.left = pos.left - pW + 'px'
      } else {
        this.style.left = pos.left + 'px'
      }
      if ((pH + pos.top) > window.innerHeight) {
        this.style.left = window.top - pH + 'px'
      } else {
        this.style.top = pos.top + 'px'
      }
      
    }
  }

  displayProperty (prop) {
    if (!this.currentSelectedComponent) {
      return
    }
    if (document.activeElement.nodeName === 'INPUT' ||
      document.activeElement.classList.contains('ql-editor') ||
      window.sledge.donorefresh
    ) {
      window.sledge.donorefresh = false
      return
    }
    let propValues = null
    this.content.innerHTML = ''
    const properties = (this.sideMenuCurrentItem && this.sideMenuCurrentItem.properties) ? this.sideMenuCurrentItem.properties : null
    if (properties && properties[this.currentSelectedComponent] && prop.key) {
      propValues = properties[this.currentSelectedComponent][prop.key] || null
    }
    let elem = document.createElement(`sd-properties-${prop.type}`)
    this.content.appendChild(elem)
    if (properties && properties[this.currentSelectedComponent]) {
      elem.init(this, prop, propValues, properties[this.currentSelectedComponent])
    } else {
      elem.init(this, prop, propValues, {})
    }
  }

  applyProperty (key, value, all) {
    if (this.applyTm) {
      window.clearTimeout(this.applyTm)
    }
    this.applyTm = window.setTimeout(() => {
      if (all) {
        this.selectedComponent.refreshProps()
      } else {
        this.selectedComponent.applyProperty(key, value)
      }
      this.applyTm = null
    }, 300)
  }

  setPropertyValue (key, value) {
    if (this.saveTm) {
      window.clearTimeout(this.saveTm)
    }
    this.saveTm = window.setTimeout(() => {
      if (!this.currentProperties[this.currentSelectedComponent]) {
        this.currentProperties[this.currentSelectedComponent] = {}
      }
      this.currentProperties[this.currentSelectedComponent][key] = value
      setViewParam(this.currentProject, this.sideMenuCurrentItem._id, {...this.sideMenuCurrentItem, properties: this.currentProperties},
        data => {
          store.dispatch({type: 'CURRENT_SIDE_MENU_ITEM', data: {...this.sideMenuCurrentItem, properties: this.currentProperties, _rev: data.rev}})
        }
      )
      this.saveTm = null
    }, 300)
  }

  createMenu (properties) {
    let markup = ''
    properties.forEach((elem, i) => {
      markup += `<li class='${(i === 0) ? 'sd_active' : ''}' data-index='${i}'>${elem.name[window.sledge.languageCode]}</li>`
    })
    this.classList.add('sd_prop_multi')
    this.menu.innerHTML = markup
  }

  menuAction (e) {
    if (!e.target.classList.contains('sd_active')) {
      this.menu.querySelector('.sd_active').classList.remove('sd_active')
      e.target.classList.add('sd_active')
      this.currentPropIndex = parseInt(e.target.dataset.index, 10)
      this.displayProperty(this.properties[this.currentPropIndex])
    }
  }

  removeMenu () {
    this.classList.remove('sd_prop_multi')
    this.menu.innerHTML = ''
  }

  hide () {
    this.content.innerHTML = ''
    this.classList.remove('sd-visible')
    this.style.left = '100px'
    this.style.top = '100px'
    store.dispatch({ type: 'SET_EDITED_PROPERTIES', value: null })
  }

  init (items) {
          //
  }

}

window.customElements.define('sd-propertiespanel', PropertiesPanel)
