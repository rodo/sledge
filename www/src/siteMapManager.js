import {connect} from './store'

export default class ProjectManager {
  constructor () {
    connect('projectConf', this)
    connect('pages', this)
    connect('rootPages', this)
    this.currentMap = []
  }

  getMap (language) {
    if (!this.projectConf.siteMap) {
      let sortedPages = this.rootPages.slice(0)
      sortedPages.sort((a, b) => {
        return a.name.localeCompare(b.name)
      })

      sortedPages = sortedPages.map(page => {
        return this.handlePage({id: page._id, kind: 'page'}, language)
      })
      return sortedPages
    } else {
      let rootMap = window.JSON.parse(this.projectConf.siteMap)
      let map = rootMap.map(obj => {
        if (obj.kind === 'page') {
          if (this.pages[obj.id]) {
            return this.handlePage(obj, language)
          } else {
            return false
          }
        } else {
          return this.explore(obj, language)
        }
      })
      return map
    }
  }

  explore (obj, language) {
    if (!obj.children || obj.children.lenght === 0) {
      return {}
    } else {
      let tree = {}
      tree.kind = 'menu'
      tree.name = obj.name
      tree.id = obj.id
      tree.localName = (obj.local && obj.local[language]) ? window.decodeURIComponent(obj.local[language]) : tree.name
      tree.children = []
      obj.children.forEach(obj => {
        if (obj.kind === 'page') {
          tree.children.push(this.handlePage(obj, language))
        } else {
          tree.children.push(this.explore(obj, language))
        }
      })
      return tree
    }
  }

  handlePage (obj, language) {
    let name = this.pages[obj.id].name
    let localName = this.pages[obj.id].name
    if (this.pages[obj.id].local && this.pages[obj.id].local[language]) {
      localName = window.decodeURIComponent(this.pages[obj.id].local[language])
    }
    return {name: name, localName: localName, id: obj.id, kind: 'page'}
  }
}
