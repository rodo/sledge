define(function () {
	"use strict";

	var obj = {};
	
	obj.create = function(base) {

		base = function(data) {
			this.init(data
				);
		}

		base.create = function(data) {
        	var result = new base(data);
        	return result;
		}

		return base;
	}

	obj.getPrototype = function(Class) {
 		var proto = Class.prototype;
 		proto.constructor = Class;
		return proto;	
	}
	
    return obj;
});