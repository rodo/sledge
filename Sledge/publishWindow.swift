//
//  publishWindow.swift
//  Sledge
//
//  Created by Rodolphe DUPERRAY on 11/07/2017.
//  Copyright © 2017 Rodolphe DUPERRAY. All rights reserved.
//

import Cocoa
import WebKit

class publishWindow: NSWindowController, WebFrameLoadDelegate {

    @IBOutlet weak var visiblePath: NSPathControl!
    @IBOutlet var webview: WebView!
    //@IBOutlet weak var progressBar: NSProgressIndicator!
    @IBOutlet weak var spinner: NSProgressIndicator!
    @IBOutlet weak var mainLabel: NSTextFieldCell!
    var localPublishURL: URL!
    
    @IBAction func setCurrentView(send: NSComboBox) {
        let key:NSString = ""
        var _:Int = 0

        webview?.stringByEvaluatingJavaScript(from: "setCurrent('\(key)')")
    }

    var appDelegate: AppDelegate  = (NSApplication.shared.delegate as? AppDelegate)!
    var pageProgressValue:Double = 0.0
    var currentViews:[NSDictionary] = []
    var currentPages:[NSDictionary] = []
    
    override func windowDidLoad() {
        super.windowDidLoad()
        let filePath = Bundle.main.path(forResource: "www/publish", ofType: "html")
        let url = URL(string:filePath!)
        
        self.webview?.frameLoadDelegate = self;
        self.webview?.mainFrame.load(URLRequest(url: url!))
        
        spinner.startAnimation(self)
        localPublishURL = UserDefaults.standard.url(forKey: "localPublishPath")!
        visiblePath.url = localPublishURL
    }

    override func showWindow(_ sender: Any?) {
        super.showWindow(sender)
        let filePath = Bundle.main.path(forResource: "www/publish", ofType: "html")
        let url = URL(string:filePath!)
        self.webview?.mainFrame.load(URLRequest(url: url!))
        //webview?.stringByEvaluatingJavaScript(from: "reloadPreview()")
    }
    
    override class func webScriptName(for aSelector: Selector) -> String? {
        var sel:NSString
        sel = ""
        
        if aSelector == #selector(publishWindow.getAllViews) {
            sel = "getAllViews"
        }
        
        if aSelector == #selector(publishWindow.getAllPages) {
            sel = "getAllPages"
        }
        
        if aSelector == #selector(publishWindow.setViewComboValue) {
            sel = "setViewComboValue"
        }
        if aSelector == #selector(publishWindow.initPublish) {
            sel = "initPublish"
        }
        if aSelector == #selector(publishWindow.saveImages) {
            sel = "saveImages"
        }
        if aSelector == #selector(publishWindow.publishPage) {
            sel = "publishPage"
        }
        
        if sel == "" {
            return nil
        } else {
            return sel as String;
        }
    }
    
    override class func isSelectorExcluded(fromWebScript aSelector: Selector) -> Bool
    {
        return false
    }
    
    func webView(_ webView: WebView!,didClearWindowObject windowObject: WebScriptObject!, for frame: WebFrame!) {
        windowObject.setValue(self, forKey: "native");
    }

    
    @objc func initPublish(name:String, themeName:String) {
        localPublishURL = UserDefaults.standard.url(forKey: "localPublishPath")!
        visiblePath.url = localPublishURL
        
        let publishPath = localPublishURL.appendingPathComponent(name)
        if FileManager.default.fileExists(atPath: publishPath.path) {
            self.copyThemeDirectory(name: themeName, projName:name)
            /*do {
                //try FileManager.default.removeItem(atPath: publishPath.path)
            } catch let error as NSError {
                print("unable to delete directory: \(error.localizedDescription)")
            }*/
        } else {
            do {
                try FileManager.default.createDirectory(at: publishPath, withIntermediateDirectories: false, attributes: nil)
                self.copyThemeDirectory(name: themeName, projName:name)
            } catch let error as NSError{
                print("unable to create directory \(error)")
            }
        }
        
        
    }
    
    func removeAtPath(path:String) {
        if FileManager.default.fileExists(atPath: path) {
            do {
                try FileManager.default.removeItem(atPath: path)
            } catch let error as NSError {
                print("unable to delete item: \(error.localizedDescription)")
            }
        }
    }
    
    func clearDirectory(projectPath:URL) {
        let fileManager = FileManager.default
        let fileUrls = fileManager.enumerator(at: projectPath, includingPropertiesForKeys: nil)
        while let fileUrl = fileUrls?.nextObject() {
            do {
                try fileManager.removeItem(at: fileUrl as! URL)
            } catch {
                print(error)
            }
        }
    }
    
    @objc func publishPage(projname:String, name:String, markup:String, status:String, language:String, files:String) {

        var path = localPublishURL.appendingPathComponent(projname)

        let JSONData = files.data(using: .utf8)
        let parsed = try? JSONSerialization.jsonObject(with: JSONData!, options: .mutableContainers) as? NSArray
        
        if parsed??.count != 0 {
            let libPath = path.appendingPathComponent("componentsLib")
            if !FileManager.default.fileExists(atPath: libPath.path) {
                try? FileManager.default.createDirectory(at: libPath, withIntermediateDirectories: false, attributes: nil)
            }
            parsed??.forEach {filePath in
                let newFilePath = libPath.path + "/" + (filePath as! String)
                self.removeAtPath(path: newFilePath)
                if !FileManager.default.fileExists(atPath: newFilePath) {
                    let originalFilePath  = Bundle.main.resourcePath! + "/WWW/framework/components/" + (filePath as! String)
                    
                    let comps = (filePath as! String).split(separator: "/")
                    var temp:URL = libPath
                    comps.forEach{item in
                        temp = temp.appendingPathComponent(String(item))
                        if (temp.pathExtension == "") {
                            try? FileManager.default.createDirectory(at: temp, withIntermediateDirectories: false, attributes: nil)
                        }
                    }
                    try? FileManager.default.copyItem(at: URL(fileURLWithPath: originalFilePath, isDirectory: false), to: URL(fileURLWithPath: newFilePath, isDirectory: false))
                }
            }
        }
        
        if language != "default" {
            path = path.appendingPathComponent(language)
            if !FileManager.default.fileExists(atPath: path.path) {
                try? FileManager.default.createDirectory(at: path, withIntermediateDirectories: false, attributes: nil)
            }
        }
        if status == "root" {
         self.writeFile(markup: markup, to: "index.html", folderPath: path)
        } else {
         self.writeFile(markup: markup, to: "\(name).html", folderPath: path)
        }
        if name != "sg-index-lang" {
            webview?.stringByEvaluatingJavaScript(from: "nextPage()")
        }
    }
    
    func writeFile (markup: String, to fileNamed: String, folderPath: URL) {
        let file = folderPath.appendingPathComponent(fileNamed)
        do {
            try markup.write(to: file, atomically: false, encoding: String.Encoding.utf8)
        } catch let error as NSError{
            print("unable to write page \(error)")
        }
        
    }
    
    @objc func saveImages (ar:String, proj:String, favicon:String, sitemap:String, siteMapPath:String) {
        let JSONData = ar.data(using: .utf8)
        let faiviconData = favicon.data(using: .utf8)
        var list = [NSDictionary]()
        var faviconList = [String]()
        
        
        if (sitemap != "") {
            let path = localPublishURL.appendingPathComponent(proj)
            self.writeFile(markup: sitemap, to: "sitemap.xml", folderPath: path)
            self.writeFile(markup: siteMapPath, to: "robots.txt", folderPath: path)
        }   
        
        do {
            let parsed = try JSONSerialization.jsonObject(with: JSONData!, options: .mutableContainers) as? NSArray
            for obj in parsed! {
                list.append(obj as! NSDictionary)
            }
            
            let faviconParsed = try JSONSerialization.jsonObject(with: faiviconData!, options: .mutableContainers) as? NSArray
            for fav in faviconParsed! {
                faviconList.append(fav as! String)
            }
            
            let saveURL = localPublishURL.appendingPathComponent(proj).appendingPathComponent("media")
            if !FileManager.default.fileExists(atPath: saveURL.path) {
                try FileManager.default.createDirectory(at: saveURL, withIntermediateDirectories: false, attributes: nil)
            }
            _ = self.appDelegate.mainView.saveImageToDisk(data: list, url: saveURL)
            _ = self.appDelegate.mainView.saveFaviconToDisk(data: faviconList, url: saveURL)
            self.close()
            self.appDelegate.mainView.closePublish()
            
            NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: localPublishURL.appendingPathComponent(proj).path)
            NSWorkspace.shared.open(
                [localPublishURL.appendingPathComponent(proj).appendingPathComponent("index.html")],
                withAppBundleIdentifier: "com.apple.safari",
                options: NSWorkspace.LaunchOptions.default,
                additionalEventParamDescriptor: nil,
                launchIdentifiers: nil
            )
        } catch let parseError {
            print(parseError)
        }
        
    }

    
    func copyThemeDirectory(name:String, projName:String) {
        
        var pathTheme = ""
        
        if name == "default" {
            pathTheme  = Bundle.main.resourcePath! + "/WWW/defaultTheme"
        } else {
            let pathDoc = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: pathDoc)
            let fullFilePath = url.appendingPathComponent("sledgeTheme")
            let pathURl = fullFilePath?.appendingPathComponent(projName)
            pathTheme = (pathURl?.appendingPathComponent(name).path)!
        }
        
        let pathFmDist  = Bundle.main.resourcePath! + "/WWW/framework/dist"
        let pathFmComp  = Bundle.main.resourcePath! + "/WWW/framework/components" //build css !
        let pathFmStyle  = Bundle.main.resourcePath! + "/WWW/framework/style.css"
        let pathJq  = Bundle.main.resourcePath! + "/WWW/node_modules/jquery/dist"
        
        self.removeAtPath(path: localPublishURL.appendingPathComponent(projName).appendingPathComponent("framework").path)
        self.removeAtPath(path: localPublishURL.appendingPathComponent(projName).appendingPathComponent("theme").path)
        self.removeAtPath(path: localPublishURL.appendingPathComponent(projName).appendingPathComponent("lib").path)
        
        do {
            try FileManager.default.copyItem(at: URL(fileURLWithPath: pathTheme, isDirectory: true), to: localPublishURL.appendingPathComponent(projName).appendingPathComponent("theme"))
            
            try FileManager.default.createDirectory(at: localPublishURL.appendingPathComponent(projName).appendingPathComponent("framework"), withIntermediateDirectories: false, attributes: nil)
            
            try FileManager.default.copyItem(at: URL(fileURLWithPath: pathFmDist, isDirectory: true), to: localPublishURL.appendingPathComponent(projName).appendingPathComponent("framework").appendingPathComponent("dist"))
            
            try FileManager.default.copyItem(at: URL(fileURLWithPath: pathFmStyle, isDirectory: false), to: localPublishURL.appendingPathComponent(projName).appendingPathComponent("framework").appendingPathComponent("style.css"))
            
            let publishPath = localPublishURL.appendingPathComponent(projName).appendingPathComponent("lib")
            
            try FileManager.default.createDirectory(at: publishPath, withIntermediateDirectories: false, attributes: nil)
            
            try FileManager.default.copyItem(at: URL(fileURLWithPath: pathJq, isDirectory: true), to: localPublishURL.appendingPathComponent(projName).appendingPathComponent("lib").appendingPathComponent("jquery"))
            
            webview?.stringByEvaluatingJavaScript(from: "publishReady()")
        
        } catch let error as NSError {
            print("unable to copy theme \(error)")
        }
        
    }
    
    @objc func getAllViews() {
        
        let viewsList:[NSDictionary] = self.appDelegate.mainView.getAllViewsFromCurrent()
        currentViews = viewsList
        
        /*if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "view") {
            combo.removeAllItems()
            for item in viewsList {
                combo.addItem(withObjectValue: item.value(forKey: "name")!)
            }
        }*/
        
        do {
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            
            webview?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')")
        } catch _ {
            print("JSON string error")
        }
        
    }
    
    @objc func setViewComboValue(idx:Int) {
        //combo.selectItem(at: idx)
    }
    
    @objc func getAllPages() {
        
        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "view") {
            //self.window?.title = "Views Preview"
        }
        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "page") {
            //self.window?.title = "Pages Preview"
        }
        
        let viewsList:[NSDictionary]? = self.appDelegate.mainView.getAllPagesFromCurrent()
        currentPages = viewsList!
        
        do {
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList!,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            
            webview?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')")
            
        } catch _ {
            print("JSON string error")
        }
    }
    
}
