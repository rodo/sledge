//
//  couch.swift
//  Sledge
//
//  Created by Rodolphe DUPERRAY on 13/12/2017.
//  Copyright © 2017 Rodolphe DUPERRAY. All rights reserved.
//

import Foundation
import Cocoa
import CouchbaseLite
import CouchbaseLiteListener

class Couch {
    
    /*var database: CBLDatabase?
    var projectDatabase: CBLDatabase?
    var manager: CBLManager?
    var listennerPort:UInt16? = nil*/
    
    init() {
    }
    
    func load(caller:mainViewCtrl) {
        caller.manager = CBLManager.sharedInstance()
        print(caller.manager!.internalURL)
        
        do {
            caller.database = try caller.manager!.databaseNamed("sledge_db")
            let listener = CBLListener.init(manager: caller.manager!, port: 0)
            listener.readOnly = false
            try listener.start();
            caller.listennerPort = listener.port
            print(listener.url)
        } catch _ {
            
        }
    }
    
    /*func addNewProject(_ name:String) {
        do {
            self.projectDatabase = try manager!.databaseNamed(name)
        } catch _ {
            
        }
    }

    func createProjectViews() {
        
        let viewsView = self.projectDatabase!.viewNamed("allViews")
        viewsView.setMapBlock({ (doc, emit) in
            
            if doc["type"] as? String == "view" {
                emit(doc, doc["id"])
            }
            
        }, version: "2")
        
        let pagesView = self.projectDatabase!.viewNamed("allPages")
        pagesView.setMapBlock({ (doc, emit) in
            
            if doc["type"] as? String == "page" {
                emit(doc, doc["id"])
            }
            
        }, version: "3")
        
        
    }
    
    func openProject(caller:mainViewCtrl, projName:String?) {
        do {
            self.projectDatabase = try self.manager!.databaseNamed(projName!)
            self.createProjectViews()
            caller.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('{}')")
        } catch _ {
            
        }
    }
    
    func getAllPagesFromCurrent() -> [NSDictionary]! {
        do {
            let query = self.projectDatabase!.viewNamed("allPages").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []
            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }
            return viewsList
            
        } catch _ {
            print("JSON string error")
        }
        return nil
    }
    
    func getAllViews(caller:mainViewCtrl) {
        
        do {
            let query = self.projectDatabase!.viewNamed("allViews").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []
            
            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }
            
            
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            caller.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')");
            
        } catch _ {
            print("JSON string error")
        }
    }
    
    func getAllViewsFromCurrent() -> [NSDictionary]! {
        do {
            let query = self.projectDatabase!.viewNamed("allViews").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []
            
            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }
            
            return viewsList
            
        } catch _ {
            print("JSON string error")
        }
        
        return nil
        
    }
    
    @objc func getAllPages(caller:mainViewCtrl) {
        do {
            let query = self.projectDatabase!.viewNamed("allPages").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []
            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }
            
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            
            caller.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')");
            
        } catch _ {
            print("JSON string error")
        }
    }
    
    func createViews(_ dbname:String) {
        let projectsView = self.database!.viewNamed("projects")
        projectsView.setMapBlock({ (doc, emit) in
            if let projs = doc["projects"] as? [String] {
                for proj in projs {
                    emit(proj, doc["name"])
                }
            }
        }, version: "2")
        
    }
    
    @objc func getAttachmentPath(caller:mainViewCtrl ,fileName: String) {
        let doc = self.projectDatabase?.document(withID: "media")
        let currenrRev = doc?.currentRevision
        let att = currenrRev?.attachmentNamed(fileName)
        let url = att?.contentURL
        
        let jsonObject: [String: AnyObject] = [
            "name": fileName as AnyObject,
            "path": url as AnyObject
        ]
        
        do {
            let theJSONData = try JSONSerialization.data(
                withJSONObject: jsonObject,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText2 = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            caller.webView?.stringByEvaluatingJavaScript(from: "attachmentPath('\(theJSONText2)')")
        } catch _  {
            print("video error")
        }
    }*/
    
}
