//
//  sledgeProducts.swift
//  Sledge
//
//  Created by Rodolphe DUPERRAY on 14/12/2017.
//  Copyright © 2017 Rodolphe DUPERRAY. All rights reserved.
//

import Foundation

public struct SledgeProducts {
    
    public static let SledgePlus = "com.sledge.Sledge.sledgePluss"
    
    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [SledgeProducts.SledgePlus]
    
    public static let store = IAPHelper(productIds: SledgeProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
