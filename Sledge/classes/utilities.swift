    //
    //  test.swift
    //  Sledge
    //
    //  Created by Rodolphe DUPERRAY on 13/12/2017.
    //  Copyright © 2017 Rodolphe DUPERRAY. All rights reserved.
    //

    import Foundation
    import Cocoa

    class Utilities {
        static let sharedInstance = Utilities() //singleton
        
        init() {
        }
        
        func setWindowName(name:String) {
            let appDelegate = NSApplication.shared.delegate as! AppDelegate
            appDelegate.window.title = name
        }

    }
