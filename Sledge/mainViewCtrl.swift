                //
//  mainViewCtrl.swift
//  Sledge
//
//  Created by Rodolphe DUPERRAY on 12/12/2015.
//  Copyright © 2015 Rodolphe DUPERRAY. All rights reserved.
//

import Cocoa
import WebKit
import CouchbaseLite
import CouchbaseLiteListener
import Zip
import StoreKit


class mainViewCtrl: NSViewController, WebUIDelegate, WebFrameLoadDelegate, NetServiceBrowserDelegate, NetServiceDelegate {
    @IBOutlet var containerView : NSView?
    var webView: WebView?
    var database: CBLDatabase?
    var projectDatabase: CBLDatabase?
    var manager: CBLManager?
    var listennerPort:UInt16? = nil
    let context = JSContext()
    var documentDirectoryURL:URL? = nil
    let couch = Couch()
    var sledgePlus:SKProduct?
    var plusVersion:Bool = false
    var compactionList = [""]
    
    var inDarkMode: Bool {
        let mode = UserDefaults.standard.string(forKey: "AppleInterfaceStyle")
        return mode == "Dark"
    }
    
    let prevWindow: NSWindowController = previewWindow(windowNibName: NSNib.Name(rawValue: "previewWindow"))
    var pubWindow: NSWindowController? = nil

    let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // SSZipArchive.unzipFileAtPath(zipPath, toDestination: unzipPath)
    
        documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let filePath = Bundle.main.path(forResource: "www/index", ofType: "html")
        let url = URL(string:filePath!)
    
        plusVersion = UserDefaults.standard.bool(forKey: "com.sledge.Sledge.sledgePluss")
        
        self.webView?.frameLoadDelegate = self;
        self.webView?.mainFrame.load(URLRequest(url: url!))
        self.webView?.uiDelegate = self
        
        //http://developer.couchbase.com/documentation/mobile/1.1.0/develop/guides/couchbase-lite/native-api/view/index.html
        //http://developer.couchbase.com/documentation/mobile/1.1.0/develop/references/couchbase-lite/rest-api/database/index.html
        //http://stackoverflow.com/questions/21245307/couchbase-lite-rename-database
    }
 
    func getAppVersion() -> String {
        return "\(Bundle.main.infoDictionary!["CFBundleShortVersionString"] ?? "")"
    }
    
    @objc func getUpdateDialState() {
        let state = UserDefaults.standard.string(forKey: "updateDialState")
        let version = self.getAppVersion()
        if state != nil {
            if (version != state) {
                UserDefaults.standard.set(version, forKey: "updateDialState")
                self.webView?.stringByEvaluatingJavaScript(from: "openUpdateDial()");
            }
        } else {
            UserDefaults.standard.set(version, forKey: "updateDialState")
            self.webView?.stringByEvaluatingJavaScript(from: "openUpdateDial()");
        }
    }

    @objc func requestStore() {
        SledgeProducts.store.requestProducts{success, products in
            if success {
                let plus:NSMutableDictionary = [:]
                self.sledgePlus = products?.first
                plus.setValue(self.sledgePlus?.localizedTitle, forKey: "title")
                plus.setValue(self.sledgePlus?.localizedDescription, forKey: "about")
                plus.setValue(self.sledgePlus?.productIdentifier, forKey: "id")
                plus.setValue(self.priceFormatter.string(from: (self.sledgePlus?.price)!), forKey: "price")
                
                let data = try! JSONSerialization.data(withJSONObject: plus, options: [])
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.webView?.stringByEvaluatingJavaScript(from: "setIapProduct('\(json!)')")
                }
            } else {
                print("error requestStore")
            }
        }
    }
    
    @objc func buyProduct() {
        SledgeProducts.store.buyProduct(self.sledgePlus!, caller: self)
    }
    
    @objc func restoreProduct() {
        SledgeProducts.store.restorePurchases(caller: self)
    }
    
    public func iapUpdate(code:NSNumber, about:String = "") {
        switch (code) {
        case 1:
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.webView?.stringByEvaluatingJavaScript(from: "cancelIap()")
            }
            break
        case 2:
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.webView?.stringByEvaluatingJavaScript(from: "cancelIap()")
            }
            let alert = NSAlert()
            alert.messageText = about
            alert.alertStyle = NSAlert.Style.informational
            alert.beginSheetModal(for: self.view.window!, completionHandler: nil)
            break
        case 3:
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.webView?.stringByEvaluatingJavaScript(from: "successIap()")
            }
            break
        default:
            self.webView?.stringByEvaluatingJavaScript(from: "cancelIap()")
        }
    }
    
    /*func webView(_ sender: WebView!, mouseDidMoveOverElement elementInformation: [AnyHashable : Any]!, modifierFlags: Int) {
        // @todo
        print("A")
        print(NSApp.currentEvent?.type == NSEvent.EventType.leftMouseDown)
        (NSApp.currentEvent?.type == NSEvent.EventType.leftMouseUp)
        print("A")
        
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        appDelegate.window.performDrag(with: NSApp.currentEvent!)

    }*/
    
    @objc func getPlus() {
        plusVersion = UserDefaults.standard.bool(forKey: "com.sledge.Sledge.sledgePluss")  
        self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack(\(plusVersion))");//plusVersion
    }
    @objc func getLocal() {

        var myDict: NSDictionary?
        if let path = Bundle.main.path(forResource: "local", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        let data = try! JSONSerialization.data(withJSONObject: myDict!, options: [])
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)

        self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(json)}')");

    }

    func webView(_ webView: WebView!,didClearWindowObject windowObject: WebScriptObject!, for frame: WebFrame!) {
        windowObject.setValue(listennerPort, forKey: "listennerPort");
        windowObject.setValue(self, forKey: "native");
        let docPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        self.webView?.stringByEvaluatingJavaScript(from: "localStorage.setItem('listennerPort', '\(listennerPort!)')");
        self.webView?.stringByEvaluatingJavaScript(from: "localStorage.setItem('documentPath', '\(docPath)')"); 

        self.webView?.stringByEvaluatingJavaScript(from: "localStorage.setItem('osxMode', '\(inDarkMode)')");

        
        var local = Locale.current.languageCode
        if (local != "en" && local != "fr") {
            local = "en"
        }
        self.webView?.stringByEvaluatingJavaScript(from: "window.nslocal='\(String(describing: local!))'");

    }
    
    override class func webScriptName(for aSelector: Selector) -> String? {
        var sel:NSString
        sel = ""
        
        if aSelector == #selector(mainViewCtrl.callSwift(_:)) {
            sel = "callSwift"
        }

        if aSelector == #selector(mainViewCtrl.getPlus) {
            sel = "getPlus"
        }
        
        if aSelector == #selector(mainViewCtrl.getUpdateDialState) {
            sel = "getUpdateDialState"
        }
        
        if aSelector == #selector(mainViewCtrl.requestStore) {
            sel = "requestStore"
        }
        
        if aSelector == #selector(mainViewCtrl.restoreProduct) {
            sel = "restoreProduct"
        }
        
        if aSelector == #selector(mainViewCtrl.buyProduct) {
            sel = "buyProduct"
        }
        
        if aSelector == #selector(mainViewCtrl.addNewProject(_:)) {
            sel = "addNewProject"
        }

        if aSelector == #selector(mainViewCtrl.updateThemeDial) {
            sel = "updateThemeDial"
        }
        
        if aSelector == #selector(mainViewCtrl.getAllViews) {
            sel = "getAllViews"
        }
        
        if aSelector == #selector(mainViewCtrl.archive) {
            sel = "archive"
        }
        
        if aSelector == #selector(mainViewCtrl.importAchiveDial) {
            sel = "importAchiveDial"
        }
        
        if aSelector == #selector(mainViewCtrl.getAllPages) {
            sel = "getAllPages"
        }
        
        if aSelector == #selector(mainViewCtrl.openProject(_:)) {
            sel = "openProject"
        }
        
        if aSelector == #selector(mainViewCtrl.getThemeConfig) {
            sel = "getThemeConfig"
        }
        
        if aSelector == #selector(mainViewCtrl.deleteReferenceOnDisk) {
            sel = "deleteReferenceOnDisk"
        }

        if aSelector == "createProjectViews:" {
            sel = "createProjectViews"
        }
        
        if aSelector == #selector(mainViewCtrl.openPreview) {
            sel = "openPreview"
        }
        
        if aSelector == #selector(mainViewCtrl.openPublish) {
            sel = "openPublish"
        }
        
        if aSelector == #selector(mainViewCtrl.removeTheme) {
            sel = "removeTheme"
        }
        
        if aSelector == #selector(mainViewCtrl.getLocal) {
            sel = "getLocal"
        }
        
        if aSelector == #selector(mainViewCtrl.getFile) {
            sel = "getFile"
        }
        
        if aSelector == #selector(mainViewCtrl.getAttachmentPath) {
            sel = "getAttachmentPath"
        }
        
        if aSelector == #selector(mainViewCtrl.deleteFile) {
            sel = "deleteFile"
        }
        
        if aSelector == #selector(mainViewCtrl.downloadTheme) {
            sel = "downloadTheme"
        }
        
        if aSelector == #selector(mainViewCtrl.getPublishPath) {
            sel = "getPublishPath"
        }
        
        if aSelector == #selector(mainViewCtrl.checkForTheme) {
            sel = "checkForTheme"
        }
        
        if aSelector == #selector(mainViewCtrl.getFavicon) {
            sel = "getFavicon"
        }
        
        if aSelector == #selector(mainViewCtrl.setWindowName) {
            sel = "setWindowName"
        }
        
        if aSelector == #selector(mainViewCtrl.openLink) {
            sel = "openLink"
        }
        
        if sel == "" {
            return nil
        } else {
            return sel as String;
        }
    }

    @objc func openLink (link:String) {
        NSWorkspace.shared.open(URL(string: link)!)
    }
    
    @objc func setWindowName(name:String) {
        Utilities.sharedInstance.setWindowName(name:name)
    }
    
    @objc func getPublishPath() {
        let panel = NSOpenPanel()
        panel.title = NSLocalizedString("Choose a publication location", comment:"")
        panel.canChooseDirectories = true
        panel.canChooseFiles = false
        let i = panel.runModal()
        if(i == NSApplication.ModalResponse.OK){
            print(panel.url!)
            let myURL = panel.url
            UserDefaults.standard.set(myURL, forKey: "localPublishPath")
            self.webView?.stringByEvaluatingJavaScript(from: "setLocalPublishPath('\((myURL?.path)!)')")
        }
    }
    
    @objc func addNewProject(_ name:String) {
        do {
            self.projectDatabase = try manager!.databaseNamed(name)
            // self.createProjectViews(self.projectDatabase!)
        } catch _ {
            
        }

    }
    
    @objc func callSwift(_ message:String) {
        let alert = NSAlert()
        alert.messageText = message
        alert.alertStyle = NSAlert.Style.informational
        alert.beginSheetModal(for: self.view.window!, completionHandler: nil)
    }
    
    override class func isSelectorExcluded(fromWebScript aSelector: Selector) -> Bool
    {
        return false
    }
    
    override func loadView() {
        super.loadView()
        couch.load(caller: self)
        self.webView = WebView(frame: self.view.frame)
        self.view = self.webView!
    }
    
    func createProjectViews() {
        
        let viewsView = self.projectDatabase!.viewNamed("allViews")
        viewsView.setMapBlock({ (doc, emit) in
            
            if doc["type"] as? String == "view" {
                emit(doc, doc["id"])
            }
            
        }, version: "2")
        
        let pagesView = self.projectDatabase!.viewNamed("allPages")
        pagesView.setMapBlock({ (doc, emit) in
            
            if doc["type"] as? String == "page" {
                emit(doc, doc["id"])
            }
            
        }, version: "3")

        
    }
    
    @objc func openProject(_ projName:String?) {
        
        do {
            self.projectDatabase = try self.manager!.databaseNamed(projName!)
            self.createProjectViews()
            if !compactionList.contains(projName!) { //run once a time for each db
                compactionList.append(projName!)
                try self.projectDatabase?.compact()
            }
            self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('{}')")
        } catch _ {
            
        }
    }
    
    @objc func openPreview() {
        prevWindow.showWindow(nil);
    }
    
    @objc func openPublish() {
        pubWindow = publishWindow(windowNibName: NSNib.Name(rawValue: "publishWindow"))
        pubWindow?.showWindow(nil)
    }
    
    func closePublish() {
        pubWindow = nil
    };
    
    func getAllPagesFromCurrent() -> [NSDictionary]! {
        do {
            let query = self.projectDatabase!.viewNamed("allPages").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []
            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }
            return viewsList
            
        } catch _ {
            print("JSON string error")
        }
        return nil
    }
    
    
    @objc func getAllViews() {
        
        do {
            let query = self.projectDatabase!.viewNamed("allViews").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []

            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }

    
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            var theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
        //    theJSONText = theJSONText?.replacingOccurrences(of: "l'article", with: "larticle") as! NSString

            self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')");

        } catch _ {
            print("JSON string error")
        }
    }
    
    func getAllViewsFromCurrent() -> [NSDictionary]! {
        do {
            let query = self.projectDatabase!.viewNamed("allViews").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []
            
            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }

            return viewsList
            
        } catch _ {
            print("JSON string error")
        }
        
        return nil
        
    }
    
    @objc func getAllPages() {
        do {
            let query = self.projectDatabase!.viewNamed("allPages").createQuery()
            let result = try query.run()
            var viewsList: [NSDictionary] = []
            while let row = result.nextRow() {
                viewsList.append(row.value(forKey: "key") as! NSDictionary)
            }
            
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            var theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            //theJSONText = theJSONText?.replacingOccurrences(of: "d'utilisation", with: "dutilisation") as! NSString
            self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')");
            
        } catch _ {
            print("JSON string error")
        }
    }
    
    func createViews(_ dbname:String) {
        let projectsView = self.database!.viewNamed("projects")
        projectsView.setMapBlock({ (doc, emit) in
            if let projs = doc["projects"] as? [String] {
                for proj in projs {
                    emit(proj, doc["name"])
                }
            }
        }, version: "2")

    }
    
    @objc func getAttachmentPath(fileName: String) {
        let doc = self.projectDatabase?.document(withID: "media")
        let currenrRev = doc?.currentRevision
        let att = currenrRev?.attachmentNamed(fileName)
        let url = att?.contentURL

        let jsonObject: [String: AnyObject] = [
            "name": fileName as AnyObject,
            "path": url as AnyObject
        ]
        
        do {
            let theJSONData = try JSONSerialization.data(
                withJSONObject: jsonObject,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText2 = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            self.webView?.stringByEvaluatingJavaScript(from: "attachmentPath('\(theJSONText2)')")
        } catch _  {
            print("video error")
        }
    }
    
    func saveFaviconToDisk(data:Array<String>, url:URL) -> Bool {
        for name:String in data {
            self.saveMediaFaviconToDisk(name: "favicon_\(name).png" as NSString, url: url)
        }
        return true
    }
    
    func saveImageToDisk(data:Array<NSDictionary>, url:URL) -> Bool {
        for obj:NSDictionary in data {
            if (obj.value(forKey: "type") != nil) && obj.value(forKey: "type") as! String == "mp3" {
                self.saveMediaToDisk(name: (obj.value(forKey: "name") as! String) + "." + (obj.value(forKey: "type") as! String) as NSString, url: url)
            } else {
                if (obj.value(forKey: "numberOfSizes") != nil) {
                    switch obj.value(forKey: "numberOfSizes") as! NSNumber {
                    case 1:
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                    break
                    case 2:
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "_small." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                    break
                    case 3:
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "_small." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "_medium." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                    break
                    case 4:
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "_small." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "_medium." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                        self.saveMediaToDisk(name: (obj.value(forKey: "media") as! String) + "_large." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                    break
                    default:
                        self.saveMediaToDisk(name: obj.value(forKey: "media") as! String + "." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                    break
                    }
                } else {
                    self.saveMediaToDisk(name: obj.value(forKey: "media") as! String + "." + (obj.value(forKey: "mediaType") as! String) as NSString, url: url)
                }
            }
        }
        return true
    }
    
    func saveMediaToDisk(name:NSString, url:URL) {
        let doc = self.projectDatabase?.document(withID: "media")
        let rev = doc?.currentRevision
        let att = rev?.attachmentNamed(name as String)
        let saveURL = url.appendingPathComponent(name as String)
        do {
            try att?.content?.write(to: saveURL)
        } catch _  {
            print("save to disk error \(name)")
        }
    }
    
    func saveMediaFaviconToDisk(name:NSString, url:URL) {
        let doc = self.projectDatabase?.document(withID: "settings")
        let rev = doc?.currentRevision
        let att = rev?.attachmentNamed(name as String)
        let saveURL = url.appendingPathComponent(name as String)
        do {
            try att?.content?.write(to: saveURL)
        } catch _  {
            print("save favicon to disk error \(name)")
        }
    }
    
    @objc func getFavicon(size:Int) {
        let fileBrowser = NSOpenPanel()
        fileBrowser.allowsMultipleSelection = false
        fileBrowser.canChooseDirectories = false
        fileBrowser.canCreateDirectories = false
        fileBrowser.canChooseFiles = true
        fileBrowser.allowedFileTypes = ["png"]
        
        let i = fileBrowser.runModal()
        
        if (i == NSApplication.ModalResponse.OK) {
            let imageData = NSImage(byReferencing: fileBrowser.url!)
            
            let rep = NSImageRep(contentsOf: fileBrowser.url!)
            let tot = CGFloat(size)
            if (size == (rep?.pixelsWide)! && size == (rep?.pixelsHigh)!) {
                
                let img:Data! = imageData.PNGRepresentation!
                let doc = self.projectDatabase?.document(withID: "settings")
                let newRev = doc?.currentRevision?.createRevision()
                let mine = "image/png"
                newRev?.setAttachmentNamed(String(format: "favicon_%.0f.png", tot), withContentType: mine , content: img)
                
                do {
                    try newRev?.save()
                    self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('\(String(format: "favicon_%.0f.png", tot))')")
                } catch _ {
                    //
                }
                
            } else {
                let alert = NSAlert()
                alert.messageText = String(format: NSLocalizedString("The height and the width of your favicon have to measure %.0f pixels", comment: ""), tot)
                alert.alertStyle = NSAlert.Style.informational
                alert.beginSheetModal(for: self.view.window!, completionHandler: nil)
            }
        }
    }
    
    @objc func getFile(onlySound:Bool, onlyImage:Bool, onlyImagePlus:Bool) {
        let mimeType:String
        let fileBrowser = NSOpenPanel()
        fileBrowser.allowsMultipleSelection = false
        fileBrowser.canChooseDirectories = false
        fileBrowser.canCreateDirectories = false
        fileBrowser.canChooseFiles = true
        //fileBrowser.allowedFileTypes = ["jpg", "png", "jpeg", "svg", "mp3", "mov", "webm", "mp4", "ogg"]
        
        if onlySound {
            fileBrowser.allowedFileTypes = ["mp3"]
        } else if (onlyImage) {
            fileBrowser.allowedFileTypes = ["jpg", "png", "jpeg", "svg"]
        } else if (onlyImagePlus) {
            fileBrowser.allowedFileTypes = ["jpg", "png", "jpeg", "svg", "pdf"]
        } else {
            fileBrowser.allowedFileTypes = ["jpg", "png", "jpeg", "svg", "mp3", "pdf"]
        }
        
        let i = fileBrowser.runModal()
        
        if (i == NSApplication.ModalResponse.OK) {
            let url = fileBrowser.url
            let name = UUID().uuidString //url?.deletingPathExtension().lastPathComponent
            let originalName = url?.deletingPathExtension().lastPathComponent
            let pathExtension = url?.pathExtension
            var numberOfSizes = 1

            let ext = pathExtension?.lowercased()
            if (ext == "svg" || ext == "mov" || ext == "mp4" || ext == "webm" || ext == "mp3" || ext == "pdf") {
            
                do {
                    let fData = try NSData(contentsOf: fileBrowser.url!, options: NSData.ReadingOptions())
                    if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
                        if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                            mimeType = mimetype as String
                            saveVideo(newVideo: fData, name: name, ext: ext!, mineType: mimeType)
                        }
                    }
                    
                    let doc = self.projectDatabase?.document(withID: "media")
                    var properties = doc?.properties
                    properties?[name] = [
                        "type": ext!,
                        "numberOfSizes": numberOfSizes as AnyObject,
                        "originalName": originalName
                    ]

                    try doc?.putProperties(properties!)
                    
                    let jsonObject: [String: AnyObject] = [
                        "name": name as AnyObject,
                        "type": ext as AnyObject,
                        "numberOfSizes": numberOfSizes as AnyObject,
                        "originalName": originalName as AnyObject
                    ]
                    
                    let theJSONData = try JSONSerialization.data(
                        withJSONObject: jsonObject,
                        options: JSONSerialization.WritingOptions(rawValue: 0))
                    let theJSONText1 = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
                    self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('\(theJSONText1)')")
                    
                } catch _ {
                    print("JSON string error")
                }
                
            } else {
                let imageData = NSImage(byReferencing: fileBrowser.url!)
                let size = imageData.size.width
                numberOfSizes = 1
                
                if (size >= 1500) {
                    saveImage(newImage:imageData, name:name, ext: ext!)
                    saveImageWithSize(image:imageData, name:name + "_large", maxSize:NSMakeSize (750, 750), ext: ext!) //1500
                    saveImageWithSize(image:imageData, name:name + "_medium", maxSize:NSMakeSize (500, 500), ext: ext!) //1000
                    saveImageWithSize(image:imageData, name:name + "_small", maxSize:NSMakeSize (250, 250), ext: ext!) // 500
                    numberOfSizes = 4
                }
                if (size <= 1500 && size >= 1000) {
                    saveImage(newImage:imageData, name:name, ext: ext!)
                    saveImageWithSize(image:imageData, name:name + "_medium", maxSize:NSMakeSize (500, 500), ext: ext!) //1000
                    saveImageWithSize(image:imageData, name:name + "_small", maxSize:NSMakeSize (250, 250), ext: ext!) // 500
                    numberOfSizes = 3
                }
                if (size <= 1000 && size >= 500) {
                    saveImage(newImage:imageData, name:name, ext: ext!)
                    saveImageWithSize(image:imageData, name:name + "_small", maxSize:NSMakeSize (250, 250), ext: ext!) // 500
                    numberOfSizes = 2
                }
                if (size <= 500) {
                    saveImage(newImage:imageData, name:name, ext: ext!)
                    numberOfSizes = 1
                }
                
                do {
                    
                    let doc = self.projectDatabase?.document(withID: "media")
                    var properties = doc?.properties
                    properties?[name] = [
                        "type": ext!,
                        "numberOfSizes": numberOfSizes as AnyObject,
                        "originalName": originalName
                    ]
                    
                    try doc?.putProperties(properties!)
                    
                    let jsonObject: [String: AnyObject] = [
                        "name": name as AnyObject,
                        "numberOfSizes": numberOfSizes as AnyObject,
                        "type": ext! as AnyObject,
                        "originalName": originalName as AnyObject
                    ]
                    
                    let theJSONData = try JSONSerialization.data(
                        withJSONObject: jsonObject,
                        options: JSONSerialization.WritingOptions(rawValue: 0))
                    let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
                    self.webView?.stringByEvaluatingJavaScript(from: "nativeCallBack('\(theJSONText)')")
                    
                } catch _ {
                    print("JSON string error")
                }
            }
            
            
        }
    }
    
    @objc func deleteFile(ref: String, numberIfItem:NSString) {
        
        let doc = self.projectDatabase?.document(withID: "media")
        
        var prop = doc?.properties
        prop?.removeValue(forKey: ref)
        try! doc?.putProperties(prop!)

        let rev = doc?.currentRevision?.createRevision()

        rev?.removeAttachmentNamed(ref + ".jpg")
        
        if (numberIfItem == "2") {
            rev?.removeAttachmentNamed(ref + "_small.jpg")
        }
        if (numberIfItem == "3") {
            rev?.removeAttachmentNamed(ref + "_small.jpg")
            rev?.removeAttachmentNamed(ref + "_medium.jpg")
        }
        if (numberIfItem == "4") {
            rev?.removeAttachmentNamed(ref + "_small.jpg")
            rev?.removeAttachmentNamed(ref + "_medium.jpg")
            rev?.removeAttachmentNamed(ref + "_large.jpg")
        }
        try! rev?.save()
        self.webView?.stringByEvaluatingJavaScript(from: "mediaUpdate()")
    }
    
    func saveVideo(newVideo:NSData, name:String, ext:String, mineType:String) {
        
        let doc = self.projectDatabase?.document(withID: "media")
        let newRev = doc?.currentRevision?.createRevision()
        newRev?.setAttachmentNamed(name + "." + ext, withContentType: mineType , content: newVideo as Data)
        
        do {
            try newRev?.save()
        } catch _ {
            
        }
    }
    
    func saveImage(newImage:NSImage, name:String, ext:String) {
        
        let img: NSData! = newImage.tiffRepresentation! as NSData
        let bitmap: NSBitmapImageRep! = NSBitmapImageRep(data: img! as Data)
        var jpgCoverImage:Data
        if (ext == "png") {
            jpgCoverImage = bitmap.representation(using: NSBitmapImageRep.FileType.png, properties: [:])!
        } else {
            jpgCoverImage = bitmap.representation(using: NSBitmapImageRep.FileType.jpeg, properties: [NSBitmapImageRep.PropertyKey.compressionFactor:0.7])!
        }
        
        let doc = self.projectDatabase?.document(withID: "media")
        let newRev = doc?.currentRevision?.createRevision()
        var mine = "image/jpeg"
        if (ext == "png") {
            mine = "image/png"
        }
        newRev?.setAttachmentNamed(name + "." + ext, withContentType: mine , content: jpgCoverImage)
        
        do {
            try newRev?.save()
        } catch _ {
            
        }
    }

    
    func saveImageWithSize(image:NSImage, name:String, maxSize:NSSize, ext:String) {
        let newImage = image.resizeWhileMaintainingAspectRatioToSize(size: maxSize)
        saveImage(newImage:newImage!, name:name, ext:ext)
    }
    
    @objc func getThemeConfig(projName: String, ref:String) {
        
        if checkForTheme(projName: projName, ref: ref) {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: path)
            let fullFilePath = url.appendingPathComponent("sledgeTheme")
            let pathURl = fullFilePath?.appendingPathComponent(projName)
            let destinationFileUrl = pathURl?.appendingPathComponent(ref).appendingPathComponent("assets").appendingPathComponent("sledge").appendingPathComponent("config.json")
            do {
                let configText = try String(contentsOf: destinationFileUrl!, encoding: String.Encoding.utf8)
                self.webView?.stringByEvaluatingJavaScript(from: "setThemeConfig(\(configText))")
            }
            catch {/* error handling here */}
        }
    }
    
    @objc func removeTheme(projName: String, ref:String) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let fullFilePath = url.appendingPathComponent("sledgeTheme")
        let pathURl = fullFilePath?.appendingPathComponent(projName)
        
        let fileManager = FileManager.default
        var doc = self.projectDatabase?.document(withID: "theme")
        
        do {
            var properties = doc?.properties
            properties?["current"] = "default"
            try doc?.putProperties(properties!)

            doc = self.projectDatabase?.document(withID: "theme")
            let newRev = doc?.currentRevision?.createRevision()
            newRev?.removeAttachmentNamed(ref + ".zip")
            try newRev?.save()

            try fileManager.removeItem(atPath: (pathURl?.path)! + "/" + ref)

            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    @objc func deleteReferenceOnDisk(projName: String) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let fullFilePath = url.appendingPathComponent("sledgeTheme")
        let pathURl = fullFilePath?.appendingPathComponent(projName)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: (pathURl?.path)!) {
            do {
                try fileManager.removeItem(atPath: (pathURl?.path)!)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func updateThemeDial() {
        let alert = NSAlert()
        alert.messageText = NSLocalizedString("An update is availale for the current theme. Would you like to update it now ?", comment:"")
        alert.alertStyle = NSAlert.Style.informational
        alert.addButton(withTitle: NSLocalizedString("Proceed", comment:""))
        alert.addButton(withTitle: NSLocalizedString("Not now", comment:""))
        alert.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
            if modalResponse == NSApplication.ModalResponse.alertFirstButtonReturn {
                self.webView?.stringByEvaluatingJavaScript(from: "updateCurrentTheme()")
            }
        })
    }
    
    @objc func checkForTheme(projName: String, ref:String) -> Bool {

        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let sledgeDirUrl = url.appendingPathComponent("sledgeTheme")
        let projDirUrl = sledgeDirUrl?.appendingPathComponent(projName)
        let refDirUrl = projDirUrl?.appendingPathComponent(ref)
        let refDirPath = refDirUrl?.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: refDirPath!) {
            //
        } else {
            
            let alert = NSAlert()
            alert.messageText = String(format: NSLocalizedString("The project %@ needs the theme %@. It can not be found in your computer. Would you like to restore it from the backup now, or do you prefere to switch to the default theme ?", comment: ""), projName, ref)
            alert.alertStyle = NSAlert.Style.informational
            alert.addButton(withTitle: NSLocalizedString("Restore", comment:""))
            alert.addButton(withTitle: NSLocalizedString("Use default", comment:""))
            alert.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
                if modalResponse == NSApplication.ModalResponse.alertFirstButtonReturn {
                    self.webView?.stringByEvaluatingJavaScript(from: "restTheme()")
                    let doc = self.projectDatabase?.document(withID: "theme")
                    let rev = doc?.currentRevision
                    let att = rev?.attachmentNamed(ref + ".zip" as String)
                    let saveURL = projDirUrl?.appendingPathComponent(ref + ".zip" as String)
                    do {
                        try att?.content?.write(to: saveURL!)
                        try Zip.unzipFile(saveURL!, destination: (projDirUrl?.appendingPathComponent("unziped"))!, overwrite: true, password: "password", progress: { progress -> () in
                            if (progress == 1.0) {
                                DispatchQueue.main.async {
                                    do {
                                        try fileManager.moveItem(at: (projDirUrl?.appendingPathComponent("unziped").appendingPathComponent("theme-\(ref)-master"))!, to: projDirUrl!.appendingPathComponent(ref))
                                        try fileManager.removeItem(atPath: (saveURL?.path)!)
                                        try fileManager.removeItem(atPath: (projDirUrl?.appendingPathComponent("unziped").path)!)
                                        self.webView?.stringByEvaluatingJavaScript(from: "downloadThemeDone('\(ref)')")
                                    } catch let error as NSError {
                                        print(error.debugDescription)
                                    }
                                }
                            }
                        })
                        
                    } catch _  {
                        print("save to disk error : theme : \(ref)")
                    }
                } else {
                    self.webView?.stringByEvaluatingJavaScript(from: "useDefaultTheme()")
                }
            })
        }
        return true
    }
    
    func manageNewTheme(ref:String, pathURl:URL, destinationFileUrl:URL) {
        let fileManager = FileManager.default
        var doc = self.projectDatabase?.document(withID: "theme")
        do {
            var properties = doc?.properties
            let oldFile = properties?["current"] as! String
            properties?["current"] = ref
            try doc?.putProperties(properties!)
            let zipData = try Data.init(contentsOf: destinationFileUrl, options: [])
            doc = self.projectDatabase?.document(withID: "theme")
            let newRev = doc?.currentRevision?.createRevision()
            newRev?.setAttachmentNamed(ref + ".zip", withContentType: "application/zip" , content: zipData)
            if (oldFile != "default") {
                if fileManager.fileExists(atPath: pathURl.path + "/" + oldFile) {
                    try fileManager.removeItem(atPath: pathURl.path + "/" + oldFile)
                }
                newRev?.removeAttachmentNamed(oldFile + ".zip")
            }
            try newRev?.save()
            
            let np = pathURl
            var newFolderPath = np.appendingPathComponent("theme-\(ref)-master")
            let newNameFolderPath = np.appendingPathComponent(ref)
            newFolderPath.setTemporaryResourceValue(ref, forKey: .nameKey)
            
            do {
                try FileManager.default.moveItem(at: newFolderPath, to: newNameFolderPath)
            } catch {
                print(error)
            }
            
            do {
                try fileManager.removeItem(atPath: destinationFileUrl.path)
            } catch let error as NSError {
                print(error.debugDescription)
            }
            
            self.webView?.stringByEvaluatingJavaScript(from: "downloadThemeDone('\(ref)')")
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    @objc func importAchiveDial () {
        let alert = NSAlert()
        alert.messageText = NSLocalizedString("Your are going to restore an archive. If an existing project as the same name, it will be overrided.", comment:"")
        alert.alertStyle = NSAlert.Style.informational
        alert.addButton(withTitle: NSLocalizedString("Proceed", comment:""))
        alert.addButton(withTitle: NSLocalizedString("Cancel", comment:""))
        alert.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
            let appDelegate = NSApplication.shared.delegate as! AppDelegate
            if modalResponse == NSApplication.ModalResponse.alertFirstButtonReturn {
                if (appDelegate.importFileName != nil) {
                    self.getArchive(filename: appDelegate.importFileName)
                } else {
                    self.importArchive()
                }
            } else {
                appDelegate.importFileName = nil
                self.webView?.stringByEvaluatingJavaScript(from: "actionReset()")
            }
        })
    }

    func getArchive (filename:String) {
        
        let myURL = URL(fileURLWithPath: filename)
        let fullName = myURL.lastPathComponent
        let range = fullName.range(of: "__")
        let name = fullName[(fullName.startIndex ..< (range?.lowerBound)!)]
        let theData:Data = try! Data(contentsOf: myURL)
        let fileWrapper:FileWrapper = FileWrapper(serializedRepresentation: theData)!
        
        var path = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)[0] as String
        path = path + "/com.sledge.Sledge/CouchbaseLite" //  + projName + ".cblite2"
        
        do {
            try fileWrapper.write(to: URL(fileURLWithPath: path + "/\(name).cblite2") , options: [.atomic, .withNameUpdating], originalContentsURL: nil)
        } catch (let writeError) {
            print("Error extracting the archive: \(writeError)")
        }
        let alert = NSAlert()
        alert.messageText = NSLocalizedString("The Archive of ", comment:"") + name + NSLocalizedString(" has been succesfully imported !", comment:"")
        alert.alertStyle = NSAlert.Style.informational
        alert.addButton(withTitle: NSLocalizedString("OK", comment:""))
        alert.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
            self.webView?.stringByEvaluatingJavaScript(from: "importDone()")
        })
        
        
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        appDelegate.importFileName = nil
    }
    
    func importArchive () {
    
        let fileBrowser = NSOpenPanel()
        fileBrowser.message = NSLocalizedString("Please choose an archive to restore", comment:"")
        fileBrowser.allowsMultipleSelection = false
        fileBrowser.canChooseDirectories = false
        fileBrowser.canCreateDirectories = false
        fileBrowser.canChooseFiles = true
        fileBrowser.allowedFileTypes = ["sledge", "sled"]
        let mod = fileBrowser.runModal()
        if (mod == NSApplication.ModalResponse.OK) {
            let myURL = fileBrowser.url!
            let fullName = myURL.lastPathComponent
            let range = fullName.range(of: "__")
            let name = fullName[(fullName.startIndex ..< (range?.lowerBound)!)]
            let theData:Data = try! Data(contentsOf: myURL)
            let fileWrapper:FileWrapper = FileWrapper(serializedRepresentation: theData)!
            
            var path = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)[0] as String
            path = path + "/com.sledge.Sledge/CouchbaseLite" //  + projName + ".cblite2"
            
            do {
                try fileWrapper.write(to: URL(fileURLWithPath: path + "/\(name).cblite2") , options: [.atomic, .withNameUpdating], originalContentsURL: nil)
            } catch (let writeError) {
                print("Error extracting the archive: \(writeError)")
            }
            let alert = NSAlert()
            alert.messageText = NSLocalizedString("The Archive of ", comment:"") + name + NSLocalizedString(" has been succesfully imported !", comment:"")
            alert.alertStyle = NSAlert.Style.informational
            alert.addButton(withTitle: NSLocalizedString("OK", comment:""))
            alert.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
                self.webView?.stringByEvaluatingJavaScript(from: "importDone()")
            })
        } else {
            self.webView?.stringByEvaluatingJavaScript(from: "actionReset()")
        }
    }
    
    @objc func archive (projName: String) {
        
        let panel = NSOpenPanel()
        panel.message = NSLocalizedString("Choose where to save your archive", comment:"")
        panel.canChooseDirectories = true
        panel.canChooseFiles = false
        panel.allowsMultipleSelection = false
        panel.canCreateDirectories = true
        let i = panel.runModal()
        if(i == NSApplication.ModalResponse.OK){
            let myURL = panel.url!
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy-HH-mm-ss"
            var dateString = formatter.string(from: Date()) 
            dateString = projName + "__" + dateString + ".sled"
            
            var path = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)[0] as String
            path = path + "/com.sledge.Sledge/CouchbaseLite/" + projName + ".cblite2"
            
            let fileWrapper = try! FileWrapper.init(url: URL(fileURLWithPath: path), options: .immediate)
            let theData = fileWrapper.serializedRepresentation
            
            let savePath = myURL.path + "/" + dateString
            let fileManager = FileManager()
            fileManager.createFile(atPath: savePath, contents: theData, attributes: nil)
            
            let alert = NSAlert()
            alert.messageText = NSLocalizedString("Archive succesfully saved !", comment:"")
            alert.alertStyle = NSAlert.Style.informational
            alert.addButton(withTitle: NSLocalizedString("Reveal in Finder", comment:""))
            alert.addButton(withTitle: NSLocalizedString("OK", comment:""))
            alert.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
                if modalResponse == NSApplication.ModalResponse.alertFirstButtonReturn {
                    NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: myURL.path)
                }
                self.webView?.stringByEvaluatingJavaScript(from: "actionReset()")
            })
        } else {
            self.webView?.stringByEvaluatingJavaScript(from: "actionReset()")
        }
    }
    
    @objc func downloadTheme(projName: String, ref:String) {
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let fullFilePath = url.appendingPathComponent("sledgeTheme")
        let pathURl = fullFilePath?.appendingPathComponent(projName)
        let filePath = pathURl?.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath!) {
            //
        } else {
            do {
                try FileManager.default.createDirectory(atPath: filePath!, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        var destinationFileUrl = pathURl?.appendingPathComponent(ref + ".zip")
        
        if fileManager.fileExists(atPath: destinationFileUrl!.path) {
            //
        } else {
        
            //Create URL to the source file you want to download
            let fileURL = URL(string: "https://github.com/sledge-app/theme-\(ref)/archive/master.zip")
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl!)
                    } catch (let writeError) {
                        print("Error creating a file \(String(describing: destinationFileUrl)) : \(writeError)")
                    }
                    
                    do {
                        try Zip.unzipFile(destinationFileUrl!, destination: pathURl!, overwrite: true, password: "password", progress: { progress -> () in
                            if (progress == 1.0) {
                                DispatchQueue.main.async {
                                    self.manageNewTheme(ref: ref, pathURl: pathURl!, destinationFileUrl: destinationFileUrl!)
                                }
                            }
                        })
                    }
                    catch let error as NSError {
                        print(error.debugDescription)
                    }
                    
                } else {
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription);
                }
            }
            task.resume()
        }
    }
}
