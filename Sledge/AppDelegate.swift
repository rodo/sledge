        //
//  AppDelegate.swift
//  Sledge
//
//  Created by Rodolphe DUPERRAY on 12/12/2015.
//  Copyright © 2015 Rodolphe DUPERRAY. All rights reserved.
//

import Cocoa
import WebKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var mainView: mainViewCtrl!
    var importFileName: String!
    
    @IBOutlet weak var window: NSWindow!
    
    @IBAction func showHelp(_ sender: Any) {
        let url = URL(string: "http://sled-app.io")
        NSWorkspace.shared.open(url!)
    }
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        #if DEBUG
        #else
            // Test whether the app's receipt exists.
            if let url = Bundle.main.appStoreReceiptURL, let _ = try? Data(contentsOf: url) {
                // The receipt exists. Do something.
            }
            else {
                // Validation fails. The receipt does not exist.
                exit(173)
            }
        #endif
        
        let defaults = UserDefaults.standard
        #if DEBUG
            defaults.set(true, forKey: "WebKitDeveloperExtras")
            defaults.synchronize()
        #else
        #endif
        self.mainView = mainViewCtrl(nibName: NSNib.Name(rawValue: "mainViewCtrl"), bundle: nil)
        self.window.contentView = self.mainView!.view
        self.window.contentViewController = self.mainView!
        self.mainView.view.frame = self.window.contentView!.bounds;
    
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func application(_ sender: NSApplication, openFile filename: String) -> Bool {
        self.importFileName = filename
        self.mainView.webView?.stringByEvaluatingJavaScript(from: "importFile('\(filename)')")
        return true
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
}

