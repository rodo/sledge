//
//  imageResize.swift
//  Sledge
//
//  Created by Rodolphe DUPERRAY on 19/05/2017.
//  Copyright © 2017 Rodolphe DUPERRAY. All rights reserved.
//

import Foundation
import Cocoa

func resizeImage(image:NSImage, maxSize:NSSize) -> NSImage {
    var ratio:Float = 0.0
    let imageWidth = Float(image.size.width)
    let imageHeight = Float(image.size.height)
    let maxWidth = Float(maxSize.width)
    let maxHeight = Float(maxSize.height)
    
    // Get ratio (landscape or portrait)
    if (imageWidth > imageHeight) {
        // Landscape
        ratio = maxWidth / imageWidth;
    }
    else {
        // Portrait
        ratio = maxHeight / imageHeight;
    }
    
    // Calculate new size based on the ratio
    let newWidth = imageWidth * ratio
    let newHeight = imageHeight * ratio
    
    // Create a new NSSize object with the newly calculated size
    let newSize:NSSize = NSSize(width: Int(newWidth), height: Int(newHeight))
    
    // Cast the NSImage to a CGImage
    var imageRect:CGRect = CGRect(x:0, y:0, width: newSize.width, height: newSize.height)
    let imageRef = image.cgImage(forProposedRect: &imageRect, context: nil, hints: nil)

    // Create NSImage from the CGImage using the new size
    let imageWithNewSize = NSImage(cgImage: imageRef!, size: newSize)
    
    imageWithNewSize.draw(in: imageRect)
    // Return the new image
    
    
    
    return imageWithNewSize
}

/*
 func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
 let size = image.size
 
 let widthRatio  = targetSize.width  / image.size.width
 let heightRatio = targetSize.height / image.size.height
 
 // Figure out what our orientation is, and use that to form the rectangle
 var newSize: CGSize
 if(widthRatio > heightRatio) {
 newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
 } else {
 newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
 }
 
 // This is the rect that we've calculated out and this is what is actually used below
 let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
 
 // Actually do the resizing to the rect using the ImageContext stuff
 UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
 image.draw(in: rect)
 let newImage = UIGraphicsGetImageFromCurrentImageContext()
 UIGraphicsEndImageContext()
 
 return newImage!
 }
 */
