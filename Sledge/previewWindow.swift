 //
//  previewWindow.swift
//  Sledge
//
//  Created by Rodolphe DUPERRAY on 10/03/2016.
//  Copyright © 2016 Rodolphe DUPERRAY. All rights reserved.
//

import Cocoa
import WebKit
import CouchbaseLite
import CouchbaseLiteListener

class previewWindow: NSWindowController, WebFrameLoadDelegate, NetServiceBrowserDelegate, NetServiceDelegate {

    @IBOutlet var mainView: NSView!
    @IBOutlet var combo: NSComboBox!
    @IBOutlet var select: NSPopUpButtonCell!
    @IBOutlet var webview: WebView!
    
    @IBAction func languageUpdate(send: NSPopUpButtonCell) {
        let index = select.indexOfSelectedItem
        webview?.stringByEvaluatingJavaScript(from: "setLanguage(\(index))")
    }
    
    @IBAction func setCurrentView(send: NSComboBox) {
        var key:NSString = ""
        var c:Int = 0
        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "view") {
            for item in currentViews {
                if (c == combo.indexOfSelectedItem) {
                    let obj = item as NSDictionary
                    key = obj.value(forKey: "_id") as! NSString
                }
                c += 1
            }
        }
        
        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "page") {
            for item in currentPages {
                if (c == combo.indexOfSelectedItem) {
                    let obj = item as NSDictionary
                    key = obj.value(forKey: "_id") as! NSString
                }
                c += 1
            }
        }

        webview?.stringByEvaluatingJavaScript(from: "setCurrent('\(key)')")
    }
    @objc func switchPreviewPage(pageId:String) {
        
    }
    
    var appDelegate: AppDelegate  = (NSApplication.shared.delegate as? AppDelegate)!
    var currentViews:[NSDictionary] = []
    var currentPages:[NSDictionary] = []

    override func windowDidLoad () {

        super.windowDidLoad()
        self.webview?.frameLoadDelegate = self;

    }
    
    override func showWindow(_ sender: Any?) {
        super.showWindow(sender)
        let filePath = Bundle.main.path(forResource: "www/preview", ofType: "html")
        let url = URL(string:filePath!)
        
        self.webview?.mainFrame.load(URLRequest(url: url!))
        select.removeAllItems()
    }
    
	func webView(sender: WebView!, runJavaScriptAlertPanelWithMessage message: String!, initiatedByFrame frame: WebFrame!) {
        
        let myPopup:NSAlert = NSAlert()
        myPopup.addButton(withTitle: "OK")
        myPopup.messageText = "Title";
        myPopup.informativeText = "Message"
        if myPopup.runModal() == NSApplication.ModalResponse.alertFirstButtonReturn {
        }
    }
    
    override class func webScriptName(for aSelector: Selector) -> String? {
        var sel:NSString
        sel = ""
        
        if aSelector == #selector(previewWindow.getAllViews) {
            sel = "getAllViews"
        }

        if aSelector == #selector(previewWindow.getAllPages) {
            sel = "getAllPages"
        }
        
        if aSelector == #selector(previewWindow.setViewComboValue) {
            sel = "setViewComboValue"
        }
        if aSelector == #selector(previewWindow.switchPreviewPage) {
            sel = "switchPreviewPage"
        }
        if aSelector == #selector(previewWindow.openLink) {
            sel = "openLink"
        }
        
        if sel == "" {
            return nil
        } else {
            return sel as String;
        }
    }
    
    override class func isSelectorExcluded(fromWebScript aSelector: Selector) -> Bool
    {
        return false
    }
    
    /*func webView(_ webView: WebView!, windowScriptObjectAvailable windowScriptObject: WebScriptObject!) {
        windowScriptObject.setValue(self, forKey: "native");
    }*/
    
    func webView(_ webView: WebView!,didClearWindowObject windowObject: WebScriptObject!, for frame: WebFrame!) {
        windowObject.setValue(self, forKey: "native");
    }
    
    func webView(_ sender: WebView!, didFinishLoadFor frame: WebFrame!) {
        let jsonstring = webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('langList')")!
        let langList: NSArray = try! JSONSerialization.jsonObject(with: (jsonstring?.data(using: .utf8))!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
            select.addItems(withTitles: langList as! [String])
        //webview?.stringByEvaluatingJavaScript(from: "reloadPreview()")
    }
    
    @objc func openLink (link:String) {
        NSWorkspace.shared.open(URL(string: link)!)
    }
    
    @objc func getAllViews() {
        
        let viewsList:[NSDictionary] = self.appDelegate.mainView.getAllViewsFromCurrent()
        currentViews = viewsList

        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "view") {
            combo.removeAllItems()
            for item in viewsList {
                let name = item.value(forKey: "name") as! String
                combo.addItem(withObjectValue: name.removingPercentEncoding!)
            }
        }

        do {
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            
            webview?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')")
        } catch _ {
            print("JSON string error")
        }
        
    }
    
    @objc func setViewComboValue(idx:Int) {
        combo.selectItem(at: idx)
    }
    
    @objc func getAllPages() {

        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "view") {
            self.window?.title = "Views Preview"
        }
        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "page") {
            self.window?.title = "Pages Preview"
        }
        
        let viewsList:[NSDictionary]? = self.appDelegate.mainView.getAllPagesFromCurrent()
        currentPages = viewsList!
        
        if (webview?.stringByEvaluatingJavaScript(from: "window.localStorage.getItem('previewKind')") == "page") {
            combo.removeAllItems()
            for item in viewsList! {
                combo.addItem(withObjectValue: item.value(forKey: "name")!)
            }
        }
        
        do {
            let theJSONData = try JSONSerialization.data(
                withJSONObject: viewsList!,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            
            webview?.stringByEvaluatingJavaScript(from: "nativeCallBack('{\(theJSONText)}')")
            
        } catch _ {
            print("JSON string error")
        }
    }
    
}
